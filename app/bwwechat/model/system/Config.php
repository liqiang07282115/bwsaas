<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace app\bwwechat\model\system;

use buwang\base\BaseModel;
use buwang\traits\JwtTrait;

/**配置表
 * Class BwAuthNode
 * @package app\manage\model
 */
class Config extends BaseModel
{

    use JwtTrait;

    protected $pk = 'id';
    // 表名
    protected $name = MINIAPP_DATABASE_PREFIX .'config';
    protected $updateTime = '';

    protected $deleteTime = '';


}
