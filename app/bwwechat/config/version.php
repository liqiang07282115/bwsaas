<?php
/**
 * types 应用类型 参数：mini_program=小程序,app=APP,h5=h5,official=公众号,system=系统
 * version    应用版本好
 * name 应用名称
 * describe 应用描述
 * is_openapp 开放平台应用  1平台、0独立
 * is_manage  启用总平台管理  0关闭1启用
 * is_wechat_pay 微信支付  0、1
 * is_alipay_pay 支付宝支付  0、1
 */
return [
    'types'         => 'mini_program,official',  //mini_program=小程序,app=APP,h5=h5,official=公众号,system=系统
    'version'       => '1.0.0',
    'title'          => '布网微信管理',
    'describe'      => "微信公众号管理：素材管理，关键词回复管理（文字、图片、图文、视频、音乐、网址、转客服），关注自动回复等；小程序：关键词回复管理（文字、图片、小程序卡片、网址、转客服）",
    'is_openapp'    => 0,
    'is_manage'     => 1,
    'is_wechat_pay' => 1,
    'is_alipay_pay' => 0,
    'database'		=> [
        'prefix'		=> 'bwwechat_'
    ]
];