<?php
return [
    [
        //角色名称/功能名称
        'name' => 'bwwechat基础功能',
        //角色唯一标识,不可重复
        'group_name' => 'bwwechat_base',
        //角色备注/功能描述
        'remark' => '',
        //功能类型:1=基础功能,2=附加功能
        'module_type' => '1',
        //功能价格
        'module_price' => 0,
        //角色拥有的节点
        'nodes' => [
            [
                "title" => '主页',
                //菜单url
                "menu_path" => '/bwwechat/admin/index',
                //实际
                "name" => '/bwwechat/admin.Index/index',
                //权限标识,必填 唯一
                "auth_name" => '_bwwechat_admin_index',
                //附加参数 ?id=1&name=demo
                "param" => '',
                //打开方式
                "target" => '_self',
                //是否菜单 1=是,0=否
                "ismenu" => '1',
                //图标
                "icon" => 'fa fa-file',
                //备注
                "remark" => '',
                //子节点
                'children' => []
            ]
        ]
    ]
];