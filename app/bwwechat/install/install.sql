
CREATE TABLE `__BWPREFIX__bwwechat_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '配置id',
  `config_name` varchar(255) NOT NULL COMMENT '字段名称',
  `tab_name` varchar(255) NOT NULL DEFAULT '' COMMENT '配置分类英文标识',
  `type` varchar(255) NOT NULL DEFAULT '' COMMENT '类型(文本框,单选按钮...)',
  `input_type` varchar(20) DEFAULT 'input' COMMENT '表单类型',
  `tab_id` int(10) unsigned NOT NULL COMMENT '配置分类id',
  `parameter` varchar(255) DEFAULT NULL COMMENT '单选框和多选框参数',
  `upload_type` tinyint(1) unsigned DEFAULT NULL COMMENT '上传格式1单图2多图3文件',
  `rule` varchar(255) DEFAULT NULL COMMENT '验证规则',
  `message` varchar(255) DEFAULT NULL COMMENT '验证规则错误提示',
  `width` int(10) unsigned DEFAULT NULL COMMENT '多行文本框的宽度',
  `high` int(10) unsigned DEFAULT NULL COMMENT '多行文框的高度',
  `value` varchar(5000) DEFAULT NULL COMMENT '默认值',
  `info` varchar(255) NOT NULL DEFAULT '' COMMENT '配置名称',
  `desc` varchar(255) DEFAULT NULL COMMENT '配置简介',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否隐藏0正常1隐藏',
  `member_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '租户Id(0为总后台管理员创建,不为0的时候是租户创建)',
  `scopes` varchar(30) NOT NULL DEFAULT 'common' COMMENT '配置范围common通用member租户',
  `disabled` tinyint(1) unsigned NOT NULL DEFAULT '2' COMMENT '是否锁值:1是2否',
  `lazy` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '是否懒加载:1是2否',
  `dir` varchar(100) DEFAULT NULL COMMENT '应用标识',
  `plugin_name` varchar(100) DEFAULT NULL COMMENT '插件标识',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `config_name` (`config_name`,`member_id`,`dir`) USING BTREE,
  KEY `type` (`type`),
  KEY `tab_id` (`tab_id`),
  KEY `input_type` (`input_type`),
  KEY `member_id` (`member_id`),
  KEY `sort` (`sort`),
  KEY `status` (`status`),
  KEY `scopes` (`scopes`),
  KEY `disabled` (`disabled`),
  KEY `lazy` (`lazy`)
) ENGINE=InnoDB AUTO_INCREMENT=13839 DEFAULT CHARSET=utf8 COMMENT='bwwechat配置设置表';



CREATE TABLE `__BWPREFIX__bwwechat_config_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '组合数据ID',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '数据组名称',
  `desc` varchar(256) NOT NULL DEFAULT '' COMMENT '数据说明',
  `config_name` varchar(255) NOT NULL DEFAULT '' COMMENT '数据字段',
  `fields` text COMMENT '数据组字段以及类型（json数据）',
  `scopes` varchar(30) NOT NULL DEFAULT 'common' COMMENT '配置范围common通用member租户',
  `dir` varchar(100) DEFAULT NULL COMMENT '应用标识',
  `plugin_name` varchar(100) DEFAULT NULL COMMENT '插件标识',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `config_name` (`config_name`,`dir`) USING BTREE,
  KEY `scopes` (`scopes`)
) ENGINE=InnoDB AUTO_INCREMENT=119 DEFAULT CHARSET=utf8 COMMENT='bwwechat分组数据字段表';


CREATE TABLE `__BWPREFIX__bwwechat_config_group_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '组合数据详情ID',
  `group_id` int(11) NOT NULL DEFAULT '0' COMMENT '对应的数据组id',
  `value` text NOT NULL COMMENT '数据字段对应的（json）数据值',
  `add_time` int(10) NOT NULL DEFAULT '0' COMMENT '新增时间',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态（1：开启；2：关闭；）',
  `member_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '租户Id(0为总后台管理员创建,不为0的时候是租户创建)',
  `scopes` varchar(30) NOT NULL DEFAULT 'common' COMMENT '配置范围common通用member租户',
  `config_name` varchar(255) NOT NULL DEFAULT '' COMMENT '数据字段',
  `dir` varchar(100) DEFAULT NULL COMMENT '应用标识',
  `plugin_name` varchar(100) DEFAULT NULL COMMENT '插件标识',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `group_id` (`group_id`),
  KEY `sort` (`sort`),
  KEY `status` (`status`),
  KEY `member_id` (`member_id`),
  KEY `scopes` (`scopes`)
) ENGINE=InnoDB AUTO_INCREMENT=8582 DEFAULT CHARSET=utf8 COMMENT='bwwechat数据组合键值表';


CREATE TABLE `__BWPREFIX__bwwechat_config_tab` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '配置分类id',
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT '配置分类名称',
  `tab_name` varchar(255) NOT NULL DEFAULT '' COMMENT '配置分类英文标识',
  `icon` varchar(30) DEFAULT NULL COMMENT '图标',
  `type` int(2) DEFAULT '0' COMMENT '配置类型',
  `is_show` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '配置分类是否显示  0不显示，1显示',
  `scopes` varchar(30) NOT NULL DEFAULT 'common' COMMENT '配置范围common通用member租户',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `desc` text COMMENT '配置说明',
  `dir` varchar(100) DEFAULT NULL COMMENT '应用标识',
  `plugin_name` varchar(100) DEFAULT NULL COMMENT '插件标识',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `tab_name` (`tab_name`,`dir`) USING BTREE,
  KEY `type` (`type`),
  KEY `is_show` (`is_show`),
  KEY `scopes` (`scopes`)
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8 COMMENT='bwwechat配置分类表';