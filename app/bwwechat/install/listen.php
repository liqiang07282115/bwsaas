<?php
/**
 * 填写需要调用的事件函数
 * 当前可用事件 userRegisterSuccess:用户注册事件,userLoginSuccess 用户登录事件,miniappInstallSuccess:应用安装事件,miniappBuySuccess:购买应用成功事件,
 * 方法传参 array $event
 * 格式：['class'=>'调用的方法所在的类(类构造方法不能带参),没有传null','method'=>'方法名','scope'=>'方法类型 （字符串：static:类方法 object 对象方法 common公共方法）']
 */
return [
];