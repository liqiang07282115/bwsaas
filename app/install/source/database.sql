/*
Navicat MySQL Data Transfer
Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2021-04-01 18:58:01
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for bw_admin
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__admin`;
CREATE TABLE `__BWPREFIX__admin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `username` varchar(20) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '用户名',
  `nickname` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '昵称',
  `password` varchar(32) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '密码',
  `salt` varchar(30) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '密码盐',
  `avatar` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '头像{image}',
  `mobile` varchar(15) CHARACTER SET utf8 DEFAULT '' COMMENT '手机号',
  `email` varchar(100) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '电子邮箱',
  `loginfailure` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '失败次数',
  `logintime` int(10) DEFAULT NULL COMMENT '登录时间{date}',
  `loginip` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '登录IP',
  `create_time` int(10) DEFAULT NULL COMMENT '创建时间{date}',
  `update_time` int(10) DEFAULT NULL COMMENT '更新时间{date}',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态{switch}(0:封禁,1:正常)',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='管理员表';

-- ----------------------------
-- Records of bw_admin
-- ----------------------------
INSERT INTO `__BWPREFIX__admin` VALUES ('1', 'admin', 'Admin', 'e0e91fa1d8a60aa0d87f1ebd95d3ffdb', '965280', '', '', 'admin@admin.com', '0', '1604380088', '127.0.0.1', '1492186163', '1591582951', '1');

-- ----------------------------
-- Table structure for bw_admin_log
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__admin_log`;
CREATE TABLE `__BWPREFIX__admin_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `admin_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '管理员ID',
  `username` varchar(30) NOT NULL DEFAULT '' COMMENT '管理员名字',
  `url` varchar(1500) NOT NULL DEFAULT '' COMMENT '操作页面',
  `title` varchar(100) NOT NULL DEFAULT '' COMMENT '日志标题',
  `content` text NOT NULL COMMENT '内容',
  `ip` varchar(50) NOT NULL DEFAULT '' COMMENT 'IP',
  `useragent` varchar(255) NOT NULL DEFAULT '' COMMENT 'User-Agent',
  `createtime` int(10) DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`id`),
  KEY `name` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='管理员日志表';

-- ----------------------------
-- Records of bw_admin_log
-- ----------------------------

-- ----------------------------
-- Table structure for bw_auth_group
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__auth_group`;
CREATE TABLE `__BWPREFIX__auth_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '父角色ID',
  `app_name` varchar(40) NOT NULL DEFAULT 'manage' COMMENT '规则所属应用名字或插件名字，如manage,api,home',
  `type` varchar(30) NOT NULL DEFAULT 'system' COMMENT '权限规则分类，请加应用前缀,如system,plugin,app',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '状态0:禁用1:正常',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `name` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '角色名称',
  `group_name` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '角色唯一标识',
  `remark` varchar(255) NOT NULL DEFAULT '' COMMENT '备注',
  `scopes` varchar(100) DEFAULT 'admin' COMMENT '登录类型',
  `member_id` int(10) DEFAULT '0' COMMENT '租户角色id（0表示系统角色）',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `group_name` (`group_name`),
  KEY `status` (`status`),
  KEY `pid` (`pid`),
  KEY `sort` (`sort`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT='角色表';

-- ----------------------------
-- Records of bw_auth_group
-- ----------------------------
INSERT INTO `__BWPREFIX__auth_group` VALUES ('1', '0', 'manage', 'system', '1', '0', '0', '33', '超级管理员组', 'super_admin', '', 'admin', '0');
INSERT INTO `__BWPREFIX__auth_group` VALUES ('62', '0', 'manage', 'system', '1', '1594372603', '0', '0', '系统基础功能', 'member_system_base', '', 'member', '0');

-- ----------------------------
-- Table structure for bw_auth_group_access
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__auth_group_access`;
CREATE TABLE `__BWPREFIX__auth_group_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(10) unsigned NOT NULL COMMENT '管理员ID',
  `group_id` int(10) unsigned NOT NULL COMMENT '角色ID',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '角色名',
  `scopes` varchar(100) DEFAULT 'admin' COMMENT '登录类型',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid_group_id_scopes` (`uid`,`group_id`,`scopes`) USING BTREE,
  KEY `uid` (`uid`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=247 DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='权限分组表';

-- ----------------------------
-- Records of bw_auth_group_access
-- ----------------------------
INSERT INTO `__BWPREFIX__auth_group_access` VALUES ('43', '1', '1', '超级管理员组', 'admin');
INSERT INTO `__BWPREFIX__auth_group_access` VALUES ('246', '64', '62', '系统基础功能', 'member');

-- ----------------------------
-- Table structure for bw_auth_group_node
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__auth_group_node`;
CREATE TABLE `__BWPREFIX__auth_group_node` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '角色组ID,非顶级租户时使用',
  `node_id` int(10) unsigned NOT NULL COMMENT '权限节点ID',
  `node_name` varchar(100) NOT NULL DEFAULT '' COMMENT '节点后台url',
  `type` varchar(30) NOT NULL DEFAULT '' COMMENT '权限规则分类,请加应用前缀,如admin_',
  `auth_name` varchar(100) CHARACTER SET utf8mb4 NOT NULL DEFAULT '' COMMENT '规则唯一英文标识,全小写',
  PRIMARY KEY (`id`),
  KEY `rule_name` (`node_name`) USING BTREE,
  KEY `role_id` (`group_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=52798 DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='权限授权表';

-- ----------------------------
-- Records of bw_auth_group_node
-- ----------------------------
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52646', '62', '828', '/manage/member.index/index', 'system', '_manage_member_index_index');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52647', '62', '1443', '/manage/member.Index/dashboard', 'system', '_manage_member_dashboard');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52648', '62', '1585', '/manage/member.Index/messageRead', 'system', 'manage_member_message_read');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52649', '62', '1584', '/manage/member.Index/messageList', 'system', 'manage_member_message_list');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52650', '62', '1523', '/manage/member.Index/getEchartsInfo', 'system', '_manage_member_echartsInfo');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52651', '62', '1522', '/manage/member.Index/getNoticeList', 'system', '_manage_member_noticeList');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52652', '62', '1515', '/manage/member.Index/getMemberLoginInfo', 'system', '_manage_member_loginInfo');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52653', '62', '1349', '/manage/member.walletBill', 'system', 'manage_member_walletBill');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52654', '62', '1350', '/manage/member.walletBill/index', 'system', '/manage/member.walletBill/index');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52655', '62', '830', '/manage/member.Role/index', 'system', '_manage_member_Role_index');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52656', '62', '863', '/manage/member.Role/getRoleTree', 'system', '/manage/member.Role/getRoleTree');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52657', '62', '1238', '/manage/member.Role/edit', 'system', '/manage/member.Role/edit');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52658', '62', '1237', '/manage/member.Role/setStatus', 'system', '/manage/member.Role/setStatus');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52659', '62', '1236', '/manage/member.Role/delete', 'system', '/manage/member.Role/delete');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52660', '62', '1221', '/manage/member.Role/getNodeTree', 'system', '/manage/member.Role/getNodeTree');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52661', '62', '1161', '/manage/member.Role/add', 'system', '/manage/member.Role/add');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52662', '62', '829', '/manage/member.Login/user', 'system', 'manage_member_Login_user');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52663', '62', '1242', '/manage/member.Login/setStatus', 'system', '/manage/member.Login/setStatus');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52664', '62', '1240', '/manage/member.Login/edit', 'system', '/manage/member.Login/edit');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52665', '62', '1239', '/manage/member.Login/add', 'system', '/manage/member.Login/add');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52666', '62', '1346', '/config', 'system', '_config');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52667', '62', '1396', '/manage/member.GroupData/index', 'system', '_manage_member_groupData');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52668', '62', '1604', '/manage/member.GroupData/softdleting', 'system', 'manage_member_GroupData_del');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52669', '62', '1363', '/manage/member.GroupData/setConfigShow', 'system', '_manage_member_groupData_setConfigShow');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52670', '62', '1361', '/manage/member.GroupData/getConfigList', 'system', '_manage_member_groupData_getConfigList');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52671', '62', '1360', '/manage/member.GroupData/softdleting', 'system', '_manage_member_groupData_softdleting');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52672', '62', '1359', '/manage/member.GroupData/add', 'system', '_manage_member_groupData_add');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52673', '62', '1358', '/manage/member.GroupData/edit', 'system', '_manage_member_groupData_edit');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52674', '62', '1347', '/manage/member.Config/showMemberConfig', 'system', '_manage_member_config_showMemberConfig_1');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52675', '62', '1348', '/manage/member.Config/setMemberValues', 'system', '_manage_member_config_setMemberValues');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52676', '62', '1408', '/manage/member.Recharge/index', 'system', '_manage_member_recharge');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52677', '62', '1411', '/manage/member.Recharge/pay', 'system', '_manage_member_recharge_pay');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52678', '62', '1508', '/manage/member.sys.Log/index', 'system', 'manage_member_sys_log_index');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52679', '62', '1512', '/manage/member.sys.Log/modify', 'system', '_manage_member_sys_log_modify');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52680', '62', '1511', '/manage/member.sys.Log/edit', 'system', '_manage_member_sys_log_edit');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52681', '62', '1510', '/manage/member.sys.Log/add', 'system', '_manage_member_sys_log_add');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52682', '62', '1509', '/manage/member.sys.Log/export', 'system', '_manage_member_sys_log_export');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52683', '62', '1172', '/app/member', 'system', '_app_member');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52684', '62', '1105', '/manage/member.miniapp/index', 'system', 'manage_member_miniapp_index');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52685', '62', '1108', '/manage/member.miniapp/buy', 'system', '/manage/member.miniapp/buy');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52686', '62', '1107', '/manage/member.miniapp/detail', 'system', '/manage/member.miniapp/detail');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52687', '62', '1589', '/manage/member.MemberMiniapp/index', 'system', 'manage_member_miniapp_my_index');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52688', '62', '2101', '/manage/member.MemberMiniapp/setDefaultMiniapp', 'system', '_manage_member_member_miniapp_default');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52689', '62', '834', '/bwmall/admin/confg', 'system', 'bwmall_admin_confg');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52690', '62', '1250', '/manage/member.wechatKeyword/index', 'system', 'manage_member_wechatKeyword_index');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52691', '62', '1254', '/manage/member.wechatKeyword/del', 'system', 'manage_member_wechatKeyword_del');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52692', '62', '1253', '/manage/member.wechatKeyword/edit', 'system', 'manage_member_wechatKeyword_edit');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52693', '62', '1252', '/manage/member.wechatKeyword/add', 'system', 'manage_member_wechatKeyword_add');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52694', '62', '1567', '/manage/member.wechatKeyword/modify', 'system', 'manage_member_wechatKeyword_modify');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52695', '62', '1565', '/manage/member.wechatKeyword/previewVoice', 'system', 'manage_member_wechatKeyword_previewVoice');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52696', '62', '1564', '/manage/member.wechatKeyword/previewVideo', 'system', 'manage_member_wechatKeyword_previewVideo');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52697', '62', '1563', '/manage/member.wechatKeyword/previewMusic', 'system', 'manage_member_wechatKeyword_previewMusic');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52698', '62', '1562', '/manage/member.wechatKeyword/previewImage', 'system', 'manage_member_wechatKeyword_previewImage');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52699', '62', '1561', '/manage/member.wechatKeyword/select', 'system', 'manage_member_wechatKeyword_select');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52700', '62', '1560', '/manage/member.wechatKeyword/previewNews', 'system', 'manage_member_wechatKeyword_previewNews');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52701', '62', '1559', '/manage/member.wechatKeyword/previewText', 'system', 'manage_member_wechatKeyword_previewText');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52702', '62', '1558', '/manage/member.wechatKeyword/addKeys', 'system', 'manage_member_wechatKeyword_addKeys');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52703', '62', '1069', '/manage/member.official/index', 'system', 'manage_member_official_index');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52704', '62', '1081', '/manage/member.official/sort', 'system', 'manage_member_official_sort');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52705', '62', '1079', '/manage/member.official/sync', 'system', 'manage_member_official_sync');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52706', '62', '1078', '/manage/member.official/del', 'system', 'manage_member_official_del');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52707', '62', '1077', '/manage/member.official/edit', 'system', 'manage_member_official_edit');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52708', '62', '1076', '/manage/member.official/add', 'system', 'manage_member_official_add');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52709', '62', '1458', '/manage/member.official/materialList', 'system', 'manage_member_official_materialList');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52710', '62', '835', '/manage/member.setting/index', 'system', 'manage_member_setting_index');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52711', '62', '1068', '/manage/member.setting/alipay', 'system', 'manage_member_setting_alipay');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52712', '62', '1067', '/manage/member.setting/wechat', 'system', 'manage_member_setting_wechat');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52713', '62', '1054', '/manage/member.setting/edit', 'system', 'manage_member_setting_edit');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52714', '62', '2375', '/manage/member.WechatMessageTemplate/index', 'system', 'manage_member_wechat_message_template_index');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52715', '62', '2380', '/manage/member.WechatMessageTemplate/del', 'system', 'manage_member_wechat_message_template_del');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52716', '62', '2379', '/manage/member.WechatMessageTemplate/modify', 'system', 'manage_member_wechat_message_template_modify');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52717', '62', '2378', '/manage/member.WechatMessageTemplate/edit', 'system', 'manage_member_wechat_message_template_edit');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52718', '62', '2377', '/manage/member.WechatMessageTemplate/add', 'system', 'manage_member_wechat_message_template_add');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52719', '62', '2376', '/manage/member.WechatMessageTemplate/export', 'system', 'manage_member_wechat_message_template_export');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52720', '62', '1583', '/manage/member.WechatKeyword/subscribe', 'system', 'manage_member_WechatKeyword_subscribe');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52721', '62', '1500', '/manage/member.wechatKeyword/materialList', 'system', 'manage_member_wechatKeyword_materialList');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52722', '62', '1590', '/manage/member.wechatKeyword/editMaterial', 'system', 'manage_member_wechatKeyword_editMaterial');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52723', '62', '1566', '/manage/member.wechatKeyword/addMaterial', 'system', 'manage_member_wechatKeyword_addMaterial');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52724', '62', '1507', '/manage/member.wechatKeyword/removeMaterial', 'system', 'manage_member_wechatKeyword_removeMaterial');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52725', '62', '1539', '/manage/member.sys.Express/index', 'system', 'manage_member_sys_express_index');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52726', '62', '1544', '/manage/member.sys.Express/del', 'system', 'manage_member_sys_express_del');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52727', '62', '1543', '/manage/member.sys.Express/modify', 'system', 'manage_member_sys_express_modify');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52728', '62', '1542', '/manage/member.sys.Express/edit', 'system', 'manage_member_sys_express_edit');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52729', '62', '1541', '/manage/member.sys.Express/add', 'system', 'manage_member_sys_express_add');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52730', '62', '1540', '/manage/member.sys.Express/export', 'system', 'manage_member_sys_express_export');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52731', '62', '1471', '/app/all/user', 'system', 'app_all_user');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52732', '62', '1485', '/manage/member/extract', 'system', '_manage_member_extract');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52733', '62', '1493', '/manage/member.user.ExtractLog/index', 'system', 'manage_member_user_extractLog_index');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52734', '62', '1498', '/manage/member.user.ExtractLog/del', 'system', '_manage_member_user_extractLog_del');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52735', '62', '1497', '/manage/member.user.ExtractLog/modify', 'system', '_manage_member_user_extractLog_modify');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52736', '62', '1496', '/manage/member.user.ExtractLog/edit', 'system', '_manage_member_user_extractLog_edit');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52737', '62', '1495', '/manage/member.user.ExtractLog/add', 'system', '_manage_member_user_extractLog_add');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52738', '62', '1494', '/manage/member.user.ExtractLog/export', 'system', '_manage_member_user_extractLog_export');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52739', '62', '1487', '/manage/member.user.Extract/index', 'system', 'manage_member_user_extract_index');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52740', '62', '1492', '/manage/member.user.Extract/del', 'system', '_manage_member_user_extract_del');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52741', '62', '1491', '/manage/member.user.Extract/modify', 'system', '_manage_member_user_extract_modify');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52742', '62', '1490', '/manage/member.user.Extract/edit', 'system', '_manage_member_user_extract_edit');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52743', '62', '1489', '/manage/member.user.Extract/add', 'system', '_manage_member_user_extract_add');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52744', '62', '1488', '/manage/member.user.Extract/export', 'system', '_manage_member_user_extract_export');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52745', '62', '1486', '/manage/member/config.ShowMemberConfig', 'system', 'manage_member_config_showMemberConfig_0_55');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52746', '62', '1472', '/manage/member.user.Bill/index', 'system', 'manage_member_user_bill_index');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52747', '62', '1476', '/manage/member.user.Bill/modify', 'system', '_manage_member_user_bill_modify');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52748', '62', '1475', '/manage/member.user.Bill/edit', 'system', '_manage_member_user_bill_edit');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52749', '62', '1474', '/manage/member.user.Bill/add', 'system', '_manage_member_user_bill_add');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52750', '62', '1473', '/manage/member.user.Bill/export', 'system', '_manage_member_user_bill_export');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52751', '62', '1462', 'rechange/config', 'system', 'rechange_config');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52752', '62', '1465', '/manage/member.user.Recharge/index', 'system', 'manage_member_user_recharge_index');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52753', '62', '1469', '/manage/member.user.Recharge/modify', 'system', '_manage_member_user_recharge_modify');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52754', '62', '1468', '/manage/member.user.Recharge/export', 'system', '_manage_member_user_recharge_export');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52755', '62', '1467', '/manage/member.user.Recharge/edit', 'system', '_manage_member_user_recharge_edit');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52756', '62', '1466', '/manage/member.user.Recharge/add', 'system', '_manage_member_user_recharge_add');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52757', '62', '1464', '/manage/member.GroupData/index', 'system', 'manage_member_groupData_recharge_select');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52758', '62', '1463', '/manage/member/config.ShowMemberConfig', 'system', 'manage_member_config_showMemberConfig_0_54');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52759', '62', '1412', '/manage/member.User/index', 'system', 'manage_member_user_index');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52760', '62', '1588', '/manage/member.User/sendMessage', 'system', 'manage_member_message_send');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52761', '62', '1419', '/manage/member.User/changeMoney', 'system', '_manage_member_user_changeMoney');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52762', '62', '1418', '/manage/member.User/editPw', 'system', '_manage_member_user_editPw');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52763', '62', '1417', '/manage/member.User/modify', 'system', '_manage_member_user_modify');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52764', '62', '1416', '/manage/member.User/export', 'system', '_manage_member_user_export');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52765', '62', '1414', '/manage/member.User/edit', 'system', '_manage_member_user_edit');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52766', '62', '1413', '/manage/member.User/add', 'system', '_manage_member_user_add');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52767', '62', '1314', 'member/addons', 'system', 'member/addons');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52768', '62', '1840', '/manage/member.plugin.Plugin/index', 'system', '');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52769', '62', '1842', '/manage/member.plugin.Plugin/buy', 'system', '');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52770', '62', '1841', '/manage/member.plugin.Plugin/detail', 'system', '');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52771', '62', '1315', 'member/cloud', 'system', 'member_cloud');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52772', '62', '1354', '/manage/member.cloud.order', 'system', '/manage/member.cloud.order');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52773', '62', '1355', '/manage/member.cloud.order/index', 'system', '/manage/member.cloud.order/index');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52774', '62', '1324', '/manage/member.cloud.walletBill', 'system', '/manage/member.cloud.walletBill');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52775', '62', '1325', '/manage/member.cloud.walletBill/index', 'system', '/manage/member.cloud.walletBill/index');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52776', '62', '1317', '/manage/member.cloud.packet', 'system', '/manage/member.cloud.packet');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52777', '62', '1322', '/manage/member.cloud.packet/buy', 'system', '/manage/member.cloud.packet/buy');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52778', '62', '1318', '/manage/member.cloud.packet/index', 'system', '/manage/member.cloud.packet/index');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52779', '62', '1309', '/manage/member.cloud.service', 'system', '_manage_member_cloud_service');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52780', '62', '1310', '/manage/member.cloud.service/index', 'system', '/manage/member.cloud.service/index');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52781', '62', '862', '/member', 'system', 'member');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52782', '62', '1171', '/manage/member.Index/dashboard', 'system', '/manage/member/dashboard');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52783', '62', '1169', '/manage/member.Index/menu', 'system', '/manage/member/menu');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52784', '62', '861', '/manage/member.index/logout', 'system', '/manage/member/logout');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52785', '62', '1537', '/manage/member.Miniapp/resetAudit', 'system', 'manage_member_resetAudit');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52786', '62', '1536', '/manage/member.Miniapp/publishCode', 'system', 'manage_member_publishCode');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52787', '62', '1535', '/manage/member.Miniapp/getAuditStatus', 'system', 'manage_member_getAuditStatus');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52788', '62', '1527', '/manage/member.Miniapp/submitAudit', 'system', 'manage_member_submitAudit');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52789', '62', '1364', '/manage/publicCommon/uploadimg', 'system', '_manage_publicCommon_uploadimg');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52790', '62', '1323', '/manage/member.Login/userPassword', 'system', '/manage/member/login/userPassword');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52791', '62', '1316', '/manage/member.Login/userSetting', 'system', '/manage/member/login/userSetting');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52792', '62', '1308', '/manage/publicCommon/upload', 'system', '_manage_publicCommon_upload');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52793', '62', '1273', '/manage/member.Miniapp/getQrCode', 'system', 'manage_member_getQrCode');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52794', '62', '1248', '/manage/member.Miniapp/upCode', 'system', 'manage_member_upCode');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52795', '62', '1243', '/manage/member.Miniapp/setDomain', 'system', 'manage_member_setDomain');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52796', '62', '1206', '/manage/member.WechatAuth/openAuthCallback', 'system', 'manage_member_openAuthCallback');
INSERT INTO `__BWPREFIX__auth_group_node` VALUES ('52797', '62', '1200', '/manage/member.WechatAuth/openAuth', 'system', 'manage_member_openAuth');

-- ----------------------------
-- Table structure for bw_auth_node
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__auth_node`;
CREATE TABLE `__BWPREFIX__auth_node` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '规则id,自增主键',
  `pid` int(10) NOT NULL DEFAULT '0' COMMENT '父ID',
  `app_name` varchar(40) NOT NULL DEFAULT '' COMMENT '规则所属应用名字或插件名字，如manage,api,home',
  `type` varchar(30) NOT NULL DEFAULT '' COMMENT '权限规则分类，请加应用前缀,如system,plugin,app',
  `title` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '规则描述标题',
  `menu_path` varchar(100) NOT NULL DEFAULT '' COMMENT '路由地址',
  `name` varchar(100) NOT NULL DEFAULT '' COMMENT 'url',
  `auth_name` varchar(100) NOT NULL DEFAULT '' COMMENT '规则唯一英文标识,全小写',
  `param` varchar(100) NOT NULL DEFAULT '' COMMENT '额外url参数',
  `target` varchar(100) NOT NULL DEFAULT '_self' COMMENT '菜单窗口打开方式(_self,_blank,_parent,_top)',
  `condition` varchar(200) NOT NULL DEFAULT '' COMMENT '规则附加条件',
  `ismenu` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否菜单',
  `icon` varchar(50) NOT NULL DEFAULT '' COMMENT '图标',
  `remark` varchar(255) NOT NULL DEFAULT '' COMMENT '备注',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '是否有效(0:无效,1:有效)',
  `create_time` int(11) DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  `sort` int(5) DEFAULT '999' COMMENT '排序权重',
  `scopes` varchar(100) DEFAULT 'admin' COMMENT '登录类型',
  PRIMARY KEY (`id`),
  KEY `module` (`app_name`,`status`,`type`),
  KEY `ismenu` (`ismenu`),
  KEY `name` (`name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2440 DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC  COMMENT='权限规则表';

-- ----------------------------
-- Records of bw_auth_node
-- ----------------------------
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1', '185', 'manage', 'system', '权限角色', '/manage/Role/index', '/manage/Role/index', 'manage_Role_indexid_2', 'id=2', '_self', '', '1', '', '哈', '1', '0', '0', '200', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('2', '3', 'manage', 'system', '会员管理', '/manage/index/index', '/manage/index/index', 'manage_index_index', '', '_self', '', '0', '', '', '1', '0', '0', '10', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('3', '186', 'manage', 'system', '平台首页', '/manage/admin/dashboard', '/manage/admin.Index/dashboard', 'manage_admin_dashboard', '', '_self', '', '1', 'fa fa-bar-chart-o', '', '1', '0', '0', '9999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('182', '185', 'manage', 'system', '管理员管理', '/manage/admin/user', '/manage/Admin/user', 'manage_admin_user', '', '_self', '', '1', '', '', '1', '0', '0', '111', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('183', '185', 'manage', 'system', '菜单节点', '/manage/node/index', '/manage/Node/index', 'manage_node_index', '', '_self', '', '1', '', '', '1', '0', '0', '911', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('185', '186', 'manage', 'system', '权限用户', '/manage/auth', '/manage/auth', 'manage_auth', '', '_self', '', '1', 'fa fa-lock', '', '1', '0', '0', '2000', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('186', '0', 'manage', 'system', '系统管理', '/manage', '/manage', '_manage', '', '_self', '', '1', 'fa fa-bank', '', '1', '0', '0', '1006', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('187', '0', 'manage', 'system', '应用管理', '/app', '/app', '_app', '', '_self', '', '1', 'fa fa-connectdevelop', '', '1', '0', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('188', '0', 'manage', 'system', '插件管理', '/plugin', '/plugin', '_plugin', '', '_self', '', '1', 'fa fa-plug', '', '1', '0', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('190', '186', 'manage', 'system', '配置管理', '/manage/weihu', '/manage/weihu', 'manage_ceshi', '', '_self', '', '1', 'fa fa-cog', '', '1', '0', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('193', '190', 'manage', 'system', '配置分类', '/manage/config/index', '/manage/config/index', '_manage_config_index', '', '_self', '', '1', 'fa fa-database', '', '1', '0', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('196', '190', 'manage', 'system', '组合配置', '/manage/gruop/set', '/manage/Gruop/set', 'manage_gruop_set', '', '_self', '', '1', 'fa fa-cubes', '', '1', '0', '0', '997', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('197', '193', 'manage', 'system', '添加', '/manage/Config/add', '/manage/Config/add', 'manageConfigadd', '', '_self', '', '0', '', '', '1', '0', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('198', '193', 'manage', 'system', '修改', '/manage/Config/edit', '/manage/Config/edit', '/manage/Config/edit', '', '_self', '', '0', '', '', '1', '0', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('199', '193', 'manage', 'system', '查询', '/manage/Config/getlist', '/manage/Config/getlist', 'config_set_select', '', '_self', '', '0', '', '', '1', '0', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('200', '193', 'manage', 'system', '删除', '/manage/config/softdleting', '/manage/Config/softdleting', '/manage/Config/softdleting', '', '_self', '', '0', '', '', '1', '0', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('202', '190', 'manage', 'system', '配置设置', '/manage/config/showconfig', '/manage/Config/showconfig', '_manage_config_showconfig', '', '_self', '', '1', 'fa fa-asterisk', '', '1', '0', '0', '998', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('206', '190', 'manage', 'system', '配置列表', '/manage/config/configindex', '/manage/config/configindex', '_manage_config_configindex', '', '_self', '', '1', 'fa fa-gears', '', '1', '1588931407', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('210', '196', 'manage', 'system', '组合分类', '/manage/group', '/manage/Group', 'manage_group', '', '_self', '', '1', '', '', '1', '1589338068', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('211', '196', 'manage', 'system', '组合数据权限', '/manage/GroupData/index', '/manage/GroupData/index', '_manage_GroupData_index', '', '_self', '', '0', 'fa fa-delicious', '', '1', '1589338171', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('497', '210', 'manage', 'system', '查看列表', '/manage/Group/getconfiglist', '/manage/Group/getconfiglist', 'manageGroupgetconfiglist', '', '_self', '', '0', '', '', '1', '1589957956', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('503', '210', 'manage', 'system', '添加', '/manage/Group/add', '/manage/Group/add', 'manageGroupadd', '', '_self', '', '0', '', '', '1', '1589961667', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('504', '211', 'manage', 'system', '列表', '/manage/GroupData/getconfiglist', '/manage/GroupData/getconfiglist', 'manageGroupDatagetconfiglist', '', '_self', '', '0', '', '', '1', '1589962104', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('505', '211', 'manage', 'system', '添加', '/manage/GroupData/add', '/manage/GroupData/add', 'manageGroupDataadd', '', '_self', '', '0', '', '', '1', '1589962212', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('506', '206', 'manage', 'system', '列表', '/manage/Config/getconfiglist', '/manage/Config/getconfiglist', 'manageConfiggetconfiglist', '', '_self', '', '0', '', '', '1', '1589962506', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('507', '210', 'manage', 'system', '添加字段', '/manage/Group/addconfig', '/manage/Group/addconfig', 'manageGroupaddconfig', '', '_self', '', '0', '', '', '1', '1589962799', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('508', '202', 'manage', 'system', '保存配置', '/manage/config/setvalues', '/manage/Config/setValues', '/manage/Config/setValues', '', '_self', '', '0', '', '', '1', '1589963167', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('509', '3', 'manage', 'system', '清理缓存', '/manage/index/clear', '/manage/index/clear', 'manageindexclear', '', '_self', '', '0', '', '', '1', '1590028966', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('510', '183', 'manage', 'system', '编辑', '/manage/Node/edit', '/manage/Node/edit', 'manageNodeedit', '', '_self', '', '0', '', '', '1', '1590039400', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('511', '210', 'manage', 'system', '编辑', '/manage/Group/edit', '/manage/Group/edit', 'manageGroupedit', '', '_self', '', '0', '', '', '1', '1590040075', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('512', '210', 'manage', 'system', '编辑字段', '/manage/Group/editconfig', '/manage/Group/editconfig', 'manageGroupeditconfig', '', '_self', '', '0', '', '', '1', '1590040245', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('513', '211', 'manage', 'system', '编辑', '/manage/GroupData/edit', '/manage/GroupData/edit', 'manageGroupDataedit', '', '_self', '', '0', '', '', '1', '1590040376', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('514', '3', 'manage', 'system', '退出登录', '/manage/admin/logout', '/manage/admin.index/logout', '/manage/admin/logout', '', '_self', '', '0', '', '', '1', '1590043364', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('807', '186', 'manage', 'system', '一键生成管理', '/manage/admin/sysCrud', '/manage/admin.SysCrud', 'manage_admin_sysCrud', '', '_self', '', '1', 'fa fa-yelp', '', '0', '1590131579', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('808', '807', 'manage', 'system', '查看', '/manage/admin/sysCrud/index', '/manage/admin.SysCrud/index', '/manage/admin.SysCrud/index', '', '_self', '', '0', '', '', '1', '1590131579', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('809', '807', 'manage', 'system', '新增', '/manage/admin/sysCrud/add', '/manage/admin.SysCrud/add', '/manage/admin.SysCrud/add', '', '_self', '', '0', '', '', '1', '1590131579', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('810', '807', 'manage', 'system', '编辑', '/manage/admin/sysCrud/edit', '/manage/admin.SysCrud/edit', '/manage/admin.SysCrud/edit', '', '_self', '', '0', '', '', '1', '1590131579', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('811', '807', 'manage', 'system', '删除', '/manage/admin/sysCrud/del', '/manage/admin.SysCrud/del', '/manage/admin.SysCrud/del', '', '_self', '', '0', '', '', '1', '1590131579', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('828', '0', 'manage', 'system', '系统管理', '/manage/member.index/index', '/manage/member.index/index', '_manage_member_index_index', '', '_self', '', '1', 'fa fa-bank', '', '1', '1590398679', '0', '5000', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('829', '828', 'manage', 'system', '管理员管理', '/manage/member.Login/user', '/manage/member.Login/user', 'manage_member_Login_user', '', '_self', '', '1', 'fa fa-user-secret', '', '1', '1590398975', '0', '999', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('830', '828', 'manage', 'system', '角色管理', '/manage/member.Role/index', '/manage/member.Role/index', '_manage_member_Role_index', '', '_self', '', '1', 'fa fa-joomla', '', '1', '1590399132', '0', '999', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('834', '1172', 'manage', 'system', '应用设置', '/bwmall/admin/confg', '/bwmall/admin/confg', 'bwmall_admin_confg', '', '_self', '', '0', 'fa fa-gear', '', '1', '1590399842', '0', '5', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('835', '834', 'manage', 'system', '关于应用', '/manage/member.setting/index', '/manage/member.setting/index', 'manage_member_setting_index', '', '_self', '', '0', 'fa fa-cubes', '', '1', '1590399937', '0', '999', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('851', '1586', 'manage', 'system', '文章列表', '/manage/admin/sysArticle', '/manage/admin.SysArticle', 'manage_admin_sysArticle', '', '_self', '', '0', 'fa fa-bars', '', '1', '1590403125', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('852', '851', 'manage', 'system', '查看', '/manage/admin/sysArticle/index', '/manage/admin.SysArticle/index', '/manage/admin.SysArticle/index', '', '_self', '', '0', '', '', '1', '1590403125', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('853', '851', 'manage', 'system', '新增', '/manage/admin/sysArticle/add', '/manage/admin.SysArticle/add', '/manage/admin.SysArticle/add', '', '_self', '', '0', '', '', '1', '1590403125', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('854', '851', 'manage', 'system', '编辑', '/manage/admin/sysArticle/edit', '/manage/admin.SysArticle/edit', '/manage/admin.SysArticle/edit', '', '_self', '', '0', '', '', '1', '1590403125', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('855', '851', 'manage', 'system', '删除', '/manage/admin/sysArticle/del', '/manage/admin.SysArticle/del', '/manage/admin.SysArticle/del', '', '_self', '', '0', '', '', '1', '1590403125', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('856', '1586', 'manage', 'system', '分类管理', '/manage/admin/sysArticleCategory', '/manage/admin.SysArticleCategory', 'manage_admin_sysArticleCategory', '', '_self', '', '0', '', '', '0', '1590403228', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('857', '856', 'manage', 'system', '查看', '/manage/admin/sysArticleCategory/index', '/manage/admin.SysArticleCategory/index', '/manage/admin.SysArticleCategory/index', '', '_self', '', '0', '', '', '1', '1590403228', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('858', '856', 'manage', 'system', '新增', '/manage/admin/sysArticleCategory/add', '/manage/admin.SysArticleCategory/add', '/manage/admin.SysArticleCategory/add', '', '_self', '', '0', '', '', '1', '1590403228', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('859', '856', 'manage', 'system', '编辑', '/manage/admin/sysArticleCategory/edit', '/manage/admin.SysArticleCategory/edit', '/manage/admin.SysArticleCategory/edit', '', '_self', '', '0', '', '', '1', '1590403228', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('860', '856', 'manage', 'system', '删除', '/manage/admin/sysArticleCategory/del', '/manage/admin.SysArticleCategory/del', '/manage/admin.SysArticleCategory/del', '', '_self', '', '0', '', '', '1', '1590403228', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('861', '862', 'manage', 'system', '退出登录', '/manage/member/logout', '/manage/member.index/logout', '/manage/member/logout', '', '_self', '', '0', '', '', '1', '1590478428', '0', '999', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('862', '0', 'manage', 'system', '租户后台通用节点', '/member', '/member', 'member', '', '_self', '', '0', '', '', '1', '1590479098', '0', '999', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('863', '830', 'manage', 'system', '分配权限', '/manage/member.Role/getRoleTree', '/manage/member.Role/getRoleTree', '/manage/member.Role/getRoleTree', '', '_self', '', '0', '', '', '1', '1590479262', '0', '999', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('874', '206', 'manage', 'system', '添加配置', '/manage/Config/addconfig', '/manage/Config/addconfig', 'manageConfigaddconfig', '', '_self', '', '0', '', '', '1', '1590566080', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('891', '188', 'manage', 'system', '系统插件', '/manage/admin.Plugin', '/manage/admin.Plugin', '_manage_admin_Plugin', '', '_self', '', '1', 'fa fa-cubes', '', '1', '1590714186', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('892', '891', 'manage', 'system', '查看', '/manage/admin.Plugin/index', '/manage/admin.Plugin/index', '/manage/admin.Plugin/index', '', '_self', '', '0', '', '', '1', '1590714186', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('893', '891', 'manage', 'system', '新增', '/manage/admin.Plugin/add', '/manage/admin.Plugin/add', '/manage/admin.Plugin/add', '', '_self', '', '0', '', '', '1', '1590714186', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('894', '891', 'manage', 'system', '编辑', '/manage/admin.Plugin/edit', '/manage/admin.Plugin/edit', '/manage/admin.Plugin/edit', '', '_self', '', '0', '', '', '1', '1590714186', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('895', '891', 'manage', 'system', '删除', '/manage/admin.Plugin/del', '/manage/admin.Plugin/del', '/manage/admin.Plugin/del', '', '_self', '', '0', '', '', '1', '1590714186', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('896', '891', 'manage', 'system', '安装', '/manage/admin.Plugin/install', '/manage/admin.Plugin/install', '/manage/admin.Plugin/install', '', '_self', '', '0', '', '', '1', '1590714186', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('897', '891', 'manage', 'system', '卸载', '/manage/admin.Plugin/uninstall', '/manage/admin.Plugin/uninstall', '/manage/admin.Plugin/uninstall', '', '_self', '', '0', '', '', '1', '1590714186', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('898', '891', 'manage', 'system', '启用', '/manage/admin.Plugin/enable', '/manage/admin.Plugin/enable', '/manage/admin.Plugin/enable', '', '_self', '', '0', '', '', '1', '1590714186', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('900', '891', 'manage', 'system', '配置', '/manage/admin.Plugin/config', '/manage/admin.Plugin/config', '/manage/admin.Plugin/config', '', '_self', '', '0', '', '', '1', '1590714186', '0', '1000', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1020', '187', 'manage', 'system', '系统应用', '/manage/admin/miniapp', '/manage/admin.Miniapp', '_manage_admin_miniapp', '', '_self', '', '1', 'fa fa-share-alt-square', '', '1', '1590816416', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1021', '1020', 'manage', 'system', '查看', '/manage/admin/miniapp/index', '/manage/admin.Miniapp/index', '/manage/admin.Miniapp/index', '', '_self', '', '0', '', '', '1', '1590816416', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1022', '1020', 'manage', 'system', '新增', '/manage/admin/miniapp/add', '/manage/admin.Miniapp/add', '/manage/admin.Miniapp/add', '', '_self', '', '0', '', '', '1', '1590816416', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1023', '1020', 'manage', 'system', '编辑', '/manage/admin/miniapp/edit', '/manage/admin.Miniapp/edit', '/manage/admin.Miniapp/edit', '', '_self', '', '0', '', '', '1', '1590816416', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1024', '1020', 'manage', 'system', '卸载', '/manage/admin/miniapp/uninstall', '/manage/admin.Miniapp/uninstall', '/manage/admin.Miniapp/uninstall', '', '_self', '', '0', '', '', '1', '1590816416', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1025', '1020', 'manage', 'system', '安装', '/manage/admin/miniapp/install', '/manage/admin.Miniapp/install', '/manage/admin.Miniapp/install', '', '_self', '', '0', '', '', '1', '1590816416', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1026', '1020', 'manage', 'system', '购买', '/manage/admin/miniapp/buy', '/manage/admin.Miniapp/buy', '/manage/admin.Miniapp/buy', '', '_self', '', '0', '', '', '1', '1590816416', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1054', '835', 'manage', 'system', '编辑', '/manage/member.setting/edit', '/manage/member.setting/edit', 'manage_member_setting_edit', '', '_self', '', '0', '', '', '1', '1590399937', '0', '999', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1066', '1', 'manage', 'system', '分配权限', '/manage/role/groupRule', '/manage/Role/groupRule', '/manage/Role/groupRule', '', '_self', '', '0', '', '', '1', '1591146947', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1067', '835', 'manage', 'system', '微信支付', '/manage/member.setting/wechat', '/manage/member.setting/wechat', 'manage_member_setting_wechat', '', '_self', '', '0', '', '', '1', '1590399937', '0', '999', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1068', '835', 'manage', 'system', '支付宝支付', '/manage/member.setting/alipay', '/manage/member.setting/alipay', 'manage_member_setting_alipay', '', '_self', '', '0', '', '', '1', '1590399937', '0', '999', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1069', '834', 'manage', 'system', '公众号菜单', '/manage/member.official/index', '/manage/member.official/index', 'manage_member_official_index', '', '_self', '', '0', 'fa fa-wechat', '', '1', '1591234617', '0', '999', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1076', '1069', 'manage', 'system', '新增', '/manage/member.official/add', '/manage/member.official/add', 'manage_member_official_add', '', '_self', '', '0', '', '', '1', '1591234676', '0', '999', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1077', '1069', 'manage', 'system', '编辑', '/manage/member.official/edit', '/manage/member.official/edit', 'manage_member_official_edit', '', '_self', '', '0', '', '', '1', '1591234676', '0', '999', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1078', '1069', 'manage', 'system', '删除', '/manage/member.official/del', '/manage/member.official/del', 'manage_member_official_del', '', '_self', '', '0', '', '', '1', '1591234676', '0', '999', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1079', '1069', 'manage', 'system', '删除', '/manage/member.official/sync', '/manage/member.official/sync', 'manage_member_official_sync', '', '_self', '', '0', '', '', '1', '1591234676', '0', '999', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1081', '1069', 'manage', 'system', '排序', '/manage/member.official/sort', '/manage/member.official/sort', 'manage_member_official_sort', '', '_self', '', '0', '', '', '1', '1591234676', '0', '999', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1093', '187', 'manage', 'system', '租户应用', '/manage/admin/memberMiniapp', '/manage/admin.MemberMiniapp', '_manage_admin_memberMiniapp', '', '_self', '', '1', 'fa fa-codepen', '', '1', '1591668372', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1094', '1093', 'manage', 'system', '查看', '/manage/admin/memberMiniapp/index', '/manage/admin.MemberMiniapp/index', '/manage/admin.MemberMiniapp/index', '', '_self', '', '0', '', '', '1', '1591668372', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1095', '1093', 'manage', 'system', '新增', '/manage/admin/memberMiniapp/add', '/manage/admin.MemberMiniapp/add', '/manage/admin.MemberMiniapp/add', '', '_self', '', '0', '', '', '1', '1591668372', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1096', '1093', 'manage', 'system', '编辑', '/manage/admin/memberMiniapp/edit', '/manage/admin.MemberMiniapp/edit', '/manage/admin.MemberMiniapp/edit', '', '_self', '', '0', '', '', '1', '1591668372', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1097', '1093', 'manage', 'system', '删除', '/manage/admin/memberMiniapp/del', '/manage/admin.MemberMiniapp/del', '/manage/admin.MemberMiniapp/del', '', '_self', '', '0', '', '', '1', '1591668372', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1098', '206', 'manage', 'system', '编辑', '/manage/Config/editconfig', '/manage/Config/editconfig', '/manage/Config/editconfig', '', '_self', '', '0', '', '', '1', '1591670413', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1105', '1172', 'manage', 'system', '应用商店', '/manage/member/miniapp/index', '/manage/member.miniapp/index', 'manage_member_miniapp_index', '', '_self', '', '1', 'fa fa-gift', '', '1', '1591779441', '0', '1800', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1107', '1105', 'manage', 'system', '详情', '/manage/member/miniapp/detail', '/manage/member.miniapp/detail', '/manage/member.miniapp/detail', '', '_self', '', '0', '', '', '1', '1591779441', '0', '999', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1108', '1105', 'manage', 'system', '购买', '/manage/member/miniapp/buy', '/manage/member.miniapp/buy', '/manage/member.miniapp/buy', '', '_self', '', '0', '', '', '1', '1591779441', '0', '999', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1114', '186', 'manage', 'system', 'crud测试管理', '/manage/crudDemo', '/manage/crudDemo', 'manage_crudDemo', '', '_self', '', '1', 'fa fa-tachometer', '', '1', '1591844500', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1115', '1114', 'manage', 'system', '查看', '/manage/crudDemo/index', '/manage/crudDemo/index', '/manage/crudDemo/index', '', '_self', '', '0', '', '', '1', '1591844500', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1116', '1114', 'manage', 'system', '新增', '/manage/crudDemo/add', '/manage/crudDemo/add', '/manage/crudDemo/add', '', '_self', '', '0', '', '', '1', '1591844500', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1117', '1114', 'manage', 'system', '编辑', '/manage/crudDemo/edit', '/manage/crudDemo/edit', '/manage/crudDemo/edit', '', '_self', '', '0', '', '', '1', '1591844500', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1118', '1114', 'manage', 'system', '删除', '/manage/crudDemo/del', '/manage/crudDemo/del', 'manage_crudDemo_del', '', '_self', '', '0', '', '', '1', '1591844500', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1124', '1545', 'manage', 'system', '租户用户', '/manage/admin/member', '/manage/admin.member', 'manage_admin_member', '', '_self', '', '1', '', '', '1', '1591928624', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1125', '1124', 'manage', 'system', '查看', '/manage/admin/member/index', '/manage/admin.member/index', '/manage/admin.member/index', '', '_self', '', '0', '', '', '1', '1591928624', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1126', '1124', 'manage', 'system', '新增', '/manage/admin/Member/add', '/manage/admin.Member/add', '/manage/admin.Member/add', '', '_self', '', '0', '', '', '1', '1591928624', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1127', '1124', 'manage', 'system', '编辑', '/manage/admin/Member/edit', '/manage/admin.Member/edit', '/manage/admin.Member/edit', '', '_self', '', '0', '', '', '1', '1591928624', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1128', '1124', 'manage', 'system', '删除', '/manage/admin.Member/del', '/manage/admin.member/del', '/manage/admin.member/del', '', '_self', '', '0', '', '', '1', '1591928624', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1161', '830', 'manage', 'system', '新增角色', '/manage/member.Role/add', '/manage/member.Role/add', '/manage/member.Role/add', '', '_self', '', '0', '', '', '1', '1592208844', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1163', '3', 'manage', 'system', '首页', '/manage/admin.Index/index', '/manage/admin.Index/index', '/manage/admin.Index/index', '', '_self', '', '0', '', '', '1', '0', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1164', '3', 'manage', 'system', '菜单', '/manage/admin.Index/menu', '/manage/admin.Index/menu', '/manage/admin.Index/menu', '', '_self', '', '0', '', '', '1', '0', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1165', '3', 'manage', 'system', '后台首页', '/manage/admin/dashboard', '/manage/admin/dashboard', '/manage/admin/dashboard', '', '_self', '', '0', '', '', '1', '0', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1166', '210', 'manage', 'system', '查看', '/manage/group/index', '/manage/Group/index', '/manage/Group/index', '', '_self', '', '0', 'fa fa-life-saver', '', '1', '1589338068', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1167', '183', 'manage', 'system', '删除', '/manage/Node/delete', '/manage/Node/delete', '/manage/Node/delete', '', '_self', '', '0', '', '', '1', '1590039400', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1168', '1', 'manage', 'system', '编辑', '/manage/role/edit', '/manage/Role/edit', '/manage/role/edit', '', '_self', '', '0', '', '', '1', '1591146947', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1169', '862', 'manage', 'system', '菜单', '/manage/member/menu', '/manage/member.Index/menu', '/manage/member/menu', '', '_self', '', '0', '', '', '1', '0', '0', '999', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1170', '3', 'manage', 'system', '清理缓存', '/manage/common/clearCache', '/manage/common/clearCache', '/manage/common/clearCache', '', '_self', '', '0', '', '', '1', '1590043364', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1171', '862', 'manage', 'system', '控制台', '/manage/member/dashboard', '/manage/member.Index/dashboard', '/manage/member/dashboard', '', '_self', '', '0', '', '', '1', '0', '0', '999', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1172', '0', 'manage', 'system', '应用管理', '/app/member', '/app/member', '_app_member', '', '_self', '', '1', '', '', '1', '0', '0', '4000', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1173', '183', '', 'system', '添加', '/manage/Node/add', '/manage/Node/add', 'manageNodeadd', '', '_self', '', '0', '', '', '1', '1594349485', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1174', '183', '', 'system', '设置显示隐藏', '/manage/Node/setShow', '/manage/Node/setShow', '/manage/Node/setShow', '', '_self', '', '0', '', '', '1', '1594349733', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1175', '183', '', 'system', '启用禁用', '/manage/Node/setStatus', '/manage/Node/setStatus', '/manage/Node/setStatus', '', '_self', '', '0', '', '', '1', '1594349919', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1176', '183', '', 'system', '节点树', '/manage/Node/getNodeTree', '/manage/Node/getNodeTree', '/manage/Node/getNodeTree', '', '_self', '', '0', '', '', '1', '1594350161', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1177', '3', '', 'system', '图标库', '/manage/PublicCommon/icon', '/manage/PublicCommon/icon', '/manage/PublicCommon/icon', '', '_self', '', '0', '', '', '1', '1594350236', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1178', '1', '', 'system', '添加', '/manage/Role/add', '/manage/Role/add', '/manage/Role/add', '', '_self', '', '0', '', '', '1', '1594350889', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1179', '1', '', 'system', '角色树', '/manage/Role/getRoleTree', '/manage/Role/getRoleTree', '/manage/Role/getRoleTree', '', '_self', '', '0', '', '', '1', '1594350984', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1180', '1', '', 'system', '删除', '/manage/Role/delete', '/manage/Role/delete', '/manage/Role/delete', '', '_self', '', '0', '', '', '1', '1594351622', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1181', '1', '', 'system', '设置状态', '/manage/Role/setStatus', '/manage/Role/setStatus', '/manage/Role/setStatus', '', '_self', '', '0', '', '', '1', '1594351997', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1182', '182', 'manage', 'system', '添加', '/manage/admin/add', '/manage/Admin/add', '/manage/Admin/add', '', '_self', '', '0', '', '', '1', '1594361599', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1183', '182', 'manage', 'system', '设置状态', '/manage/admin/setStatus', '/manage/Admin/setStatus', '/manage/Admin/setStatus', '', '_self', '', '0', '', '', '1', '1594362339', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1184', '182', 'manage', 'system', '编辑', '/manage/admin/edit', '/manage/Admin/edit', '/manage/Admin/edit', '', '_self', '', '0', '', '', '1', '1594362616', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1185', '182', 'manage', 'system', '删除', '/manage/admin/delete', '/manage/Admin/delete', '/manage/Admin/delete', '', '_self', '', '0', '', '', '1', '1594362797', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1191', '193', '', 'system', '设置状态', '/manage/Config/setshow', '/manage/Config/setshow', '/manage/Config/setshow', '', '_self', '', '0', '', '', '1', '1594365118', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1192', '206', '', 'system', '设置状态', '/manage/Config/setconfigshow', '/manage/Config/setconfigshow', '/manage/Config/setconfigshow', '', '_self', '', '0', '', '', '1', '1594365737', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1193', '206', '', 'system', '删除', '/manage/Config/configsoftdleting', '/manage/Config/configsoftdleting', '/manage/Config/configsoftdleting', '', '_self', '', '0', '', '', '1', '1594367599', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1194', '210', '', 'system', '删除', '/manage/Group/configsoftdleting', '/manage/Group/configsoftdleting', '/manage/Group/configsoftdleting', '', '_self', '', '0', '', '', '1', '1594368934', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1200', '862', 'manage', 'system', '开放平台授权', '/manage/member/openAuth', '/manage/member.WechatAuth/openAuth', 'manage_member_openAuth', '', '_self', '', '0', '', '', '1', '1594371589', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1206', '862', 'manage', 'system', '开放平台授权回调', '/manage/member/openAuthCallback', '/manage/member.WechatAuth/openAuthCallback', 'manage_member_openAuthCallback', '', '_blank', '', '0', '', '', '1', '1594397684', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1221', '830', 'manage', 'system', '租户权限列表', '/manage/member.Role/getNodeTree', '/manage/member.Role/getNodeTree', '/manage/member.Role/getNodeTree', '', '_self', '', '0', '', '', '1', '1594629488', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1236', '830', 'manage', 'system', '删除', '/manage/member.Role/delete', '/manage/member.Role/delete', '/manage/member.Role/delete', '', '_self', '', '0', '', '', '1', '1594631484', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1237', '830', 'manage', 'system', '状态', '/manage/member.Role/setStatus', '/manage/member.Role/setStatus', '/manage/member.Role/setStatus', '', '_self', '', '0', '', '', '1', '1594631863', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1238', '830', 'manage', 'system', '编辑', '/manage/member.Role/edit', '/manage/member.Role/edit', '/manage/member.Role/edit', '', '_self', '', '0', '', '', '1', '1594632056', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1239', '829', 'manage', 'system', '添加', '/manage/member.Login/add', '/manage/member.Login/add', '/manage/member.Login/add', '', '_self', '', '0', '', '', '1', '1594635443', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1240', '829', 'manage', 'system', '编辑', '/manage/member.Login/edit', '/manage/member.Login/edit', '/manage/member.Login/edit', '', '_self', '', '0', '', '', '1', '1594637416', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1242', '829', 'manage', 'system', '设置状态', '/manage/member.Login/setStatus', '/manage/member.Login/setStatus', '/manage/member.Login/setStatus', '', '_self', '', '0', '', '', '1', '1594639953', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1243', '862', 'manage', 'system', '小程序修改授权域名', '/manage/member/setDomain', '/manage/member.Miniapp/setDomain', 'manage_member_setDomain', '', '_self', '', '0', '', '', '1', '1594653668', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1244', '1', 'manage', 'system', '[应用/功能/模块]列表', '/manage/Role/modelRole', '/manage/Role/modelRole', '/manage/Role/modelRole', '', '_self', '', '0', '', '', '1', '1594692608', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1245', '1', 'manage', 'system', '添加[应用/功能/模块]组', '/manage/Role/addModel', '/manage/Role/addModel', '/manage/Role/addModel', '', '_self', '', '0', '', '', '1', '1594693362', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1246', '1', 'manage', 'system', '编辑[应用/功能/模块]组', '/manage/Role/editModel', '/manage/Role/editModel', '/manage/Role/editModel', '', '_self', '', '0', '', '', '1', '1594693413', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1247', '1', 'manage', 'system', '应用功能组树', '/manage/Role/getRoleModelTree', '/manage/Role/getRoleModelTree', '/manage/Role/getRoleModelTree', '', '_self', '', '0', '', '', '1', '1594695179', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1248', '862', 'manage', 'system', '提交代码', '/manage/member/upCode', '/manage/member.Miniapp/upCode', 'manage_member_upCode', '', '_self', '', '0', '', '', '1', '1594733819', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1249', '1', 'manage', 'system', '租户角色树', '/manage/Role/getRoleMemberTree', '/manage/Role/getRoleMemberTree', '/manage/Role/getRoleMemberTree', '', '_self', '', '0', '', '', '1', '1594783298', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1250', '834', 'manage', 'system', '回复规则管理', '/manage/member.wechatKeyword/index', '/manage/member.wechatKeyword/index', 'manage_member_wechatKeyword_index', '', '_self', '', '0', 'fa fa-paint-brush', '', '1', '1594790225', '0', '999', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1252', '1250', 'manage', 'system', '新增', '/manage/member.wechatKeyword/add', '/manage/member.wechatKeyword/add', 'manage_member_wechatKeyword_add', '', '_self', '', '0', '', '', '1', '1594790225', '0', '999', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1253', '1250', 'manage', 'system', '编辑', '/manage/member.wechatKeyword/edit', '/manage/member.wechatKeyword/edit', 'manage_member_wechatKeyword_edit', '', '_self', '', '0', '', '', '1', '1594790225', '0', '999', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1254', '1250', 'manage', 'system', '删除', '/manage/member.wechatKeyword/del', '/manage/member.wechatKeyword/del', 'manage_member_wechatKeyword_del', '', '_self', '', '0', '', '', '1', '1594790225', '0', '999', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1255', '1124', 'manage', 'system', '状态', '/manage/admin/member/setStatus', '/manage/admin.member/setStatus', '/manage/admin/member/setStatus', '', '_self', '', '0', '', '', '1', '1594793850', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1256', '1', 'manage', 'system', '租户角色', '/manage/Role/memberRole', '/manage/Role/memberRole', '/manage/Role/memberRole', '', '_self', '', '0', '', '', '1', '1594800045', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1257', '1', 'manage', 'system', '租户添加', '/manage/Role/addMember', '/manage/Role/addMember', '/manage/Role/addMember', '', '_self', '', '0', '', '', '1', '1594801463', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1258', '1', 'manage', 'system', '租户编辑', '/manage/Role/editMember', '/manage/Role/editMember', '/manage/Role/editMember', '', '_self', '', '0', '', '', '1', '1594801517', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1259', '1', 'manage', 'system', '顶级租户节点树', '/manage/Role/getNodeTree', '/manage/Role/getNodeTree', '/manage/Role/getNodeTree', '', '_self', '', '0', '', '', '1', '1594804763', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1270', '183', 'manage', 'system', '租户节点', '/manage/node/memberIndex', '/manage/Node/memberIndex', '/manage/Node/add', '', '_self', '', '0', '', '', '1', '1594877533', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1271', '183', 'manage', 'system', '添加租户节点', '/manage/node/addMember', '/manage/Node/addMember', '/manage/Node/addMember', '', '_self', '', '0', '', '', '1', '1594878879', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1272', '183', 'manage', 'system', '编辑租户节点', '/manage/node/editMember', '/manage/Node/editMember', '/manage/Node/editMember', '', '_self', '', '0', '', '', '1', '1594878964', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1273', '862', 'manage', 'system', '获取小程序体验二维码', '/manage/member/getQrCode', '/manage/member.Miniapp/getQrCode', 'manage_member_getQrCode', '', '_self', '', '0', '', '', '1', '1594879921', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1274', '183', 'manage', 'system', '租户节点树', '/manage/node/getMemberNodeTree', '/manage/Node/getMemberNodeTree', '/manage/Node/getMemberNodeTree', '', '_self', '', '0', '', '', '1', '1594880801', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1275', '1306', 'manage', 'system', '云市场服务管理', '/manage/admin/cloud/service', '/manage/admin.cloud.service', '_manage_admin_cloud_service', '', '_self', '', '1', 'fa fa-cloud-upload', '', '1', '1594951199', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1276', '1275', 'manage', 'system', '查看', '/manage/admin/cloud/service/index', '/manage/admin.cloud.service/index', '/manage/admin.cloud.service/index', '', '_self', '', '0', '', '', '1', '1594951199', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1277', '1275', 'manage', 'system', '新增', '/manage/admin/cloud/service/add', '/manage/admin.cloud.service/add', '/manage/admin.cloud.service/add', '', '_self', '', '0', '', '', '1', '1594951199', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1278', '1275', 'manage', 'system', '编辑', '/manage/admin/cloud/service/edit', '/manage/admin.cloud.service/edit', '/manage/admin.cloud.service/edit', '', '_self', '', '0', '', '', '1', '1594951199', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1279', '1275', 'manage', 'system', '删除', '/manage/admin/cloud/service/del', '/manage/admin.cloud.service/del', '/manage/admin.cloud.service/del', '', '_self', '', '0', '', '', '1', '1594951199', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1280', '1306', 'manage', 'system', '云市场套餐管理', '/manage/admin/cloud/packet', '/manage/admin.cloud.packet', '/manage/admin.cloud.packet', '', '_self', '', '0', '', '', '1', '1594965011', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1281', '1280', 'manage', 'system', '查看', '/manage/admin/cloud/packet/index', '/manage/admin.cloud.packet/index', '/manage/admin.cloud.packet/index', '', '_self', '', '0', '', '', '1', '1594965011', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1282', '1280', 'manage', 'system', '新增', '/manage/admin/cloud/packet/add', '/manage/admin.cloud.packet/add', '/manage/admin.cloud.packet/add', '', '_self', '', '0', '', '', '1', '1594965011', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1283', '1280', 'manage', 'system', '编辑', '/manage/admin/cloud/packet/edit', '/manage/admin.cloud.packet/edit', '/manage/admin.cloud.packet/edit', '', '_self', '', '0', '', '', '1', '1594965011', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1284', '1280', 'manage', 'system', '删除', '/manage/admin/cloud/packet/del', '/manage/admin.cloud.packet/del', '/manage/admin.cloud.packet/del', '', '_self', '', '0', '', '', '1', '1594965011', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1285', '1306', 'manage', 'system', '云市场租户余额', '/manage/admin/cloud/wallet', '/manage/admin.cloud.wallet', '/manage/admin.cloud.wallet', '', '_self', '', '0', '', '', '1', '1594965034', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1286', '1285', 'manage', 'system', '查看', '/manage/admin/cloud/wallet/index', '/manage/admin.cloud.wallet/index', '/manage/admin.cloud.wallet/index', '', '_self', '', '0', '', '', '1', '1594965034', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1287', '1285', 'manage', 'system', '新增', '/manage/admin/cloud/wallet/add', '/manage/admin.cloud.wallet/add', '/manage/admin.cloud.wallet/add', '', '_self', '', '0', '', '', '1', '1594965034', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1288', '1285', 'manage', 'system', '编辑', '/manage/admin/cloud/wallet/edit', '/manage/admin.cloud.wallet/edit', '/manage/admin.cloud.wallet/edit', '', '_self', '', '0', '', '', '1', '1594965034', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1289', '1285', 'manage', 'system', '删除', '/manage/admin/cloud/wallet/del', '/manage/admin.cloud.wallet/del', '/manage/admin.cloud.wallet/del', '', '_self', '', '0', '', '', '1', '1594965034', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1290', '1306', 'manage', 'system', '云市场钱包记录管理', '/manage/admin/cloud/walletBill', '/manage/admin.cloud.walletBill', '/manage/admin.cloud.walletBill', '', '_self', '', '0', '', '', '1', '1594965052', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1291', '1290', 'manage', 'system', '查看', '/manage/admin/cloud/walletBill/index', '/manage/admin.cloud.walletBill/index', '/manage/admin.cloud.walletBill/index', '', '_self', '', '0', '', '', '1', '1594965052', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1292', '1290', 'manage', 'system', '新增', '/manage/admin/cloud/walletBill/add', '/manage/admin.cloud.walletBill/add', '/manage/admin.cloud.walletBill/add', '', '_self', '', '0', '', '', '1', '1594965052', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1293', '1290', 'manage', 'system', '编辑', '/manage/admin/cloud/walletBill/edit', '/manage/admin.cloud.walletBill/edit', '/manage/admin.cloud.walletBill/edit', '', '_self', '', '0', '', '', '1', '1594965052', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1294', '1290', 'manage', 'system', '删除', '/manage/admin/cloud/walletBill/del', '/manage/admin.cloud.walletBill/del', '/manage/admin.cloud.walletBill/del', '', '_self', '', '0', '', '', '1', '1594965052', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1295', '1306', 'manage', 'system', '云市场订单管理', '/manage/admin/cloud/order', '/manage/admin.cloud.order', 'manage_admin_cloud_order', '', '_self', '', '0', '', '', '1', '1594965069', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1296', '1295', 'manage', 'system', '查看', '/manage/admin/cloud/order/index', '/manage/admin.cloud.order/index', '/manage/admin.cloud.order/index', '', '_self', '', '0', '', '', '1', '1594965069', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1297', '1295', 'manage', 'system', '新增', '/manage/admin/cloud/order/add', '/manage/admin.cloud.order/add', '/manage/admin.cloud.order/add', '', '_self', '', '0', '', '', '1', '1594965069', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1298', '1295', 'manage', 'system', '编辑', '/manage/admin/cloud/order/edit', '/manage/admin.cloud.order/edit', '/manage/admin.cloud.order/edit', '', '_self', '', '0', '', '', '1', '1594965069', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1299', '1295', 'manage', 'system', '删除', '/manage/admin/cloud/order/del', '/manage/admin.cloud.order/del', '/manage/admin.cloud.order/del', '', '_self', '', '0', '', '', '1', '1594965069', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1306', '188', 'manage', 'system', '云市场', 'cloud', 'cloud', 'cloud', '', '_self', '', '1', 'fa fa-skyatlas', '', '1', '1594970291', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1307', '186', 'manage', 'system', '上传文件', '/manage/publicCommon/upload', '/manage/publicCommon/upload', 'public_common_upload', '', '_self', '', '0', '', '', '1', '1594974304', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1308', '862', 'manage', 'system', '上传文件', '/manage/publicCommon/upload', '/manage/publicCommon/upload', '_manage_publicCommon_upload', '', '_self', '', '0', '', '', '1', '1594974394', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1309', '1315', 'manage', 'system', '云市场服务管理', '/manage/member/cloud/service', '/manage/member.cloud.service', '_manage_member_cloud_service', '', '_self', '', '1', 'fa fa-cloud-upload', '', '1', '1595034953', '0', '999', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1310', '1309', 'manage', 'system', '查看', '/manage/member/cloud/service/index', '/manage/member.cloud.service/index', '/manage/member.cloud.service/index', '', '_self', '', '0', '', '', '1', '1595034953', '0', '999', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1314', '0', 'manage', 'system', '插件管理', 'member/addons', 'member/addons', 'member/addons', '', '_self', '', '1', '', '', '1', '1595035502', '0', '3000', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1315', '1314', 'manage', 'system', '云市场', 'member/cloud', 'member/cloud', 'member_cloud', '', '_self', '', '1', 'fa fa-skyatlas', '', '1', '1595035578', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1316', '862', 'manage', 'system', '基本资料', '/manage/member/login/userSetting', '/manage/member.Login/userSetting', '/manage/member/login/userSetting', '', '_self', '', '0', '', '', '1', '1595038359', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1317', '1315', 'manage', 'system', '云市场套餐管理', '/manage/member/cloud/packet', '/manage/member.cloud.packet', '/manage/member.cloud.packet', '', '_self', '', '0', '', '', '1', '1595039367', '0', '999', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1318', '1317', 'manage', 'system', '查看', '/manage/member/cloud/packet/index', '/manage/member.cloud.packet/index', '/manage/member.cloud.packet/index', '', '_self', '', '0', '', '', '1', '1595039367', '0', '999', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1322', '1317', 'manage', 'system', '购买', '/manage/member/cloud/packet/buy', '/manage/member.cloud.packet/buy', '/manage/member.cloud.packet/buy', '', '_self', '', '0', '', '', '1', '1595039367', '0', '999', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1323', '862', 'manage', 'system', '修改密码', '/manage/member/login/userPassword', '/manage/member.Login/userPassword', '/manage/member/login/userPassword', '', '_self', '', '0', '', '', '1', '1595040696', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1324', '1315', 'manage', 'system', '云市场钱包记录管理', '/manage/member/cloud/walletBill', '/manage/member.cloud.walletBill', '/manage/member.cloud.walletBill', '', '_self', '', '0', '', '', '1', '1595042219', '0', '999', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1325', '1324', 'manage', 'system', '查看', '/manage/member/cloud/walletBill/index', '/manage/member.cloud.walletBill/index', '/manage/member.cloud.walletBill/index', '', '_self', '', '0', '', '', '1', '1595042219', '0', '999', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1329', '3', 'manage', 'system', '基本资料', '/manage/admin/userSetting', '/manage/admin.Index/userSetting', '/manage/admin/userSetting', '', '_self', '', '0', '', '', '1', '1595049242', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1330', '3', 'manage', 'system', '修改密码', '/manage/admin/userPassword', '/manage/admin.Index/userPassword', '/manage/admin/userPassword', '', '_self', '', '0', '', '', '1', '1595049296', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1331', '1545', 'manage', 'system', '租户钱包', '/manage/admin/member/wallet', '/manage/admin.member.wallet', 'manage_admin_member_wallet', '', '_self', '', '1', '', '', '1', '1595050145', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1332', '1331', 'manage', 'system', '查看', '/manage/admin/member/wallet/index', '/manage/admin.member.wallet/index', '/manage/admin.member.wallet/index', '', '_self', '', '0', '', '', '1', '1595050145', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1333', '1331', 'manage', 'system', '新增', '/manage/admin/member/wallet/add', '/manage/admin.member.wallet/add', '/manage/admin.member.wallet/add', '', '_self', '', '0', '', '', '1', '1595050145', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1334', '1331', 'manage', 'system', '编辑', '/manage/admin/member/wallet/edit', '/manage/admin.member.wallet/edit', '/manage/admin.member.wallet/edit', '', '_self', '', '0', '', '', '1', '1595050145', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1335', '1331', 'manage', 'system', '删除', '/manage/admin/member/wallet/del', '/manage/admin.member.wallet/del', '/manage/admin.member.wallet/del', '', '_self', '', '0', '', '', '1', '1595050145', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1341', '1545', 'manage', 'system', '租户财务', '/manage/admin/member/walletBill', '/manage/admin.member.walletBill', 'manage_admin_member_walletBill', '', '_self', '', '1', '', '', '1', '1595052713', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1342', '1341', 'manage', 'system', '查看', '/manage/admin/member/walletBill/index', '/manage/admin.member.walletBill/index', '/manage/admin.member.walletBill/index', '', '_self', '', '0', '', '', '1', '1595052713', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1343', '1341', 'manage', 'system', '新增', '/manage/admin/member/walletBill/add', '/manage/admin.member.walletBill/add', '/manage/admin.member.walletBill/add', '', '_self', '', '0', '', '', '1', '1595052713', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1344', '1341', 'manage', 'system', '编辑', '/manage/admin/member/walletBill/edit', '/manage/admin.member.walletBill/edit', '/manage/admin.member.walletBill/edit', '', '_self', '', '0', '', '', '1', '1595052713', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1345', '1341', 'manage', 'system', '删除', '/manage/admin/member/walletBill/del', '/manage/admin.member.walletBill/del', '/manage/admin.member.walletBill/del', '', '_self', '', '0', '', '', '1', '1595052713', '0', '999', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1346', '828', 'manage', 'system', '配置管理', '/config', '/config', '_config', '', '_self', '', '0', 'fa fa-gear', '', '1', '1595054309', '0', '888', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1347', '1346', 'manage', 'system', '配置管理', '/manage/member/config/showMemberConfig/1', '/manage/member.Config/showMemberConfig', '_manage_member_config_showMemberConfig_1', '', '_self', '', '0', 'fa fa-eyedropper', '', '1', '1595054503', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1348', '1347', 'manage', 'system', '保存配置', '/manage/member/config/setMemberValues', '/manage/member.Config/setMemberValues', '_manage_member_config_setMemberValues', '', '_self', '', '0', '', '', '1', '1595054662', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1349', '828', 'manage', 'system', '财务管理', '/manage/member/walletBill', '/manage/member.walletBill', 'manage_member_walletBill', '', '_self', '', '1', 'fa fa-edit', '', '1', '1595206543', '0', '999', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1350', '1349', 'manage', 'system', '查看', '/manage/member/walletBill/index', '/manage/member.walletBill/index', '/manage/member.walletBill/index', '', '_self', '', '0', '', '', '1', '1595206543', '0', '999', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1351', '1349', 'manage', 'system', '新增', '/manage/member/walletBill/add', '/manage/member.walletBill/add', '/manage/member.walletBill/add', '', '_self', '', '0', '', '', '1', '1595206543', '0', '999', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1352', '1349', 'manage', 'system', '编辑', '/manage/member/walletBill/edit', '/manage/member.walletBill/edit', '/manage/member.walletBill/edit', '', '_self', '', '0', '', '', '1', '1595206543', '0', '999', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1354', '1315', 'manage', 'system', '云市场订单管理', '/manage/member/cloud/order', '/manage/member.cloud.order', '/manage/member.cloud.order', '', '_self', '', '0', '', '', '1', '1595039367', '0', '999', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1355', '1354', 'manage', 'system', '查看', '/manage/member/cloud/order/index', '/manage/member.cloud.order/index', '/manage/member.cloud.order/index', '', '_self', '', '0', '', '', '1', '1595039367', '0', '999', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1358', '1396', 'manage', 'system', '编辑', '/manage/member/groupData/edit', '/manage/member.GroupData/edit', '_manage_member_groupData_edit', '', '_self', '', '0', '', '', '1', '1595296632', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1359', '1396', 'manage', 'system', '添加', '/manage/member/groupData/add', '/manage/member.GroupData/add', '_manage_member_groupData_add', '', '_self', '', '0', '', '', '1', '1595296688', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1360', '1396', 'manage', 'system', '删除', '/manage/member/groupData/softdleting', '/manage/member.GroupData/softdleting', '_manage_member_groupData_softdleting', '', '_self', '', '0', '', '', '1', '1595296730', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1361', '1396', 'manage', 'system', '列表', '/manage/member/groupData/getConfigList', '/manage/member.GroupData/getConfigList', '_manage_member_groupData_getConfigList', '', '_self', '', '0', '', '', '1', '1595296788', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1362', '211', 'manage', 'system', '状态变更', '/manage/groupData/setConfigShow', '/manage/GroupData/setConfigShow', '/manage/GroupData/setConfigShow', '', '_self', '', '0', '', '', '1', '1595297231', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1363', '1396', 'manage', 'system', '更改状态', '/manage/member/groupData/setConfigShow', '/manage/member.GroupData/setConfigShow', '_manage_member_groupData_setConfigShow', '', '_self', '', '0', '', '', '1', '1595297407', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1364', '862', 'manage', 'system', '上传图片(layui返回格式)', '/manage/publicCommon/uploadimg', '/manage/publicCommon/uploadimg', '_manage_publicCommon_uploadimg', '', '_self', '', '0', '', '', '1', '1595298389', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1365', '186', 'manage', 'system', '上传图片(layui返回格式)', '/manage/publicCommon/uploadimg', '/manage/publicCommon/uploadimg', '/manage/publicCommon/uploadimg', '', '_self', '', '0', '', '', '1', '1595298467', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1391', '186', 'manage', 'system', '附件管理', '/manage/uploadfile/index', '/manage/admin.Uploadfile/index', 'manage_uploadfile_index', '', '_self', '', '1', 'fa fa-cloud-upload', '', '1', '1598422130', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1392', '1391', 'manage', 'system', '文件添加', '/manage/uploadfile/add', '/manage/admin.Uploadfile/add', '_manage_uploadfile_add', '', '_self', '', '0', '', '', '1', '1598422471', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1393', '1391', 'manage', 'system', '文件删除', '/manage/uploadfile/del', '/manage/admin.Uploadfile/del', '_manage_uploadfile_del', '', '_self', '', '0', '', '', '1', '1598422620', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1394', '1391', 'manage', 'system', '文件导出', '/manage/uploadfile/export', '/manage/admin.Uploadfile/export', '_manage_uploadfile_export', '', '_self', '', '0', '', '', '1', '1598422670', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1396', '1346', 'manage', 'system', '组合数据管理', '/manage/member/groupData', '/manage/member.GroupData/index', '_manage_member_groupData', '', '_self', '', '0', '', '', '1', '1598424995', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1408', '828', 'manage', 'system', '充值', '/manage/member/recharge', '/manage/member.Recharge/index', '_manage_member_recharge', '', '_self', '', '0', '', '', '1', '1599015300', '0', '666', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1411', '1408', 'manage', 'system', '提交', '/manage/member/recharge/pay', '/manage/member.Recharge/pay', '_manage_member_recharge_pay', '', '_self', '', '0', '', '', '1', '1599027718', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1412', '1471', 'manage', 'system', '用户列表', '/manage/member/user/index', '/manage/member.User/index', 'manage_member_user_index', '', '_self', '', '1', 'fa fa-user-plus', '', '1', '1599114629', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1413', '1412', 'manage', 'system', '新增', '/manage/member/user/add', '/manage/member.User/add', '_manage_member_user_add', '', '_self', '', '0', '', '', '1', '1599114909', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1414', '1412', 'manage', 'system', '修改', '/manage/member/user/edit', '/manage/member.User/edit', '_manage_member_user_edit', '', '_self', '', '0', '', '', '1', '1599114949', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1416', '1412', 'manage', 'system', '导出', '/manage/member/user/export', '/manage/member.User/export', '_manage_member_user_export', '', '_self', '', '0', '', '', '1', '1599115319', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1417', '1412', 'manage', 'system', '修改', '/manage/member/user/modify', '/manage/member.User/modify', '_manage_member_user_modify', '', '_self', '', '0', '', '', '1', '1599115360', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1418', '1412', 'manage', 'system', '修改密码', '/manage/member/user/editPw', '/manage/member.User/editPw', '_manage_member_user_editPw', '', '_self', '', '0', '', '', '1', '1599120886', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1419', '1412', 'manage', 'system', '资金余额', '/manage/member/user/changeMoney', '/manage/member.User/changeMoney', '_manage_member_user_changeMoney', '', '_self', '', '0', '', '', '1', '1599130844', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1443', '828', 'manage', 'system', '首页', '/manage/member/dashboard', '/manage/member.Index/dashboard', '_manage_member_dashboard', '', '_self', '', '1', 'fa fa-bar-chart-o', '', '1', '1599546960', '0', '9999', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1458', '1069', 'manage', 'system', '素材列表', '/manage/member.official/materialList', '/manage/member.official/materialList', 'manage_member_official_materialList', '', '_self', '', '0', '', '', '1', '1600239056', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1462', '1471', 'manage', 'system', '用户充值', 'rechange/config', 'rechange/config', 'rechange_config', '', '_self', '', '1', 'fa fa-cny', '', '1', '1600509225', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1463', '1462', 'manage', 'system', '充值设置', '/manage/member/config/showMemberConfig/0/54', '/manage/member/config.ShowMemberConfig', 'manage_member_config_showMemberConfig_0_54', '', '_self', '', '1', '', '', '1', '1600509355', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1464', '1462', 'manage', 'system', '充值套餐', '/manage/member/groupData/recharge_select', '/manage/member.GroupData/index', 'manage_member_groupData_recharge_select', '', '_self', '', '1', '', '', '1', '1600509525', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1465', '1462', 'manage', 'system', '充值记录', '/manage/member/user/recharge/index', '/manage/member.user.Recharge/index', 'manage_member_user_recharge_index', '', '_self', '', '1', '', '', '1', '1600669249', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1466', '1465', 'manage', 'system', '添加', '/manage/member/user/recharge/add', '/manage/member.user.Recharge/add', '_manage_member_user_recharge_add', '', '_self', '', '0', '', '', '1', '1600669582', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1467', '1465', 'manage', 'system', '编辑', '/manage/member/user/recharge/edit', '/manage/member.user.Recharge/edit', '_manage_member_user_recharge_edit', '', '_self', '', '0', '', '', '1', '1600669661', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1468', '1465', 'manage', 'system', '导出', '/manage/member/user/recharge/export', '/manage/member.user.Recharge/export', '_manage_member_user_recharge_export', '', '_self', '', '0', '', '', '1', '1600669752', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1469', '1465', 'manage', 'system', '修改', '/manage/member/user/recharge/modify', '/manage/member.user.Recharge/modify', '_manage_member_user_recharge_modify', '', '_self', '', '0', '', '', '1', '1600669842', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1471', '1172', 'manage', 'system', '全部会员', '/app/all/user', '/app/all/user', 'app_all_user', '', '_self', '', '1', 'fa fa-male', '', '1', '1600680925', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1472', '1471', 'manage', 'system', '财务记录', '/manage/member/user/bill/index', '/manage/member.user.Bill/index', 'manage_member_user_bill_index', '', '_self', '', '1', 'fa fa-pencil', '', '1', '1600681941', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1473', '1472', 'manage', 'system', '导出', '/manage/member/user/bill/export', '/manage/member.user.Bill/export', '_manage_member_user_bill_export', '', '_self', '', '0', '', '', '1', '1600682018', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1474', '1472', 'manage', 'system', '添加', '/manage/member/user/bill/add', '/manage/member.user.Bill/add', '_manage_member_user_bill_add', '', '_self', '', '0', '', '', '1', '1600682140', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1475', '1472', 'manage', 'system', '编辑', '/manage/member/user/bill/edit', '/manage/member.user.Bill/edit', '_manage_member_user_bill_edit', '', '_self', '', '0', '', '', '1', '1600682206', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1476', '1472', 'manage', 'system', '修改', '/manage/member/user/bill/modify', '/manage/member.user.Bill/modify', '_manage_member_user_bill_modify', '', '_self', '', '0', '', '', '1', '1600682264', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1485', '1471', 'manage', 'system', '用户提现', '/manage/member/extract', '/manage/member/extract', '_manage_member_extract', '', '_self', '', '1', 'fa fa-money', '', '1', '1600767960', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1486', '1485', 'manage', 'system', '提现配置', '/manage/member/config/showMemberConfig/0/55', '/manage/member/config.ShowMemberConfig', 'manage_member_config_showMemberConfig_0_55', '', '_self', '', '1', '', '', '1', '1600768150', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1487', '1485', 'manage', 'system', '绑定管理', '/manage/member/user/extract/index', '/manage/member.user.Extract/index', 'manage_member_user_extract_index', '', '_self', '', '1', '', '', '1', '1600844996', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1488', '1487', 'manage', 'system', '导出', '/manage/member/user/extract/export', '/manage/member.user.Extract/export', '_manage_member_user_extract_export', '', '_self', '', '0', '', '', '1', '1600845091', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1489', '1487', 'manage', 'system', '添加', '/manage/member/user/extract/add', '/manage/member.user.Extract/add', '_manage_member_user_extract_add', '', '_self', '', '0', '', '', '1', '1600845293', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1490', '1487', 'manage', 'system', '编辑', '/manage/member/user/extract/edit', '/manage/member.user.Extract/edit', '_manage_member_user_extract_edit', '', '_self', '', '0', '', '', '1', '1600845388', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1491', '1487', 'manage', 'system', '修改', '/manage/member/user/extract/modify', '/manage/member.user.Extract/modify', '_manage_member_user_extract_modify', '', '_self', '', '0', '', '', '1', '1600845433', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1492', '1487', 'manage', 'system', '删除', '/manage/member/user/extract/del', '/manage/member.user.Extract/del', '_manage_member_user_extract_del', '', '_self', '', '0', '', '', '1', '1600845502', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1493', '1485', 'manage', 'system', '提现管理', '/manage/member/user/extractLog/index', '/manage/member.user.ExtractLog/index', 'manage_member_user_extractLog_index', '', '_self', '', '1', '', '', '1', '1600845647', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1494', '1493', 'manage', 'system', '导出', '/manage/member/user/extractLog/export', '/manage/member.user.ExtractLog/export', '_manage_member_user_extractLog_export', '', '_self', '', '0', '', '', '1', '1600845718', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1495', '1493', 'manage', 'system', '添加', '/manage/member/user/extractLog/add', '/manage/member.user.ExtractLog/add', '_manage_member_user_extractLog_add', '', '_self', '', '0', '', '', '1', '1600845981', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1496', '1493', 'manage', 'system', '编辑', '/manage/member/user/extractLog/edit', '/manage/member.user.ExtractLog/edit', '_manage_member_user_extractLog_edit', '', '_self', '', '0', '', '', '1', '1600846099', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1497', '1493', 'manage', 'system', '修改', '/manage/member/user/extractLog/modify', '/manage/member.user.ExtractLog/modify', '_manage_member_user_extractLog_modify', '', '_self', '', '0', '', '', '1', '1600846183', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1498', '1493', 'manage', 'system', '删除', '/manage/member/user/extractLog/del', '/manage/member.user.ExtractLog/del', '_manage_member_user_extractLog_del', '', '_self', '', '0', '', '', '1', '1600846240', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1500', '834', 'manage', 'system', '微信图文管理', '/manage/member.wechatKeyword/materialList', '/manage/member.wechatKeyword/materialList', 'manage_member_wechatKeyword_materialList', '', '_self', '', '0', 'fa fa-weixin', '', '1', '1600929033', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1501', '185', 'manage', 'system', '操作日志', '/manage/admin/sys/log/index', '/manage/admin.sys.Log/index', 'manage_admin_sys_log_index', '', '_self', '', '1', '', '', '1', '1600936300', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1502', '1501', 'manage', 'system', '导出', '/manage/admin/sys/log/export', '/manage/admin.sys.Log/export', '_manage_admin_sys_log_export', '', '_self', '', '0', '', '', '1', '1600936381', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1503', '1501', 'manage', 'system', '添加', '/manage/admin/sys/log/add', '/manage/admin.sys.Log/add', '_manage_admin_sys_log_add', '', '_self', '', '0', '', '', '1', '1600936473', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1504', '1501', 'manage', 'system', '编辑', '/manage/admin/sys/log/edit', '/manage/admin.sys.Log/edit', '_manage_admin_sys_log_edit', '', '_self', '', '0', '', '', '1', '1600936641', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1505', '1501', 'manage', 'system', '修改', '/manage/admin/sys/log/modify', '/manage/admin.sys.Log/modify', '_manage_admin_sys_log_modify', '', '_self', '', '0', '', '', '1', '1600936688', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1506', '1501', 'manage', 'system', '删除', '/manage/admin/sys/log/del', '/manage/admin.sys.Log/del', '_manage_admin_sys_log_del', '', '_self', '', '0', '', '', '1', '1600936742', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1507', '1500', 'manage', 'system', '删除图文', '/manage/member.wechatKeyword/removeMaterial', '/manage/member.wechatKeyword/removeMaterial', 'manage_member_wechatKeyword_removeMaterial', '', '_self', '', '0', '', '', '1', '1600936810', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1508', '828', 'manage', 'system', '操作日志', '/manage/member/sys/log/index', '/manage/member.sys.Log/index', 'manage_member_sys_log_index', '', '_self', '', '1', 'fa fa-book', '', '1', '1600938881', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1509', '1508', 'manage', 'system', '导出', '/manage/member/sys/log/export', '/manage/member.sys.Log/export', '_manage_member_sys_log_export', '', '_self', '', '0', '', '', '1', '1600938928', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1510', '1508', 'manage', 'system', '添加', '/manage/member/sys/log/add', '/manage/member.sys.Log/add', '_manage_member_sys_log_add', '', '_self', '', '0', '', '', '1', '1600938998', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1511', '1508', 'manage', 'system', '编辑', '/manage/member/sys/log/edit', '/manage/member.sys.Log/edit', '_manage_member_sys_log_edit', '', '_self', '', '0', '', '', '1', '1600939043', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1512', '1508', 'manage', 'system', '修改', '/manage/member/sys/log/modify', '/manage/member.sys.Log/modify', '_manage_member_sys_log_modify', '', '_self', '', '0', '', '', '1', '1600939126', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1514', '3', 'manage', 'system', '登录日志', '/manage/admin/loginInfo', '/manage/admin.Index/getAdminLoginInfo', '_manage_admin_loginInfo', '', '_self', '', '0', '', '', '1', '1600951437', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1515', '1443', 'manage', 'system', '登录日志', '/manage/member/loginInfo', '/manage/member.Index/getMemberLoginInfo', '_manage_member_loginInfo', '', '_self', '', '0', '', '', '1', '1600998614', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1516', '1586', 'manage', 'system', '租户资讯', '/manage/admin/sys/notice/index', '/manage/admin.sys.Notice/index', 'manage_admin_sys_notice_index', '', '_self', '', '1', '', '', '1', '1601015082', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1517', '1516', 'manage', 'system', '导出', '/manage/admin/sys/notice/export', '/manage/admin.sys.Notice/export', '_manage_admin_sys_notice_export', '', '_self', '', '0', '', '', '1', '1601015138', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1518', '1516', 'manage', 'system', '添加', '/manage/admin/sys/notice/add', '/manage/admin.sys.Notice/add', '_manage_admin_sys_notice_add', '', '_self', '', '0', '', '', '1', '1601015217', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1519', '1516', 'manage', 'system', '编辑', '/manage/admin/sys/notice/edit', '/manage/admin.sys.Notice/edit', '_manage_admin_sys_notice_edit', '', '_self', '', '0', '', '', '1', '1601015279', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1520', '1516', 'manage', 'system', '修改', '/manage/admin/sys/notice/modify', '/manage/admin.sys.Notice/modify', '_manage_admin_sys_notice_modify', '', '_self', '', '0', '', '', '1', '1601015357', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1521', '1516', 'manage', 'system', '删除', '/manage/admin/sys/notice/del', '/manage/admin.sys.Notice/del', '_manage_admin_sys_notice_del', '', '_self', '', '0', '', '', '1', '1601015420', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1522', '1443', 'manage', 'system', '系统公告列表', '/manage/member/noticeList', '/manage/member.Index/getNoticeList', '_manage_member_noticeList', '', '_self', '', '0', '', '', '1', '1601016826', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1523', '1443', 'manage', 'system', '用户图表数据', '/manage/member/echartsInfo', '/manage/member.Index/getEchartsInfo', '_manage_member_echartsInfo', '', '_self', '', '0', '', '', '1', '1601035845', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1526', '891', 'manage', 'system', '禁用', '/manage/admin.Plugin/disable', '/manage/admin.Plugin/disable', '/manage/admin.Plugin/disable', '', '_self', '', '0', '', '', '1', '1601275727', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1527', '862', 'manage', 'system', '提交代码给微信官网审核', '/manage/member/submitAudit', '/manage/member.Miniapp/submitAudit', 'manage_member_submitAudit', '', '_self', '', '0', '', '', '1', '1601276072', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1528', '1020', 'manage', 'system', '应用功能', '/manage/admin/miniapp/module/index', '/bwmall/admin.Template/index', '_manage_admin_miniapp_module_index', '', '_self', '', '0', 'fa fa-clipboard', '', '1', '1598843234', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1529', '1528', 'manage', 'system', '导出', '/manage/admin/miniapp/module/export', '/bwmall/admin.Template/export', 'manage_admin_miniapp_module_export', '', '_self', '', '0', '', '', '1', '1598843541', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1530', '1528', 'manage', 'system', '新增', '/manage/admin/miniapp/module/add', '/bwmall/admin.Template/add', 'manage_admin_miniapp_module_add', '', '_self', '', '0', '', '', '1', '1598843630', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1531', '1528', 'manage', 'system', '编辑', '/manage/admin/miniapp/module/edit', '/bwmall/admin.Template/edit', 'manage_admin_miniapp_module_edit', '', '_self', '', '0', '', '', '1', '1598843679', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1532', '1528', 'manage', 'system', '删除', '/manage/admin/miniapp/module/del', '/bwmall/admin.Template/del', 'manage_admin_miniapp_module_del', '', '_self', '', '0', '', '', '1', '1598843807', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1533', '1528', 'manage', 'system', '修改', '/manage/admin/miniapp/module/modify', '/bwmall/admin.Template/modify', 'manage_admin_miniapp_module_modify', '', '_self', '', '0', '', '', '1', '1598843884', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1534', '211', 'manage', 'system', '删除', '/manage/GroupData/softdleting', '/manage/GroupData/softdleting', '_manage_GroupData_softdleting', '', '_self', '', '0', '', '', '1', '1601295884', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1535', '862', 'manage', 'system', '获取代码审核状态', '/manage/member/getAuditStatus', '/manage/member.Miniapp/getAuditStatus', 'manage_member_getAuditStatus', '', '_self', '', '0', '', '', '1', '1601308907', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1536', '862', 'manage', 'system', '发布小程序', '/manage/member/publishCode', '/manage/member.Miniapp/publishCode', 'manage_member_publishCode', '', '_self', '', '0', '', '', '1', '1601311068', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1537', '862', 'manage', 'system', '强制撤销审核', '/manage/member/resetAudit', '/manage/member.Miniapp/resetAudit', 'manage_member_resetAudit', '', '_self', '', '0', '', '', '1', '1601311131', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1539', '1172', 'manage', 'system', '物流快递管理', '/manage/member/sys/express/index', '/manage/member.sys.Express/index', 'manage_member_sys_express_index', '', '_self', '', '1', 'fa fa-bus', '', '0', '1602124262', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1540', '1539', 'manage', 'system', '导出', '/manage/member/sys/express/export', '/manage/member.sys.Express/export', 'manage_member_sys_express_export', '', '_self', '', '0', '', '', '1', '1602124367', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1541', '1539', 'manage', 'system', '添加', '/manage/member/sys/express/add', '/manage/member.sys.Express/add', 'manage_member_sys_express_add', '', '_self', '', '0', '', '', '1', '1602124487', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1542', '1539', 'manage', 'system', '编辑', '/manage/member/sys/express/edit', '/manage/member.sys.Express/edit', 'manage_member_sys_express_edit', '', '_self', '', '0', '', '', '1', '1602124567', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1543', '1539', 'manage', 'system', '修改', '/manage/member/sys/express/modify', '/manage/member.sys.Express/modify', 'manage_member_sys_express_modify', '', '_self', '', '0', '', '', '1', '1602124635', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1544', '1539', 'manage', 'system', '删除', '/manage/member/sys/express/del', '/manage/member.sys.Express/del', 'manage_member_sys_express_del', '', '_self', '', '0', '', '', '1', '1602124720', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1545', '186', 'manage', 'system', '租户管理', '/', '/', '', '', '_self', '', '1', 'fa fa-user', '', '1', '1602253898', '0', '1500', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1558', '1250', 'manage', 'system', '添加规则', '/manage/member.wechatKeyword/addKeys', '/manage/member.wechatKeyword/addKeys', 'manage_member_wechatKeyword_addKeys', '', '_self', '', '0', '', '', '1', '1602837519', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1559', '1250', 'manage', 'system', '预览text', '/manage/member.wechatKeyword/previewText', '/manage/member.wechatKeyword/previewText', 'manage_member_wechatKeyword_previewText', '', '_self', '', '0', '', '', '1', '1603100713', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1560', '1250', 'manage', 'system', '预览news', '/manage/member.wechatKeyword/previewNews', '/manage/member.wechatKeyword/previewNews', 'manage_member_wechatKeyword_previewNews', '', '_self', '', '0', '', '', '1', '1603113498', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1561', '1250', 'manage', 'system', '选择图文', '/manage/member.wechatKeyword/select', '/manage/member.wechatKeyword/select', 'manage_member_wechatKeyword_select', '', '_self', '', '0', '', '', '1', '1603115169', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1562', '1250', 'manage', 'system', '预览图片', '/manage/member.wechatKeyword/previewImage', '/manage/member.wechatKeyword/previewImage', 'manage_member_wechatKeyword_previewImage', '', '_self', '', '0', '', '', '1', '1603119741', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1563', '1250', 'manage', 'system', '预览music', '/manage/member.wechatKeyword/previewMusic', '/manage/member.wechatKeyword/previewMusic', 'manage_member_wechatKeyword_previewMusic', '', '_self', '', '0', '', '', '1', '1603126619', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1564', '1250', 'manage', 'system', '预览video', '/manage/member.wechatKeyword/previewVideo', '/manage/member.wechatKeyword/previewVideo', 'manage_member_wechatKeyword_previewVideo', '', '_self', '', '0', '', '', '1', '1603158266', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1565', '1250', 'manage', 'system', '预览语音', '/manage/member.wechatKeyword/previewVoice', '/manage/member.wechatKeyword/previewVoice', 'manage_member_wechatKeyword_previewVoice', '', '_self', '', '0', '', '', '1', '1603159167', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1566', '1500', 'manage', 'system', '添加图文', '/manage/member.wechatKeyword/addMaterial', '/manage/member.wechatKeyword/addMaterial', 'manage_member_wechatKeyword_addMaterial', '', '_self', '', '0', '', '', '1', '1603170723', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1567', '1250', 'manage', 'system', '状态修改', '/manage/member.wechatKeyword/modify', '/manage/member.wechatKeyword/modify', 'manage_member_wechatKeyword_modify', '', '_self', '', '0', '', '', '1', '1603174720', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1583', '834', 'manage', 'system', '关注回复配置', '/manage/member.WechatKeyword/subscribe', '/manage/member.WechatKeyword/subscribe', 'manage_member_WechatKeyword_subscribe', '', '_self', '', '0', 'fa fa-wechat', '', '1', '1603194063', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1584', '1443', 'manage', 'system', '站内信列表', '/manage/member/message/list', '/manage/member.Index/messageList', 'manage_member_message_list', '', '_self', '', '0', '', '', '1', '1603273452', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1585', '1443', 'manage', 'system', '站内信已读', '/manage/member/message/read', '/manage/member.Index/messageRead', 'manage_member_message_read', '', '_self', '', '0', '', '', '1', '1603273452', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1586', '186', 'manage', 'system', '公告管理', '/manage/sysArticle', '/manage/sysArticle', 'manage_sysArticle', '', '_self', '', '1', 'fa fa-bell-o', '', '1', '1603345080', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1587', '1124', 'manage', 'system', '发送站内信', '/manage/admin/message/send', '/manage/admin.member/sendMessage', 'manage_admin_message_send', '', '_self', '', '0', '', '', '1', '1603352194', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1588', '1412', 'manage', 'system', '发送站内信', '/manage/member/message/send', '/manage/member.User/sendMessage', 'manage_member_message_send', '', '_self', '', '0', '', '', '1', '1603352194', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1589', '1172', 'manage', 'system', '已购应用', '/manage/member/miniapp/my/index', '/manage/member.MemberMiniapp/index', 'manage_member_miniapp_my_index', '', '_self', '', '1', 'fa fa-gift', '', '1', '1591779441', '0', '999', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1590', '1500', 'manage', 'system', '编辑图文', '/manage/member.wechatKeyword/editMaterial', '/manage/member.wechatKeyword/editMaterial', 'manage_member_wechatKeyword_editMaterial', '', '_self', '', '0', '', '', '1', '1603949972', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1604', '1396', 'manage', 'system', '删除', '/manage/member.GroupData/del', '/manage/member.GroupData/softdleting', 'manage_member_GroupData_del', '', '_self', '', '0', '', '', '1', '1605072083', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1840', '1314', 'manage', 'system', '插件市场', '/manage/member/plugin', '/manage/member.plugin.Plugin/index', '', '', '_self', '', '1', 'fa fa-gift', '', '1', '1591779441', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1841', '1840', 'manage', 'system', '详情', '/manage/member/plugin/detail', '/manage/member.plugin.Plugin/detail', '', '', '_self', '', '0', 'fa fa-gift', '', '1', '1591779441', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('1842', '1840', 'manage', 'system', '购买', '/manage/member/plugin/buy', '/manage/member.plugin.Plugin/buy', '', '', '_self', '', '0', 'fa fa-gift', '', '1', '1591779441', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('2101', '1589', 'manage', 'system', '设置默认应用', '/manage/member/miniapp/my/default', '/manage/member.MemberMiniapp/setDefaultMiniapp', '_manage_member_member_miniapp_default', '', '_self', '', '0', 'fa fa-gift', '', '1', '1591779441', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('2170', '190', 'manage', 'system', '城市数据', '/manage/admin/sys/city/index', '/manage/admin.sys.City/index', 'manage_admin_sys_city_index', '', '_self', '', '1', 'fa fa-subway', '', '1', '1600936300', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('2171', '2170', 'manage', 'system', '导出', '/manage/admin/sys/city/export', '/manage/admin.sys.City/export', 'manage_admin_sys_city_export', '', '_self', '', '0', '', '', '1', '1600936381', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('2172', '2170', 'manage', 'system', '添加', '/manage/admin/sys/city/add', '/manage/admin.sys.City/add', 'manage_admin_sys_city_add', '', '_self', '', '0', '', '', '1', '1600936473', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('2173', '2170', 'manage', 'system', '编辑', '/manage/admin/sys/city/edit', '/manage/admin.sys.City/edit', 'manage_admin_sys_city_edit', '', '_self', '', '0', '', '', '1', '1600936641', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('2174', '2170', 'manage', 'system', '修改', '/manage/admin/sys/city/modify', '/manage/admin.sys.City/modify', 'manage_admin_sys_city_modify', '', '_self', '', '0', '', '', '1', '1600936688', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('2175', '2170', 'manage', 'system', '删除', '/manage/admin/sys/city/del', '/manage/admin.sys.City/del', 'manage_admin_sys_city_del', '', '_self', '', '0', '', '', '1', '1600936742', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('2176', '2170', 'manage', 'system', '下载城市数据sql', '/manage/admin/sys/city/download', '/manage/admin.sys.City/download', 'manage_admin_sys_city_download', '', '_self', '', '0', '', '', '1', '1600936742', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('2375', '834', 'manage', 'system', '小程序订阅消息', '/manage/member/wechat/message/template/index', '/manage/member.WechatMessageTemplate/index', 'manage_member_wechat_message_template_index', '', '_self', '', '0', 'fa fa-server', '', '1', '1613642074', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('2376', '2375', 'manage', 'system', '导出', '/manage/member/wechat/message/template/export', '/manage/member.WechatMessageTemplate/export', 'manage_member_wechat_message_template_export', '', '_self', '', '0', 'fa fa-server', '', '1', '1613642074', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('2377', '2375', 'manage', 'system', '新增', '/manage/member/wechat/message/template/add', '/manage/member.WechatMessageTemplate/add', 'manage_member_wechat_message_template_add', '', '_self', '', '0', 'fa fa-server', '', '1', '1613642074', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('2378', '2375', 'manage', 'system', '编辑', '/manage/member/wechat/message/template/edit', '/manage/member.WechatMessageTemplate/edit', 'manage_member_wechat_message_template_edit', '', '_self', '', '0', 'fa fa-server', '', '1', '1613642074', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('2379', '2375', 'manage', 'system', '修改', '/manage/member/wechat/message/template/modify', '/manage/member.WechatMessageTemplate/modify', 'manage_member_wechat_message_template_modify', '', '_self', '', '0', 'fa fa-server', '', '1', '1613642074', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('2380', '2375', 'manage', 'system', '删除', '/manage/member/wechat/message/template/del', '/manage/member.WechatMessageTemplate/del', 'manage_member_wechat_message_template_del', '', '_self', '', '0', 'fa fa-server', '', '1', '1613642074', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('2427', '1020', 'manage', 'system', '模板管理', '/manage/admin/template', '/manage/admin.MiniappTemplate/index', 'manage_admin_template', '', '_self', '', '0', '', '', '1', '1617265833', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('2428', '2427', 'manage', 'system', '新增', '/manage/admin/template/add', '/manage/admin.MiniappTemplate/add', 'manage_admin_template_add', '', '_self', '', '0', '', '', '1', '1617265956', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('2429', '2427', 'manage', 'system', '编辑', '/manage/admin/template/edit', '/manage/admin.MiniappTemplate/edit', 'manage_admin_template_edit', '', '_self', '', '0', '', '', '1', '1617265997', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('2430', '2427', 'manage', 'system', '删除', '/manage/admin/template/del', '/manage/admin.MiniappTemplate/del', 'manage_admin_template_del', '', '_self', '', '0', '', '', '1', '1617266038', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('2431', '2427', 'manage', 'system', '租户应用列表', '/manage/admin/getMemberMiniappList', '/manage/admin.MiniappTemplate/getMemberMiniappList', 'manage_admin_getMemberMiniappList', '', '_self', '', '0', '', '', '1', '1617266129', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('2432', '2427', 'manage', 'system', '版本库列表', '/manage/admin/getThirdTemplate', '/manage/admin.MiniappTemplate/getThirdTemplate', 'manage_admin_getThirdTemplate', '', '_self', '', '0', '', '', '1', '1617266230', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('2433', '1020', 'manage', 'system', '售卖套餐', '/manage/admin/miniapp/module/index', '/manage/admin.miniappModule/index', 'manage_admin_miniapp_module_index', '', '_self', '', '0', '', '', '1', '1617267142', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('2434', '2433', 'manage', 'system', '导出', '/manage/admin/miniapp/module/export', '/manage/admin.miniappModule/export', 'manage_admin_miniapp_module_export', '', '_self', '', '0', '', '', '1', '1617267226', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('2435', '2433', 'manage', 'system', '新增', '/manage/admin/miniapp/module/add', '/manage/admin.miniappModule/add', 'manage_admin_miniapp_module_add', '', '_self', '', '0', '', '', '1', '1617267332', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('2436', '2433', 'manage', 'system', '编辑', '/manage/admin/miniapp/module/edit', '/manage/admin.miniappModule/edit', 'manage_admin_miniapp_module_edit', '', '_self', '', '0', '', '', '1', '1617267361', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('2437', '2433', 'manage', 'system', '修改', '/manage/admin/miniapp/module/modify', '/manage/admin.miniappModule/modify', 'manage_admin_miniapp_module_modify', '', '_self', '', '0', '', '', '1', '1617267389', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('2438', '2433', 'manage', 'system', '删除', '/manage/admin/miniapp/module/del', '/manage/admin.miniappModule/del', 'manage_admin_miniapp_module_del', '', '_self', '', '0', '', '', '1', '1617267476', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` VALUES ('2439', '2433', 'manage', 'system', '套餐组', '/manage/admin/miniapp/module/group', '/manage/admin.miniappModule/group', 'manage_admin_miniapp_module_group', '', '_self', '', '0', '', '', '1', '1617267546', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` (`id`, `pid`, `app_name`, `type`, `title`, `menu_path`, `name`, `auth_name`, `param`, `target`, `condition`, `ismenu`, `icon`, `remark`, `status`, `create_time`, `update_time`, `sort`, `scopes`) VALUES ('2446', '1586', 'manage', 'system', '资讯分类', '/manage/admin/sys/notice/category/index', '/manage/admin.sys.NoticeCategory/index', 'manage_admin_sys_notice_category_index', '', '_self', '', '1', '', '', '1', '1617791703', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` (`id`, `pid`, `app_name`, `type`, `title`, `menu_path`, `name`, `auth_name`, `param`, `target`, `condition`, `ismenu`, `icon`, `remark`, `status`, `create_time`, `update_time`, `sort`, `scopes`) VALUES ('2447', '2446', 'manage', 'system', '添加', '/manage/admin/sys/notice/category/add', '/manage/admin.sys.NoticeCategory/add', 'manage_admin_sys_notice_category_add', '', '_self', '', '0', '', '', '1', '1617862625', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` (`id`, `pid`, `app_name`, `type`, `title`, `menu_path`, `name`, `auth_name`, `param`, `target`, `condition`, `ismenu`, `icon`, `remark`, `status`, `create_time`, `update_time`, `sort`, `scopes`) VALUES ('2448', '2446', 'manage', 'system', '编辑', '/manage/admin/sys/notice/category/edit', '/manage/admin.sys.NoticeCategory/edit', 'manage_admin_sys_notice_category_edit', '', '_self', '', '0', '', '', '1', '1617862654', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` (`id`, `pid`, `app_name`, `type`, `title`, `menu_path`, `name`, `auth_name`, `param`, `target`, `condition`, `ismenu`, `icon`, `remark`, `status`, `create_time`, `update_time`, `sort`, `scopes`) VALUES ('2449', '2446', 'manage', 'system', '删除', '/manage/admin/sys/notice/category/del', '/manage/admin.sys.NoticeCategory/del', 'manage_admin_sys_notice_category_del', '', '_self', '', '0', '', '', '1', '1617862686', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` (`id`, `pid`, `app_name`, `type`, `title`, `menu_path`, `name`, `auth_name`, `param`, `target`, `condition`, `ismenu`, `icon`, `remark`, `status`, `create_time`, `update_time`, `sort`, `scopes`) VALUES ('2450', '2446', 'manage', 'system', '修改', '/manage/admin/sys/notice/category/modify', '/manage/admin.sys.NoticeCategory/modify', 'manage_admin_sys_notice_category_modify', '', '_self', '', '0', '', '', '1', '1617862718', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` (`id`, `pid`, `app_name`, `type`, `title`, `menu_path`, `name`, `auth_name`, `param`, `target`, `condition`, `ismenu`, `icon`, `remark`, `status`, `create_time`, `update_time`, `sort`, `scopes`) VALUES ('2451', '2446', 'manage', 'system', '导出', '/manage/admin/sys/notice/category/export', '/manage/admin.sys.NoticeCategory/export', 'manage_admin_sys_notice_category_export', '', '_self', '', '0', '', '', '1', '1617862768', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` (`id`, `pid`, `app_name`, `type`, `title`, `menu_path`, `name`, `auth_name`, `param`, `target`, `condition`, `ismenu`, `icon`, `remark`, `status`, `create_time`, `update_time`, `sort`, `scopes`) VALUES ('2454', '1020', 'manage', 'system', '检查更新', '/manage/admin/miniapp/update', '/manage/admin.Miniapp/update', 'manage_admin_miniapp_update', '', '_self', '', '0', '', '', '1', '1618392234', '0', '0', 'admin');


-- ----------------------------
-- Table structure for bw_cloud_order
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__cloud_order`;
CREATE TABLE `__BWPREFIX__cloud_order` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '序号',
  `member_id` int(10) unsigned NOT NULL COMMENT '租户ID',
  `service_id` int(10) unsigned NOT NULL COMMENT '服务ID',
  `packet_id` int(10) unsigned NOT NULL COMMENT '套餐ID',
  `amount` int(10) unsigned NOT NULL COMMENT '次数',
  `price` decimal(10,2) unsigned NOT NULL COMMENT '价格',
  `days` int(10) unsigned NOT NULL COMMENT '有效天数',
  `create_time` int(10) unsigned NOT NULL COMMENT '创建时间',
  `update_time` int(10) unsigned NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `member_id` (`member_id`),
  KEY `service_id` (`service_id`),
  KEY `packet_id` (`packet_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT='云市场订单表';

-- ----------------------------
-- Records of bw_cloud_order
-- ----------------------------

-- ----------------------------
-- Table structure for bw_cloud_packet
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__cloud_packet`;
CREATE TABLE `__BWPREFIX__cloud_packet` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '序号',
  `service_id` int(10) unsigned NOT NULL COMMENT '服务ID',
  `amount` int(10) unsigned NOT NULL COMMENT '次数',
  `price` decimal(12,2) unsigned NOT NULL COMMENT '价格',
  `days` int(10) unsigned NOT NULL COMMENT '有效天数',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `delete_time` int(10) DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`),
  KEY `service_id` (`service_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT='云市场套餐表';

-- ----------------------------
-- Records of bw_cloud_packet
-- ----------------------------
INSERT INTO `__BWPREFIX__cloud_packet` VALUES ('1', '3', '100', '10.00', '365', '1594969942', '1594969942', '0');
INSERT INTO `__BWPREFIX__cloud_packet` VALUES ('2', '2', '1', '0.10', '365', '1595058043', '1595058043', '0');
INSERT INTO `__BWPREFIX__cloud_packet` VALUES ('3', '2', '100', '10.00', '365', '1595058069', '1595058069', '0');
INSERT INTO `__BWPREFIX__cloud_packet` VALUES ('5', '4', '10', '1.00', '365', '1595208506', '1595208506', '0');
INSERT INTO `__BWPREFIX__cloud_packet` VALUES ('6', '4', '1', '0.10', '365', '1595209849', '1595209849', '0');

-- ----------------------------
-- Table structure for bw_cloud_service
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__cloud_service`;
CREATE TABLE `__BWPREFIX__cloud_service` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `name` varchar(50) NOT NULL COMMENT '名称',
  `file_name` varchar(50) NOT NULL COMMENT '文件名',
  `params` varchar(255) NOT NULL COMMENT '参数',
  `create_time` int(10) NOT NULL,
  `update_time` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `file_name` (`file_name`),
  KEY `id` (`id`,`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT='云市场服务表';

-- ----------------------------
-- Records of bw_cloud_service
-- ----------------------------
INSERT INTO `__BWPREFIX__cloud_service` VALUES ('1', '短信', 'Sms', 'mobile,code', '1594881243', '1594881243');
INSERT INTO `__BWPREFIX__cloud_service` VALUES ('2', 'IP归属地查询', 'Ip', 'ip', '1594881243', '1594881243');
INSERT INTO `__BWPREFIX__cloud_service` VALUES ('3', '身份证实名认证', 'Idcard', 'idcard,realname', '1594881243', '1594881243');
INSERT INTO `__BWPREFIX__cloud_service` VALUES ('4', '全国快递物流查询', 'Kdi', 'no', '1595037496', '1595037496');

-- ----------------------------
-- Table structure for bw_cloud_wallet
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__cloud_wallet`;
CREATE TABLE `__BWPREFIX__cloud_wallet` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '序号',
  `member_id` int(10) unsigned NOT NULL COMMENT '租户ID',
  `service_id` int(10) unsigned NOT NULL COMMENT '服务ID',
  `amount` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '剩余次数',
  `total` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '总次数',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `member_id` (`member_id`,`service_id`) USING BTREE,
  KEY `service_id` (`service_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT='云市场钱包表';

-- ----------------------------
-- Records of bw_cloud_wallet
-- ----------------------------

-- ----------------------------
-- Table structure for bw_cloud_wallet_bill
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__cloud_wallet_bill`;
CREATE TABLE `__BWPREFIX__cloud_wallet_bill` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '序号',
  `member_id` int(10) unsigned NOT NULL COMMENT '租户ID',
  `service_id` int(10) unsigned NOT NULL COMMENT '服务ID',
  `type` enum('buy','use') DEFAULT 'buy' COMMENT '类型:buy=购买套餐,use=使用服务',
  `value` int(10) NOT NULL COMMENT '变更数量',
  `before` int(10) unsigned NOT NULL COMMENT '变更前',
  `after` int(10) unsigned NOT NULL COMMENT '变更后',
  `memo` varchar(255) DEFAULT '' COMMENT '备注',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `member_id` (`member_id`),
  KEY `service_id` (`service_id`),
  KEY `type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT='云市场钱包记录表';

-- ----------------------------
-- Records of bw_cloud_wallet_bill
-- ----------------------------

-- ----------------------------
-- Table structure for bw_config_apis
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__config_apis`;
CREATE TABLE `__BWPREFIX__config_apis` (
  `id` tinyint(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `apikey` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT='阿里API市场接口配置';

-- ----------------------------
-- Records of bw_config_apis
-- ----------------------------
INSERT INTO `__BWPREFIX__config_apis` VALUES ('1', 'wechatopen', '{\"app_id\":\"111111111111111111\",\"secret\":\"11111111111111111111111111111111\",\"token\":\"xxxxxxxxxxxxxxxxxxxxxxxxxxx\",\"aes_key\":\"xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\"}');

-- ----------------------------
-- Table structure for bw_crontab_crontab
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__crontab_crontab`;
CREATE TABLE `__BWPREFIX__crontab_crontab` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `type` enum('url','sql','shell') NOT NULL DEFAULT 'url' COMMENT '类型{radio}(url:请求url,sql:执行SQL,shell:执行Shell)',
  `title` varchar(100) NOT NULL DEFAULT '' COMMENT '标题',
  `content` text NOT NULL COMMENT '事件内容',
  `schedule` varchar(20) NOT NULL DEFAULT '' COMMENT '执行周期',
  `maximums` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最大执行次数(0为不限)',
  `executes` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '已执行次数',
  `execute_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最后执行时间',
  `begin_time` int(10) NOT NULL DEFAULT '0' COMMENT '开始时间',
  `end_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '结束时间',
  `weigh` int(10) NOT NULL DEFAULT '0' COMMENT '权重',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '状态{radio}(0:禁用,1:正常,2:已完成,3:已过期)',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `delete_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '删除时间',
  PRIMARY KEY (`id`),
  KEY `status` (`status`,`begin_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT='定时任务表';

-- ----------------------------
-- Records of bw_crontab_crontab
-- ----------------------------

-- ----------------------------
-- Table structure for bw_crontab_crontab_log
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__crontab_crontab_log`;
CREATE TABLE `__BWPREFIX__crontab_crontab_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `crontab_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '定时任务ID',
  `content` text NOT NULL COMMENT '执行结果',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '状态{radio}(0:失败,1:成功)',
  `begin_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '开始时间',
  `end_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '结束时间',
  PRIMARY KEY (`id`),
  KEY `crontab_id` (`crontab_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT='定时任务日志表';

-- ----------------------------
-- Records of bw_crontab_crontab_log
-- ----------------------------

-- ----------------------------
-- Table structure for bw_crud_demo
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__crud_demo`;
CREATE TABLE `__BWPREFIX__crud_demo` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(20) NOT NULL COMMENT '名称',
  `image` varchar(255) NOT NULL COMMENT '单图',
  `images` varchar(500) DEFAULT NULL COMMENT '多图',
  `sales` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '销量',
  `desc` text COMMENT '描述{editor}',
  `switch` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '开关{switch}(0:关闭,1:打开)',
  `pay_time` int(10) unsigned DEFAULT NULL COMMENT '支付时间{date}(range)',
  `status` enum('0','1') DEFAULT '1' COMMENT '状态{switch}(0:下架,1:上架)',
  `create_time` int(10) NOT NULL COMMENT '创建时间{date}(range)',
  `update_time` int(10) DEFAULT '0' COMMENT '更新时间{date}(range)',
  `delete_time` int(10) DEFAULT '0' COMMENT '删除时间{date}(range)',
  `send` date DEFAULT NULL COMMENT '发放时间{date}(date)',
  `fruit` set('apple','orange','banana') DEFAULT 'orange,banana' COMMENT '水果{checkbox}(apple:苹果,orange:橘子,banana:香蕉)',
  `interest` set('basket','run','swim') NOT NULL DEFAULT '' COMMENT '爱好{checkbox}(swim:游泳,run:跑步,basket:篮球)',
  `cate_id` int(11) unsigned DEFAULT NULL COMMENT '分类{select}(1:服装,2:家电)',
  `h_video` varchar(255) DEFAULT NULL COMMENT '单视频{file}(mp4)',
  `s_videos` varchar(255) DEFAULT NULL COMMENT '多视频{files}(mp4)',
  `crt_file` varchar(255) DEFAULT '' COMMENT '证书文件{file}',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT='crud测试表';

-- ----------------------------
-- Records of bw_crud_demo
-- ----------------------------

-- ----------------------------
-- Table structure for bw_crud_demo_cate
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__crud_demo_cate`;
CREATE TABLE `__BWPREFIX__crud_demo_cate` (
  `id` int(11) unsigned NOT NULL COMMENT '序号',
  `name` varchar(255) NOT NULL COMMENT '名称',
  `create_time` int(10) unsigned NOT NULL COMMENT '创建时间',
  `update_time` int(10) unsigned NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci;

-- ----------------------------
-- Records of bw_crud_demo_cate
-- ----------------------------

-- ----------------------------
-- Table structure for bw_crud_demo_catee
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__crud_demo_catee`;
CREATE TABLE `__BWPREFIX__crud_demo_catee` (
  `id` int(11) unsigned NOT NULL COMMENT '序号',
  `name` varchar(255) NOT NULL COMMENT '分类名称',
  `create_time` int(10) unsigned NOT NULL COMMENT '创建时间',
  `update_time` int(10) unsigned NOT NULL COMMENT '更新时间',
  `headimage` varchar(255) DEFAULT NULL COMMENT '头像',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci;

-- ----------------------------
-- Records of bw_crud_demo_catee
-- ----------------------------

-- ----------------------------
-- Table structure for bw_member
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__member`;
CREATE TABLE `__BWPREFIX__member` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `username` varchar(50) NOT NULL COMMENT '用户名',
  `nickname` varchar(50) DEFAULT '' COMMENT '昵称',
  `mobile` varchar(15) DEFAULT '' COMMENT '手机号',
  `avatar` varchar(255) DEFAULT '' COMMENT '头像',
  `password` varchar(255) NOT NULL COMMENT '密码',
  `safe_password` varchar(255) NOT NULL DEFAULT '' COMMENT '安全密码',
  `parent_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '上级id',
  `top_id` bigint(20) DEFAULT '0' COMMENT '顶级租户id',
  `bind_member_miniapp_id` int(11) DEFAULT '0' COMMENT '应用id',
  `ticket` varchar(255) DEFAULT NULL,
  `openid` varchar(250) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态:1=正常,0=冻结',
  `lock_config` enum('0','1') DEFAULT '0' COMMENT '配置权限:1=锁定,0=未锁定',
  `login_ip` varchar(20) DEFAULT NULL COMMENT '登录ip',
  `login_time` int(11) DEFAULT NULL COMMENT '登录时间',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
  `default_miniapp_dir` varchar(20) NOT NULL DEFAULT 'manage' COMMENT '后台菜单默认应用目录',
  PRIMARY KEY (`id`),
  UNIQUE KEY `mobile` (`mobile`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT='租户成员';

-- ----------------------------
-- Records of bw_member
-- ----------------------------
INSERT INTO `__BWPREFIX__member` VALUES ('64', 'bw17777777777', 'bw17777777777', '17777777777', '', '$2y$10$5ijQtNxDnfJgalIkDuDS5uFcP5LIQh7OqFq..MQkSCJD6uwKQIN16', '$2y$10$W/m69OAvSn9AUoSuJNq0F.LEvQB3MR/kUI9m8juL30rb58IbPFUEm', '0', '64', '0', null, null, '1', '0', '127.0.0.1', '1608712976', '1608712951', null, 'manage');

-- ----------------------------
-- Table structure for bw_member_form
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__member_form`;
CREATE TABLE `__BWPREFIX__member_form` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `form_id` varchar(100) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `is_del` tinyint(2) DEFAULT '0',
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT='小程序模板消息ID';

-- ----------------------------
-- Records of bw_member_form
-- ----------------------------

-- ----------------------------
-- Table structure for bw_member_miniapp
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__member_miniapp`;
CREATE TABLE `__BWPREFIX__member_miniapp` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `service_id` varchar(200) DEFAULT '' COMMENT '客户端ID',
  `miniapp_order_id` int(11) DEFAULT NULL COMMENT '订单ID',
  `member_id` int(11) DEFAULT '0' COMMENT '创建人/购买人',
  `miniapp_id` int(11) DEFAULT '0' COMMENT '应用id',
  `appname` varchar(100) DEFAULT '' COMMENT '应用名称',
  `template_id` int(5) NOT NULL DEFAULT '0' COMMENT '购买时小程序模板id',
  `version` varchar(50) NOT NULL DEFAULT '' COMMENT '购买应用时版本号',
  `head_img` varchar(255) DEFAULT NULL COMMENT 'Logo{image}',
  `qrcode_url` varchar(255) DEFAULT NULL COMMENT '演示二维码',
  `miniapp_appid` char(50) DEFAULT NULL COMMENT '小程序appid',
  `miniapp_open_auth` enum('0','1') DEFAULT '0' COMMENT '小程序开放平台是否授权',
  `miniapp_secret` varchar(255) DEFAULT NULL COMMENT '小程序secret',
  `miniapp_head_img` varchar(255) DEFAULT NULL COMMENT '小程序logo{image}',
  `miniapp_qrcode_url` varchar(255) DEFAULT NULL COMMENT '小程序演示二维码',
  `navbar_color` varchar(50) DEFAULT NULL COMMENT '导航栏颜色',
  `navbar_style` varchar(50) DEFAULT NULL COMMENT '导航栏样式',
  `service_time` int(11) DEFAULT '0' COMMENT '服务时间',
  `mp_appid` varchar(30) DEFAULT NULL COMMENT '公众号appid',
  `mp_secret` varchar(50) DEFAULT NULL COMMENT '公众号secret',
  `mp_head_img` varchar(255) DEFAULT NULL COMMENT '公众号logo{image}',
  `mp_qrcode_url` varchar(255) DEFAULT NULL COMMENT '公众号演示二维码',
  `mp_open_auth` enum('1','0') DEFAULT '0' COMMENT '公众号开放平台是否授权',
  `mp_token` varchar(50) DEFAULT NULL COMMENT '公众号token',
  `mp_aes_key` varchar(50) DEFAULT NULL COMMENT '公众号AesKey',
  `is_psp` enum('0','1') DEFAULT '0' COMMENT '微信服务商:0=独立应用,1=微信服务商',
  `psp_appid` varchar(32) DEFAULT NULL COMMENT '服务商appid',
  `is_open` enum('0','1') DEFAULT '0' COMMENT '开放平台授权情况，0：未授权1：已授权',
  `is_lock` enum('0','1') DEFAULT '0' COMMENT '状态:0=正常,1=锁定',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
  `expire_time` int(10) DEFAULT '0' COMMENT '过期时间',
  `mp_nick_name` varchar(255) DEFAULT NULL COMMENT '公众号名称',
  `mp_principal_name` varchar(255) DEFAULT NULL COMMENT '公众号主体',
  `miniapp_nick_name` varchar(255) DEFAULT NULL COMMENT '小程序名称',
  `miniapp_principal_name` varchar(255) DEFAULT NULL COMMENT '小程序主体',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT='客户应用表';

-- ----------------------------
-- Records of bw_member_miniapp
-- ----------------------------

-- ----------------------------
-- Table structure for bw_member_miniapp_audit
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__member_miniapp_audit`;
CREATE TABLE `__BWPREFIX__member_miniapp_audit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) DEFAULT '0',
  `member_miniapp_id` int(11) DEFAULT '0',
  `state` tinyint(1) DEFAULT '0',
  `is_commit` tinyint(1) DEFAULT '0' COMMENT '1基础信息设置2上传代码3提交审核4发布小程序',
  `template_id` int(10) DEFAULT '0' COMMENT '提交审核的模板ID',
  `version` varchar(50) DEFAULT '' COMMENT '提交审核的版本',
  `trial_qrcode` varchar(255) DEFAULT NULL COMMENT '二维码',
  `auditid` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT='小程序提审表';

-- ----------------------------
-- Records of bw_member_miniapp_audit
-- ----------------------------

-- ----------------------------
-- Table structure for bw_member_miniapp_order
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__member_miniapp_order`;
CREATE TABLE `__BWPREFIX__member_miniapp_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `member_id` int(11) NOT NULL COMMENT '租户ID',
  `miniapp_id` int(11) NOT NULL COMMENT '应用ID',
  `miniapp_module_id` int(10) NOT NULL DEFAULT '0' COMMENT '功能id',
  `title` varchar(20) NOT NULL COMMENT '应用名字',
  `update_var` int(11) DEFAULT '0' COMMENT '小程序模板ID',
  `price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '价格',
  `valid_days` int(10) NOT NULL DEFAULT '0' COMMENT '有效天数',
  `expire_time` int(10) NOT NULL COMMENT '到期时间',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态:0=关闭,1=正常',
  `create_time` int(10) NOT NULL COMMENT '购买时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `miniapp_module_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '功能类型:1=基础功能,2=附加功能',
  PRIMARY KEY (`id`),
  KEY `member_id` (`member_id`),
  KEY `miniapp_id` (`miniapp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT='应用购买订单表';

-- ----------------------------
-- Records of bw_member_miniapp_order
-- ----------------------------

-- ----------------------------
-- Table structure for bw_member_miniapp_template
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__member_miniapp_template`;
CREATE TABLE `__BWPREFIX__member_miniapp_template` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `miniapp_id` int(11) NOT NULL DEFAULT '0' COMMENT '应用id',
  `member_id` bigint(20) DEFAULT '0' COMMENT '租户id',
  `member_miniapp_id` int(11) NOT NULL DEFAULT '0' COMMENT '租户应用id',
  `version` varchar(50) NOT NULL DEFAULT '' COMMENT '版本号',
  `template_id` int(11) NOT NULL DEFAULT '0' COMMENT '模板id',
  `images` varchar(255) DEFAULT '' COMMENT '模板图',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间{date}(range)',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间{date}(range)',
  `desc` varchar(255) DEFAULT NULL COMMENT '描述{text}',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT='租户成员';

-- ----------------------------
-- Records of bw_member_miniapp_template
-- ----------------------------

-- ----------------------------
-- Table structure for bw_member_official_menu
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__member_official_menu`;
CREATE TABLE `__BWPREFIX__member_official_menu` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `member_miniapp_id` int(11) DEFAULT '0' COMMENT '用户应用id',
  `appid` varchar(255) CHARACTER SET utf8mb4 NOT NULL DEFAULT '' COMMENT '小程序APPID',
  `types` varchar(20) NOT NULL DEFAULT 'view' COMMENT '链接类型:view=网页类型,miniprogram=小程序类型,click=点击类型',
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT '父菜单id',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '菜单名称',
  `sort` int(10) DEFAULT '0' COMMENT '排序',
  `url` varchar(255) DEFAULT '' COMMENT '链接',
  `key` varchar(255) DEFAULT '' COMMENT '回复规则id',
  `pagepath` varchar(255) DEFAULT '' COMMENT '小程序地址',
  `update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  `create_time` int(11) DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT='公众号菜单关联';

-- ----------------------------
-- Records of bw_member_official_menu
-- ----------------------------

-- ----------------------------
-- Table structure for bw_member_payment
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__member_payment`;
CREATE TABLE `__BWPREFIX__member_payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `apiname` char(20) DEFAULT NULL,
  `member_id` int(11) DEFAULT NULL,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `config` text,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci;

-- ----------------------------
-- Records of bw_member_payment
-- ----------------------------

-- ----------------------------
-- Table structure for bw_member_plugin_order
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__member_plugin_order`;
CREATE TABLE `__BWPREFIX__member_plugin_order` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `member_id` int(10) unsigned NOT NULL COMMENT '租户ID',
  `plugin_id` int(10) unsigned NOT NULL COMMENT '插件ID',
  `price` decimal(12,2) unsigned NOT NULL COMMENT '价格',
  `create_time` int(10) unsigned NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT='插件订单表';

-- ----------------------------
-- Records of bw_member_plugin_order
-- ----------------------------

-- ----------------------------
-- Table structure for bw_member_wallet
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__member_wallet`;
CREATE TABLE `__BWPREFIX__member_wallet` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `member_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '租户ID',
  `money` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '帐号余额',
  `freeze_money` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '冻结金额',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  `update_time` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `member_id` (`member_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT='租户资金表';

-- ----------------------------
-- Records of bw_member_wallet
-- ----------------------------
INSERT INTO `__BWPREFIX__member_wallet` VALUES ('38', '64', '0.00', '0.00', '1608712951', '1608712951');

-- ----------------------------
-- Table structure for bw_member_wallet_bill
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__member_wallet_bill`;
CREATE TABLE `__BWPREFIX__member_wallet_bill` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `member_id` int(11) DEFAULT '0' COMMENT '租户ID',
  `money_type` enum('money','freeze_money') NOT NULL DEFAULT 'money' COMMENT '资金类型',
  `type` varchar(20) NOT NULL DEFAULT '' COMMENT '类型:admin=系统,recharge=充值.miniapp=购买应用,cloud_packet=购买云市场套餐',
  `value` decimal(10,2) DEFAULT '0.00' COMMENT '变更',
  `before` decimal(10,2) DEFAULT '0.00' COMMENT '变更前',
  `after` decimal(10,2) DEFAULT '0.00' COMMENT '变更后',
  `memo` varchar(255) DEFAULT '' COMMENT '备注',
  `create_time` int(10) DEFAULT NULL COMMENT '创建时间',
  `update_time` int(10) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `member_id` (`member_id`),
  KEY `type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT='租户资金变动表';

-- ----------------------------
-- Records of bw_member_wallet_bill
-- ----------------------------

-- ----------------------------
-- Table structure for bw_member_wallet_recharge
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__member_wallet_recharge`;
CREATE TABLE `__BWPREFIX__member_wallet_recharge` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) DEFAULT NULL,
  `order_sn` varchar(255) DEFAULT NULL,
  `money` decimal(10,2) DEFAULT NULL,
  `state` tinyint(1) unsigned DEFAULT '0',
  `remarks` varchar(255) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  `transaction_id` varchar(255) DEFAULT NULL,
  `type` int(2) DEFAULT '0' COMMENT '0为微信充值 1为支付宝',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci ;

-- ----------------------------
-- Records of bw_member_wallet_recharge
-- ----------------------------

-- ----------------------------
-- Table structure for bw_member_wechat_tpl
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__member_wechat_tpl`;
CREATE TABLE `__BWPREFIX__member_wechat_tpl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `tplmsg_common_app` varchar(255) DEFAULT NULL,
  `tplmsg_common_wechat` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of bw_member_wechat_tpl
-- ----------------------------

-- ----------------------------
-- Table structure for bw_message
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__message`;
CREATE TABLE `__BWPREFIX__message` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '类型{radio}(1:平台->租户,2:平台->用户,3:租户->用户,4:租户->用户系统通知)',
  `sender_id` int(10) unsigned NOT NULL COMMENT '发送者ID',
  `receiver_id` int(10) unsigned NOT NULL COMMENT '接收者ID',
  `msg` varchar(255) NOT NULL COMMENT '内容',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态{radio}(0:未读,1:已读)',
  `create_time` int(10) unsigned NOT NULL COMMENT '创建时间',
  `update_time` int(10) unsigned NOT NULL COMMENT '更新时间',
  `delete_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '删除时间',
  PRIMARY KEY (`id`),
  KEY `type` (`type`,`sender_id`,`receiver_id`,`status`,`delete_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT='站内信';

-- ----------------------------
-- Records of bw_message
-- ----------------------------

-- ----------------------------
-- Table structure for bw_miniapp
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__miniapp`;
CREATE TABLE `__BWPREFIX__miniapp` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `dir` varchar(50) NOT NULL COMMENT '目录',
  `title` varchar(50) NOT NULL COMMENT '名称',
  `describe` varchar(255) NOT NULL COMMENT '描述内容',
  `logo_image` varchar(255) NOT NULL COMMENT 'Logo',
  `qrcode_image` varchar(255) NOT NULL COMMENT '演示二维码',
  `style_images` varchar(500) NOT NULL COMMENT '应用截图',
  `sell_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '销售价',
  `market_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '市场价',
  `version` varchar(50) NOT NULL COMMENT '版本',
  `template_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序代码库中的代码模板 ID',
  `expire_day` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '体验天数',
  `content` text COMMENT '详情介绍',
  `types` set('mini_program','app','h5','official','system') NOT NULL DEFAULT 'mini_program' COMMENT '应用类型:mini_program=小程序,app=APP,h5=h5,official=公众号,system=系统',
  `is_manage` enum('0','1') DEFAULT '0' COMMENT '平台管理:0=关闭,1=开启',
  `is_openapp` enum('0','1') DEFAULT '0' COMMENT '接入方式:0=独立应用,1=开放平台',
  `is_wechat_pay` enum('0','1') DEFAULT '0' COMMENT '微信支付:0=关闭,1=开启',
  `is_alipay_pay` enum('0','1') DEFAULT '0' COMMENT '支付宝支付:0=关闭,1=开启',
  `sort` int(11) unsigned DEFAULT '0' COMMENT '排序',
  `status` enum('-1','0','1') DEFAULT '0' COMMENT '状态:-1=未安装,0=关闭,1=开启',
  `create_time` int(11) unsigned NOT NULL COMMENT '安装时间',
  `update_time` int(11) unsigned NOT NULL COMMENT '更新时间',
  `is_diy` tinyint(1) unsigned DEFAULT '0' COMMENT '是否定制应用:0=否,1=是',
  `diy_member_ids` varchar(255) DEFAULT '' COMMENT '定制租户ID,使用,分割',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT='应用表';

-- ----------------------------
-- Records of bw_miniapp
-- ----------------------------

-- ----------------------------
-- Table structure for bw_miniapp_module
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__miniapp_module`;
CREATE TABLE `__BWPREFIX__miniapp_module` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `miniapp_id` int(11) NOT NULL DEFAULT '0' COMMENT '应用id',
  `group_id` int(11) NOT NULL COMMENT '组id',
  `name` varchar(100) NOT NULL COMMENT '功能名',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '类型{radio}(1:基础功能,2:附加功能)',
  `price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '价格',
  `valid_days` int(10) unsigned DEFAULT '365' COMMENT '有效天数',
  `desc` text COMMENT '描述',
  `create_time` int(11) DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `group_id` (`group_id`) USING BTREE,
  KEY `miniapp_id` (`miniapp_id`),
  KEY `type` (`type`),
  KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT='应用功能表';

-- ----------------------------
-- Records of bw_miniapp_module
-- ----------------------------

-- ----------------------------
-- Table structure for bw_miniapp_third_template
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__miniapp_third_template`;
CREATE TABLE `__BWPREFIX__miniapp_third_template` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_version` varchar(50) NOT NULL DEFAULT '' COMMENT '版本号',
  `template_id` int(11) NOT NULL DEFAULT '0' COMMENT '模板id',
  `miniapp_id` int(11) DEFAULT '0' COMMENT '系统应用',
  `create_time` int(11) NOT NULL COMMENT '创建时间{date}(range)',
  `source_miniprogram_appid` varchar(50) NOT NULL,
  `source_miniprogram` varchar(50) NOT NULL,
  `developer` varchar(50) NOT NULL,
  `user_desc` varchar(255) DEFAULT NULL COMMENT '描述{text}',
  `diy_desc` varchar(255) DEFAULT '' COMMENT '版本描述{text}',
  PRIMARY KEY (`id`),
  KEY `IX_template_id` (`template_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT='租户成员';

-- ----------------------------
-- Records of bw_miniapp_third_template
-- ----------------------------

-- ----------------------------
-- Table structure for bw_plugin
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__plugin`;
CREATE TABLE `__BWPREFIX__plugin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(40) NOT NULL COMMENT '标识',
  `title` varchar(20) NOT NULL DEFAULT '' COMMENT '名称',
  `description` text COMMENT '描述',
  `config` text COMMENT '配置',
  `author` varchar(40) DEFAULT '' COMMENT '作者',
  `version` varchar(20) DEFAULT '' COMMENT '版本号',
  `status` enum('1','0','-1') NOT NULL DEFAULT '-1' COMMENT '状态:-1=未安装,0=禁用,1=启用',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '安装时间',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `price` decimal(12,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '价格',
  `type` varchar(20) NOT NULL COMMENT '类型:admin_system平台系统插件,member_system租户系统插件,member_bwmall租户应用插件',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='插件表';

-- ----------------------------
-- Records of bw_plugin
-- ----------------------------

-- ----------------------------
-- Table structure for bw_sms
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__sms`;
CREATE TABLE `__BWPREFIX__sms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `event` varchar(30) NOT NULL DEFAULT '' COMMENT '事件',
  `mobile` varchar(20) NOT NULL DEFAULT '' COMMENT '手机号',
  `code` varchar(10) NOT NULL DEFAULT '' COMMENT '验证码',
  `times` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '验证次数',
  `ip` varchar(30) NOT NULL DEFAULT '' COMMENT 'IP',
  `create_time` int(10) unsigned DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT='短信验证码表';

-- ----------------------------
-- Records of bw_sms
-- ----------------------------

-- ----------------------------
-- Table structure for bw_sys_article
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__sys_article`;
CREATE TABLE `__BWPREFIX__sys_article` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `admin_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '管理员id',
  `category_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '分类id',
  `title` varchar(255) NOT NULL COMMENT '文章标题',
  `synopsis` varchar(255) DEFAULT NULL COMMENT '文章简介',
  `author` varchar(255) DEFAULT NULL COMMENT '文章作者',
  `image` varchar(255) NOT NULL COMMENT '文章图片',
  `visit` int(10) DEFAULT '0' COMMENT '浏览次数',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '状态:0=隐藏,1=正常',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `category_id` (`category_id`),
  KEY `sort` (`sort`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT='文章表';

-- ----------------------------
-- Records of bw_sys_article
-- ----------------------------

-- ----------------------------
-- Table structure for bw_sys_article_category
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__sys_article_category`;
CREATE TABLE `__BWPREFIX__sys_article_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '文章分类id',
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '父级ID',
  `title` varchar(255) NOT NULL COMMENT '文章分类标题',
  `intr` varchar(255) DEFAULT NULL COMMENT '文章分类简介',
  `image` varchar(255) NOT NULL COMMENT '文章分类图片',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '状态:0=隐藏,1=正常',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `delete_time` int(10) unsigned DEFAULT '0' COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT='文章分类表';

-- ----------------------------
-- Records of bw_sys_article_category
-- ----------------------------

-- ----------------------------
-- Table structure for bw_sys_article_content
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__sys_article_content`;
CREATE TABLE `__BWPREFIX__sys_article_content` (
  `article_id` int(10) unsigned NOT NULL COMMENT '文章id',
  `content` text NOT NULL COMMENT '文章内容',
  KEY `article_id` (`article_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT='文章内容表';

-- ----------------------------
-- Records of bw_sys_article_content
-- ----------------------------

-- ----------------------------
-- Table structure for bw_sys_city
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__sys_city`;
CREATE TABLE `__BWPREFIX__sys_city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_id` int(11) NOT NULL DEFAULT '0' COMMENT '城市id',
  `level` int(11) NOT NULL DEFAULT '0' COMMENT '省市级别',
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT '父级id',
  `area_code` varchar(30) NOT NULL DEFAULT '' COMMENT '行政区划代码',
  `name` varchar(100) NOT NULL DEFAULT '' COMMENT '名称',
  `merger_name` varchar(255) NOT NULL DEFAULT '' COMMENT '合并名称',
  `lng` varchar(50) NOT NULL DEFAULT '' COMMENT '经度',
  `lat` varchar(50) NOT NULL DEFAULT '' COMMENT '纬度',
  `is_show` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否展示{radio}(1:是,0:否)',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `city_id` (`city_id`),
  KEY `name` (`name`),
  KEY `parent_id` (`parent_id`),
  KEY `id` (`id`),
  KEY `level` (`level`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT='城市表';

-- ----------------------------
-- Records of bw_sys_city
-- ----------------------------

-- ----------------------------
-- Table structure for bw_sys_config
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__sys_config`;
CREATE TABLE `__BWPREFIX__sys_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '配置id',
  `config_name` varchar(255) NOT NULL COMMENT '字段名称',
  `tab_name` varchar(255) NOT NULL DEFAULT '' COMMENT '配置分类英文标识',
  `type` varchar(255) NOT NULL DEFAULT '' COMMENT '类型(文本框,单选按钮...)',
  `input_type` varchar(20) DEFAULT 'input' COMMENT '表单类型',
  `tab_id` int(10) unsigned NOT NULL COMMENT '配置分类id',
  `parameter` varchar(255) DEFAULT NULL COMMENT '单选框和多选框参数',
  `upload_type` tinyint(1) unsigned DEFAULT NULL COMMENT '上传格式1单图2多图3文件',
  `rule` varchar(255) DEFAULT NULL COMMENT '验证规则',
  `message` varchar(255) DEFAULT NULL COMMENT '验证规则错误提示',
  `width` int(10) unsigned DEFAULT NULL COMMENT '多行文本框的宽度',
  `high` int(10) unsigned DEFAULT NULL COMMENT '多行文框的高度',
  `value` varchar(5000) DEFAULT NULL COMMENT '默认值',
  `info` varchar(255) NOT NULL DEFAULT '' COMMENT '配置名称',
  `desc` varchar(255) DEFAULT NULL COMMENT '配置简介',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否隐藏0正常1隐藏',
  `member_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '租户Id(0为总后台管理员创建,不为0的时候是租户创建)',
  `scopes` varchar(30) NOT NULL DEFAULT 'common' COMMENT '配置范围common通用member租户',
  `disabled` tinyint(1) unsigned NOT NULL DEFAULT '2' COMMENT '是否锁值:1是2否',
  `lazy` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '是否懒加载:1是2否',
  `dir` varchar(100) DEFAULT NULL COMMENT '应用标识',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `type` (`type`),
  KEY `tab_id` (`tab_id`),
  KEY `input_type` (`input_type`),
  KEY `member_id` (`member_id`),
  KEY `sort` (`sort`),
  KEY `status` (`status`),
  KEY `scopes` (`scopes`),
  KEY `disabled` (`disabled`),
  KEY `lazy` (`lazy`)
) ENGINE=InnoDB AUTO_INCREMENT=13247 DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT='配置设置表';

-- ----------------------------
-- Records of bw_sys_config
-- ----------------------------
INSERT INTO `__BWPREFIX__sys_config` VALUES ('261', 'site_upload', 'ceshi', 'upload', 'text', '31', '', '1', '', '', '100', '200', '{\"name\":\"15934162269740.png\",\"src\":\"/uploads/image/20200721/65f57110f8dbc6461f84279ec8680b24.jpg\",\"size\":80468,\"type\":\"image/png\"}', '文件上传1', '文件上传1', '0', '0', '0', 'common', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('263', 'qnoss_secret_key', 'qnoss', 'text', 'text', '32', '本地=&gt;public', '1', '', '', '100', '200', 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx', '私钥信息', '安全密钥', '4', '0', '0', 'common', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('266', 'qnoss_access_key', 'qnoss', 'text', 'text', '32', '', '1', 'require', '', '100', '200', 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx', '访问密钥', '访问密钥', '3', '0', '0', 'common', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('267', 'express_secret_key', 'express', 'text', 'text', '34', '', '1', '', '', '100', '200', 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx', '快递查询秘钥', '请输入快递查询秘钥', '0', '0', '0', 'common', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('268', 'asdas_wenjian', 'ceshi', 'upload', 'text', '31', '', '3', '', '', '100', '200', '{\"name\":\"15934162269740.png\",\"size\":95201,\"type\":\"image/png\",\"src\":\"/uploads/image/20200721/dff5399882b699b357add7b50cc59c10.png\"}', '文件', '文件', '0', '0', '0', 'common', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('269', 'base_login_mode', 'base', 'radio', 'text', '35', '允许=&gt;1\n不允许=&gt;2', '1', '', '', '100', '200', '1', '同账号多端登录', '是否允许同账号多端登录（只记录最后登录者的登录信息）', '0', '0', '0', 'common', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('270', 'app_id', 'wechatopen', 'text', 'text', '36', '', '1', 'require', '', '100', '200', 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx', 'AppId', 'app_id', '0', '0', '0', 'common', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('271', 'secret', 'wechatopen', 'text', 'text', '36', '', '1', 'require', '', '100', '200', 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx', 'AppSecret', '请输入secret', '0', '0', '0', 'common', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('272', 'token', 'wechatopen', 'text', 'text', '36', '', '1', '', '', '100', '200', 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx', '消息验证token', '消息验证token', '0', '0', '0', 'common', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('273', 'auth_event_url', 'wechatopen', 'text', 'text', '36', '', '1', '', '', '100', '200', 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx', '授权事件接收', '授权事件接收', '0', '0', '0', 'common', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('274', 'msg_event_url', 'wechatopen', 'text', 'text', '36', '', '1', '', '', '100', '200', 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx', '消息与事件接收', '消息与事件接收', '0', '0', '0', 'common', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('275', 'aes_key', 'wechatopen', 'text', 'text', '36', '', '1', '', '', '100', '200', 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx', '消息加解密key', '消息加解密key', '0', '0', '0', 'common', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('276', 'base_login_number', 'base', 'text', 'number', '35', '', '1', '', '', '100', '200', '259200', '刷新token时间', '在还有多少秒即将过期时访问中间件刷新token', '0', '0', '0', 'common', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('277', 'web_name', 'web_config', 'text', 'text', '38', '', '1', '', '', '100', '200', 'SaaS云管理系统', '站点名称', '站点名称', '10', '0', '0', 'common', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('278', 'web_title', 'web_config', 'text', 'text', '38', '', '1', '', '', '100', '200', 'SaaS云管理系统', '站点副标题', '站点副标题', '9', '0', '0', 'common', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('279', 'web_url', 'web_config', 'text', 'text', '38', '', '1', '', '', '100', '200', 'https://xxx.xxxxxxxxxxx.com', '域名', '域名', '8', '0', '0', 'common', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('280', 'web_logo', 'web_config', 'upload', 'text', '38', '', '1', '', '', '100', '200', '{\"name\":\"f3354e72261d50286f2c1a174a68bbfe.png\",\"size\":0,\"type\":\"image\\/png\",\"src\":\"https:\\/\\/oss.buwangyun.com\\/upload\\/20201215\\/402463348b2d34ec6703e928d2087cb3.png\"}', '站点logo', '站点logo', '7', '0', '0', 'common', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('281', 'web_keywords', 'web_config', 'textarea', 'text', '38', '', '1', '', '', '100', '200', 'SaaS云管理系统', 'SEO关键词', 'SEO关键词', '6', '0', '0', 'common', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('282', 'web_description', 'web_config', 'textarea', 'text', '38', '', '1', '', '', '100', '200', 'SaaS云管理系统', 'SEO描述', 'SEO描述', '5', '0', '0', 'common', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('283', 'web_icp', 'web_config', 'text', 'text', '38', '', '1', '', '', '100', '200', 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx', 'ICP备案号', 'ICP备案号', '4', '0', '0', 'common', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('284', 'web_contacts', 'web_config', 'text', 'text', '38', '', '1', '', '', '100', '200', '0379-69576957', '联系方式', '联系方式', '3', '0', '0', 'common', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('285', 'web_address', 'web_config', 'text', 'text', '38', '', '1', '', '', '100', '200', 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx', '公司地址', '公司地址', '2', '0', '0', 'common', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('286', 'dsfsdf', 'ceshi', 'upload', 'text', '31', '', '2', '', '', '100', '200', '[{\"name\":\"15934162269740.png\",\"src\":\"/uploads/image/20200721/e0cdad5ae6f54104500a81280b92ae9f.png\",\"size\":95201,\"type\":\"image/png\"}]', '手打', 'cvxcv', '0', '0', '0', 'common', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('288', 'site_wenben', 'ceshi', 'text', 'text', '31', '', '1', '', '', '100', '200', '300', '测试文本', '测试文本', '0', '0', '0', 'common', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('290', 'AppCode', 'aliapi', 'text', 'text', '40', '', '1', '', '', '100', '200', 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxx', 'AppCode', '您的阿里云AppCode', '0', '0', '0', 'common', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('291', 'price', 'aliapi', 'text', 'float', '40', '', '1', '', '', '100', '200', '0.1', '价格(RMB)/次', '每次调用的价格', '0', '0', '0', 'common', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('416', 'upload_allow_ext', 'base', 'text', 'text', '35', '', '1', '', '', '100', '200', 'doc,gif,ico,icon,jpg,mp3,mp4,p12,pem,png,rar,jpeg,7z,pdf', '允许上传类型', '英文逗号,做分隔符', '7', '0', '0', 'common', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('417', 'upload_allow_size', 'base', 'text', 'text', '35', '', '1', '', '', '100', '200', '20971520', '允许上传大小', '允许上传大小', '6', '0', '0', 'common', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('418', 'alioss_access_key_id', 'alioss', 'text', 'text', '44', '', '1', '', '', '100', '200', 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx', '公钥信息', '阿里云oss公钥', '5', '0', '0', 'common', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('419', 'alioss_access_key_secret', 'alioss', 'text', 'text', '44', '', '1', '', '', '100', '200', 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx', '私钥信息', '阿里云oss私钥', '4', '0', '0', 'common', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('420', 'alioss_endpoint', 'alioss', 'text', 'text', '44', '', '1', '', '', '100', '200', 'https://xxxxxxxxx', '数据中心', '阿里云oss数据中心', '3', '0', '0', 'common', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('421', 'alioss_bucket', 'alioss', 'text', 'text', '44', '', '1', '', '', '100', '200', 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx', '空间名称', '阿里云oss空间名称', '2', '0', '0', 'common', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('422', 'alioss_domain', 'alioss', 'text', 'text', '44', '', '1', '', '', '100', '200', 'https://xxxxxxxxx', '访问域名', '阿里云oss访问域名', '1', '0', '0', 'common', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('423', 'upload_type', 'base', 'radio', 'text', '35', '本地存储=&gt;local\n阿里云oss=&gt;alioss\n七牛云oss=&gt;qnoss', '1', '', '', '100', '200', 'local', '上传存储类型', '上传存储类型', '0', '0', '0', 'common', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('424', 'qnoss_bucket', 'qnoss', 'text', 'text', '32', '', '1', 'require', '', '100', '200', 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx', '存储空间', '存储空间', '2', '0', '0', 'common', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('425', 'qnoss_domain', 'qnoss', 'text', 'text', '32', '', '1', 'require', '', '100', '200', 'https://xxxxxxxxx', '访问域名', '访问域名', '1', '0', '0', 'common', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('427', 'fuxuan_ceshi', 'ceshi', 'checkbox', 'text', '31', '男=>1\n男=>2\n不分=>3', '1', '', '', '100', '200', '1,2', 'fuxuanceshi', 'fuxuanceshi', '0', '0', '0', 'common', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('428', 'copyright', 'web_config', 'text', 'text', '38', '', '1', '', '', '100', '200', '©版权所有 2015-2020 布网云', '版权信息', '版权信息', '1', '0', '0', 'common', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('437', 'wechat_app_id', 'wechat_pay', 'text', 'text', '41', '', '1', '', '', '100', '200', 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx', '应用ID(app_id)', '应用ID', '5', '0', '0', 'common', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('438', 'wechat_mch_id', 'wechat_pay', 'text', 'text', '41', '', '1', '', '', '100', '200', 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx', '商户ID(mch_id)', '商户ID(mch_id)', '4', '0', '0', 'common', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('439', 'wechat_key', 'wechat_pay', 'text', 'text', '41', '', '1', '', '', '100', '200', 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx', 'API密钥', 'API密钥', '3', '0', '0', 'common', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('440', 'wechat_cert_path', 'wechat_pay', 'upload', 'text', '41', '', '3', '', '', '100', '200', '', '支付证书', '支付证书', '2', '0', '0', 'common', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('441', 'wechat_key_path', 'wechat_pay', 'upload', 'text', '41', '', '3', '', '', '100', '200', '', '证书密钥', '证书密钥:', '1', '0', '0', 'common', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('458', 'backstage_top_title', 'web_config', 'text', 'text', '38', '', '1', '', '', '100', '200', '控制台', '后台首页标题', '后台首页显示标题', '0', '1', '0', 'common', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('459', 'backstage_top_url', 'web_config', 'text', 'text', '38', '', '1', '', '', '100', '200', '/manage/admin/dashboard', '后台首页url', '后台首页url', '0', '1', '0', 'common', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('460', 'member_top_title', 'web_config', 'text', 'text', '38', '', '1', '', '', '100', '200', '首页', '租户后台首页标题', '租户后台首页标题', '0', '1', '0', 'common', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('461', 'member_top_url', 'web_config', 'text', 'text', '38', '', '1', '', '', '100', '200', '/manage/member/dashboard', '租户后台首页url', '租户后台首页url', '1', '1', '0', 'common', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('462', 'auth_url', 'scan', 'text', 'text', '50', '', '1', '', '', '100', '200', 'http://xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx', '服务器地址(URL)', '服务器地址(URL)', '9', '0', '0', 'common', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('463', 'qrcode_login', 'scan', 'radio', 'text', '50', '开启=>1\n关闭=>0', '1', '', '', '100', '200', '1', '开启扫码', '是否开启扫码', '8', '0', '0', 'common', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('464', 'mp_id', 'scan', 'text', 'text', '50', '', '1', '', '', '100', '200', 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx', 'AppID(公众号)', '把服务器地址(URL)中$APPID$替换为你的AppID', '7', '0', '0', 'common', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('465', 'mp_secret', 'scan', 'text', 'text', '50', '', '1', '', '', '100', '200', 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx', 'AppSecret(公众号)', 'AppSecret(公众号)', '6', '0', '0', 'common', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('466', 'mp_token', 'scan', 'text', 'text', '50', '', '1', '', '', '100', '200', 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx', 'Token(公众号)', 'Token必须为英文或数字，长度为3-32字符。如不填写则默认为“TOKEN”', '5', '0', '0', 'common', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('467', 'mp_aes_key', 'scan', 'text', 'text', '50', '', '1', '', '', '100', '200', 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx', 'EncodingAESKey', '公众号消息加密密钥由43位字符组成，可随机修改，字符范围为A-Z，a-z，0-9', '4', '0', '0', 'common', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('468', 'site_danxuan1', 'ceshi', 'radio', 'text', '31', '小明=>0\n小红=>2', '1', '', '', '100', '200', '0', '测试单选', '测试单选', '22', '0', '0', 'common', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('469', 'asd', 'ceshi', 'r_textarea', 'text', '31', '', '1', '', '', '100', '200', '<p>友情提示:<br><br>1、请打开微信公众号平台,网址:https://mp.weixin.qq.com,申请公众号必须是服务号。<br>2、登录->开发->基本配置->服务器配置(已启用)<br>3、帐号详情->功能设置->填写相关授权域名：(域名如：mall.buwangkeji.com)<br>4、设置正确后,前台会员支持扫码关注公众号登录管理。</p>', 'asd', 'asd', '0', '0', '0', 'common', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('3853', 'extract_money_min', 'extract_config', 'text', 'float', '55', '', '1', '', '', '100', '200', '10', '余额最低提现', '余额最低提现金额', '0', '0', '0', 'member', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('3854', 'extract_money_fee', 'extract_config', 'text', 'number', '55', '', '1', 'number|between:0,100', 'extract_money_fee.between=>数值只能在1-100之间', '100', '200', '0', '余额手续费', '余额提现手续费(百分比)', '0', '0', '0', 'member', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('3855', 'extract_desc', 'extract_config', 'r_textarea', 'text', '55', '', '1', '', '', '100', '200', '无', '提现说明', '提现说明', '0', '0', '0', 'member', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('3856', 'recharge_info', 'recharge_config', 'r_textarea', 'text', '54', '', '1', '', '', '100', '200', '<p><span><b>注意事项：</b></span></p><p>充值后帐户的金额不能提现，可用于商城消费使用</p><p>佣金导入账户之后不能再次导出、不可提现</p><p>账户充值出现问题可联系商城客服，也可拨打商城客服热线：4008888888</p>', '充值说明', '充值说明', '0', '0', '0', 'member', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('3857', 'ceshi_jianjie', 'zuhu_setting', 'textarea', 'text', '43', '', '1', '', '', '100', '200', '11', '测试简介', '测试简介', '0', '0', '0', 'member', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('3858', 'site_ceshi2', 'zuhu_setting', 'select', 'text', '43', '男=&gt;1\n女=&gt;2', '1', '', '', '100', '200', '1', '测试下拉2', '测试下拉2', '0', '0', '0', 'member', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('3859', 'site_xiala', 'zuhu_setting', 'select', 'text', '43', '男=&gt;1\n女=&gt;2', '1', '', '', '100', '200', '1', '测试下拉', '测试下拉', '10', '0', '0', 'member', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('3860', 'site_danxuan', 'zuhu_setting', 'radio', 'text', '43', '男=&gt;1\n女=&gt;2', '1', '', '', '100', '200', '1', '测试单选', '测试单选', '20', '0', '0', 'member', '2', '2', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('3861', 'ceshi_yanse', 'zuhu_setting', 'text', 'color', '43', '', '1', '', '', '100', '200', 'rgba(171, 59, 59, 1)', '测试颜色', 'haha', '12', '0', '0', 'member', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('3862', 'site_fuxuan', 'zuhu_setting', 'checkbox', 'text', '43', '男=&gt;1\n女=&gt;2\n伪娘=&gt;3', '1', '', '', '100', '200', '1,2,3', '测试复选框', '测试复选框', '200', '0', '0', 'member', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('3863', 'address_home', 'zuhu_setting', 'text', 'text', '43', '', '1', '', '', '100', '200', '家庭地址112233', '家庭地址', '家庭地址', '0', '0', '0', 'member', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('3864', 'mobile_zuhu', 'zuhu_setting', 'text', 'text', '43', '', '1', '', '', '100', '200', '15236108586', '手机号', '11位手机号', '0', '0', '0', 'member', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('3865', 'sitedanxuan', 'zu_hu_fen_lei', 'radio', 'text', '42', '甜=&gt;1\n咸=&gt;2', '1', '', '', '100', '200', '2', '测试租户单选框配置', '测试租户单选框配置', '0', '0', '0', 'member', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('3866', 'afadfsdf', 'zu_hu_fen_lei', 'text', 'text', '42', '', '1', '', '', '100', '200', '1111', '测试文本232', '测试文本232', '0', '0', '0', 'member', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('3867', 'asdfadf', 'zu_hu_fen_lei', 'text', 'text', '42', '', '1', '', '', '100', '200', '33', '测试配置2', '测试配置2', '0', '0', '0', 'member', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('4529', 'sys_log_time', 'base', 'text', 'number', '35', '', '1', '', '', '100', '200', '30', '系统日志清除时限', '系统日志清除时限（单位：秒，0为不清除）', '0', '0', '0', 'common', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('13232', 'extract_money_min', 'extract_config', 'text', 'float', '55', '', '1', '', '', '100', '200', '10', '余额最低提现', '余额最低提现金额', '0', '0', '64', 'member', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('13233', 'extract_money_fee', 'extract_config', 'text', 'number', '55', '', '1', 'number|between:0,100', 'extract_money_fee.between=>数值只能在1-100之间', '100', '200', '0', '余额手续费', '余额提现手续费(百分比)', '0', '0', '64', 'member', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('13234', 'extract_desc', 'extract_config', 'r_textarea', 'text', '55', '', '1', '', '', '100', '200', '无', '提现说明', '提现说明', '0', '0', '64', 'member', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('13235', 'recharge_info', 'recharge_config', 'r_textarea', 'text', '54', '', '1', '', '', '100', '200', '<p><span><b>注意事项：</b></span></p><p>充值后帐户的金额不能提现，可用于商城消费使用</p><p>佣金导入账户之后不能再次导出、不可提现</p><p>账户充值出现问题可联系商城客服，也可拨打商城客服热线：4008888888</p>', '充值说明', '充值说明', '0', '0', '64', 'member', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('13236', 'ceshi_jianjie', 'zuhu_setting', 'textarea', 'text', '43', '', '1', '', '', '100', '200', '11', '测试简介', '测试简介', '0', '0', '64', 'member', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('13237', 'site_ceshi2', 'zuhu_setting', 'select', 'text', '43', '男=&gt;1\n女=&gt;2', '1', '', '', '100', '200', '1', '测试下拉2', '测试下拉2', '0', '0', '64', 'member', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('13238', 'site_xiala', 'zuhu_setting', 'select', 'text', '43', '男=&gt;1\n女=&gt;2', '1', '', '', '100', '200', '1', '测试下拉', '测试下拉', '10', '0', '64', 'member', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('13239', 'site_danxuan', 'zuhu_setting', 'radio', 'text', '43', '男=&gt;1\n女=&gt;2', '1', '', '', '100', '200', '1', '测试单选', '测试单选', '20', '0', '64', 'member', '2', '2', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('13240', 'ceshi_yanse', 'zuhu_setting', 'text', 'color', '43', '', '1', '', '', '100', '200', 'rgba(171, 59, 59, 1)', '测试颜色', 'haha', '12', '0', '64', 'member', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('13241', 'site_fuxuan', 'zuhu_setting', 'checkbox', 'text', '43', '男=&gt;1\n女=&gt;2\n伪娘=&gt;3', '1', '', '', '100', '200', '1,2,3', '测试复选框', '测试复选框', '200', '0', '64', 'member', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('13242', 'address_home', 'zuhu_setting', 'text', 'text', '43', '', '1', '', '', '100', '200', '家庭地址112233', '家庭地址', '家庭地址', '0', '0', '64', 'member', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('13243', 'mobile_zuhu', 'zuhu_setting', 'text', 'text', '43', '', '1', '', '', '100', '200', '15236108586', '手机号', '11位手机号', '0', '0', '64', 'member', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('13244', 'sitedanxuan', 'zu_hu_fen_lei', 'radio', 'text', '42', '甜=&gt;1\n咸=&gt;2', '1', '', '', '100', '200', '2', '测试租户单选框配置', '测试租户单选框配置', '0', '0', '64', 'member', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('13245', 'afadfsdf', 'zu_hu_fen_lei', 'text', 'text', '42', '', '1', '', '', '100', '200', '1111', '测试文本232', '测试文本232', '0', '0', '64', 'member', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` VALUES ('13246', 'asdfadf', 'zu_hu_fen_lei', 'text', 'text', '42', '', '1', '', '', '100', '200', '33', '测试配置2', '测试配置2', '0', '0', '64', 'member', '2', '1', '');
INSERT INTO `__BWPREFIX__sys_config` (`id`, `config_name`, `tab_name`, `type`, `input_type`, `tab_id`, `parameter`, `upload_type`, `rule`, `message`, `width`, `high`, `value`, `info`, `desc`, `sort`, `status`, `member_id`, `scopes`, `disabled`, `lazy`, `dir`) VALUES ('13493', 'web_qq', 'web_config', 'text', 'text', '38', '', '1', '', '', '100', '200', '911098002', '客服QQ', '客服QQ', '4', '0', '0', 'common', '2', '2', '');


-- ----------------------------
-- Table structure for bw_sys_config_group
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__sys_config_group`;
CREATE TABLE `__BWPREFIX__sys_config_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '组合数据ID',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '数据组名称',
  `desc` varchar(256) NOT NULL DEFAULT '' COMMENT '数据说明',
  `config_name` varchar(255) NOT NULL DEFAULT '' COMMENT '数据字段',
  `fields` text COMMENT '数据组字段以及类型（json数据）',
  `scopes` varchar(30) NOT NULL DEFAULT 'common' COMMENT '配置范围common通用member租户',
  `dir` varchar(100) DEFAULT NULL COMMENT '应用标识',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `scopes` (`scopes`)
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT='分组数据字段表';

-- ----------------------------
-- Records of bw_sys_config_group
-- ----------------------------
INSERT INTO `__BWPREFIX__sys_config_group` VALUES ('85', '充值套餐选择', '充值套餐选择', 'recharge_select', '[{\"id\":\"5f65d41996bc7\",\"info\":\"售价\",\"config_name\":\"price\",\"desc\":\"售价\",\"must\":\"1\",\"input_type\":\"float\",\"rule\":\"\",\"message\":\"\",\"value\":\"1.0\",\"width\":\"100\",\"high\":\"200\",\"parameter\":\"\",\"upload_type\":\"1\",\"type\":\"text\"},{\"id\":\"5f65d4792346e\",\"info\":\"赠送\",\"config_name\":\"give\",\"desc\":\"赠送\",\"must\":\"2\",\"input_type\":\"float\",\"rule\":\"\",\"message\":\"\",\"value\":\"1.0\",\"width\":\"100\",\"high\":\"200\",\"parameter\":\"\",\"upload_type\":\"1\",\"type\":\"text\"}]', 'member', '');

-- ----------------------------
-- Table structure for bw_sys_config_group_data
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__sys_config_group_data`;
CREATE TABLE `__BWPREFIX__sys_config_group_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '组合数据详情ID',
  `group_id` int(11) NOT NULL DEFAULT '0' COMMENT '对应的数据组id',
  `value` text NOT NULL COMMENT '数据字段对应的（json）数据值',
  `add_time` int(10) NOT NULL DEFAULT '0' COMMENT '新增时间',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态（1：开启；2：关闭；）',
  `member_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '租户Id(0为总后台管理员创建,不为0的时候是租户创建)',
  `scopes` varchar(30) NOT NULL DEFAULT 'common' COMMENT '配置范围common通用member租户',
  `config_name` varchar(255) NOT NULL DEFAULT '' COMMENT '数据字段',
  `dir` varchar(100) DEFAULT NULL COMMENT '应用标识',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `group_id` (`group_id`),
  KEY `sort` (`sort`),
  KEY `status` (`status`),
  KEY `member_id` (`member_id`),
  KEY `scopes` (`scopes`)
) ENGINE=InnoDB AUTO_INCREMENT=3430 DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT='数据组合键值表';

-- ----------------------------
-- Records of bw_sys_config_group_data
-- ----------------------------
INSERT INTO `__BWPREFIX__sys_config_group_data` VALUES ('3416', '85', '{\"price\":{\"type\":\"float\",\"value\":\"20\"},\"give\":{\"type\":\"float\",\"value\":\"2\"}}', '1600509664', '0', '1', '0', 'member', 'recharge_select', '');
INSERT INTO `__BWPREFIX__sys_config_group_data` VALUES ('3417', '85', '{\"price\":{\"type\":\"float\",\"value\":\"30\"},\"give\":{\"type\":\"float\",\"value\":\"3\"}}', '1600509671', '0', '1', '0', 'member', 'recharge_select', '');
INSERT INTO `__BWPREFIX__sys_config_group_data` VALUES ('3418', '85', '{\"price\":{\"type\":\"float\",\"value\":\"50\"},\"give\":{\"type\":\"float\",\"value\":\"5\"}}', '1600509678', '0', '1', '0', 'member', 'recharge_select', '');
INSERT INTO `__BWPREFIX__sys_config_group_data` VALUES ('3419', '85', '{\"price\":{\"type\":\"float\",\"value\":\"500\"},\"give\":{\"type\":\"float\",\"value\":\"50\"}}', '1600509706', '0', '1', '0', 'member', 'recharge_select', '');
INSERT INTO `__BWPREFIX__sys_config_group_data` VALUES ('3420', '85', '{\"price\":{\"type\":\"float\",\"value\":\"800\"},\"give\":{\"type\":\"float\",\"value\":\"100\"}}', '1600509716', '0', '1', '0', 'member', 'recharge_select', '');
INSERT INTO `__BWPREFIX__sys_config_group_data` VALUES ('3421', '85', '{\"price\":{\"type\":\"float\",\"value\":\"0.01\"},\"give\":{\"type\":\"float\",\"value\":\"0\"}}', '1600670096', '0', '1', '0', 'member', 'recharge_select', '');
INSERT INTO `__BWPREFIX__sys_config_group_data` VALUES ('3422', '85', '{\"price\":{\"type\":\"float\",\"value\":\"1000\"},\"give\":{\"type\":\"float\",\"value\":\"300\"}}', '1601296083', '0', '1', '0', 'member', 'recharge_select', '');
INSERT INTO `__BWPREFIX__sys_config_group_data` VALUES ('3423', '85', '{\"price\":{\"type\":\"float\",\"value\":\"20\"},\"give\":{\"type\":\"float\",\"value\":\"2\"}}', '1600509664', '0', '1', '64', 'member', 'recharge_select', '');
INSERT INTO `__BWPREFIX__sys_config_group_data` VALUES ('3424', '85', '{\"price\":{\"type\":\"float\",\"value\":\"30\"},\"give\":{\"type\":\"float\",\"value\":\"3\"}}', '1600509671', '0', '1', '64', 'member', 'recharge_select', '');
INSERT INTO `__BWPREFIX__sys_config_group_data` VALUES ('3425', '85', '{\"price\":{\"type\":\"float\",\"value\":\"50\"},\"give\":{\"type\":\"float\",\"value\":\"5\"}}', '1600509678', '0', '1', '64', 'member', 'recharge_select', '');
INSERT INTO `__BWPREFIX__sys_config_group_data` VALUES ('3426', '85', '{\"price\":{\"type\":\"float\",\"value\":\"500\"},\"give\":{\"type\":\"float\",\"value\":\"50\"}}', '1600509706', '0', '1', '64', 'member', 'recharge_select', '');
INSERT INTO `__BWPREFIX__sys_config_group_data` VALUES ('3427', '85', '{\"price\":{\"type\":\"float\",\"value\":\"800\"},\"give\":{\"type\":\"float\",\"value\":\"100\"}}', '1600509716', '0', '1', '64', 'member', 'recharge_select', '');
INSERT INTO `__BWPREFIX__sys_config_group_data` VALUES ('3428', '85', '{\"price\":{\"type\":\"float\",\"value\":\"0.01\"},\"give\":{\"type\":\"float\",\"value\":\"0\"}}', '1600670096', '0', '1', '64', 'member', 'recharge_select', '');
INSERT INTO `__BWPREFIX__sys_config_group_data` VALUES ('3429', '85', '{\"price\":{\"type\":\"float\",\"value\":\"1000\"},\"give\":{\"type\":\"float\",\"value\":\"300\"}}', '1601296083', '0', '1', '64', 'member', 'recharge_select', '');

-- ----------------------------
-- Table structure for bw_sys_config_tab
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__sys_config_tab`;
CREATE TABLE `__BWPREFIX__sys_config_tab` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '配置分类id',
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT '配置分类名称',
  `tab_name` varchar(255) NOT NULL DEFAULT '' COMMENT '配置分类英文标识',
  `icon` varchar(30) DEFAULT NULL COMMENT '图标',
  `type` int(2) DEFAULT '0' COMMENT '配置类型',
  `is_show` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '配置分类是否显示  0不显示，1显示',
  `scopes` varchar(30) NOT NULL DEFAULT 'common' COMMENT '配置范围common通用member租户',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `desc` text COMMENT '配置说明',
  `dir` varchar(100) DEFAULT NULL COMMENT '应用标识',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `type` (`type`),
  KEY `is_show` (`is_show`),
  KEY `scopes` (`scopes`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT='配置分类表';

-- ----------------------------
-- Records of bw_sys_config_tab
-- ----------------------------
INSERT INTO `__BWPREFIX__sys_config_tab` VALUES ('31', '测试配置', 'ceshi', 'fa fa-diamond       ', '1', '0', 'common', '0', '', '');
INSERT INTO `__BWPREFIX__sys_config_tab` VALUES ('32', '七牛云oss', 'qnoss', 'fa fa-folder-open-o', '1', '1', 'common', '10', '', '');
INSERT INTO `__BWPREFIX__sys_config_tab` VALUES ('34', '物流配置', 'express', 'fa fa-truck', '1', '1', 'common', '0', '', '');
INSERT INTO `__BWPREFIX__sys_config_tab` VALUES ('35', '基础配置', 'base', 'fa fa-cubes  ', '1', '1', 'common', '0', '', '');
INSERT INTO `__BWPREFIX__sys_config_tab` VALUES ('36', '微信开放平台配置', 'wechatopen', 'fa fa-wechat', '1', '1', 'common', '0', '', '');
INSERT INTO `__BWPREFIX__sys_config_tab` VALUES ('38', '站点配置', 'web_config', 'fa fa-server', '1', '1', 'common', '0', '', '');
INSERT INTO `__BWPREFIX__sys_config_tab` VALUES ('40', '阿里云市场API', 'aliapi', 'fa fa-cubes', '1', '1', 'common', '0', '', '');
INSERT INTO `__BWPREFIX__sys_config_tab` VALUES ('41', '微信支付', 'wechat_pay', 'fa fa-edit', '1', '1', 'common', '111', '', '');
INSERT INTO `__BWPREFIX__sys_config_tab` VALUES ('42', '测试租户分类', 'zu_hu_fen_lei', 'fa fa-fast-backward', '1', '1', 'member', '0', '', '');
INSERT INTO `__BWPREFIX__sys_config_tab` VALUES ('43', '租户信息设置', 'zuhu_setting', 'fa fa-diamond   ', '1', '1', 'member', '333', '', '');
INSERT INTO `__BWPREFIX__sys_config_tab` VALUES ('44', '阿里云oss', 'alioss', 'fa fa-server', '1', '1', 'common', '11', '', '');
INSERT INTO `__BWPREFIX__sys_config_tab` VALUES ('50', '扫码登录', 'scan', 'fa fa-qrcode         ', '1', '1', 'common', '6', '<p>友情提示:<br><br>1、请打开微信公众号平台,网址:https://mp.weixin.qq.com,申请公众号必须是服务号。<br>2、登录-&gt;开发-&gt;基本配置-&gt;服务器配置(已启用)。<br></p><p>3、<b>注意如果需要配置扫码登录的公众号已经绑定过任何一个租户应用，此处只需要配置</b><span style=\"text-align: center;\">开启扫码和</span><span style=\"text-align: center;\">AppID(公众号)</span><b>就可以正常使用。</b></p>4、设置正确后,前台会员支持扫码关注公众号登录管理。<p></p>', '');
INSERT INTO `__BWPREFIX__sys_config_tab` VALUES ('54', '充值配置', 'recharge_config', 'fa fa-diamond', '3', '1', 'member', '0', '', '');
INSERT INTO `__BWPREFIX__sys_config_tab` VALUES ('55', '提现配置', 'extract_config', 'fa fa-calculator', '1', '1', 'member', '0', '', '');

-- ----------------------------
-- Table structure for bw_sys_crud
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__sys_crud`;
CREATE TABLE `__BWPREFIX__sys_crud` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `app_name` varchar(50) NOT NULL COMMENT '应用名称',
  `table_name` varchar(50) NOT NULL COMMENT '表名',
  `controller_name_diy` varchar(50) NOT NULL COMMENT '自定义控制器名',
  `model_name_diy` varchar(50) NOT NULL COMMENT '自定义模型名',
  `status` enum('1','0') NOT NULL DEFAULT '1' COMMENT '状态:1=成功,0=失败',
  `create_time` int(10) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(10) NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT='一键生成表';

-- ----------------------------
-- Records of bw_sys_crud
-- ----------------------------

-- ----------------------------
-- Table structure for bw_sys_express
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__sys_express`;
CREATE TABLE `__BWPREFIX__sys_express` (
  `id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '快递公司id',
  `member_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '租户ID',
  `code` varchar(50) NOT NULL DEFAULT '' COMMENT '快递公司编码',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '快递公司全称',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `is_show` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否显示{switch}(0:取消,1:展示)',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `code` (`code`,`member_id`) USING BTREE,
  KEY `is_show` (`is_show`) USING BTREE,
  KEY `member_id` (`member_id`)
) ENGINE=InnoDB AUTO_INCREMENT=426 DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT='快递公司表';

-- ----------------------------
-- Records of bw_sys_express
-- ----------------------------
INSERT INTO `__BWPREFIX__sys_express` VALUES ('1', '0', 'LIMINWL', '利民物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('2', '0', 'XINTIAN', '鑫天顺物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('3', '0', 'henglu', '恒路物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('4', '0', 'klwl', '康力物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('5', '0', 'meiguo', '美国快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('6', '0', 'a2u', 'A2U速递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('7', '0', 'benteng', '奔腾物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('8', '0', 'ahdf', '德方物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('9', '0', 'timedg', '万家通', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('10', '0', 'ztong', '智通物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('11', '0', 'xindan', '新蛋物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('12', '0', 'bgpyghx', '挂号信', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('13', '0', 'XFHONG', '鑫飞鸿物流快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('14', '0', 'ALP', '阿里物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('15', '0', 'BFWL', '滨发物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('16', '0', 'SJWL', '宋军物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('17', '0', 'SHUNFAWL', '顺发物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('18', '0', 'TIANHEWL', '天河物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('19', '0', 'YBWL', '邮联物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('20', '0', 'SWHY', '盛旺货运', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('21', '0', 'TSWL', '汤氏物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('22', '0', 'YUANYUANWL', '圆圆物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('23', '0', 'BALIANGWL', '八梁物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('24', '0', 'ZGWL', '振刚物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('25', '0', 'JIAYU', '佳宇物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('26', '0', 'SHHX', '昊昕物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('27', '0', 'ande', '安得物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('28', '0', 'ppbyb', '贝邮宝', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('29', '0', 'dida', '递达快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('30', '0', 'jppost', '日本邮政', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('31', '0', 'intmail', '中国邮政', '96', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('32', '0', 'HENGCHENGWL', '恒诚物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('33', '0', 'HENGFENGWL', '恒丰物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('34', '0', 'gdems', '广东ems快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('35', '0', 'xlyt', '祥龙运通', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('36', '0', 'gjbg', '国际包裹', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('37', '0', 'uex', 'UEX', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('38', '0', 'singpost', '新加坡邮政', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('39', '0', 'guangdongyouzhengwuliu', '广东邮政', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('40', '0', 'bht', 'BHT', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('41', '0', 'cces', 'CCES快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('42', '0', 'cloudexpress', 'CE易欧通国际速递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('43', '0', 'dasu', '达速物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('44', '0', 'pfcexpress', '皇家物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('45', '0', 'hjs', '猴急送', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('46', '0', 'huilian', '辉联物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('47', '0', 'huanqiu', '环球速运', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('48', '0', 'huada', '华达快运', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('49', '0', 'htwd', '华通务达物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('50', '0', 'hipito', '海派通', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('51', '0', 'hqtd', '环球通达', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('52', '0', 'airgtc', '航空快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('53', '0', 'haoyoukuai', '好又快物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('54', '0', 'hanrun', '韩润物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('55', '0', 'ccd', '河南次晨达', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('56', '0', 'hfwuxi', '和丰同城', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('57', '0', 'Sky', '荷兰', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('58', '0', 'hongxun', '鸿讯物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('59', '0', 'hongjie', '宏捷国际物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('60', '0', 'httx56', '汇通天下物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('61', '0', 'lqht', '恒通快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('62', '0', 'jinguangsudikuaijian', '京广速递快件', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('63', '0', 'junfengguoji', '骏丰国际速递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('64', '0', 'jiajiatong56', '佳家通', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('65', '0', 'jrypex', '吉日优派', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('66', '0', 'jinchengwuliu', '锦程国际物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('67', '0', 'jgwl', '景光物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('68', '0', 'pzhjst', '急顺通', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('69', '0', 'ruexp', '捷网俄全通', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('70', '0', 'jmjss', '金马甲', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('71', '0', 'lanhu', '蓝弧快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('72', '0', 'ltexp', '乐天速递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('73', '0', 'lutong', '鲁通快运', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('74', '0', 'ledii', '乐递供应链', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('75', '0', 'lundao', '论道国际物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('76', '0', 'mailikuaidi', '麦力快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('77', '0', 'mchy', '木春货运', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('78', '0', 'meiquick', '美快国际物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('79', '0', 'valueway', '美通快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('80', '0', 'nuoyaao', '偌亚奥国际', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('81', '0', 'euasia', '欧亚专线', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('82', '0', 'pca', '澳大利亚PCA快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('83', '0', 'pingandatengfei', '平安达腾飞', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('84', '0', 'pjbest', '品骏快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('85', '0', 'qbexpress', '秦邦快运', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('86', '0', 'quanxintong', '全信通快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('87', '0', 'quansutong', '全速通国际快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('88', '0', 'qinyuan', '秦远物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('89', '0', 'qichen', '启辰国际物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('90', '0', 'quansu', '全速快运', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('91', '0', 'qzx56', '全之鑫物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('92', '0', 'qskdyxgs', '千顺快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('93', '0', 'runhengfeng', '全时速运', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('94', '0', 'rytsd', '日益通速递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('95', '0', 'ruidaex', '瑞达国际速递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('96', '0', 'shiyun', '世运快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('97', '0', 'sfift', '十方通物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('98', '0', 'stkd', '顺通快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('99', '0', 'bgn', '布谷鸟快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('100', '0', 'jiahuier', '佳惠尔快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('101', '0', 'pingyou', '小包', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('102', '0', 'yumeijie', '誉美捷快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('103', '0', 'meilong', '美龙快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('104', '0', 'guangtong', '广通速递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('105', '0', 'STARS', '星晨急便', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('106', '0', 'NANHANG', '中国南方航空股份有限公司', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('107', '0', 'lanbiao', '蓝镖快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('109', '0', 'baotongda', '宝通达物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('110', '0', 'dashun', '大顺物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('111', '0', 'dada', '大达物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('112', '0', 'fangfangda', '方方达物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('113', '0', 'hebeijianhua', '河北建华物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('114', '0', 'haolaiyun', '好来运快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('115', '0', 'jinyue', '晋越快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('116', '0', 'kuaitao', '快淘快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('117', '0', 'peixing', '陪行物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('118', '0', 'hkpost', '香港邮政', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('119', '0', 'ytfh', '一统飞鸿快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('120', '0', 'zhongxinda', '中信达快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('121', '0', 'zhongtian', '中天快运', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('122', '0', 'zuochuan', '佐川急便', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('123', '0', 'chengguang', '程光快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('124', '0', 'cszx', '城市之星', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('125', '0', 'chuanzhi', '传志快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('126', '0', 'feibao', '飞豹快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('127', '0', 'huiqiang', '汇强快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('128', '0', 'lejiedi', '乐捷递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('129', '0', 'lijisong', '成都立即送快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('130', '0', 'minbang', '民邦速递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('131', '0', 'ocs', 'OCS国际快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('132', '0', 'santai', '三态速递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('133', '0', 'saiaodi', '赛澳递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('134', '0', 'jd', '京东快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('135', '0', 'zengyi', '增益快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('136', '0', 'fanyu', '凡宇速递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('137', '0', 'fengda', '丰达快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('138', '0', 'coe', '东方快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('139', '0', 'ees', '百福东方快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('140', '0', 'disifang', '递四方速递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('141', '0', 'rufeng', '如风达快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('142', '0', 'changtong', '长通物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('143', '0', 'chengshi100', '城市100快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('144', '0', 'feibang', '飞邦物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('145', '0', 'haosheng', '昊盛物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('146', '0', 'yinsu', '音速速运', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('147', '0', 'kuanrong', '宽容物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('148', '0', 'tongcheng', '通成物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('149', '0', 'tonghe', '通和天下物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('150', '0', 'zhima', '芝麻开门', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('151', '0', 'ririshun', '日日顺物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('152', '0', 'anxun', '安迅物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('153', '0', 'baiqian', '百千诚国际物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('154', '0', 'chukouyi', '出口易', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('155', '0', 'diantong', '店通快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('156', '0', 'dajin', '大金物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('157', '0', 'feite', '飞特物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('159', '0', 'gnxb', '国内小包', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('160', '0', 'huacheng', '华诚物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('161', '0', 'huahan', '华翰物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('162', '0', 'hengyu', '恒宇运通', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('163', '0', 'huahang', '华航快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('164', '0', 'jiuyi', '久易快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('165', '0', 'jiete', '捷特快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('166', '0', 'jingshi', '京世物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('167', '0', 'kuayue', '跨越快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('168', '0', 'mengsu', '蒙速快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('169', '0', 'nanbei', '南北快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('171', '0', 'pinganda', '平安达快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('172', '0', 'ruifeng', '瑞丰速递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('173', '0', 'rongqing', '荣庆物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('174', '0', 'suijia', '穗佳物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('175', '0', 'simai', '思迈快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('176', '0', 'suteng', '速腾快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('177', '0', 'shengbang', '晟邦物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('178', '0', 'suchengzhaipei', '速呈宅配', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('179', '0', 'wuhuan', '五环速递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('180', '0', 'xingchengzhaipei', '星程宅配', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('181', '0', 'yinjie', '顺捷丰达', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('183', '0', 'yanwen', '燕文物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('184', '0', 'zongxing', '纵行物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('185', '0', 'aae', 'AAE快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('186', '0', 'dhl', 'DHL快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('187', '0', 'feihu', '飞狐快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('188', '0', 'shunfeng', '顺丰速运', '92', '1');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('189', '0', 'spring', '春风物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('190', '0', 'yidatong', '易达通快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('191', '0', 'PEWKEE', '彪记快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('192', '0', 'PHOENIXEXP', '凤凰快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('193', '0', 'CNGLS', 'GLS快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('194', '0', 'BHTEXP', '华慧快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('195', '0', 'B2B', '卡行天下', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('196', '0', 'PEISI', '配思货运', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('197', '0', 'SUNDAPOST', '上大物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('198', '0', 'SUYUE', '苏粤货运', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('199', '0', 'F5XM', '伍圆速递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('200', '0', 'GZWENJIE', '文捷航空速递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('201', '0', 'yuancheng', '远成物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('202', '0', 'dpex', 'DPEX快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('203', '0', 'anjie', '安捷快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('204', '0', 'jldt', '嘉里大通', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('205', '0', 'yousu', '优速快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('206', '0', 'wanbo', '万博快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('207', '0', 'sure', '速尔物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('208', '0', 'sutong', '速通物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('209', '0', 'JUNCHUANWL', '骏川物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('210', '0', 'guada', '冠达快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('211', '0', 'dsu', 'D速快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('212', '0', 'LONGSHENWL', '龙胜物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('213', '0', 'abc', '爱彼西快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('214', '0', 'eyoubao', 'E邮宝', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('215', '0', 'aol', 'AOL快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('216', '0', 'jixianda', '急先达物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('217', '0', 'haihong', '山东海红快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('218', '0', 'feiyang', '飞洋快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('219', '0', 'rpx', 'RPX保时达', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('220', '0', 'zhaijisong', '宅急送', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('221', '0', 'tiantian', '天天快递', '99', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('222', '0', 'yunwuliu', '云物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('223', '0', 'jiuye', '九曳供应链', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('224', '0', 'bsky', '百世快运', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('225', '0', 'higo', '黑狗物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('226', '0', 'arke', '方舟速递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('227', '0', 'zwsy', '中外速运', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('228', '0', 'jxy', '吉祥邮', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('229', '0', 'aramex', 'Aramex', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('230', '0', 'guotong', '国通快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('231', '0', 'jiayi', '佳怡物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('232', '0', 'longbang', '龙邦快运', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('233', '0', 'minhang', '民航快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('234', '0', 'quanyi', '全一快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('235', '0', 'quanchen', '全晨快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('236', '0', 'usps', 'USPS快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('237', '0', 'xinbang', '新邦物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('238', '0', 'yuanzhi', '元智捷诚快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('239', '0', 'zhongyou', '中邮物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('240', '0', 'yuxin', '宇鑫物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('241', '0', 'cnpex', '中环快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('242', '0', 'shengfeng', '盛丰物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('243', '0', 'yuantong', '圆通速递', '97', '1');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('244', '0', 'jiayunmei', '加运美物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('245', '0', 'ywfex', '源伟丰快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('246', '0', 'xinfeng', '信丰物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('247', '0', 'wanxiang', '万象物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('248', '0', 'menduimen', '门对门', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('249', '0', 'mingliang', '明亮物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('250', '0', 'fengxingtianxia', '风行天下', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('251', '0', 'gongsuda', '共速达物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('252', '0', 'zhongtong', '中通快递', '100', '1');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('253', '0', 'quanritong', '全日通快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('254', '0', 'ems', 'EMS', '1', '1');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('255', '0', 'wanjia', '万家物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('256', '0', 'yuntong', '运通快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('257', '0', 'feikuaida', '飞快达物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('258', '0', 'haimeng', '海盟速递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('259', '0', 'zhongsukuaidi', '中速快件', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('260', '0', 'yuefeng', '越丰快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('261', '0', 'shenghui', '盛辉物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('262', '0', 'datian', '大田物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('263', '0', 'quanjitong', '全际通快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('264', '0', 'longlangkuaidi', '隆浪快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('265', '0', 'neweggozzo', '新蛋奥硕物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('266', '0', 'shentong', '申通快递', '95', '1');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('267', '0', 'haiwaihuanqiu', '海外环球', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('268', '0', 'yad', '源安达快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('269', '0', 'jindawuliu', '金大物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('270', '0', 'sevendays', '七天连锁', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('271', '0', 'tnt', 'TNT快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('272', '0', 'huayu', '天地华宇物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('273', '0', 'lianhaotong', '联昊通快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('274', '0', 'nengda', '港中能达快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('275', '0', 'LBWL', '联邦物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('276', '0', 'ontrac', 'onTrac', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('277', '0', 'feihang', '原飞航快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('278', '0', 'bangsongwuliu', '邦送物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('279', '0', 'huaxialong', '华夏龙物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('280', '0', 'ztwy', '中天万运快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('281', '0', 'fkd', '飞康达物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('282', '0', 'anxinda', '安信达快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('283', '0', 'quanfeng', '全峰快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('284', '0', 'shengan', '圣安物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('285', '0', 'jiaji', '佳吉物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('286', '0', 'yunda', '韵达快运', '94', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('287', '0', 'ups', 'UPS快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('288', '0', 'debang', '德邦物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('289', '0', 'yafeng', '亚风速递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('290', '0', 'kuaijie', '快捷速递', '98', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('291', '0', 'huitong', '百世快递', '93', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('293', '0', 'aolau', 'AOL澳通速递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('294', '0', 'anneng', '安能物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('295', '0', 'auexpress', '澳邮中国快运', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('296', '0', 'exfresh', '安鲜达', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('297', '0', 'bcwelt', 'BCWELT', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('298', '0', 'youzhengguonei', '挂号信', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('299', '0', 'xiaohongmao', '北青小红帽', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('300', '0', 'lbbk', '宝凯物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('301', '0', 'byht', '博源恒通', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('302', '0', 'idada', '百成大达物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('303', '0', 'baitengwuliu', '百腾物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('304', '0', 'birdex', '笨鸟海淘', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('305', '0', 'bsht', '百事亨通', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('306', '0', 'dayang', '大洋物流快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('307', '0', 'dechuangwuliu', '德创物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('308', '0', 'donghanwl', '东瀚物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('309', '0', 'dfpost', '达方物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('310', '0', 'dongjun', '东骏快捷物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('311', '0', 'dindon', '叮咚澳洲转运', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('312', '0', 'dazhong', '大众佐川急便', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('313', '0', 'decnlh', '德中快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('314', '0', 'dekuncn', '德坤供应链', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('315', '0', 'eshunda', '俄顺达', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('316', '0', 'ewe', 'EWE全球快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('317', '0', 'fedexuk', 'FedEx英国', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('318', '0', 'fox', 'FOX国际速递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('319', '0', 'rufengda', '凡客如风达', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('320', '0', 'fandaguoji', '颿达国际快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('321', '0', 'hnfy', '飞鹰物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('322', '0', 'flysman', '飞力士物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('323', '0', 'sccod', '丰程物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('324', '0', 'farlogistis', '泛远国际物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('325', '0', 'gsm', 'GSM', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('326', '0', 'gaticn', 'GATI快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('327', '0', 'gts', 'GTS快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('328', '0', 'gangkuai', '港快速递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('329', '0', 'gtsd', '高铁速递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('330', '0', 'tiandihuayu', '华宇物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('331', '0', 'huangmajia', '黄马甲快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('332', '0', 'ucs', '合众速递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('333', '0', 'huoban', '伙伴物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('334', '0', 'nedahm', '红马速递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('335', '0', 'huiwen', '汇文配送', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('336', '0', 'nmhuahe', '华赫物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('337', '0', 'hangyu', '航宇快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('338', '0', 'minsheng', '闽盛物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('339', '0', 'riyu', '日昱物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('340', '0', 'sxhongmajia', '山西红马甲', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('341', '0', 'syjiahuier', '沈阳佳惠尔', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('342', '0', 'shlindao', '上海林道货运', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('343', '0', 'shunjiefengda', '顺捷丰达', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('344', '0', 'subida', '速必达物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('345', '0', 'bphchina', '速方国际物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('346', '0', 'sendtochina', '速递中国', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('347', '0', 'suning', '苏宁快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('348', '0', 'sihaiet', '四海快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('349', '0', 'tianzong', '天纵物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('350', '0', 'chinatzx', '同舟行物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('351', '0', 'nntengda', '腾达速递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('352', '0', 'sd138', '泰国138', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('353', '0', 'tongdaxing', '通达兴物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('354', '0', 'tlky', '天联快运', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('355', '0', 'youshuwuliu', 'UC优速快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('356', '0', 'ueq', 'UEQ快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('357', '0', 'weitepai', '微特派快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('358', '0', 'wtdchina', '威时沛运', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('359', '0', 'wzhaunyun', '微转运', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('360', '0', 'gswtkd', '万通快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('361', '0', 'wotu', '渥途国际速运', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('362', '0', 'xiyoute', '希优特快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('363', '0', 'xilaikd', '喜来快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('364', '0', 'xsrd', '鑫世锐达', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('365', '0', 'xtb', '鑫通宝物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('366', '0', 'xintianjie', '信天捷快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('367', '0', 'xaetc', '西安胜峰', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('368', '0', 'xianfeng', '先锋快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('369', '0', 'sunspeedy', '新速航', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('370', '0', 'xipost', '西邮寄', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('371', '0', 'sinatone', '信联通', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('372', '0', 'sunjex', '新杰物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('373', '0', 'yundaexus', '韵达美国件', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('374', '0', 'yxwl', '宇鑫物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('375', '0', 'yitongda', '易通达', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('376', '0', 'yiqiguojiwuliu', '一柒物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('377', '0', 'yilingsuyun', '亿领速运', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('378', '0', 'yujiawuliu', '煜嘉物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('379', '0', 'gml', '英脉物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('380', '0', 'leopard', '云豹国际货运', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('381', '0', 'czwlyn', '云南中诚', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('382', '0', 'sdyoupei', '优配速运', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('383', '0', 'yongchang', '永昌物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('384', '0', 'yufeng', '御风速运', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('385', '0', 'yamaxunwuliu', '亚马逊物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('386', '0', 'yousutongda', '优速通达', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('387', '0', 'yishunhang', '亿顺航', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('388', '0', 'yongwangda', '永旺达快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('389', '0', 'ecmscn', '易满客', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('390', '0', 'yingchao', '英超物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('391', '0', 'edlogistics', '益递物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('392', '0', 'yyexpress', '远洋国际', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('393', '0', 'onehcang', '一号仓', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('394', '0', 'ycgky', '远成快运', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('395', '0', 'lineone', '一号线', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('396', '0', 'ypsd', '壹品速递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('397', '0', 'vipexpress', '鹰运国际速递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('398', '0', 'el56', '易联通达物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('399', '0', 'yyqc56', '一运全成物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('400', '0', 'zhongtie', '中铁快运', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('401', '0', 'ZTKY', '中铁物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('402', '0', 'zzjh', '郑州建华快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('403', '0', 'zhongruisudi', '中睿速递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('404', '0', 'zhongwaiyun', '中外运速递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('405', '0', 'zengyisudi', '增益速递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('406', '0', 'sujievip', '郑州速捷', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('407', '0', 'zhichengtongda', '至诚通达快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('408', '0', 'zhdwl', '众辉达物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('409', '0', 'kuachangwuliu', '直邮易', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('410', '0', 'topspeedex', '中运全速', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('411', '0', 'otobv', '中欧快运', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('412', '0', 'zsky123', '准实快运', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('413', '0', 'donghong', '东红物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('414', '0', 'kuaiyouda', '快优达速递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('415', '0', 'balunzhi', '巴伦支快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('416', '0', 'hutongwuliu', '户通物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('417', '0', 'xianchenglian', '西安城联速递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('418', '0', 'youbijia', '邮必佳', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('419', '0', 'feiyuan', '飞远物流', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('420', '0', 'chengji', '城际速递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('421', '0', 'huaqi', '华企快运', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('422', '0', 'yibang', '一邦快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('423', '0', 'citylink', 'CityLink快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('424', '0', 'meixi', '美西快递', '1', '0');
INSERT INTO `__BWPREFIX__sys_express` VALUES ('425', '0', 'acs', 'ACS', '1', '0');

-- ----------------------------
-- Table structure for bw_sys_log
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__sys_log`;
CREATE TABLE `__BWPREFIX__sys_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '管理员操作记录ID',
  `admin_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '管理员id',
  `member_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '租户id',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `name` varchar(64) NOT NULL DEFAULT '' COMMENT '访问者',
  `username` varchar(20) NOT NULL DEFAULT '' COMMENT '账号',
  `path` varchar(128) NOT NULL DEFAULT '' COMMENT '链接',
  `menu_path` varchar(100) CHARACTER SET utf8mb4 NOT NULL DEFAULT '' COMMENT '路由地址',
  `method` varchar(128) NOT NULL DEFAULT '' COMMENT '访问类型',
  `param` text COMMENT '参数',
  `ip` varchar(16) NOT NULL DEFAULT '' COMMENT '登录IP',
  `add_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '操作时间{date}',
  `scopes` varchar(30) NOT NULL DEFAULT 'admin' COMMENT '范围{radio}(admin:总后台,member:租户后台,mini_program:小程序,h5:h5,app:app,official:公众号)',
  `city_name` varchar(255) NOT NULL DEFAULT '' COMMENT '城市名',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `admin_id` (`admin_id`) USING BTREE,
  KEY `add_time` (`add_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT='管理员操作记录表';

-- ----------------------------
-- Records of bw_sys_log
-- ----------------------------

-- ----------------------------
-- Table structure for bw_sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__sys_notice`;
CREATE TABLE `__BWPREFIX__sys_notice` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `cate_id` int(11) NOT NULL DEFAULT '0' COMMENT '公告分类id',
  `content` text NOT NULL COMMENT '内容{editor}',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '状态{switch}(0:关闭1:开启)',
  `add_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '发布时间{date}',
  `title` varchar(255) NOT NULL COMMENT '标题',
  PRIMARY KEY (`id`),
  KEY `cate_id` (`cate_id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT='系统资讯';

-- ----------------------------
-- Records of bw_sys_notice
-- ----------------------------
INSERT INTO `__BWPREFIX__sys_notice` (`id`, `cate_id`, `content`, `status`, `add_time`, `title`) VALUES (null, '1', '<p>介绍</p>\n\n<p>&ldquo;bwsaas多端SAAS平台运营系统&rdquo;接入微信开放平台（第三方服务商）,微信公众号管理及微信小程序一键授权发布，具备多租户管理、多应用上架购买、多终端（公众号，H5，小程序，PC，APP）可接入、强大的权限节点控制（管理员权限，租户及租户应用权限）、 强大的一键CRUD生成代码（页面JS,控制器controller，模型及关联模型model）、基于ThinkPHP6及layui快速布局扩展等等特性、详细的二次开发及系统使用说明文档！让您不管是自己学习使用还是公司运营，轻松快速完成二次的开发集成。</p>\n\n<p>软件架构</p>\n\n<ul>\n	<li>技术：Thinkphp6.X+Layui2.5+easywechat4.X(微信开发框架)</li>\n	<li>后台：租户管理后台(域名+/manage/member/login)和平台管理后台(域名+/manage/admin/login)</li>\n	<li>环境（建议使用宝塔面板一键搭建lnmp）：</li>\n	<li>系统：Windows,Linux(推荐)</li>\n	<li>PHP &gt; 7.2(推荐7.4)</li>\n	<li>Nginx &gt;=1.14或者apache &gt;=2.4</li>\n	<li>Mysql &gt;=5.7 数据库引擎InnoDB</li>\n	<li>Redis &gt;=5.0</li>\n	<li>PHP扩展 fileinfo,curl,openssl,simpleXML,redis,mbstring,mysqli,openssl,gd,zip</li>\n</ul>\n', '1', '1614138209', '布网云SAAS应用开发管理系统');
INSERT INTO `__BWPREFIX__sys_notice` (`id`, `cate_id`, `content`, `status`, `add_time`, `title`) VALUES (null, '1', '<p>本次共更新135个文件</p>\n\n<p>增加<br />\n1 home应用参考demo（自行开发自己的首页）<br />\n2 第三方平台小程序代码模板管理，支持同步第三方平台模板库<br />\n3 租户小程序应用单独上架升级代码管理（可对租户应用的升级粒化控制使用哪个代码模板）<br />\n4 增加小程序订阅消息发布（公众号模板消息待更新）<br />\n5 开放数据库管理备份插件<br />\n等等<br />\n优化<br />\n1 整理安装文件，删除节点权限判断Auth自定义标签，整理代码注释排版<br />\n2 bwsaas更新安装框架，屏蔽日志队列<br />\n3 应用购买逻辑完善，修复小程序代码上架BUG<br />\n4 应用购买逻辑完善，修复小程序代码上架BUG<br />\n5 增加2个新表，删除5个无用表，SQL待更新<br />\n6 修复优惠券海报：调整场景值长度<br />\n等等</p>\n', '1', '1617697858', 'bwsaas公众号小程序APP开发框架大更新-假期后第一版');

-- ----------------------------
-- Table structure for bw_sys_notice_category
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__sys_notice_category`;
CREATE TABLE `__BWPREFIX__sys_notice_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(255) DEFAULT NULL COMMENT '分类名',
  `image` varchar(255) CHARACTER SET utf8 DEFAULT '' COMMENT '缩略图{image}',
  `weight` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '权重',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '状态{radio}(0:隐藏,1:正常)',
  `create_time` int(10) unsigned NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `label` varchar(255) DEFAULT NULL COMMENT '标签',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT='系统资讯分类';

-- ----------------------------
-- Records of bw_sys_notice_category
-- ----------------------------
INSERT INTO `__BWPREFIX__sys_notice_category` (`id`, `name`, `image`, `weight`, `status`, `create_time`, `update_time`, `label`) VALUES ('1', '系统公告', '', '0', '1', '1617842338', '1617842338', NULL);
INSERT INTO `__BWPREFIX__sys_notice_category` (`id`, `name`, `image`, `weight`, `status`, `create_time`, `update_time`, `label`) VALUES ('2', '平台资讯', '', '0', '1', '1617842917', '1617842917', NULL);


-- ----------------------------
-- Table structure for bw_sys_region
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__sys_region`;
CREATE TABLE `__BWPREFIX__sys_region` (
  `id` int(6) NOT NULL COMMENT '序号',
  `name` varchar(50) NOT NULL COMMENT '名称',
  `pid` int(6) NOT NULL COMMENT '上级id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT='城市列表';

-- ----------------------------
-- Records of bw_sys_region
-- ----------------------------

-- ----------------------------
-- Table structure for bw_sys_uploadfile
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__sys_uploadfile`;
CREATE TABLE `__BWPREFIX__sys_uploadfile` (
  `id` int(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `upload_type` varchar(20) NOT NULL DEFAULT 'local' COMMENT '存储位置',
  `original_name` varchar(255) DEFAULT NULL COMMENT '文件原名',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '物理路径',
  `image_width` varchar(30) NOT NULL DEFAULT '' COMMENT '宽度',
  `image_height` varchar(30) NOT NULL DEFAULT '' COMMENT '高度',
  `image_type` varchar(30) NOT NULL DEFAULT '' COMMENT '图片类型',
  `image_frames` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '图片帧数',
  `mime_type` varchar(100) NOT NULL DEFAULT '' COMMENT 'mime类型',
  `file_size` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文件大小',
  `file_ext` varchar(100) DEFAULT NULL,
  `sha1` varchar(40) NOT NULL DEFAULT '' COMMENT '文件 sha1编码',
  `create_time` int(10) DEFAULT NULL COMMENT '创建日期',
  `update_time` int(10) DEFAULT NULL COMMENT '更新时间',
  `upload_time` int(10) DEFAULT NULL COMMENT '上传时间',
  `member_id` int(10) NOT NULL DEFAULT '0' COMMENT '租户id',
  PRIMARY KEY (`id`),
  KEY `upload_type` (`upload_type`),
  KEY `original_name` (`original_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci ROW_FORMAT=COMPACT COMMENT='上传文件表';

-- ----------------------------
-- Records of bw_sys_uploadfile
-- ----------------------------

-- ----------------------------
-- Table structure for bw_system_config
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__system_config`;
CREATE TABLE `__BWPREFIX__system_config` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `type` varchar(20) DEFAULT '' COMMENT '分类',
  `name` varchar(100) DEFAULT '' COMMENT '配置名',
  `value` varchar(500) DEFAULT '' COMMENT '配置值',
  PRIMARY KEY (`id`),
  KEY `idx_system_config_type` (`type`) USING BTREE,
  KEY `idx_system_config_name` (`name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT='系统-配置';

-- ----------------------------
-- Records of bw_system_config
-- ----------------------------
INSERT INTO `__BWPREFIX__system_config` VALUES ('1', 'base', 'site_copy', '©版权所有 2019-2020 科技');
INSERT INTO `__BWPREFIX__system_config` VALUES ('2', 'base', 'site_name', 'ThinkAdmin');
INSERT INTO `__BWPREFIX__system_config` VALUES ('3', 'base', 'site_icon', 'https://v6.thinkadmin.top/upload/f47b8fe06e38ae99/08e8398da45583b9.png');
INSERT INTO `__BWPREFIX__system_config` VALUES ('4', 'base', 'site_copy', '©版权所有 2019-2020 楚才科技');
INSERT INTO `__BWPREFIX__system_config` VALUES ('5', 'base', 'app_name', 'ThinkAdmin');
INSERT INTO `__BWPREFIX__system_config` VALUES ('6', 'base', 'app_version', 'v6.0');
INSERT INTO `__BWPREFIX__system_config` VALUES ('7', 'base', 'miitbeian', '粤ICP备16006642号-2');
INSERT INTO `__BWPREFIX__system_config` VALUES ('8', 'storage', 'qiniu_http_protocol', 'http');
INSERT INTO `__BWPREFIX__system_config` VALUES ('9', 'storage', 'type', 'local');
INSERT INTO `__BWPREFIX__system_config` VALUES ('10', 'storage', 'allow_exts', 'doc,gif,icon,jpg,mp3,mp4,p12,pem,png,rar,xls,xlsx');
INSERT INTO `__BWPREFIX__system_config` VALUES ('11', 'storage', 'qiniu_region', '华东');
INSERT INTO `__BWPREFIX__system_config` VALUES ('12', 'storage', 'qiniu_bucket', '');
INSERT INTO `__BWPREFIX__system_config` VALUES ('13', 'storage', 'qiniu_http_domain', '');
INSERT INTO `__BWPREFIX__system_config` VALUES ('14', 'storage', 'qiniu_access_key', '');
INSERT INTO `__BWPREFIX__system_config` VALUES ('15', 'storage', 'qiniu_secret_key', '');
INSERT INTO `__BWPREFIX__system_config` VALUES ('16', 'wechat', 'type', 'api');
INSERT INTO `__BWPREFIX__system_config` VALUES ('17', 'wechat', 'token', '');
INSERT INTO `__BWPREFIX__system_config` VALUES ('18', 'wechat', 'appid', '');
INSERT INTO `__BWPREFIX__system_config` VALUES ('19', 'wechat', 'appsecret', '');
INSERT INTO `__BWPREFIX__system_config` VALUES ('20', 'wechat', 'encodingaeskey', '');
INSERT INTO `__BWPREFIX__system_config` VALUES ('21', 'wechat', 'thr_appid', '');
INSERT INTO `__BWPREFIX__system_config` VALUES ('22', 'wechat', 'thr_appkey', '');
INSERT INTO `__BWPREFIX__system_config` VALUES ('23', 'wechat', 'mch_id', '');
INSERT INTO `__BWPREFIX__system_config` VALUES ('24', 'wechat', 'mch_key', '');
INSERT INTO `__BWPREFIX__system_config` VALUES ('25', 'wechat', 'mch_ssl_type', 'pem');
INSERT INTO `__BWPREFIX__system_config` VALUES ('26', 'wechat', 'mch_ssl_p12', '');
INSERT INTO `__BWPREFIX__system_config` VALUES ('27', 'wechat', 'mch_ssl_key', '');
INSERT INTO `__BWPREFIX__system_config` VALUES ('28', 'wechat', 'mch_ssl_cer', '');
INSERT INTO `__BWPREFIX__system_config` VALUES ('29', 'storage', 'alioss_http_protocol', 'http');
INSERT INTO `__BWPREFIX__system_config` VALUES ('30', 'storage', 'alioss_point', 'oss-cn-hangzhou.aliyuncs.com');
INSERT INTO `__BWPREFIX__system_config` VALUES ('31', 'storage', 'alioss_bucket', '');
INSERT INTO `__BWPREFIX__system_config` VALUES ('32', 'storage', 'alioss_http_domain', '');
INSERT INTO `__BWPREFIX__system_config` VALUES ('33', 'storage', 'alioss_access_key', '');
INSERT INTO `__BWPREFIX__system_config` VALUES ('34', 'storage', 'alioss_secret_key', '');


-- ----------------------------
-- Table structure for bw_token
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__token`;
CREATE TABLE `__BWPREFIX__token` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` text NOT NULL,
  `user_id` int(10) NOT NULL COMMENT '用户id',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  `expires_time` int(11) NOT NULL COMMENT '到期时间',
  `login_ip` varchar(60) CHARACTER SET utf8 DEFAULT NULL COMMENT '登录ip',
  `scopes` varchar(100) DEFAULT NULL COMMENT '登录类型',
  PRIMARY KEY (`id`),
  KEY `uid` (`user_id`),
  KEY `login_ip` (`login_ip`),
  KEY `scopes` (`scopes`),
  KEY `create_time` (`create_time`),
  KEY `expires_time` (`expires_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT='token表';

-- ----------------------------
-- Records of bw_token
-- ----------------------------

-- ----------------------------
-- Table structure for bw_user
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__user`;
CREATE TABLE `__BWPREFIX__user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `invite_code` varchar(50) DEFAULT '0',
  `member_miniapp_id` int(11) DEFAULT '0' COMMENT '应用',
  `password` varchar(255) DEFAULT NULL COMMENT '密码',
  `mobile` varchar(20) DEFAULT '' COMMENT '手机号',
  `unionid` varchar(32) DEFAULT '' COMMENT '开放平台unionid',
  `miniapp_uid` varchar(32) DEFAULT '' COMMENT 'openid',
  `official_uid` varchar(32) DEFAULT '' COMMENT '公众号用户openid',
  `session_key` varchar(255) DEFAULT '',
  `safe_password` varchar(255) DEFAULT '' COMMENT '支付密码',
  `nickname` varchar(50) DEFAULT '' COMMENT '昵称',
  `avatar` varchar(255) DEFAULT '' COMMENT '头像{image}',
  `login_ip` varchar(20) DEFAULT '' COMMENT '登录IP',
  `login_time` int(11) unsigned zerofill DEFAULT '00000000000' COMMENT '登录时间{date}',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态{switch}(0:锁定,1:正常)',
  `is_delete` tinyint(1) DEFAULT '0' COMMENT '是否删除{switch}(0:正常,1:删除)',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间{date}',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间{date}',
  `integral` decimal(10,2) unsigned DEFAULT '0.00' COMMENT '积分',
  `money` decimal(10,2) unsigned DEFAULT '0.00' COMMENT '用户余额',
  `brokerage` decimal(10,2) unsigned DEFAULT '0.00' COMMENT '佣金',
  `username` varchar(50) DEFAULT '' COMMENT '用户名',
  PRIMARY KEY (`id`),
  KEY `CODE` (`invite_code`),
  KEY `MINIAPP_ID` (`member_miniapp_id`),
  KEY `WECHAT_ID` (`miniapp_uid`),
  KEY `OPEN_ID` (`official_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT='APP用户表';

-- ----------------------------
-- Records of bw_user
-- ----------------------------

-- ----------------------------
-- Table structure for bw_user_address
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__user_address`;
CREATE TABLE `__BWPREFIX__user_address` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户地址id',
  `user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `real_name` varchar(32) NOT NULL DEFAULT '' COMMENT '收货人姓名',
  `mobile` varchar(16) NOT NULL DEFAULT '' COMMENT '收货人电话',
  `province` varchar(64) NOT NULL DEFAULT '' COMMENT '收货人所在省',
  `city` varchar(64) NOT NULL DEFAULT '' COMMENT '收货人所在市',
  `city_id` int(11) NOT NULL DEFAULT '0' COMMENT '城市id',
  `city_code` int(11) NOT NULL DEFAULT '0' COMMENT '城市表city_id',
  `district` varchar(64) NOT NULL DEFAULT '' COMMENT '收货人所在区',
  `detail` varchar(256) NOT NULL DEFAULT '' COMMENT '收货人详细地址',
  `post_code` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '邮编',
  `longitude` varchar(16) NOT NULL DEFAULT '0' COMMENT '经度',
  `latitude` varchar(16) NOT NULL DEFAULT '0' COMMENT '纬度',
  `is_default` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否默认',
  `is_del` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '添加时间',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `member_miniapp_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `uid` (`user_id`) USING BTREE,
  KEY `is_default` (`is_default`) USING BTREE,
  KEY `is_del` (`is_del`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT='用户地址表';

-- ----------------------------
-- Records of bw_user_address
-- ----------------------------

-- ----------------------------
-- Table structure for bw_user_bill
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__user_bill`;
CREATE TABLE `__BWPREFIX__user_bill` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户账单id',
  `uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID',
  `link_id` varchar(32) NOT NULL DEFAULT '0' COMMENT '关联id',
  `pm` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '收支{radio}(0:支出,1:获得)',
  `title` varchar(64) NOT NULL DEFAULT '' COMMENT '账单标题',
  `category` varchar(64) NOT NULL DEFAULT '' COMMENT '明细币种{radio}{radio}(money:余额,integral:积分,brokerage:佣金)',
  `type` varchar(64) NOT NULL DEFAULT '' COMMENT '明细类型{radio}(money:余额,integral:积分,brokerage:佣金)',
  `number` decimal(8,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '明细数字',
  `balance` decimal(8,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '剩余',
  `mark` varchar(512) NOT NULL DEFAULT '' COMMENT '备注',
  `add_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '添加时间{date}',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态{radio}(0:待确认,1:有效,-1:无效)',
  `take` tinyint(1) NOT NULL DEFAULT '0' COMMENT '收货状态{radio}(0:未收货,1:已收货)',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `openid` (`uid`) USING BTREE,
  KEY `status` (`status`) USING BTREE,
  KEY `add_time` (`add_time`) USING BTREE,
  KEY `pm` (`pm`) USING BTREE,
  KEY `type` (`category`,`type`,`link_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT='用户账单表';

-- ----------------------------
-- Records of bw_user_bill
-- ----------------------------

-- ----------------------------
-- Table structure for bw_user_extract
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__user_extract`;
CREATE TABLE `__BWPREFIX__user_extract` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` text NOT NULL COMMENT '用户ID',
  `bank_name` varchar(60) DEFAULT NULL COMMENT '银行名称',
  `bank_username` varchar(60) DEFAULT NULL COMMENT '银行收款人姓名',
  `bank_zone` varchar(100) DEFAULT NULL COMMENT '开户地址',
  `bank_detail` varchar(100) DEFAULT NULL COMMENT '详细开户地址',
  `bank_card` varchar(255) DEFAULT '' COMMENT '银行卡号',
  `city_id` int(11) NOT NULL DEFAULT '0' COMMENT '城市id',
  `wx_name` varchar(50) DEFAULT NULL COMMENT '微信账户人姓名',
  `wxImag` varchar(300) DEFAULT NULL COMMENT '微信收款码{image}',
  `ali_name` varchar(50) DEFAULT NULL COMMENT '阿里账户人姓名',
  `ali_account` varchar(100) DEFAULT NULL COMMENT '支付宝账号',
  `aliImag` varchar(300) DEFAULT NULL COMMENT '支付宝收款码{image}',
  `add_time` int(11) NOT NULL COMMENT '添加时间{date}',
  `deletetime` int(11) DEFAULT '0' COMMENT '删除时间{date}',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='用户提现表';

-- ----------------------------
-- Records of bw_user_extract
-- ----------------------------

-- ----------------------------
-- Table structure for bw_user_extract_log
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__user_extract_log`;
CREATE TABLE `__BWPREFIX__user_extract_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL COMMENT '用户id',
  `real_name` varchar(64) DEFAULT NULL COMMENT '提现人姓名',
  `bank_name` varchar(60) DEFAULT NULL COMMENT '银行名称',
  `bank_zone` varchar(100) DEFAULT NULL COMMENT '开户地址',
  `bank_detail` varchar(100) DEFAULT NULL COMMENT '详细开户地址',
  `bank_card` varchar(255) DEFAULT '' COMMENT '银行卡号',
  `city_id` int(11) NOT NULL DEFAULT '0' COMMENT '城市id',
  `wxImag` varchar(300) DEFAULT NULL COMMENT '微信收款码{image}',
  `aliImag` varchar(300) DEFAULT NULL COMMENT '支付宝收款码{image}',
  `ali_account` varchar(100) DEFAULT NULL COMMENT '支付宝账号',
  `extract_price` decimal(8,2) unsigned DEFAULT '0.00' COMMENT '提现金额',
  `fee` decimal(8,2) unsigned DEFAULT NULL COMMENT '手续费',
  `before` decimal(8,2) unsigned DEFAULT '0.00' COMMENT '提现前数额',
  `after` decimal(8,2) unsigned DEFAULT '0.00' COMMENT '提现后数额',
  `mark` varchar(512) DEFAULT NULL COMMENT '备注',
  `extract_type` enum('bank','alipay','wx') DEFAULT 'bank' COMMENT '提现方式{radio}(bank:银行卡,alipa:支付宝,wx:微信)',
  `type` enum('money') DEFAULT 'money' COMMENT '提现类型{radio}(money:余额)',
  `status` enum('-1','0','1') DEFAULT '0' COMMENT '提现状态{radio}(-1:未通过,0:审核中,1:已提现)',
  `fail_msg` varchar(128) DEFAULT NULL COMMENT '提现失败原因',
  `updatetime` int(10) unsigned DEFAULT NULL COMMENT '更新时间{date}',
  `createtime` int(10) unsigned DEFAULT NULL COMMENT '添加时间{date}',
  `deletetime` int(10) DEFAULT NULL COMMENT '删除时间{date}',
  `return_price` decimal(8,2) unsigned DEFAULT '0.00' COMMENT '退款金额',
  PRIMARY KEY (`id`),
  KEY `extract_type` (`extract_type`) USING BTREE,
  KEY `status` (`status`) USING BTREE,
  KEY `add_time` (`createtime`) USING BTREE,
  KEY `openid` (`user_id`) USING BTREE,
  KEY `fail_time` (`updatetime`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='用户提现记录表';

-- ----------------------------
-- Records of bw_user_extract_log
-- ----------------------------

-- ----------------------------
-- Table structure for bw_user_recharge
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__user_recharge`;
CREATE TABLE `__BWPREFIX__user_recharge` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(10) DEFAULT NULL COMMENT '充值用户ID',
  `order_id` varchar(32) DEFAULT NULL COMMENT '订单号',
  `price` decimal(8,2) DEFAULT NULL COMMENT '充值金额',
  `give_price` decimal(8,2) NOT NULL DEFAULT '0.00' COMMENT '购买赠送金额',
  `recharge_type` varchar(32) DEFAULT NULL COMMENT '充值类型{radio}(mini_program:微信充值)',
  `paid` tinyint(1) DEFAULT '0' COMMENT '是否充值{switch}(0:未充值,1:已充值)',
  `pay_time` int(10) DEFAULT NULL COMMENT '充值支付时间{date}',
  `add_time` int(12) DEFAULT NULL COMMENT '充值时间{date}',
  `refund_price` decimal(10,2) DEFAULT '0.00' COMMENT '退款金额',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `order_id` (`order_id`) USING BTREE,
  KEY `uid` (`uid`) USING BTREE,
  KEY `recharge_type` (`recharge_type`) USING BTREE,
  KEY `paid` (`paid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT='用户充值表';

-- ----------------------------
-- Records of bw_user_recharge
-- ----------------------------

-- ----------------------------
-- Table structure for bw_wechat_fans
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__wechat_fans`;
CREATE TABLE `__BWPREFIX__wechat_fans` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `appid` varchar(50) DEFAULT '' COMMENT '公众号APPID',
  `unionid` varchar(100) DEFAULT '' COMMENT '粉丝unionid',
  `openid` varchar(100) DEFAULT '' COMMENT '粉丝openid',
  `tagid_list` varchar(100) DEFAULT '' COMMENT '粉丝标签id',
  `is_black` tinyint(1) unsigned DEFAULT '0' COMMENT '是否为黑名单状态',
  `subscribe` tinyint(1) unsigned DEFAULT '0' COMMENT '关注状态(0未关注,1已关注)',
  `nickname` varchar(200) DEFAULT '' COMMENT '用户昵称',
  `sex` tinyint(1) unsigned DEFAULT '0' COMMENT '用户性别(1男性,2女性,0未知)',
  `country` varchar(50) DEFAULT '' COMMENT '用户所在国家',
  `province` varchar(50) DEFAULT '' COMMENT '用户所在省份',
  `city` varchar(50) DEFAULT '' COMMENT '用户所在城市',
  `language` varchar(50) DEFAULT '' COMMENT '用户的语言(zh_CN)',
  `headimgurl` varchar(500) DEFAULT '' COMMENT '用户头像',
  `subscribe_time` bigint(20) unsigned DEFAULT '0' COMMENT '关注时间',
  `subscribe_at` datetime DEFAULT NULL COMMENT '关注时间',
  `remark` varchar(50) DEFAULT '' COMMENT '备注',
  `subscribe_scene` varchar(200) DEFAULT '' COMMENT '扫码关注场景',
  `qr_scene` varchar(100) DEFAULT '' COMMENT '二维码场景值',
  `qr_scene_str` varchar(200) DEFAULT '' COMMENT '二维码场景内容',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `index_wechat_fans_openid` (`openid`) USING BTREE,
  KEY `index_wechat_fans_unionid` (`unionid`) USING BTREE,
  KEY `index_wechat_fans_is_back` (`is_black`) USING BTREE,
  KEY `index_wechat_fans_subscribe` (`subscribe`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT='微信-粉丝';

-- ----------------------------
-- Records of bw_wechat_fans
-- ----------------------------

-- ----------------------------
-- Table structure for bw_wechat_fans_tags
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__wechat_fans_tags`;
CREATE TABLE `__BWPREFIX__wechat_fans_tags` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '标签ID',
  `appid` varchar(50) DEFAULT '' COMMENT '公众号APPID',
  `name` varchar(35) DEFAULT NULL COMMENT '标签名称',
  `count` bigint(20) unsigned DEFAULT '0' COMMENT '总数',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  KEY `index_wechat_fans_tags_id` (`id`) USING BTREE,
  KEY `index_wechat_fans_tags_appid` (`appid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT='微信-标签';

-- ----------------------------
-- Records of bw_wechat_fans_tags
-- ----------------------------

-- ----------------------------
-- Table structure for bw_wechat_keys
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__wechat_keys`;
CREATE TABLE `__BWPREFIX__wechat_keys` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `appid` char(100) DEFAULT '' COMMENT '公众号APPID',
  `type` varchar(20) NOT NULL DEFAULT '' COMMENT '类型(text,image,news)',
  `keys` varchar(100) NOT NULL DEFAULT '' COMMENT '关键字',
  `content` text COMMENT '文本内容',
  `image_url` varchar(255) DEFAULT '' COMMENT '图片链接',
  `voice_url` varchar(255) DEFAULT '' COMMENT '语音链接',
  `music_title` varchar(100) DEFAULT '' COMMENT '音乐标题',
  `music_url` varchar(255) DEFAULT '' COMMENT '音乐链接',
  `music_image` varchar(255) DEFAULT '' COMMENT '缩略图片',
  `music_desc` varchar(255) DEFAULT '' COMMENT '音乐描述',
  `video_title` varchar(100) DEFAULT '' COMMENT '视频标题',
  `video_url` varchar(255) DEFAULT '' COMMENT '视频URL',
  `video_desc` varchar(255) DEFAULT '' COMMENT '视频描述',
  `news_id` bigint(20) unsigned DEFAULT '0' COMMENT '图文ID',
  `sort` bigint(20) unsigned DEFAULT '0' COMMENT '排序字段',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '状态(0禁用,1启用)',
  `create_by` bigint(20) unsigned DEFAULT '0' COMMENT '创建人',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `member_miniapp_id` int(11) NOT NULL DEFAULT '0' COMMENT '所属租户应用的id',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `index_wechat_keys_appid` (`appid`) USING BTREE,
  KEY `index_wechat_keys_type` (`type`) USING BTREE,
  KEY `index_wechat_keys_keys` (`keys`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT='微信-关键字';

-- ----------------------------
-- Records of bw_wechat_keys
-- ----------------------------

-- ----------------------------
-- Table structure for bw_wechat_keyword
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__wechat_keyword`;
CREATE TABLE `__BWPREFIX__wechat_keyword` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `member_miniapp_id` int(11) NOT NULL COMMENT '租户应用id',
  `is_miniapp` enum('0','1') NOT NULL DEFAULT '0' COMMENT '小程序{switch}(0:否,1:是)',
  `type` enum('text','image') NOT NULL DEFAULT 'text' COMMENT '类型{select}(text:文本,image:图片)',
  `keyword` varchar(100) NOT NULL DEFAULT '' COMMENT '关键字',
  `title` varchar(255) DEFAULT '' COMMENT '标题',
  `url` varchar(255) DEFAULT '' COMMENT '网址',
  `image` varchar(255) DEFAULT '' COMMENT '图片',
  `content` text COMMENT '文本{editor}',
  `media_id` varchar(255) DEFAULT NULL COMMENT '资源id',
  `media` text COMMENT '资源',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  `update_time` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `member_miniapp_id` (`member_miniapp_id`),
  KEY `keyword` (`keyword`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT='微信公众号关键字服务';

-- ----------------------------
-- Records of bw_wechat_keyword
-- ----------------------------

-- ----------------------------
-- Table structure for bw_wechat_media
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__wechat_media`;
CREATE TABLE `__BWPREFIX__wechat_media` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `appid` varchar(100) DEFAULT '' COMMENT '公众号ID',
  `md5` varchar(32) DEFAULT '' COMMENT '文件md5',
  `type` varchar(20) DEFAULT '' COMMENT '媒体类型',
  `media_id` varchar(100) DEFAULT '' COMMENT '永久素材MediaID',
  `local_url` varchar(300) DEFAULT '' COMMENT '本地文件链接',
  `media` varchar(255) DEFAULT '' COMMENT '素材media数据',
  `media_url` varchar(300) DEFAULT '' COMMENT '远程图片链接',
  `create_at` int(12) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `index_wechat_media_appid` (`appid`) USING BTREE,
  KEY `index_wechat_media_md5` (`md5`) USING BTREE,
  KEY `index_wechat_media_type` (`type`) USING BTREE,
  KEY `index_wechat_media_media_id` (`media_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT='微信-素材';

-- ----------------------------
-- Records of bw_wechat_media
-- ----------------------------

-- ----------------------------
-- Table structure for bw_wechat_message_log
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__wechat_message_log`;
CREATE TABLE `__BWPREFIX__wechat_message_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '类型{radio}(0:小程序订阅消息,1:公众号模板消息)',
  `member_miniapp_id` int(10) unsigned NOT NULL COMMENT '租户应用ID',
  `user_id` int(10) unsigned NOT NULL COMMENT '用户ID',
  `key` varchar(20) NOT NULL DEFAULT '' COMMENT '模板标识',
  `param` text NOT NULL COMMENT '参数',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态{radio}(0:失败,1:成功)',
  `fail_msg` varchar(255) NOT NULL DEFAULT '' COMMENT '失败原因',
  `create_time` int(10) unsigned NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT='微信消息记录';

-- ----------------------------
-- Records of bw_wechat_message_log
-- ----------------------------

-- ----------------------------
-- Table structure for bw_wechat_message_template
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__wechat_message_template`;
CREATE TABLE `__BWPREFIX__wechat_message_template` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `member_miniapp_id` int(10) unsigned NOT NULL COMMENT '租户应用ID',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '类型{radio}(0:小程序订阅消息,1:公众号模板消息)',
  `name` varchar(100) NOT NULL DEFAULT '' COMMENT '模板名',
  `key` varchar(20) NOT NULL DEFAULT '' COMMENT '模板标识',
  `template_id` varchar(50) NOT NULL DEFAULT '' COMMENT '模板ID',
  `content` varchar(255) NOT NULL DEFAULT '' COMMENT '回复内容',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '状态{radio}(0:关闭,1:开启)',
  `create_time` int(10) unsigned NOT NULL COMMENT '创建时间',
  `update_time` int(10) unsigned NOT NULL COMMENT '更新时间',
  `delete_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '删除时间',
  PRIMARY KEY (`id`),
  KEY `member_miniapp_id` (`member_miniapp_id`,`type`,`key`,`status`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT='微信消息模板';

-- ----------------------------
-- Records of bw_wechat_message_template
-- ----------------------------

-- ----------------------------
-- Table structure for bw_wechat_news
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__wechat_news`;
CREATE TABLE `__BWPREFIX__wechat_news` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `media_id` varchar(100) DEFAULT '' COMMENT '永久素材MediaID',
  `local_url` varchar(300) DEFAULT '' COMMENT '永久素材外网URL',
  `article_id` varchar(60) DEFAULT '' COMMENT '关联图文ID(用英文逗号做分割)',
  `is_deleted` tinyint(1) unsigned DEFAULT '0' COMMENT '删除状态(0未删除,1已删除)',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_by` bigint(20) DEFAULT NULL COMMENT '创建人',
  `member_miniapp_id` int(11) DEFAULT '0' COMMENT '所属租户应用的id',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `index_wechat_news_artcle_id` (`article_id`) USING BTREE,
  KEY `index_wechat_news_media_id` (`media_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT='微信-图文';

-- ----------------------------
-- Records of bw_wechat_news
-- ----------------------------

-- ----------------------------
-- Table structure for bw_wechat_news_article
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__wechat_news_article`;
CREATE TABLE `__BWPREFIX__wechat_news_article` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT '' COMMENT '素材标题',
  `local_url` varchar(300) DEFAULT '' COMMENT '永久素材显示URL',
  `show_cover_pic` tinyint(4) unsigned DEFAULT '0' COMMENT '显示封面(0不显示,1显示)',
  `author` varchar(20) DEFAULT '' COMMENT '文章作者',
  `digest` varchar(300) DEFAULT '' COMMENT '摘要内容',
  `content` longtext COMMENT '图文内容',
  `content_source_url` varchar(200) DEFAULT '' COMMENT '原文地址',
  `read_num` bigint(20) unsigned DEFAULT '0' COMMENT '阅读数量',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT='微信-文章';

-- ----------------------------
-- Records of bw_wechat_news_article
-- ----------------------------

-- ----------------------------
-- Table structure for bw_wechat_notify_queue
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__wechat_notify_queue`;
CREATE TABLE `__BWPREFIX__wechat_notify_queue` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(11) DEFAULT NULL,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `is_send` tinyint(2) DEFAULT NULL,
  `param` text,
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ISSEND` (`is_send`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT='站内信';

-- ----------------------------
-- Records of bw_wechat_notify_queue
-- ----------------------------

-- ----------------------------
-- Table structure for bw_wechat_open_token
-- ----------------------------
DROP TABLE IF EXISTS `__BWPREFIX__wechat_open_token`;
CREATE TABLE `__BWPREFIX__wechat_open_token` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `authorizer_appid` varchar(255) DEFAULT NULL,
  `authorizer_access_token` varchar(255) DEFAULT NULL,
  `authorizer_refresh_token` varchar(255) DEFAULT NULL,
  `expires_in` varchar(255) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci;

-- ----------------------------
-- Records of bw_wechat_open_token
-- ----------------------------


-- ----------------------------
-- 2021/4/29 新增
-- ----------------------------

-- 平台插件中心菜单
INSERT INTO `__BWPREFIX__auth_node` (`id`, `pid`, `app_name`, `type`, `title`, `menu_path`, `name`, `auth_name`, `param`, `target`, `condition`, `ismenu`, `icon`, `remark`, `status`, `create_time`, `update_time`, `sort`, `scopes`) VALUES ('2851', '2850', 'manage', 'system', '插件菜单', '/manage/member/plugin/core/menu', '/manage/member.plugin.Core/menu', 'manage_member_plugin_core_menu', '', '_self', '', '0', '', '', '1', '1619589595', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` (`id`, `pid`, `app_name`, `type`, `title`, `menu_path`, `name`, `auth_name`, `param`, `target`, `condition`, `ismenu`, `icon`, `remark`, `status`, `create_time`, `update_time`, `sort`, `scopes`) VALUES ('2850', '1314', 'manage', 'system', '已购插件', '/manage/member/plugin/core/index', '/manage/member.plugin.Core/index', 'manage_member_plugin_core_index', '', '_self', '', '1', 'fa fa-gamepad', '', '1', '1619513960', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` (`id`, `pid`, `app_name`, `type`, `title`, `menu_path`, `name`, `auth_name`, `param`, `target`, `condition`, `ismenu`, `icon`, `remark`, `status`, `create_time`, `update_time`, `sort`, `scopes`) VALUES ('2846', '2845', 'manage', 'system', '插件菜单', '/manage/admin/plugin/core/menu', '/manage/admin.Plugin.Core/menu', 'manage_admin_plugin_core_menu', '', '_self', '', '0', '', '', '1', '1619429325', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` (`id`, `pid`, `app_name`, `type`, `title`, `menu_path`, `name`, `auth_name`, `param`, `target`, `condition`, `ismenu`, `icon`, `remark`, `status`, `create_time`, `update_time`, `sort`, `scopes`) VALUES ('2845', '188', 'manage', 'system', '我的插件', '/manage/admin/plugin/core/index', '/manage/admin.plugin.Core/index', 'manage_admin_plugin_core_index', '', '_self', '', '1', 'fa fa-gamepad', '', '1', '1619419843', '0', '0', 'admin');
-- 节点菜单名修改
UPDATE `__BWPREFIX__auth_node` SET `title`='系统' WHERE (`id`='186');
UPDATE `__BWPREFIX__auth_node` SET `title`='应用' WHERE (`id`='187');
UPDATE `__BWPREFIX__auth_node` SET `title`='插件' WHERE (`id`='188');
UPDATE `__BWPREFIX__auth_node` SET `title`='系统' WHERE (`id`='828');
UPDATE `__BWPREFIX__auth_node` SET `title`='应用' WHERE (`id`='1172');
UPDATE `__BWPREFIX__auth_node` SET `title`='插件' WHERE (`id`='1314');
UPDATE `__BWPREFIX__auth_node` SET `title`='插件管理' WHERE (`id`='891');
-- 租户基础权限
INSERT INTO `__BWPREFIX__auth_group_node` (`id`, `group_id`, `node_id`, `node_name`, `type`, `auth_name`) VALUES (null, '62', '2850', '/manage/member.plugin.Core/index', 'system', 'manage_member_plugin_core_index');
INSERT INTO `__BWPREFIX__auth_group_node` (`id`, `group_id`, `node_id`, `node_name`, `type`, `auth_name`) VALUES (null, '62', '2851', '/manage/member.plugin.Core/menu', 'system', 'manage_member_plugin_core_menu');
-- 表 bw_plugin 增加logo字段
ALTER TABLE `__BWPREFIX__plugin` ADD COLUMN `logo_image`  varchar(255) NOT NULL DEFAULT '' COMMENT 'Logo' AFTER `type`;

-- ----------------------------
-- 2021 /4 /30 系统附件表加字段
-- ----------------------------
-- 表 __BWPREFIX__sys_uploadfile 增加字段
ALTER TABLE `__BWPREFIX__sys_uploadfile` ADD COLUMN `dir_path`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci  DEFAULT ''  COMMENT '相对路径' AFTER `url`;


-- ----------------------------
-- 2021 /5 /10 数据库数据变更：调整配置菜单
-- ----------------------------
INSERT INTO `__BWPREFIX__auth_node` (`id`, `pid`, `app_name`, `type`, `title`, `menu_path`, `name`, `auth_name`, `param`, `target`, `condition`, `ismenu`, `icon`, `remark`, `status`, `create_time`, `update_time`, `sort`, `scopes`) VALUES ('2463', '186', 'manage', 'system', '开发管理', 'development', 'development', 'evelopment', '', '_self', '', '1', 'fa fa-cogs', '', '1', '1620633056', '0', '0', 'admin');
UPDATE `__BWPREFIX__auth_node` SET `pid`='2463' WHERE (`id`='211');
UPDATE `__BWPREFIX__auth_node` SET `ismenu`='0' WHERE (`id`='206');
UPDATE `__BWPREFIX__auth_node` SET `ismenu`='0' WHERE (`id`='196');
UPDATE `__BWPREFIX__auth_node` SET `pid`='2463', `title`='组合数据', `icon`='fa fa-cubes' WHERE (`id`='210');
UPDATE `__BWPREFIX__auth_node` SET `pid`='2463' WHERE (`id`='193');

-- ----------------------------
-- 2021/5/11 系统配置增加字段
-- ----------------------------
ALTER TABLE `__BWPREFIX__sys_config` ADD COLUMN `plugin_name`  varchar(100)  DEFAULT NULL COMMENT '插件标识' AFTER `dir`;
ALTER TABLE `__BWPREFIX__sys_config_group` ADD COLUMN `plugin_name`  varchar(100)  DEFAULT NULL COMMENT '插件标识' AFTER `dir`;
ALTER TABLE `__BWPREFIX__sys_config_group_data` ADD COLUMN `plugin_name`  varchar(100)  DEFAULT NULL COMMENT '插件标识' AFTER `dir`;
ALTER TABLE `__BWPREFIX__sys_config_tab` ADD COLUMN `plugin_name`  varchar(100)  DEFAULT NULL COMMENT '插件标识' AFTER `dir`;

-- ----------------------------
-- 2021/5/19 user表加头条字段
-- ----------------------------
ALTER TABLE `__BWPREFIX__user` ADD COLUMN `tt_session_key`  varchar(255)  DEFAULT '' COMMENT '头条系小程序session_key' AFTER `username`;
ALTER TABLE `__BWPREFIX__user` ADD COLUMN `tt_uid`  varchar(40)  DEFAULT '' COMMENT '头条用户openid';
ALTER TABLE `__BWPREFIX__user` ADD COLUMN `tt_avatar`  varchar(255)  DEFAULT '' COMMENT '头条头像{image}';
ALTER TABLE `__BWPREFIX__user` ADD COLUMN `tt_unionid`  varchar(40)  DEFAULT '' COMMENT '头条开放平台unionid';
ALTER TABLE `__BWPREFIX__user` ADD COLUMN `tt_nickname`  varchar(50)  DEFAULT '' COMMENT '头条昵称';

-- ----------------------------
-- 2021/5/27 增加应用套餐表 和菜单
-- ----------------------------
CREATE TABLE `__BWPREFIX__miniapp_term` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '套餐售价',
  `days` int(10) NOT NULL DEFAULT '0' COMMENT '套餐天数',
  `name` varchar(255) NOT NULL COMMENT '套餐名称',
  `miniapp_id` int(11) DEFAULT '0' COMMENT '应用id',
  `miniapp_module_id` int(11) DEFAULT NULL COMMENT '基础模块id',
  `desc` text COMMENT '描述',
  `create_time` int(11) DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  `status` int(2) DEFAULT '1' COMMENT '使用状态',
  `dir` varchar(100) DEFAULT NULL COMMENT '应用标识',
  `weight` int(11) DEFAULT '0' COMMENT '权重',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT='应用套餐表';


--  增加微信消息菜单
INSERT INTO `__BWPREFIX__auth_node` (`id`, `pid`, `app_name`, `type`, `title`, `menu_path`, `name`, `auth_name`, `param`, `target`, `condition`, `ismenu`, `icon`, `remark`, `status`, `create_time`, `update_time`, `sort`, `scopes`) VALUES ('2452', '834', 'manage', 'system', '订阅消息发送记录', '/manage/member/wechat/message/log/index', '/manage/member.WechatMessageLog/index', 'manage_member_wechat_message_log_index', '', '_self', '', '0', '', '', '1', '1618306440', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` (`id`, `pid`, `app_name`, `type`, `title`, `menu_path`, `name`, `auth_name`, `param`, `target`, `condition`, `ismenu`, `icon`, `remark`, `status`, `create_time`, `update_time`, `sort`, `scopes`) VALUES ('2453', '2452', 'manage', 'system', '删除', '/manage/member/wechat/message/log/del', '/manage/member.WechatMessageLog/del', 'manage_member_wechat_message_log_del', '', '_self', '', '0', '', '', '1', '1618306880', '0', '0', 'member');
--  增加官网菜单
INSERT INTO `__BWPREFIX__auth_node` (`id`, `pid`, `app_name`, `type`, `title`, `menu_path`, `name`, `auth_name`, `param`, `target`, `condition`, `ismenu`, `icon`, `remark`, `status`, `create_time`, `update_time`, `sort`, `scopes`) VALUES ('2455', '1314', 'manage', 'system', '官网', 'home/index', 'home/index', 'ome_index', '', '_self', '', '1', 'fa fa-home', '', '1', '1618827267', '0', '0', 'member');
--  附件删除功能菜单
INSERT INTO `__BWPREFIX__auth_node` (`id`, `pid`, `app_name`, `type`, `title`, `menu_path`, `name`, `auth_name`, `param`, `target`, `condition`, `ismenu`, `icon`, `remark`, `status`, `create_time`, `update_time`, `sort`, `scopes`) VALUES ('2458', '1391', 'manage', 'system', '文件（带oss）删除', '/manage/publicCommon/delFile', '/manage/publicCommon/delFile', 'manage_publicCommon_delFile', '', '_self', '', '0', '', '', '1', '1619601668', '0', '0', 'admin');

--  应用付费套餐菜单
INSERT INTO `__BWPREFIX__auth_node` (`id`, `pid`, `app_name`, `type`, `title`, `menu_path`, `name`, `auth_name`, `param`, `target`, `condition`, `ismenu`, `icon`, `remark`, `status`, `create_time`, `update_time`, `sort`, `scopes`) VALUES ('2742', '1589', 'manage', 'system', '应用续费', '/manage/member/miniapp/my/renew', '/manage/member.MemberMiniapp/renew', 'manage_member_miniapp_my_renew', '', '_self', '', '0', '', '', '1', '1621914155', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_node` (`id`, `pid`, `app_name`, `type`, `title`, `menu_path`, `name`, `auth_name`, `param`, `target`, `condition`, `ismenu`, `icon`, `remark`, `status`, `create_time`, `update_time`, `sort`, `scopes`) VALUES ('2743', '828', 'manage', 'system', '子账号', '/manage/member', '/manage/member', 'manage_member', '', '_self', '', '1', 'fa fa-street-view', '', '1', '1621929497', '0', '999', 'member');
INSERT INTO `__BWPREFIX__auth_node` (`id`, `pid`, `app_name`, `type`, `title`, `menu_path`, `name`, `auth_name`, `param`, `target`, `condition`, `ismenu`, `icon`, `remark`, `status`, `create_time`, `update_time`, `sort`, `scopes`) VALUES ('2746', '1020', 'manage', 'system', '续费套餐', '/manage/admin/miniapp/term/index', '/manage/admin.MiniappTerm/index', 'manage_admin_miniapp_term_index', '', '_self', '', '0', '', '', '1', '1621999192', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` (`id`, `pid`, `app_name`, `type`, `title`, `menu_path`, `name`, `auth_name`, `param`, `target`, `condition`, `ismenu`, `icon`, `remark`, `status`, `create_time`, `update_time`, `sort`, `scopes`) VALUES ('2747', '2746', 'manage', 'system', '导出', '/manage/admin/miniapp/term/export', '/manage/admin.MiniappTerm/export', 'manage_admin_miniapp_term_export', '', '_self', '', '0', '', '', '1', '1621999243', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` (`id`, `pid`, `app_name`, `type`, `title`, `menu_path`, `name`, `auth_name`, `param`, `target`, `condition`, `ismenu`, `icon`, `remark`, `status`, `create_time`, `update_time`, `sort`, `scopes`) VALUES ('2748', '2746', 'manage', 'system', '新增', '/manage/admin/miniapp/term/add', '/manage/admin.MiniappTerm/add', 'manage_admin_miniapp_term_add', '', '_self', '', '0', '', '', '1', '1621999290', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` (`id`, `pid`, `app_name`, `type`, `title`, `menu_path`, `name`, `auth_name`, `param`, `target`, `condition`, `ismenu`, `icon`, `remark`, `status`, `create_time`, `update_time`, `sort`, `scopes`) VALUES ('2749', '2746', 'manage', 'system', '编辑', '/manage/admin/miniapp/term/edit', '/manage/admin.MiniappTerm/edit', 'manage_admin_miniapp_term_edit', '', '_self', '', '0', '', '', '1', '1621999352', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` (`id`, `pid`, `app_name`, `type`, `title`, `menu_path`, `name`, `auth_name`, `param`, `target`, `condition`, `ismenu`, `icon`, `remark`, `status`, `create_time`, `update_time`, `sort`, `scopes`) VALUES ('2750', '2746', 'manage', 'system', '修改', '/manage/admin/miniapp/term/modify', '/manage/admin.MiniappTerm/modify', 'manage_admin_miniapp_term_modify', '', '_self', '', '0', '', '', '1', '1621999391', '0', '0', 'admin');
INSERT INTO `__BWPREFIX__auth_node` (`id`, `pid`, `app_name`, `type`, `title`, `menu_path`, `name`, `auth_name`, `param`, `target`, `condition`, `ismenu`, `icon`, `remark`, `status`, `create_time`, `update_time`, `sort`, `scopes`) VALUES ('2751', '2746', 'manage', 'system', '删除', '/manage/admin/miniapp/term/del', '/manage/admin.MiniappTerm/del', 'manage_admin_miniapp_term_del', '', '_self', '', '0', '', '', '1', '1621999430', '0', '0', 'admin');
--  更新租户账户菜单数据
UPDATE `__BWPREFIX__auth_node` SET `id`='829', `pid`='2743', `app_name`='manage', `type`='system', `title`='账号列表', `menu_path`='/manage/member.Login/user', `name`='/manage/member.Login/user', `auth_name`='manage_member_Login_user', `param`='', `target`='_self', `condition`='', `ismenu`='1', `icon`='fa fa-user-secret', `remark`='', `status`='1', `create_time`='1590398975', `update_time`='0', `sort`='999', `scopes`='member' WHERE (`id`='829');
UPDATE `__BWPREFIX__auth_node` SET `id`='830', `pid`='', `app_name`='manage', `type`='system', `title`='角色管理', `menu_path`='/manage/member.Role/index', `name`='/manage/member.Role/index', `auth_name`='manage_member_Role_index', `param`='', `target`='_self', `condition`='', `ismenu`='1', `icon`='fa fa-joomla', `remark`='', `status`='1', `create_time`='1590399132', `update_time`='0', `sort`='999', `scopes`='member' WHERE (`id`='830');
-- 表 bw_plugin 增加标签字段c
ALTER TABLE `__BWPREFIX__plugin` ADD COLUMN `label`  varchar(100)  DEFAULT '' COMMENT '插件标签' AFTER `logo_image`;
-- 基础角色赋予权限
INSERT INTO `__BWPREFIX__auth_group_node` (`id`, `group_id`, `node_id`, `node_name`, `type`, `auth_name`) VALUES (null, '62', '2742', '/manage/member.MemberMiniapp/renew', 'system', 'manage_member_miniapp_my_renew');
INSERT INTO `__BWPREFIX__auth_group_node` (`id`, `group_id`, `node_id`, `node_name`, `type`, `auth_name`) VALUES (null, '62', '2453', '/manage/member.WechatMessageLog/del', 'system', 'manage_member_wechat_message_log_del');
INSERT INTO `__BWPREFIX__auth_group_node` (`id`, `group_id`, `node_id`, `node_name`, `type`, `auth_name`) VALUES (null, '62', '2452', '/manage/member.WechatMessageLog/index', 'system', 'manage_member_wechat_message_log_index');



-- ----------------------------
-- 2021/5/29 头条小程序增加表字段
-- ----------------------------
-- 应用表增加新类型
ALTER TABLE __BWPREFIX__miniapp MODIFY types set('mini_program','tt_program','app','h5','official','system') NOT NULL DEFAULT '' COMMENT '应用类型:mini_program=微信小程序,app=APP,h5=h5,official=公众号,system=系统,tt_program=头条小程序';
-- 客户应用表增加新字段
ALTER TABLE __BWPREFIX__member_miniapp ADD tt_appid varchar(30) DEFAULT NULL COMMENT '头条小程序appid' AFTER miniapp_principal_name;
ALTER TABLE __BWPREFIX__member_miniapp ADD tt_secret varchar(50) DEFAULT NULL COMMENT '头条小程序secret' AFTER tt_appid;
--增加头条小程序配置节点
INSERT INTO `__BWPREFIX__auth_node` (`id`, `pid`, `app_name`, `type`, `title`, `menu_path`, `name`, `auth_name`, `param`, `target`, `condition`, `ismenu`, `icon`, `remark`, `status`, `create_time`, `update_time`, `sort`, `scopes`) VALUES ('2763', '835', 'manage', 'system', '头条微信支付', '/manage/member.setting/tt_wechat', '/manage/member.setting/tt_wechat', 'manage_member_setting_tt_wechat', '', '_self', '', '0', '', '', '1', '1622193932', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_group_node` (`id`, `group_id`, `node_id`, `node_name`, `type`, `auth_name`) VALUES (null, '62', '2763', '/manage/member.setting/tt_wechat', 'system', 'manage_member_setting_tt_wechat');

-- ----------------------------
-- 2021/5/29 整合平台租户功能和租户应用功能
-- ----------------------------
--平台租户菜单更新
UPDATE `__BWPREFIX__auth_node` SET `id`='1331', `pid`='1545', `app_name`='manage', `type`='system', `title`='租户钱包', `menu_path`='/manage/admin/member/wallet', `name`='/manage/admin.member.wallet', `auth_name`='manage_admin_member_wallet', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1595050145', `update_time`='0', `sort`='999', `scopes`='admin' WHERE (`id`='1331');
UPDATE `__BWPREFIX__auth_node` SET `id`='1341', `pid`='1545', `app_name`='manage', `type`='system', `title`='租户财务', `menu_path`='/manage/admin/member/walletBill', `name`='/manage/admin.member.walletBill', `auth_name`='manage_admin_member_walletBill', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1595052713', `update_time`='0', `sort`='999', `scopes`='admin' WHERE (`id`='1341');
INSERT INTO `__BWPREFIX__auth_node` (`id`, `pid`, `app_name`, `type`, `title`, `menu_path`, `name`, `auth_name`, `param`, `target`, `condition`, `ismenu`, `icon`, `remark`, `status`, `create_time`, `update_time`, `sort`, `scopes`) VALUES ('2764', '1124', 'manage', 'system', '详情', '/manage/admin/member/detail', '/manage/admin.member/detail', 'manage_admin_member_detail', '', '_self', '', '0', '', '', '1', '1622269826', '0', '0', 'admin');
--租户菜单更新
UPDATE `__BWPREFIX__auth_node` SET `id`='1472', `pid`='1471', `app_name`='manage', `type`='system', `title`='财务记录', `menu_path`='/manage/member/user/bill/index', `name`='/manage/member.user.Bill/index', `auth_name`='manage_member_user_bill_index', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='fa fa-pencil', `remark`='', `status`='1', `create_time`='1600681941', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1472');
INSERT INTO `__BWPREFIX__auth_node` (`id`, `pid`, `app_name`, `type`, `title`, `menu_path`, `name`, `auth_name`, `param`, `target`, `condition`, `ismenu`, `icon`, `remark`, `status`, `create_time`, `update_time`, `sort`, `scopes`) VALUES ('2753', '1412', 'manage', 'system', '详情', '/manage/member/user/detail', '/manage/member.User/detail', 'manage_member_user_detail', '', '_self', '', '0', '', '', '1', '1622109394', '0', '0', 'member');
INSERT INTO `__BWPREFIX__auth_group_node` (`id`, `group_id`, `node_id`, `node_name`, `type`, `auth_name`) VALUES (null, '62', '2753', '/manage/member.User/detail', 'system', 'manage_member_user_detail');

-- ----------------------------
-- 2021/6/1 租户插件中心整合
-- ----------------------------
UPDATE `__BWPREFIX__auth_node` SET `id`='2460', `pid`='1314', `app_name`='manage', `type`='system', `title`='插件中心', `menu_path`='/manage/member/plugin/core/index', `name`='/manage/member.plugin.Core/index', `auth_name`='manage_member_plugin_core_index', `param`='', `target`='_self', `condition`='', `ismenu`='1', `icon`='fa fa-gamepad', `remark`='', `status`='1', `create_time`='1619513960', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='2460');
UPDATE `__BWPREFIX__auth_node` SET `id`='1840', `pid`='1314', `app_name`='manage', `type`='system', `title`='插件市场', `menu_path`='/manage/member/plugin', `name`='/manage/member.plugin.Plugin/index', `auth_name`='manage_member_plugin', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='fa fa-gift', `remark`='', `status`='1', `create_time`='1591779441', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1840');
--应用加标签
ALTER TABLE __BWPREFIX__miniapp ADD label varchar(100) DEFAULT NULL COMMENT '标签' AFTER is_tt_wechat_pay;
--变更应用中心菜单
UPDATE `__BWPREFIX__auth_node` SET `id`='1589', `pid`='1172', `app_name`='manage', `type`='system', `title`='已购应用', `menu_path`='/manage/member/miniapp/my/index', `name`='/manage/member.MemberMiniapp/index', `auth_name`='manage_member_miniapp_my_index', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='fa fa-gift', `remark`='', `status`='1', `create_time`='1591779441', `update_time`='0', `sort`='999', `scopes`='member' WHERE (`id`='1589');
UPDATE `__BWPREFIX__auth_node` SET `id`='1105', `pid`='1172', `app_name`='manage', `type`='system', `title`='应用中心', `menu_path`='/manage/member/miniapp/index', `name`='/manage/member.miniapp/index', `auth_name`='manage_member_miniapp_index', `param`='', `target`='_self', `condition`='', `ismenu`='1', `icon`='fa fa-gift', `remark`='', `status`='1', `create_time`='1591779441', `update_time`='0', `sort`='1800', `scopes`='member' WHERE (`id`='1105');
--插件表加字段
ALTER TABLE __BWPREFIX__plugin ADD subtitle varchar(255) DEFAULT '' COMMENT '副标题' AFTER title;


-- ----------------------------
-- 2021/6/10 租户系统节点更改
-- ----------------------------
--租户后台增加新菜单
INSERT INTO `__BWPREFIX__auth_node` (`id`, `pid`, `app_name`, `type`, `title`, `menu_path`, `name`, `auth_name`, `param`, `target`, `condition`, `ismenu`, `icon`, `remark`, `status`, `create_time`, `update_time`, `sort`, `scopes`) VALUES ('2810', '0', 'manage', 'system', '运营管理', '/', '/', '', '', '_self', '', '1', '', '', '1', '1623285434', '0', '2999', 'member');
INSERT INTO `__BWPREFIX__auth_node` (`id`, `pid`, `app_name`, `type`, `title`, `menu_path`, `name`, `auth_name`, `param`, `target`, `condition`, `ismenu`, `icon`, `remark`, `status`, `create_time`, `update_time`, `sort`, `scopes`) VALUES ('2811', '0', 'manage', 'system', '服务市场', '/', '/', '', '', '_self', '', '1', '', '', '1', '1623290745', '0', '2997', 'member');
INSERT INTO `__BWPREFIX__auth_node` (`id`, `pid`, `app_name`, `type`, `title`, `menu_path`, `name`, `auth_name`, `param`, `target`, `condition`, `ismenu`, `icon`, `remark`, `status`, `create_time`, `update_time`, `sort`, `scopes`) VALUES ('2812', '0', 'manage', 'system', '财务管理', '/', '/', '', '', '_self', '', '1', '', '', '1', '1623291201', '0', '2998', 'member');
--租户后台其他菜单更新
UPDATE `__BWPREFIX__auth_node` SET `id`='828', `pid`='0', `app_name`='manage', `type`='system', `title`='系统管理', `menu_path`='/manage/member.index/index', `name`='/manage/member.index/index', `auth_name`='manage_member_index_index', `param`='', `target`='_self', `condition`='', `ismenu`='1', `icon`='fa fa-bank', `remark`='', `status`='1', `create_time`='1590398679', `update_time`='0', `sort`='5000', `scopes`='member' WHERE (`id`='828');
UPDATE `__BWPREFIX__auth_node` SET `id`='829', `pid`='2743', `app_name`='manage', `type`='system', `title`='账号列表', `menu_path`='/manage/member.Login/user', `name`='/manage/member.Login/user', `auth_name`='manage_member_Login_user', `param`='', `target`='_self', `condition`='', `ismenu`='1', `icon`='fa fa-user-secret', `remark`='', `status`='1', `create_time`='1590398975', `update_time`='0', `sort`='999', `scopes`='member' WHERE (`id`='829');
UPDATE `__BWPREFIX__auth_node` SET `id`='830', `pid`='2743', `app_name`='manage', `type`='system', `title`='角色管理', `menu_path`='/manage/member.Role/index', `name`='/manage/member.Role/index', `auth_name`='manage_member_Role_index', `param`='', `target`='_self', `condition`='', `ismenu`='1', `icon`='fa fa-joomla', `remark`='', `status`='1', `create_time`='1590399132', `update_time`='0', `sort`='999', `scopes`='member' WHERE (`id`='830');
UPDATE `__BWPREFIX__auth_node` SET `id`='834', `pid`='1172', `app_name`='manage', `type`='system', `title`='应用设置', `menu_path`='/bwmall/admin/confg', `name`='/bwmall/admin/confg', `auth_name`='bwmall_admin_confg', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='fa fa-gear', `remark`='', `status`='1', `create_time`='1590399842', `update_time`='0', `sort`='5', `scopes`='member' WHERE (`id`='834');
UPDATE `__BWPREFIX__auth_node` SET `id`='835', `pid`='834', `app_name`='manage', `type`='system', `title`='关于应用', `menu_path`='/manage/member.setting/index', `name`='/manage/member.setting/index', `auth_name`='manage_member_setting_index', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='fa fa-cubes', `remark`='', `status`='1', `create_time`='1590399937', `update_time`='0', `sort`='999', `scopes`='member' WHERE (`id`='835');
UPDATE `__BWPREFIX__auth_node` SET `id`='861', `pid`='862', `app_name`='manage', `type`='system', `title`='退出登录', `menu_path`='/manage/member/logout', `name`='/manage/member.index/logout', `auth_name`='/manage/member/logout', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1590478428', `update_time`='0', `sort`='999', `scopes`='member' WHERE (`id`='861');
UPDATE `__BWPREFIX__auth_node` SET `id`='862', `pid`='0', `app_name`='manage', `type`='system', `title`='租户基础权限节点', `menu_path`='/', `name`='/', `auth_name`='', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1590479098', `update_time`='0', `sort`='2000', `scopes`='member' WHERE (`id`='862');
UPDATE `__BWPREFIX__auth_node` SET `id`='863', `pid`='830', `app_name`='manage', `type`='system', `title`='分配权限', `menu_path`='/manage/member.Role/getRoleTree', `name`='/manage/member.Role/getRoleTree', `auth_name`='/manage/member.Role/getRoleTree', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1590479262', `update_time`='0', `sort`='999', `scopes`='member' WHERE (`id`='863');
UPDATE `__BWPREFIX__auth_node` SET `id`='1054', `pid`='835', `app_name`='manage', `type`='system', `title`='编辑', `menu_path`='/manage/member.setting/edit', `name`='/manage/member.setting/edit', `auth_name`='manage_member_setting_edit', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1590399937', `update_time`='0', `sort`='999', `scopes`='member' WHERE (`id`='1054');
UPDATE `__BWPREFIX__auth_node` SET `id`='1067', `pid`='835', `app_name`='manage', `type`='system', `title`='微信支付', `menu_path`='/manage/member.setting/wechat', `name`='/manage/member.setting/wechat', `auth_name`='manage_member_setting_wechat', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1590399937', `update_time`='0', `sort`='999', `scopes`='member' WHERE (`id`='1067');
UPDATE `__BWPREFIX__auth_node` SET `id`='1068', `pid`='835', `app_name`='manage', `type`='system', `title`='支付宝支付', `menu_path`='/manage/member.setting/alipay', `name`='/manage/member.setting/alipay', `auth_name`='manage_member_setting_alipay', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1590399937', `update_time`='0', `sort`='999', `scopes`='member' WHERE (`id`='1068');
UPDATE `__BWPREFIX__auth_node` SET `id`='1069', `pid`='834', `app_name`='manage', `type`='system', `title`='公众号菜单', `menu_path`='/manage/member.official/index', `name`='/manage/member.official/index', `auth_name`='manage_member_official_index', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='fa fa-wechat', `remark`='', `status`='1', `create_time`='1591234617', `update_time`='0', `sort`='999', `scopes`='member' WHERE (`id`='1069');
UPDATE `__BWPREFIX__auth_node` SET `id`='1076', `pid`='1069', `app_name`='manage', `type`='system', `title`='新增', `menu_path`='/manage/member.official/add', `name`='/manage/member.official/add', `auth_name`='manage_member_official_add', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1591234676', `update_time`='0', `sort`='999', `scopes`='member' WHERE (`id`='1076');
UPDATE `__BWPREFIX__auth_node` SET `id`='1077', `pid`='1069', `app_name`='manage', `type`='system', `title`='编辑', `menu_path`='/manage/member.official/edit', `name`='/manage/member.official/edit', `auth_name`='manage_member_official_edit', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1591234676', `update_time`='0', `sort`='999', `scopes`='member' WHERE (`id`='1077');
UPDATE `__BWPREFIX__auth_node` SET `id`='1078', `pid`='1069', `app_name`='manage', `type`='system', `title`='删除', `menu_path`='/manage/member.official/del', `name`='/manage/member.official/del', `auth_name`='manage_member_official_del', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1591234676', `update_time`='0', `sort`='999', `scopes`='member' WHERE (`id`='1078');
UPDATE `__BWPREFIX__auth_node` SET `id`='1079', `pid`='1069', `app_name`='manage', `type`='system', `title`='删除', `menu_path`='/manage/member.official/sync', `name`='/manage/member.official/sync', `auth_name`='manage_member_official_sync', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1591234676', `update_time`='0', `sort`='999', `scopes`='member' WHERE (`id`='1079');
UPDATE `__BWPREFIX__auth_node` SET `id`='1081', `pid`='1069', `app_name`='manage', `type`='system', `title`='排序', `menu_path`='/manage/member.official/sort', `name`='/manage/member.official/sort', `auth_name`='manage_member_official_sort', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1591234676', `update_time`='0', `sort`='999', `scopes`='member' WHERE (`id`='1081');
UPDATE `__BWPREFIX__auth_node` SET `id`='1105', `pid`='1172', `app_name`='manage', `type`='system', `title`='应用中心', `menu_path`='/manage/member/miniapp/index', `name`='/manage/member.miniapp/index', `auth_name`='manage_member_miniapp_index', `param`='', `target`='_self', `condition`='', `ismenu`='1', `icon`='fa fa-gift', `remark`='', `status`='1', `create_time`='1591779441', `update_time`='0', `sort`='1800', `scopes`='member' WHERE (`id`='1105');
UPDATE `__BWPREFIX__auth_node` SET `id`='1107', `pid`='1105', `app_name`='manage', `type`='system', `title`='详情', `menu_path`='/manage/member/miniapp/detail', `name`='/manage/member.miniapp/detail', `auth_name`='/manage/member.miniapp/detail', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1591779441', `update_time`='0', `sort`='999', `scopes`='member' WHERE (`id`='1107');
UPDATE `__BWPREFIX__auth_node` SET `id`='1108', `pid`='1105', `app_name`='manage', `type`='system', `title`='购买', `menu_path`='/manage/member/miniapp/buy', `name`='/manage/member.miniapp/buy', `auth_name`='/manage/member.miniapp/buy', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1591779441', `update_time`='0', `sort`='999', `scopes`='member' WHERE (`id`='1108');
UPDATE `__BWPREFIX__auth_node` SET `id`='1161', `pid`='830', `app_name`='manage', `type`='system', `title`='新增角色', `menu_path`='/manage/member.Role/add', `name`='/manage/member.Role/add', `auth_name`='/manage/member.Role/add', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1592208844', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1161');
UPDATE `__BWPREFIX__auth_node` SET `id`='1169', `pid`='862', `app_name`='manage', `type`='system', `title`='菜单', `menu_path`='/manage/member/menu', `name`='/manage/member.Index/menu', `auth_name`='/manage/member/menu', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='0', `update_time`='0', `sort`='999', `scopes`='member' WHERE (`id`='1169');
UPDATE `__BWPREFIX__auth_node` SET `id`='1171', `pid`='862', `app_name`='manage', `type`='system', `title`='控制台', `menu_path`='/manage/member/dashboard', `name`='/manage/member.Index/dashboard', `auth_name`='/manage/member/dashboard', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='0', `update_time`='0', `sort`='999', `scopes`='member' WHERE (`id`='1171');
UPDATE `__BWPREFIX__auth_node` SET `id`='1172', `pid`='0', `app_name`='manage', `type`='system', `title`='应用管理', `menu_path`='/', `name`='/', `auth_name`='', `param`='', `target`='_self', `condition`='', `ismenu`='1', `icon`='', `remark`='', `status`='1', `create_time`='0', `update_time`='0', `sort`='4000', `scopes`='member' WHERE (`id`='1172');
UPDATE `__BWPREFIX__auth_node` SET `id`='1200', `pid`='862', `app_name`='manage', `type`='system', `title`='开放平台授权', `menu_path`='/manage/member/openAuth', `name`='/manage/member.WechatAuth/openAuth', `auth_name`='manage_member_openAuth', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1594371589', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1200');
UPDATE `__BWPREFIX__auth_node` SET `id`='1206', `pid`='862', `app_name`='manage', `type`='system', `title`='开放平台授权回调', `menu_path`='/manage/member/openAuthCallback', `name`='/manage/member.WechatAuth/openAuthCallback', `auth_name`='manage_member_openAuthCallback', `param`='', `target`='_blank', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1594397684', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1206');
UPDATE `__BWPREFIX__auth_node` SET `id`='1221', `pid`='830', `app_name`='manage', `type`='system', `title`='租户权限列表', `menu_path`='/manage/member.Role/getNodeTree', `name`='/manage/member.Role/getNodeTree', `auth_name`='/manage/member.Role/getNodeTree', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1594629488', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1221');
UPDATE `__BWPREFIX__auth_node` SET `id`='1236', `pid`='830', `app_name`='manage', `type`='system', `title`='删除', `menu_path`='/manage/member.Role/delete', `name`='/manage/member.Role/delete', `auth_name`='/manage/member.Role/delete', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1594631484', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1236');
UPDATE `__BWPREFIX__auth_node` SET `id`='1237', `pid`='830', `app_name`='manage', `type`='system', `title`='状态', `menu_path`='/manage/member.Role/setStatus', `name`='/manage/member.Role/setStatus', `auth_name`='/manage/member.Role/setStatus', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1594631863', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1237');
UPDATE `__BWPREFIX__auth_node` SET `id`='1238', `pid`='830', `app_name`='manage', `type`='system', `title`='编辑', `menu_path`='/manage/member.Role/edit', `name`='/manage/member.Role/edit', `auth_name`='/manage/member.Role/edit', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1594632056', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1238');
UPDATE `__BWPREFIX__auth_node` SET `id`='1239', `pid`='829', `app_name`='manage', `type`='system', `title`='添加', `menu_path`='/manage/member.Login/add', `name`='/manage/member.Login/add', `auth_name`='/manage/member.Login/add', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1594635443', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1239');
UPDATE `__BWPREFIX__auth_node` SET `id`='1240', `pid`='829', `app_name`='manage', `type`='system', `title`='编辑', `menu_path`='/manage/member.Login/edit', `name`='/manage/member.Login/edit', `auth_name`='/manage/member.Login/edit', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1594637416', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1240');
UPDATE `__BWPREFIX__auth_node` SET `id`='1242', `pid`='829', `app_name`='manage', `type`='system', `title`='设置状态', `menu_path`='/manage/member.Login/setStatus', `name`='/manage/member.Login/setStatus', `auth_name`='/manage/member.Login/setStatus', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1594639953', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1242');
UPDATE `__BWPREFIX__auth_node` SET `id`='1243', `pid`='862', `app_name`='manage', `type`='system', `title`='小程序修改授权域名', `menu_path`='/manage/member/setDomain', `name`='/manage/member.Miniapp/setDomain', `auth_name`='manage_member_setDomain', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1594653668', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1243');
UPDATE `__BWPREFIX__auth_node` SET `id`='1248', `pid`='862', `app_name`='manage', `type`='system', `title`='提交代码', `menu_path`='/manage/member/upCode', `name`='/manage/member.Miniapp/upCode', `auth_name`='manage_member_upCode', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1594733819', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1248');
UPDATE `__BWPREFIX__auth_node` SET `id`='1250', `pid`='834', `app_name`='manage', `type`='system', `title`='回复规则管理', `menu_path`='/manage/member.wechatKeyword/index', `name`='/manage/member.wechatKeyword/index', `auth_name`='manage_member_wechatKeyword_index', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='fa fa-paint-brush', `remark`='', `status`='1', `create_time`='1594790225', `update_time`='0', `sort`='999', `scopes`='member' WHERE (`id`='1250');
UPDATE `__BWPREFIX__auth_node` SET `id`='1252', `pid`='1250', `app_name`='manage', `type`='system', `title`='新增', `menu_path`='/manage/member.wechatKeyword/add', `name`='/manage/member.wechatKeyword/add', `auth_name`='manage_member_wechatKeyword_add', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1594790225', `update_time`='0', `sort`='999', `scopes`='member' WHERE (`id`='1252');
UPDATE `__BWPREFIX__auth_node` SET `id`='1253', `pid`='1250', `app_name`='manage', `type`='system', `title`='编辑', `menu_path`='/manage/member.wechatKeyword/edit', `name`='/manage/member.wechatKeyword/edit', `auth_name`='manage_member_wechatKeyword_edit', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1594790225', `update_time`='0', `sort`='999', `scopes`='member' WHERE (`id`='1253');
UPDATE `__BWPREFIX__auth_node` SET `id`='1254', `pid`='1250', `app_name`='manage', `type`='system', `title`='删除', `menu_path`='/manage/member.wechatKeyword/del', `name`='/manage/member.wechatKeyword/del', `auth_name`='manage_member_wechatKeyword_del', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1594790225', `update_time`='0', `sort`='999', `scopes`='member' WHERE (`id`='1254');
UPDATE `__BWPREFIX__auth_node` SET `id`='1273', `pid`='862', `app_name`='manage', `type`='system', `title`='获取小程序体验二维码', `menu_path`='/manage/member/getQrCode', `name`='/manage/member.Miniapp/getQrCode', `auth_name`='manage_member_getQrCode', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1594879921', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1273');
UPDATE `__BWPREFIX__auth_node` SET `id`='1308', `pid`='862', `app_name`='manage', `type`='system', `title`='上传文件', `menu_path`='/manage/publicCommon/upload', `name`='/manage/publicCommon/upload', `auth_name`='_manage_publicCommon_upload', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1594974394', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1308');
UPDATE `__BWPREFIX__auth_node` SET `id`='1309', `pid`='1315', `app_name`='manage', `type`='system', `title`='服务列表', `menu_path`='/manage/member/cloud/service', `name`='/manage/member.cloud.service', `auth_name`='manage_member_cloud_service', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='fa fa-cloud-upload', `remark`='', `status`='1', `create_time`='1595034953', `update_time`='0', `sort`='999', `scopes`='member' WHERE (`id`='1309');
UPDATE `__BWPREFIX__auth_node` SET `id`='1310', `pid`='1315', `app_name`='manage', `type`='system', `title`='查看', `menu_path`='/manage/member/cloud/service/index', `name`='/manage/member.cloud.service/index', `auth_name`='manage_member_cloud_service_index', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1595034953', `update_time`='0', `sort`='999', `scopes`='member' WHERE (`id`='1310');
UPDATE `__BWPREFIX__auth_node` SET `id`='1314', `pid`='0', `app_name`='manage', `type`='system', `title`='插件管理', `menu_path`='/', `name`='/', `auth_name`='', `param`='', `target`='_self', `condition`='', `ismenu`='1', `icon`='', `remark`='', `status`='1', `create_time`='1595035502', `update_time`='0', `sort`='3000', `scopes`='member' WHERE (`id`='1314');
UPDATE `__BWPREFIX__auth_node` SET `id`='1315', `pid`='2811', `app_name`='manage', `type`='system', `title`='云市场', `menu_path`='/manage/member/cloud/service/index', `name`='/manage/member.cloud.service/index', `auth_name`='manage_member_cloud_service_index', `param`='', `target`='_self', `condition`='', `ismenu`='1', `icon`='fa fa-skyatlas', `remark`='', `status`='1', `create_time`='1595035578', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1315');
UPDATE `__BWPREFIX__auth_node` SET `id`='1316', `pid`='862', `app_name`='manage', `type`='system', `title`='基本资料', `menu_path`='/manage/member/login/userSetting', `name`='/manage/member.Login/userSetting', `auth_name`='/manage/member/login/userSetting', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1595038359', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1316');
UPDATE `__BWPREFIX__auth_node` SET `id`='1317', `pid`='1315', `app_name`='manage', `type`='system', `title`='云市场套餐管理', `menu_path`='/manage/member/cloud/packet', `name`='/manage/member.cloud.packet', `auth_name`='/manage/member.cloud.packet', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1595039367', `update_time`='0', `sort`='999', `scopes`='member' WHERE (`id`='1317');
UPDATE `__BWPREFIX__auth_node` SET `id`='1318', `pid`='1317', `app_name`='manage', `type`='system', `title`='查看', `menu_path`='/manage/member/cloud/packet/index', `name`='/manage/member.cloud.packet/index', `auth_name`='/manage/member.cloud.packet/index', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1595039367', `update_time`='0', `sort`='999', `scopes`='member' WHERE (`id`='1318');
UPDATE `__BWPREFIX__auth_node` SET `id`='1322', `pid`='1317', `app_name`='manage', `type`='system', `title`='购买', `menu_path`='/manage/member/cloud/packet/buy', `name`='/manage/member.cloud.packet/buy', `auth_name`='/manage/member.cloud.packet/buy', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1595039367', `update_time`='0', `sort`='999', `scopes`='member' WHERE (`id`='1322');
UPDATE `__BWPREFIX__auth_node` SET `id`='1323', `pid`='862', `app_name`='manage', `type`='system', `title`='修改密码', `menu_path`='/manage/member/login/userPassword', `name`='/manage/member.Login/userPassword', `auth_name`='/manage/member/login/userPassword', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1595040696', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1323');
UPDATE `__BWPREFIX__auth_node` SET `id`='1324', `pid`='1315', `app_name`='manage', `type`='system', `title`='云市场钱包记录管理', `menu_path`='/manage/member/cloud/walletBill', `name`='/manage/member.cloud.walletBill', `auth_name`='/manage/member.cloud.walletBill', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1595042219', `update_time`='0', `sort`='999', `scopes`='member' WHERE (`id`='1324');
UPDATE `__BWPREFIX__auth_node` SET `id`='1325', `pid`='1324', `app_name`='manage', `type`='system', `title`='查看', `menu_path`='/manage/member/cloud/walletBill/index', `name`='/manage/member.cloud.walletBill/index', `auth_name`='/manage/member.cloud.walletBill/index', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1595042219', `update_time`='0', `sort`='999', `scopes`='member' WHERE (`id`='1325');
UPDATE `__BWPREFIX__auth_node` SET `id`='1346', `pid`='828', `app_name`='manage', `type`='system', `title`='配置管理', `menu_path`='/config', `name`='/config', `auth_name`='config', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='fa fa-gear', `remark`='', `status`='1', `create_time`='1595054309', `update_time`='0', `sort`='888', `scopes`='member' WHERE (`id`='1346');
UPDATE `__BWPREFIX__auth_node` SET `id`='1347', `pid`='1346', `app_name`='manage', `type`='system', `title`='配置管理', `menu_path`='/manage/member/config/showMemberConfig/1', `name`='/manage/member.Config/showMemberConfig', `auth_name`='_manage_member_config_showMemberConfig_1', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='fa fa-eyedropper', `remark`='', `status`='1', `create_time`='1595054503', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1347');
UPDATE `__BWPREFIX__auth_node` SET `id`='1348', `pid`='1347', `app_name`='manage', `type`='system', `title`='保存配置', `menu_path`='/manage/member/config/setMemberValues', `name`='/manage/member.Config/setMemberValues', `auth_name`='_manage_member_config_setMemberValues', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1595054662', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1348');
UPDATE `__BWPREFIX__auth_node` SET `id`='1349', `pid`='2812', `app_name`='manage', `type`='system', `title`='财务管理', `menu_path`='/manage/member/walletBill', `name`='/manage/member.walletBill', `auth_name`='manage_member_walletBill', `param`='', `target`='_self', `condition`='', `ismenu`='1', `icon`='fa fa-edit', `remark`='', `status`='1', `create_time`='1595206543', `update_time`='0', `sort`='999', `scopes`='member' WHERE (`id`='1349');
UPDATE `__BWPREFIX__auth_node` SET `id`='1350', `pid`='1349', `app_name`='manage', `type`='system', `title`='查看', `menu_path`='/manage/member/walletBill/index', `name`='/manage/member.walletBill/index', `auth_name`='/manage/member.walletBill/index', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1595206543', `update_time`='0', `sort`='999', `scopes`='member' WHERE (`id`='1350');
UPDATE `__BWPREFIX__auth_node` SET `id`='1351', `pid`='1349', `app_name`='manage', `type`='system', `title`='新增', `menu_path`='/manage/member/walletBill/add', `name`='/manage/member.walletBill/add', `auth_name`='/manage/member.walletBill/add', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1595206543', `update_time`='0', `sort`='999', `scopes`='member' WHERE (`id`='1351');
UPDATE `__BWPREFIX__auth_node` SET `id`='1352', `pid`='1349', `app_name`='manage', `type`='system', `title`='编辑', `menu_path`='/manage/member/walletBill/edit', `name`='/manage/member.walletBill/edit', `auth_name`='/manage/member.walletBill/edit', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1595206543', `update_time`='0', `sort`='999', `scopes`='member' WHERE (`id`='1352');
UPDATE `__BWPREFIX__auth_node` SET `id`='1354', `pid`='1315', `app_name`='manage', `type`='system', `title`='云市场订单管理', `menu_path`='/manage/member/cloud/order', `name`='/manage/member.cloud.order', `auth_name`='/manage/member.cloud.order', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1595039367', `update_time`='0', `sort`='999', `scopes`='member' WHERE (`id`='1354');
UPDATE `__BWPREFIX__auth_node` SET `id`='1355', `pid`='1354', `app_name`='manage', `type`='system', `title`='查看', `menu_path`='/manage/member/cloud/order/index', `name`='/manage/member.cloud.order/index', `auth_name`='/manage/member.cloud.order/index', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1595039367', `update_time`='0', `sort`='999', `scopes`='member' WHERE (`id`='1355');
UPDATE `__BWPREFIX__auth_node` SET `id`='1358', `pid`='1396', `app_name`='manage', `type`='system', `title`='编辑', `menu_path`='/manage/member/groupData/edit', `name`='/manage/member.GroupData/edit', `auth_name`='_manage_member_groupData_edit', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1595296632', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1358');
UPDATE `__BWPREFIX__auth_node` SET `id`='1359', `pid`='1396', `app_name`='manage', `type`='system', `title`='添加', `menu_path`='/manage/member/groupData/add', `name`='/manage/member.GroupData/add', `auth_name`='_manage_member_groupData_add', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1595296688', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1359');
UPDATE `__BWPREFIX__auth_node` SET `id`='1360', `pid`='1396', `app_name`='manage', `type`='system', `title`='删除', `menu_path`='/manage/member/groupData/softdleting', `name`='/manage/member.GroupData/softdleting', `auth_name`='_manage_member_groupData_softdleting', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1595296730', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1360');
UPDATE `__BWPREFIX__auth_node` SET `id`='1361', `pid`='1396', `app_name`='manage', `type`='system', `title`='列表', `menu_path`='/manage/member/groupData/getConfigList', `name`='/manage/member.GroupData/getConfigList', `auth_name`='_manage_member_groupData_getConfigList', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1595296788', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1361');
UPDATE `__BWPREFIX__auth_node` SET `id`='1363', `pid`='1396', `app_name`='manage', `type`='system', `title`='更改状态', `menu_path`='/manage/member/groupData/setConfigShow', `name`='/manage/member.GroupData/setConfigShow', `auth_name`='_manage_member_groupData_setConfigShow', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1595297407', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1363');
UPDATE `__BWPREFIX__auth_node` SET `id`='1364', `pid`='862', `app_name`='manage', `type`='system', `title`='上传图片(layui返回格式)', `menu_path`='/manage/publicCommon/uploadimg', `name`='/manage/publicCommon/uploadimg', `auth_name`='_manage_publicCommon_uploadimg', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1595298389', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1364');
UPDATE `__BWPREFIX__auth_node` SET `id`='1396', `pid`='1346', `app_name`='manage', `type`='system', `title`='组合数据管理', `menu_path`='/manage/member/groupData', `name`='/manage/member.GroupData/index', `auth_name`='_manage_member_groupData', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1598424995', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1396');
UPDATE `__BWPREFIX__auth_node` SET `id`='1408', `pid`='828', `app_name`='manage', `type`='system', `title`='充值', `menu_path`='/manage/member/recharge', `name`='/manage/member.Recharge/index', `auth_name`='_manage_member_recharge', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1599015300', `update_time`='0', `sort`='666', `scopes`='member' WHERE (`id`='1408');
UPDATE `__BWPREFIX__auth_node` SET `id`='1411', `pid`='1408', `app_name`='manage', `type`='system', `title`='提交', `menu_path`='/manage/member/recharge/pay', `name`='/manage/member.Recharge/pay', `auth_name`='_manage_member_recharge_pay', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1599027718', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1411');
UPDATE `__BWPREFIX__auth_node` SET `id`='1412', `pid`='1471', `app_name`='manage', `type`='system', `title`='用户列表', `menu_path`='/manage/member/user/index', `name`='/manage/member.User/index', `auth_name`='manage_member_user_index', `param`='', `target`='_self', `condition`='', `ismenu`='1', `icon`='fa fa-user-plus', `remark`='', `status`='1', `create_time`='1599114629', `update_time`='0', `sort`='999', `scopes`='member' WHERE (`id`='1412');
UPDATE `__BWPREFIX__auth_node` SET `id`='1413', `pid`='1412', `app_name`='manage', `type`='system', `title`='新增', `menu_path`='/manage/member/user/add', `name`='/manage/member.User/add', `auth_name`='_manage_member_user_add', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1599114909', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1413');
UPDATE `__BWPREFIX__auth_node` SET `id`='1414', `pid`='1412', `app_name`='manage', `type`='system', `title`='修改', `menu_path`='/manage/member/user/edit', `name`='/manage/member.User/edit', `auth_name`='_manage_member_user_edit', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1599114949', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1414');
UPDATE `__BWPREFIX__auth_node` SET `id`='1416', `pid`='1412', `app_name`='manage', `type`='system', `title`='导出', `menu_path`='/manage/member/user/export', `name`='/manage/member.User/export', `auth_name`='_manage_member_user_export', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1599115319', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1416');
UPDATE `__BWPREFIX__auth_node` SET `id`='1417', `pid`='1412', `app_name`='manage', `type`='system', `title`='修改', `menu_path`='/manage/member/user/modify', `name`='/manage/member.User/modify', `auth_name`='_manage_member_user_modify', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1599115360', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1417');
UPDATE `__BWPREFIX__auth_node` SET `id`='1418', `pid`='1412', `app_name`='manage', `type`='system', `title`='修改密码', `menu_path`='/manage/member/user/editPw', `name`='/manage/member.User/editPw', `auth_name`='_manage_member_user_editPw', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1599120886', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1418');
UPDATE `__BWPREFIX__auth_node` SET `id`='1419', `pid`='1412', `app_name`='manage', `type`='system', `title`='资金余额', `menu_path`='/manage/member/user/changeMoney', `name`='/manage/member.User/changeMoney', `auth_name`='_manage_member_user_changeMoney', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1599130844', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1419');
UPDATE `__BWPREFIX__auth_node` SET `id`='1443', `pid`='828', `app_name`='manage', `type`='system', `title`='首页', `menu_path`='/manage/member/dashboard', `name`='/manage/member.Index/dashboard', `auth_name`='_manage_member_dashboard', `param`='', `target`='_self', `condition`='', `ismenu`='1', `icon`='fa fa-bar-chart-o', `remark`='', `status`='1', `create_time`='1599546960', `update_time`='0', `sort`='9999', `scopes`='member' WHERE (`id`='1443');
UPDATE `__BWPREFIX__auth_node` SET `id`='1458', `pid`='1069', `app_name`='manage', `type`='system', `title`='素材列表', `menu_path`='/manage/member.official/materialList', `name`='/manage/member.official/materialList', `auth_name`='manage_member_official_materialList', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1600239056', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1458');
UPDATE `__BWPREFIX__auth_node` SET `id`='1462', `pid`='1471', `app_name`='manage', `type`='system', `title`='用户充值', `menu_path`='rechange/config', `name`='rechange/config', `auth_name`='rechange_config', `param`='', `target`='_self', `condition`='', `ismenu`='1', `icon`='fa fa-cny', `remark`='', `status`='1', `create_time`='1600509225', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1462');
UPDATE `__BWPREFIX__auth_node` SET `id`='1463', `pid`='1462', `app_name`='manage', `type`='system', `title`='充值设置', `menu_path`='/manage/member/config/showMemberConfig/0/54', `name`='/manage/member/config.ShowMemberConfig', `auth_name`='manage_member_config_showMemberConfig_0_54', `param`='', `target`='_self', `condition`='', `ismenu`='1', `icon`='', `remark`='', `status`='1', `create_time`='1600509355', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1463');
UPDATE `__BWPREFIX__auth_node` SET `id`='1464', `pid`='1462', `app_name`='manage', `type`='system', `title`='充值套餐', `menu_path`='/manage/member/groupData/recharge_select', `name`='/manage/member.GroupData/index', `auth_name`='manage_member_groupData_recharge_select', `param`='', `target`='_self', `condition`='', `ismenu`='1', `icon`='', `remark`='', `status`='1', `create_time`='1600509525', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1464');
UPDATE `__BWPREFIX__auth_node` SET `id`='1465', `pid`='1462', `app_name`='manage', `type`='system', `title`='充值记录', `menu_path`='/manage/member/user/recharge/index', `name`='/manage/member.user.Recharge/index', `auth_name`='manage_member_user_recharge_index', `param`='', `target`='_self', `condition`='', `ismenu`='1', `icon`='', `remark`='', `status`='1', `create_time`='1600669249', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1465');
UPDATE `__BWPREFIX__auth_node` SET `id`='1466', `pid`='1465', `app_name`='manage', `type`='system', `title`='添加', `menu_path`='/manage/member/user/recharge/add', `name`='/manage/member.user.Recharge/add', `auth_name`='_manage_member_user_recharge_add', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1600669582', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1466');
UPDATE `__BWPREFIX__auth_node` SET `id`='1467', `pid`='1465', `app_name`='manage', `type`='system', `title`='编辑', `menu_path`='/manage/member/user/recharge/edit', `name`='/manage/member.user.Recharge/edit', `auth_name`='_manage_member_user_recharge_edit', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1600669661', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1467');
UPDATE `__BWPREFIX__auth_node` SET `id`='1468', `pid`='1465', `app_name`='manage', `type`='system', `title`='导出', `menu_path`='/manage/member/user/recharge/export', `name`='/manage/member.user.Recharge/export', `auth_name`='_manage_member_user_recharge_export', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1600669752', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1468');
UPDATE `__BWPREFIX__auth_node` SET `id`='1469', `pid`='1465', `app_name`='manage', `type`='system', `title`='修改', `menu_path`='/manage/member/user/recharge/modify', `name`='/manage/member.user.Recharge/modify', `auth_name`='_manage_member_user_recharge_modify', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1600669842', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1469');
UPDATE `__BWPREFIX__auth_node` SET `id`='1471', `pid`='2810', `app_name`='manage', `type`='system', `title`='应用会员', `menu_path`='/', `name`='/', `auth_name`='', `param`='', `target`='_self', `condition`='', `ismenu`='1', `icon`='fa fa-male', `remark`='', `status`='1', `create_time`='1600680925', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1471');
UPDATE `__BWPREFIX__auth_node` SET `id`='1472', `pid`='1471', `app_name`='manage', `type`='system', `title`='财务记录', `menu_path`='/manage/member/user/bill/index', `name`='/manage/member.user.Bill/index', `auth_name`='manage_member_user_bill_index', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='fa fa-pencil', `remark`='', `status`='1', `create_time`='1600681941', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1472');
UPDATE `__BWPREFIX__auth_node` SET `id`='1473', `pid`='1472', `app_name`='manage', `type`='system', `title`='导出', `menu_path`='/manage/member/user/bill/export', `name`='/manage/member.user.Bill/export', `auth_name`='_manage_member_user_bill_export', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1600682018', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1473');
UPDATE `__BWPREFIX__auth_node` SET `id`='1474', `pid`='1472', `app_name`='manage', `type`='system', `title`='添加', `menu_path`='/manage/member/user/bill/add', `name`='/manage/member.user.Bill/add', `auth_name`='_manage_member_user_bill_add', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1600682140', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1474');
UPDATE `__BWPREFIX__auth_node` SET `id`='1475', `pid`='1472', `app_name`='manage', `type`='system', `title`='编辑', `menu_path`='/manage/member/user/bill/edit', `name`='/manage/member.user.Bill/edit', `auth_name`='_manage_member_user_bill_edit', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1600682206', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1475');
UPDATE `__BWPREFIX__auth_node` SET `id`='1476', `pid`='1472', `app_name`='manage', `type`='system', `title`='修改', `menu_path`='/manage/member/user/bill/modify', `name`='/manage/member.user.Bill/modify', `auth_name`='_manage_member_user_bill_modify', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1600682264', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1476');
UPDATE `__BWPREFIX__auth_node` SET `id`='1485', `pid`='1471', `app_name`='manage', `type`='system', `title`='用户提现', `menu_path`='/manage/member/extract', `name`='/manage/member/extract', `auth_name`='_manage_member_extract', `param`='', `target`='_self', `condition`='', `ismenu`='1', `icon`='fa fa-money', `remark`='', `status`='1', `create_time`='1600767960', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1485');
UPDATE `__BWPREFIX__auth_node` SET `id`='1486', `pid`='1485', `app_name`='manage', `type`='system', `title`='提现配置', `menu_path`='/manage/member/config/showMemberConfig/0/55', `name`='/manage/member/config.ShowMemberConfig', `auth_name`='manage_member_config_showMemberConfig_0_55', `param`='', `target`='_self', `condition`='', `ismenu`='1', `icon`='', `remark`='', `status`='1', `create_time`='1600768150', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1486');
UPDATE `__BWPREFIX__auth_node` SET `id`='1487', `pid`='1485', `app_name`='manage', `type`='system', `title`='绑定管理', `menu_path`='/manage/member/user/extract/index', `name`='/manage/member.user.Extract/index', `auth_name`='manage_member_user_extract_index', `param`='', `target`='_self', `condition`='', `ismenu`='1', `icon`='', `remark`='', `status`='1', `create_time`='1600844996', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1487');
UPDATE `__BWPREFIX__auth_node` SET `id`='1488', `pid`='1487', `app_name`='manage', `type`='system', `title`='导出', `menu_path`='/manage/member/user/extract/export', `name`='/manage/member.user.Extract/export', `auth_name`='_manage_member_user_extract_export', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1600845091', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1488');
UPDATE `__BWPREFIX__auth_node` SET `id`='1489', `pid`='1487', `app_name`='manage', `type`='system', `title`='添加', `menu_path`='/manage/member/user/extract/add', `name`='/manage/member.user.Extract/add', `auth_name`='_manage_member_user_extract_add', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1600845293', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1489');
UPDATE `__BWPREFIX__auth_node` SET `id`='1490', `pid`='1487', `app_name`='manage', `type`='system', `title`='编辑', `menu_path`='/manage/member/user/extract/edit', `name`='/manage/member.user.Extract/edit', `auth_name`='_manage_member_user_extract_edit', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1600845388', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1490');
UPDATE `__BWPREFIX__auth_node` SET `id`='1491', `pid`='1487', `app_name`='manage', `type`='system', `title`='修改', `menu_path`='/manage/member/user/extract/modify', `name`='/manage/member.user.Extract/modify', `auth_name`='_manage_member_user_extract_modify', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1600845433', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1491');
UPDATE `__BWPREFIX__auth_node` SET `id`='1492', `pid`='1487', `app_name`='manage', `type`='system', `title`='删除', `menu_path`='/manage/member/user/extract/del', `name`='/manage/member.user.Extract/del', `auth_name`='_manage_member_user_extract_del', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1600845502', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1492');
UPDATE `__BWPREFIX__auth_node` SET `id`='1493', `pid`='1485', `app_name`='manage', `type`='system', `title`='提现管理', `menu_path`='/manage/member/user/extractLog/index', `name`='/manage/member.user.ExtractLog/index', `auth_name`='manage_member_user_extractLog_index', `param`='', `target`='_self', `condition`='', `ismenu`='1', `icon`='', `remark`='', `status`='1', `create_time`='1600845647', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1493');
UPDATE `__BWPREFIX__auth_node` SET `id`='1494', `pid`='1493', `app_name`='manage', `type`='system', `title`='导出', `menu_path`='/manage/member/user/extractLog/export', `name`='/manage/member.user.ExtractLog/export', `auth_name`='_manage_member_user_extractLog_export', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1600845718', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1494');
UPDATE `__BWPREFIX__auth_node` SET `id`='1495', `pid`='1493', `app_name`='manage', `type`='system', `title`='添加', `menu_path`='/manage/member/user/extractLog/add', `name`='/manage/member.user.ExtractLog/add', `auth_name`='_manage_member_user_extractLog_add', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1600845981', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1495');
UPDATE `__BWPREFIX__auth_node` SET `id`='1496', `pid`='1493', `app_name`='manage', `type`='system', `title`='编辑', `menu_path`='/manage/member/user/extractLog/edit', `name`='/manage/member.user.ExtractLog/edit', `auth_name`='_manage_member_user_extractLog_edit', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1600846099', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1496');
UPDATE `__BWPREFIX__auth_node` SET `id`='1497', `pid`='1493', `app_name`='manage', `type`='system', `title`='修改', `menu_path`='/manage/member/user/extractLog/modify', `name`='/manage/member.user.ExtractLog/modify', `auth_name`='_manage_member_user_extractLog_modify', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1600846183', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1497');
UPDATE `__BWPREFIX__auth_node` SET `id`='1498', `pid`='1493', `app_name`='manage', `type`='system', `title`='删除', `menu_path`='/manage/member/user/extractLog/del', `name`='/manage/member.user.ExtractLog/del', `auth_name`='_manage_member_user_extractLog_del', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1600846240', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1498');
UPDATE `__BWPREFIX__auth_node` SET `id`='1500', `pid`='834', `app_name`='manage', `type`='system', `title`='微信图文管理', `menu_path`='/manage/member.wechatKeyword/materialList', `name`='/manage/member.wechatKeyword/materialList', `auth_name`='manage_member_wechatKeyword_materialList', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='fa fa-weixin', `remark`='', `status`='1', `create_time`='1600929033', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1500');
UPDATE `__BWPREFIX__auth_node` SET `id`='1507', `pid`='1500', `app_name`='manage', `type`='system', `title`='删除图文', `menu_path`='/manage/member.wechatKeyword/removeMaterial', `name`='/manage/member.wechatKeyword/removeMaterial', `auth_name`='manage_member_wechatKeyword_removeMaterial', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1600936810', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1507');
UPDATE `__BWPREFIX__auth_node` SET `id`='1508', `pid`='828', `app_name`='manage', `type`='system', `title`='操作日志', `menu_path`='/manage/member/sys/log/index', `name`='/manage/member.sys.Log/index', `auth_name`='manage_member_sys_log_index', `param`='', `target`='_self', `condition`='', `ismenu`='1', `icon`='fa fa-book', `remark`='', `status`='1', `create_time`='1600938881', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1508');
UPDATE `__BWPREFIX__auth_node` SET `id`='1509', `pid`='1508', `app_name`='manage', `type`='system', `title`='导出', `menu_path`='/manage/member/sys/log/export', `name`='/manage/member.sys.Log/export', `auth_name`='_manage_member_sys_log_export', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1600938928', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1509');
UPDATE `__BWPREFIX__auth_node` SET `id`='1510', `pid`='1508', `app_name`='manage', `type`='system', `title`='添加', `menu_path`='/manage/member/sys/log/add', `name`='/manage/member.sys.Log/add', `auth_name`='_manage_member_sys_log_add', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1600938998', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1510');
UPDATE `__BWPREFIX__auth_node` SET `id`='1511', `pid`='1508', `app_name`='manage', `type`='system', `title`='编辑', `menu_path`='/manage/member/sys/log/edit', `name`='/manage/member.sys.Log/edit', `auth_name`='_manage_member_sys_log_edit', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1600939043', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1511');
UPDATE `__BWPREFIX__auth_node` SET `id`='1512', `pid`='1508', `app_name`='manage', `type`='system', `title`='修改', `menu_path`='/manage/member/sys/log/modify', `name`='/manage/member.sys.Log/modify', `auth_name`='_manage_member_sys_log_modify', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1600939126', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1512');
UPDATE `__BWPREFIX__auth_node` SET `id`='1515', `pid`='1443', `app_name`='manage', `type`='system', `title`='登录日志', `menu_path`='/manage/member/loginInfo', `name`='/manage/member.Index/getMemberLoginInfo', `auth_name`='_manage_member_loginInfo', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1600998614', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1515');
UPDATE `__BWPREFIX__auth_node` SET `id`='1522', `pid`='1443', `app_name`='manage', `type`='system', `title`='系统公告列表', `menu_path`='/manage/member/noticeList', `name`='/manage/member.Index/getNoticeList', `auth_name`='_manage_member_noticeList', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1601016826', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1522');
UPDATE `__BWPREFIX__auth_node` SET `id`='1523', `pid`='1443', `app_name`='manage', `type`='system', `title`='用户图表数据', `menu_path`='/manage/member/echartsInfo', `name`='/manage/member.Index/getEchartsInfo', `auth_name`='_manage_member_echartsInfo', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1601035845', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1523');
UPDATE `__BWPREFIX__auth_node` SET `id`='1527', `pid`='862', `app_name`='manage', `type`='system', `title`='提交代码给微信官网审核', `menu_path`='/manage/member/submitAudit', `name`='/manage/member.Miniapp/submitAudit', `auth_name`='manage_member_submitAudit', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1601276072', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1527');
UPDATE `__BWPREFIX__auth_node` SET `id`='1535', `pid`='862', `app_name`='manage', `type`='system', `title`='获取代码审核状态', `menu_path`='/manage/member/getAuditStatus', `name`='/manage/member.Miniapp/getAuditStatus', `auth_name`='manage_member_getAuditStatus', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1601308907', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1535');
UPDATE `__BWPREFIX__auth_node` SET `id`='1536', `pid`='862', `app_name`='manage', `type`='system', `title`='发布小程序', `menu_path`='/manage/member/publishCode', `name`='/manage/member.Miniapp/publishCode', `auth_name`='manage_member_publishCode', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1601311068', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1536');
UPDATE `__BWPREFIX__auth_node` SET `id`='1537', `pid`='862', `app_name`='manage', `type`='system', `title`='强制撤销审核', `menu_path`='/manage/member/resetAudit', `name`='/manage/member.Miniapp/resetAudit', `auth_name`='manage_member_resetAudit', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1601311131', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1537');
UPDATE `__BWPREFIX__auth_node` SET `id`='1539', `pid`='1172', `app_name`='manage', `type`='system', `title`='物流快递', `menu_path`='/manage/member/sys/express/index', `name`='/manage/member.sys.Express/index', `auth_name`='manage_member_sys_express_index', `param`='', `target`='_self', `condition`='', `ismenu`='1', `icon`='fa fa-bus', `remark`='', `status`='0', `create_time`='1602124262', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1539');
UPDATE `__BWPREFIX__auth_node` SET `id`='1540', `pid`='1539', `app_name`='manage', `type`='system', `title`='导出', `menu_path`='/manage/member/sys/express/export', `name`='/manage/member.sys.Express/export', `auth_name`='manage_member_sys_express_export', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1602124367', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1540');
UPDATE `__BWPREFIX__auth_node` SET `id`='1541', `pid`='1539', `app_name`='manage', `type`='system', `title`='添加', `menu_path`='/manage/member/sys/express/add', `name`='/manage/member.sys.Express/add', `auth_name`='manage_member_sys_express_add', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1602124487', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1541');
UPDATE `__BWPREFIX__auth_node` SET `id`='1542', `pid`='1539', `app_name`='manage', `type`='system', `title`='编辑', `menu_path`='/manage/member/sys/express/edit', `name`='/manage/member.sys.Express/edit', `auth_name`='manage_member_sys_express_edit', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1602124567', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1542');
UPDATE `__BWPREFIX__auth_node` SET `id`='1543', `pid`='1539', `app_name`='manage', `type`='system', `title`='修改', `menu_path`='/manage/member/sys/express/modify', `name`='/manage/member.sys.Express/modify', `auth_name`='manage_member_sys_express_modify', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1602124635', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1543');
UPDATE `__BWPREFIX__auth_node` SET `id`='1544', `pid`='1539', `app_name`='manage', `type`='system', `title`='删除', `menu_path`='/manage/member/sys/express/del', `name`='/manage/member.sys.Express/del', `auth_name`='manage_member_sys_express_del', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1602124720', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1544');
UPDATE `__BWPREFIX__auth_node` SET `id`='1558', `pid`='1250', `app_name`='manage', `type`='system', `title`='添加规则', `menu_path`='/manage/member.wechatKeyword/addKeys', `name`='/manage/member.wechatKeyword/addKeys', `auth_name`='manage_member_wechatKeyword_addKeys', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1602837519', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1558');
UPDATE `__BWPREFIX__auth_node` SET `id`='1559', `pid`='1250', `app_name`='manage', `type`='system', `title`='预览text', `menu_path`='/manage/member.wechatKeyword/previewText', `name`='/manage/member.wechatKeyword/previewText', `auth_name`='manage_member_wechatKeyword_previewText', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1603100713', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1559');
UPDATE `__BWPREFIX__auth_node` SET `id`='1560', `pid`='1250', `app_name`='manage', `type`='system', `title`='预览news', `menu_path`='/manage/member.wechatKeyword/previewNews', `name`='/manage/member.wechatKeyword/previewNews', `auth_name`='manage_member_wechatKeyword_previewNews', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1603113498', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1560');
UPDATE `__BWPREFIX__auth_node` SET `id`='1561', `pid`='1250', `app_name`='manage', `type`='system', `title`='选择图文', `menu_path`='/manage/member.wechatKeyword/select', `name`='/manage/member.wechatKeyword/select', `auth_name`='manage_member_wechatKeyword_select', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1603115169', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1561');
UPDATE `__BWPREFIX__auth_node` SET `id`='1562', `pid`='1250', `app_name`='manage', `type`='system', `title`='预览图片', `menu_path`='/manage/member.wechatKeyword/previewImage', `name`='/manage/member.wechatKeyword/previewImage', `auth_name`='manage_member_wechatKeyword_previewImage', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1603119741', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1562');
UPDATE `__BWPREFIX__auth_node` SET `id`='1563', `pid`='1250', `app_name`='manage', `type`='system', `title`='预览music', `menu_path`='/manage/member.wechatKeyword/previewMusic', `name`='/manage/member.wechatKeyword/previewMusic', `auth_name`='manage_member_wechatKeyword_previewMusic', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1603126619', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1563');
UPDATE `__BWPREFIX__auth_node` SET `id`='1564', `pid`='1250', `app_name`='manage', `type`='system', `title`='预览video', `menu_path`='/manage/member.wechatKeyword/previewVideo', `name`='/manage/member.wechatKeyword/previewVideo', `auth_name`='manage_member_wechatKeyword_previewVideo', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1603158266', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1564');
UPDATE `__BWPREFIX__auth_node` SET `id`='1565', `pid`='1250', `app_name`='manage', `type`='system', `title`='预览语音', `menu_path`='/manage/member.wechatKeyword/previewVoice', `name`='/manage/member.wechatKeyword/previewVoice', `auth_name`='manage_member_wechatKeyword_previewVoice', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1603159167', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1565');
UPDATE `__BWPREFIX__auth_node` SET `id`='1566', `pid`='1500', `app_name`='manage', `type`='system', `title`='添加图文', `menu_path`='/manage/member.wechatKeyword/addMaterial', `name`='/manage/member.wechatKeyword/addMaterial', `auth_name`='manage_member_wechatKeyword_addMaterial', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1603170723', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1566');
UPDATE `__BWPREFIX__auth_node` SET `id`='1567', `pid`='1250', `app_name`='manage', `type`='system', `title`='状态修改', `menu_path`='/manage/member.wechatKeyword/modify', `name`='/manage/member.wechatKeyword/modify', `auth_name`='manage_member_wechatKeyword_modify', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1603174720', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1567');
UPDATE `__BWPREFIX__auth_node` SET `id`='1583', `pid`='834', `app_name`='manage', `type`='system', `title`='关注回复配置', `menu_path`='/manage/member.WechatKeyword/subscribe', `name`='/manage/member.WechatKeyword/subscribe', `auth_name`='manage_member_WechatKeyword_subscribe', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='fa fa-wechat', `remark`='', `status`='1', `create_time`='1603194063', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1583');
UPDATE `__BWPREFIX__auth_node` SET `id`='1584', `pid`='1443', `app_name`='manage', `type`='system', `title`='站内信列表', `menu_path`='/manage/member/message/list', `name`='/manage/member.Index/messageList', `auth_name`='manage_member_message_list', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1603273452', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1584');
UPDATE `__BWPREFIX__auth_node` SET `id`='1585', `pid`='1443', `app_name`='manage', `type`='system', `title`='站内信已读', `menu_path`='/manage/member/message/read', `name`='/manage/member.Index/messageRead', `auth_name`='manage_member_message_read', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1603273452', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1585');
UPDATE `__BWPREFIX__auth_node` SET `id`='1588', `pid`='1412', `app_name`='manage', `type`='system', `title`='发送站内信', `menu_path`='/manage/member/message/send', `name`='/manage/member.User/sendMessage', `auth_name`='manage_member_message_send', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1603352194', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1588');
UPDATE `__BWPREFIX__auth_node` SET `id`='1589', `pid`='1172', `app_name`='manage', `type`='system', `title`='已购应用', `menu_path`='/manage/member/miniapp/my/index', `name`='/manage/member.MemberMiniapp/index', `auth_name`='manage_member_miniapp_my_index', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='fa fa-gift', `remark`='', `status`='1', `create_time`='1591779441', `update_time`='0', `sort`='999', `scopes`='member' WHERE (`id`='1589');
UPDATE `__BWPREFIX__auth_node` SET `id`='1590', `pid`='1500', `app_name`='manage', `type`='system', `title`='编辑图文', `menu_path`='/manage/member.wechatKeyword/editMaterial', `name`='/manage/member.wechatKeyword/editMaterial', `auth_name`='manage_member_wechatKeyword_editMaterial', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1603949972', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1590');
UPDATE `__BWPREFIX__auth_node` SET `id`='1604', `pid`='1396', `app_name`='manage', `type`='system', `title`='删除', `menu_path`='/manage/member.GroupData/del', `name`='/manage/member.GroupData/softdleting', `auth_name`='manage_member_GroupData_del', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1605072083', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1604');
UPDATE `__BWPREFIX__auth_node` SET `id`='1840', `pid`='1314', `app_name`='manage', `type`='system', `title`='插件市场', `menu_path`='/manage/member/plugin', `name`='/manage/member.plugin.Plugin/index', `auth_name`='manage_member_plugin', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='fa fa-gift', `remark`='', `status`='1', `create_time`='1591779441', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1840');
UPDATE `__BWPREFIX__auth_node` SET `id`='1841', `pid`='1840', `app_name`='manage', `type`='system', `title`='详情', `menu_path`='/manage/member/plugin/detail', `name`='/manage/member.plugin.Plugin/detail', `auth_name`='', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='fa fa-gift', `remark`='', `status`='1', `create_time`='1591779441', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1841');
UPDATE `__BWPREFIX__auth_node` SET `id`='1842', `pid`='1840', `app_name`='manage', `type`='system', `title`='购买', `menu_path`='/manage/member/plugin/buy', `name`='/manage/member.plugin.Plugin/buy', `auth_name`='', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='fa fa-gift', `remark`='', `status`='1', `create_time`='1591779441', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='1842');
UPDATE `__BWPREFIX__auth_node` SET `id`='2101', `pid`='1589', `app_name`='manage', `type`='system', `title`='设置默认应用', `menu_path`='/manage/member/miniapp/my/default', `name`='/manage/member.MemberMiniapp/setDefaultMiniapp', `auth_name`='_manage_member_member_miniapp_default', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='fa fa-gift', `remark`='', `status`='1', `create_time`='1591779441', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='2101');
UPDATE `__BWPREFIX__auth_node` SET `id`='2375', `pid`='834', `app_name`='manage', `type`='system', `title`='小程序订阅消息', `menu_path`='/manage/member/wechat/message/template/index', `name`='/manage/member.WechatMessageTemplate/index', `auth_name`='manage_member_wechat_message_template_index', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='fa fa-server', `remark`='', `status`='1', `create_time`='1613642074', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='2375');
UPDATE `__BWPREFIX__auth_node` SET `id`='2376', `pid`='2375', `app_name`='manage', `type`='system', `title`='导出', `menu_path`='/manage/member/wechat/message/template/export', `name`='/manage/member.WechatMessageTemplate/export', `auth_name`='manage_member_wechat_message_template_export', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='fa fa-server', `remark`='', `status`='1', `create_time`='1613642074', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='2376');
UPDATE `__BWPREFIX__auth_node` SET `id`='2377', `pid`='2375', `app_name`='manage', `type`='system', `title`='新增', `menu_path`='/manage/member/wechat/message/template/add', `name`='/manage/member.WechatMessageTemplate/add', `auth_name`='manage_member_wechat_message_template_add', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='fa fa-server', `remark`='', `status`='1', `create_time`='1613642074', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='2377');
UPDATE `__BWPREFIX__auth_node` SET `id`='2378', `pid`='2375', `app_name`='manage', `type`='system', `title`='编辑', `menu_path`='/manage/member/wechat/message/template/edit', `name`='/manage/member.WechatMessageTemplate/edit', `auth_name`='manage_member_wechat_message_template_edit', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='fa fa-server', `remark`='', `status`='1', `create_time`='1613642074', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='2378');
UPDATE `__BWPREFIX__auth_node` SET `id`='2379', `pid`='2375', `app_name`='manage', `type`='system', `title`='修改', `menu_path`='/manage/member/wechat/message/template/modify', `name`='/manage/member.WechatMessageTemplate/modify', `auth_name`='manage_member_wechat_message_template_modify', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='fa fa-server', `remark`='', `status`='1', `create_time`='1613642074', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='2379');
UPDATE `__BWPREFIX__auth_node` SET `id`='2380', `pid`='2375', `app_name`='manage', `type`='system', `title`='删除', `menu_path`='/manage/member/wechat/message/template/del', `name`='/manage/member.WechatMessageTemplate/del', `auth_name`='manage_member_wechat_message_template_del', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='fa fa-server', `remark`='', `status`='1', `create_time`='1613642074', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='2380');
UPDATE `__BWPREFIX__auth_node` SET `id`='2452', `pid`='834', `app_name`='manage', `type`='system', `title`='订阅消息发送记录', `menu_path`='/manage/member/wechat/message/log/index', `name`='/manage/member.WechatMessageLog/index', `auth_name`='manage_member_wechat_message_log_index', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1618306440', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='2452');
UPDATE `__BWPREFIX__auth_node` SET `id`='2453', `pid`='2452', `app_name`='manage', `type`='system', `title`='删除', `menu_path`='/manage/member/wechat/message/log/del', `name`='/manage/member.WechatMessageLog/del', `auth_name`='manage_member_wechat_message_log_del', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1618306880', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='2453');
UPDATE `__BWPREFIX__auth_node` SET `id`='2459', `pid`='2460', `app_name`='manage', `type`='system', `title`='插件菜单', `menu_path`='/manage/member/plugin/core/menu', `name`='/manage/member.plugin.Core/menu', `auth_name`='manage_member_plugin_core_menu', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1619589595', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='2459');
UPDATE `__BWPREFIX__auth_node` SET `id`='2460', `pid`='1314', `app_name`='manage', `type`='system', `title`='插件中心', `menu_path`='/manage/member/plugin/core/index', `name`='/manage/member.plugin.Core/index', `auth_name`='manage_member_plugin_core_index', `param`='', `target`='_self', `condition`='', `ismenu`='1', `icon`='fa fa-gamepad', `remark`='', `status`='1', `create_time`='1619513960', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='2460');
UPDATE `__BWPREFIX__auth_node` SET `id`='2742', `pid`='1589', `app_name`='manage', `type`='system', `title`='应用续费', `menu_path`='/manage/member/miniapp/my/renew', `name`='/manage/member.MemberMiniapp/renew', `auth_name`='manage_member_miniapp_my_renew', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1621914155', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='2742');
UPDATE `__BWPREFIX__auth_node` SET `id`='2743', `pid`='828', `app_name`='manage', `type`='system', `title`='子账号', `menu_path`='/manage/member', `name`='/manage/member', `auth_name`='manage_member', `param`='', `target`='_self', `condition`='', `ismenu`='1', `icon`='fa fa-street-view', `remark`='', `status`='1', `create_time`='1621929497', `update_time`='0', `sort`='999', `scopes`='member' WHERE (`id`='2743');
UPDATE `__BWPREFIX__auth_node` SET `id`='2753', `pid`='1412', `app_name`='manage', `type`='system', `title`='详情', `menu_path`='/manage/member/user/detail', `name`='/manage/member.User/detail', `auth_name`='manage_member_user_detail', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1622109394', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='2753');
UPDATE `__BWPREFIX__auth_node` SET `id`='2763', `pid`='835', `app_name`='manage', `type`='system', `title`='头条微信支付', `menu_path`='/manage/member.setting/tt_wechat', `name`='/manage/member.setting/tt_wechat', `auth_name`='manage_member_setting_tt_wechat', `param`='', `target`='_self', `condition`='', `ismenu`='0', `icon`='', `remark`='', `status`='1', `create_time`='1622193932', `update_time`='0', `sort`='0', `scopes`='member' WHERE (`id`='2763');
--给租户角色赋值
INSERT INTO `__BWPREFIX__auth_group_node` (`id`, `group_id`, `node_id`, `node_name`, `type`, `auth_name`) VALUES (null, '62', '2812', '/', 'system', '');
INSERT INTO `__BWPREFIX__auth_group_node` (`id`, `group_id`, `node_id`, `node_name`, `type`, `auth_name`) VALUES (null, '62', '2811', '/', 'system', '');
INSERT INTO `__BWPREFIX__auth_group_node` (`id`, `group_id`, `node_id`, `node_name`, `type`, `auth_name`) VALUES (null, '62', '2810', '/', 'system', '');

-- ----------------------------
-- 2021/7/8 应用套餐字段变更
-- ----------------------------
--应用套餐增加价格规格
ALTER TABLE __BWPREFIX__miniapp_module ADD price_json text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT '价格套餐配置' AFTER update_time;
--修改表应用套餐组id字段
ALTER TABLE `__BWPREFIX__miniapp_module` MODIFY COLUMN `group_id` varchar(255) NOT NULL COMMENT '组id' AFTER `miniapp_id`;
--应用增加头条支付和标签字段
ALTER TABLE __BWPREFIX__miniapp ADD `is_tt_wechat_pay` enum('0','1') DEFAULT '0' COMMENT '头条微信支付:0=关闭,1=开启' AFTER diy_member_ids;
ALTER TABLE __BWPREFIX__miniapp ADD `label` varchar(100) DEFAULT NULL COMMENT '标签' AFTER is_tt_wechat_pay;
