<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace app\api\controller\cloud;

use buwang\base\BaseController;
use app\common\model\User;

/**bwsaas 云服务接口基类
 * Class Basic
 * @package app\api\controller\cloud
 */
class Basic extends BaseController
{
    protected $service; //服务调用对象
    protected $service_class; //服务调用类名
    protected function initialize()
    {
        parent::initialize();
        $this->service = app('buwang\service\MiniappCloudService');
        $this->service_class = \buwang\service\MiniappCloudService::class;
    }

}