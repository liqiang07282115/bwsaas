<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace app\api\controller\v1;

use buwang\base\Manage;
use app\common\model\UserRecharge;
use buwang\facade\WechatPay;

/**
 * 框架用到的充值回调
 */
class Notify extends Manage
{
    /**
     * 用户钱包余额充值回调(小程序)
     * @param $appid
     * @return void
     */
    public function rechargeMini($appid)
    {
        $response = WechatPay::doPay($appid)->handlePaidNotify(function ($message, $fail) use ($appid) {
            //记录回调信息至日志
            if ($message['return_code'] === 'SUCCESS' && $message['result_code'] === 'SUCCESS') {
                $ispay = WechatPay::doPay($appid)->order->queryByOutTradeNumber($message['out_trade_no']);
                if ($ispay['return_code'] === 'SUCCESS' && $ispay['result_code'] === 'SUCCESS' && $ispay['trade_state'] === 'SUCCESS') {
                    $res = UserRecharge::paySeccess($message['out_trade_no'], bcmul($ispay['total_fee'], 0.01, 2), true);
                    if (!$res) {
                        file_put_contents('notify_shop_pay.txt', date('Y-m-d H:i:s', time()) . '-' . '订单[' . $message['out_trade_no'] . ']回调更新支付状态失败，原因：' . UserRecharge::getError('未知') . PHP_EOL, FILE_APPEND);//写日志
                        return $fail(UserRecharge::getError('未知'));
                    }
                    return true;
                }
            }
            return $fail('通信失败,请稍后再通知我');
        });
        $response->send();

    }
    /**
     * 用户钱包余额充值回调(公众号)
     * @param $appid
     * @return void
     */
    public function rechargeOfficial($appid)
    {
        $response = WechatPay::doPay($appid,true)->handlePaidNotify(function ($message, $fail) use ($appid) {
            //记录回调信息至日志
            if ($message['return_code'] === 'SUCCESS' && $message['result_code'] === 'SUCCESS') {
                $ispay = WechatPay::doPay($appid,true)->order->queryByOutTradeNumber($message['out_trade_no']);
                if ($ispay['return_code'] === 'SUCCESS' && $ispay['result_code'] === 'SUCCESS' && $ispay['trade_state'] === 'SUCCESS') {
                    $res = UserRecharge::paySeccess($message['out_trade_no'], bcmul($ispay['total_fee'], 0.01, 2), true);
                    if (!$res) {
                        file_put_contents('notify_shop_pay.txt', date('Y-m-d H:i:s', time()) . '-' . '订单[' . $message['out_trade_no'] . ']回调更新支付状态失败，原因：' . UserRecharge::getError('未知') . PHP_EOL, FILE_APPEND);//写日志
                        return $fail(UserRecharge::getError('未知'));
                    }
                    return true;
                }
            }
            return $fail('通信失败,请稍后再通知我');
        });
        $response->send();

    }


    /**
     * 用户钱包余额充值回调(扫码支付)
     * @param $appid
     * @return void
     */
    public function rechargeNative($appid)
    {
        $response = WechatPay::doPay($appid)->handlePaidNotify(function ($message, $fail) use ($appid) {
            //记录回调信息至日志
            if ($message['return_code'] === 'SUCCESS' && $message['result_code'] === 'SUCCESS') {
                $ispay = WechatPay::doPay($appid)->order->queryByOutTradeNumber($message['out_trade_no']);
                if ($ispay['return_code'] === 'SUCCESS' && $ispay['result_code'] === 'SUCCESS' && $ispay['trade_state'] === 'SUCCESS') {
                    $res = UserRecharge::paySeccess($message['out_trade_no'], bcmul($ispay['total_fee'], 0.01, 2), true);
                    if (!$res) {
                        file_put_contents('notify_shop_pay.txt', date('Y-m-d H:i:s', time()) . '-' . '订单[' . $message['out_trade_no'] . ']回调更新支付状态失败，原因：' . UserRecharge::getError('未知') . PHP_EOL, FILE_APPEND);//写日志
                        return $fail(UserRecharge::getError('未知'));
                    }
                    return true;
                }
            }
            return $fail('通信失败,请稍后再通知我');
        });
        $response->send();
    }
}