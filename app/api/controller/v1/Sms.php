<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace app\api\controller\v1;

use app\manage\model\Member;
use app\common\model\User;
use buwang\base\BaseController;
use buwang\util\Caches;

/**
 * 验证码
 * Class Sms
 * @package app\api\controller\v1
 */
class Sms extends BaseController
{
    protected function initialize()
    {
        parent::initialize();
    }

    public function send()
    {
        $mobile = $this->request->post('mobile/s');
        $event = $this->request->post('event/s') ?: 'register';

        if (!$mobile || !preg_match_all("/^1\d{10}$/", $mobile)) {
            return $this->error('手机号不正确');
        }
        if (Caches::get("{$mobile}-{$event}")) return $this->error('验证码5分钟内有效');
        $last = \app\common\model\Sms::where(['mobile' => $mobile, 'event' => $event])->find();
        if ($last && time() - $last['create_time'] < 60) {
            return $this->error('发送频繁');
        }
        $ipSendTotal = \app\common\model\Sms::where(['ip' => get_real_ip()])->whereTime('create_time', '-1 hours')->count();
        if ($ipSendTotal >= 5) {
            return $this->error('发送频繁');
        }

        if ($event == 'register' && Member::where('mobile', $mobile)->find()) {
            return $this->error('手机号已存在');
        }
        //jyk 20210416 应用用户注册
        if($event == 'user_register'){
            $data = get_login_info();
            if (!$data['member_miniapp']) return $this->error('缺少应用信息');
            $user = User::where('member_miniapp_id', $data['member_miniapp']['id'])->where('mobile', $mobile)->find();
            if ($user) return $this->error('手机号已存在');
        }
        //jyk 20201209 用户更改密码
        if ($event == 'password') {
             $data = get_login_info();
            if (!$data['member_miniapp']) return $this->error('缺少应用信息');
            $user = User::where('member_miniapp_id', $data['member_miniapp']['id'])->where('mobile', $mobile)->find();
            if (!$user) return $this->error('找不到手机号');
        }
        if ($event == 'change_phone') {
            $data = get_login_info();
            if (!$data['member_miniapp']) return $this->error('缺少应用信息');
            $user = User::where('member_miniapp_id', $data['member_miniapp']['id'])->where('mobile', $mobile)->find();
            if ($user) return $this->error('手机号已存在');
        }

        $code = mt_rand(1000, 9999);
        \app\common\model\Sms::beginTrans();
        try {
            if (!\app\common\model\Sms::send($mobile, $code, $event)) {
                \app\common\model\Sms::rollbackTrans();
                return $this->error(\app\common\model\Sms::getError());
            }

            \app\common\model\Sms::commitTrans();
            return $this->success('发送成功');
        } catch (\Exception $e) {
            \app\common\model\Sms::rollbackTrans();
            return $this->error($e->getMessage());
        }
    }
}