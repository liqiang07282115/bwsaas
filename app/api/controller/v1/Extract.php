<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace app\api\controller\v1;

use app\api\controller\Basic;
use app\common\model\UserExtract;
use app\manage\model\User;

/**提现控制器
 * Class Extract
 * @package app\api\controller\v1
 */
class Extract extends Basic
{

    protected function initialize()
    {
        parent::initialize();
        $this->isUserAuth();
    }

    /**
     * 提现信息提交接口(银行卡)
     * @ApiMethod(POST)
     */
    public function bankInfo()
    {
        $user = $this->user;//登录用户
        //查询审核数据
        if (request()->isGet()) {
            $user_id = $user['id'];//用户id
            //当前认证状态
            $info = UserExtract::getExtractInfo($user_id);
            return $this->success('查询成功', $info);
        }
        //提款信息
        if (request()->isPost()) {
            $type = $this->request->post("type/s");//绑定类型
            $bank_name = $this->request->post("bank_name/s");//银行名称
            $bank_username = $this->request->post('bank_username/s');//银行收款人姓名
            $bank_zone = $this->request->post('bank_zone/s');//开户地址
            $bank_detail = $this->request->post('bank_detail/s');//开户详细地址
            $bank_card = $this->request->post('bank_card/s');//银行卡号
            $wx_name = $this->request->post("wx_name/s");//微信账户人姓名
            $wxImag = $this->request->post('wxImag/s');//微信收款码
            $ali_name = $this->request->post("ali_name/s");//阿里账户人姓名
            $ali_account = $this->request->post("ali_account/s");//支付宝账号
            $aliImag = $this->request->post('aliImag/s');//支付宝收款码
            $safe_password = $this->request->post('safe_password/s');//安全密码
            $city_id = $this->request->post('city_id/s');//城市编码
            $res = UserExtract::bankInfo($user, $type, $bank_name, $bank_username, $bank_zone, $bank_detail, $bank_card, $wx_name, $wxImag, $ali_name, $ali_account, $aliImag, $safe_password, $city_id, true);
            if (!$res) return $this->error(UserExtract::getError());
            return $this->success('绑定成功', $res);
        }
    }


    /**
     * 余额提现
     * ApiMethod (POST)
     * @param float $amount 余额
     * @param string $type 提现方式,bank/wx/alipay
     * @param string $paypwd 支付密码
     */
    public function extract()
    {
        $user = $this->user;//登录用户
        if (request()->isGet()) {
            $balance = User::getMoney($user['id']);//余额
            $min = bw_config('extract_money_min', 0);//最低提现金额
            $fee = bw_config('extract_money_fee', 0);//提现手续费
            $desc = bw_config('extract_desc', '');//提现标语
            $extract_total = UserExtract::getExtractTotal($user['id']);//累计提现额
            $extract_review_total = UserExtract::getExtractingTotal($user['id']);//提现中数额

            //TODO 这里需要获取用户已绑定的支付方式,并返回给前台
            $bind = UserExtract::getExtractInfo($user['id']);
            return $this->success('获取成功', compact('balance', 'min', 'fee', 'bind', 'desc', 'extract_total', 'extract_review_total'));
        } elseif (request()->isPost()) {
            $amount = $this->request->post('amount/f');
            $type = $this->request->post('type/s');
            $safe_password = $this->request->post('safe_password/s');
            $mark = $this->request->post('mark/s');//备注
            $res = UserExtract::extract($user, $amount, 'money', $type, $safe_password, $mark, true);
            if (!$res) return $this->error(UserExtract::getError());
            return $this->success('提现申请提交成功', $res);
        }
    }

    /**
     * 提现列表
     */
    public function extractList()
    {
        $user = $this->user;//登录用户
        if (request()->isGet()) {
            $page = $this->request->get('page/d');
            $limit = $this->request->get('limit/d');
            $list = UserExtract::extractList($user['id'], $page, $limit);
            return $this->success('查询成功', $list);
        }
    }
}