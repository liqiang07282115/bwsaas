<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace app\api\controller;

use buwang\base\Api;
use app\common\model\User;

class Basic extends Api
{
    protected $uid;
    protected function initialize()
    {
        parent::initialize();
        $this->uid = isset($this->user->id) ? $this->user->id : 0;
    }

    /**
     * 通过invite_code获取邀请人用户信息
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    protected function getInviteCodeUser()
    {
        self::isUserAuth();
        $user_id = de_code(strtoupper($this->request->param('invite_code/s')));
        if (empty($user_id)) {
            return $this->code(200)->error('邀请码信息错误');
        }
        $result = User::where(['id' => $user_id])->where('member_miniapp_id', $this->bwapp->id)->field('id,nickname,invite_code,avatar')->find();
        if (empty($result)) {
            return $this->code(200)->error('没有查询到用户内容');
        } else {
            return $this->code(200)->success('获取成功',$result);
        }
    }
}