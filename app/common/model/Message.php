<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace app\common\model;

use buwang\base\BaseModel;
use think\facade\Db;

/**
 * 站内信
 * Class Message
 * @package app\common\model
 */
class Message extends BaseModel
{

    // 追加属性
    protected $append = [
        'create_time_text',
    ];


    /**
     * 发送站内信
     * @param int $type 类型:1=平台->租户,2=平台->用户,3=租户->用户
     * @param int $sender_id 发送者ID
     * @param string $receiver_ids 接收者ID,多个接收者时使用,分割
     * @param string $msg 消息内容
     * @return bool
     * @throws \Exception
     */
    public static function send(int $type, int $sender_id, string $receiver_ids, string $msg)
    {
        !in_array($type, [1, 2, 3, 4]) && exception('消息类型有误');

        $data = [];
        foreach (array_filter(explode(',', $receiver_ids)) as $receiver_id) {
            $data[] = compact('type', 'sender_id', 'receiver_id', 'msg');
        }

        $data && Db::transaction(function () use ($data) {
            (new self())->saveAll($data);
        });

        return true;
    }


    public function getCreateTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['create_time']) ? $data['create_time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }

    /**
     * 消息列表
     * @param int $type 类型:1=平台->租户,2=平台->用户,3=租户->用户
     * @param int $receiver_id 接收者ID
     * @param int $page 页数
     * @param int $limit 条数
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function list(int $type, int $receiver_id, int $page = 1, int $limit = 10,$status = null)
    {
        $self = self::where(['type' => $type, 'receiver_id' => $receiver_id, 'delete_time' => 0]);
        if($page&&$limit)$self = $self->page($page, $limit);
        if($status !==null)$self = $self->where('status',$status);
        $list = $self->order('id DESC')->field('id,sender_id,msg,status,create_time')->select()->toArray();
        return $list;
    }

    /**
     * 消息数量
     * @param int $type
     * @param int $receiver_id
     * @param int $status
     * @return int
     */
    public static function total(int $type, int $receiver_id, $status = -1)
    {
        $where = ['type' => $type, 'receiver_id' => $receiver_id, 'delete_time' => 0];
        in_array($status, [0, 1]) && $where['status'] = $status;

        return self::where($where)->count() ?: 0;
    }

    /**
     * 消息已读
     * @param int $id 消息ID
     * @return bool
     */
    public static function read(int $id)
    {
        Db::transaction(function () use ($id) {
            self::update(['status' => 1], ['id' => $id, 'status' => 0]);
        });

        return true;
    }

    /**
     * 全部消息已读
     * @param int $type 消息类型1:平台->租户,2:平台->用户,3:租户->用户,4:租户->用户系统通知)
     * @param int $receiver_id 接收者ID
     * @return bool
     */
    public static function readAll(int $type, int $receiver_id)
    {
        Db::transaction(function () use ($type, $receiver_id) {
            self::update(['status' => 1], ['type' => $type, 'receiver_id' => $receiver_id, 'status' => 0]);
        });
        return true;
    }
}