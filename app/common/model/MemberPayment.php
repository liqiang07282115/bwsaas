<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace app\common\model;

use buwang\base\BaseModel;

class MemberPayment extends BaseModel
{
    protected $pk = 'id';
    /**
     * 获取配置参数
     * @param int $bw_member_app_id
     * @param string $payType
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function getPayConfig(int $bw_member_app_id, string $payType)
    {
        $rel = self::field('config')->where(['member_miniapp_id' => $bw_member_app_id, 'apiname' => $payType])->find();
        if (empty($rel) || !$rel) {
            return [];
        }
        $config = json_decode($rel['config'], true);
        $config['cert_path'] = empty($config['cert_path']) ? '' : $config['cert_path'];
        $config['key_path'] = empty($config['key_path']) ? '' : $config['key_path'];
        return $config;
    }
}