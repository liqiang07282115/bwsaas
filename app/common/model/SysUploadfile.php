<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace app\common\model;

use buwang\base\TimeModel;

class SysUploadfile extends TimeModel
{

    /**添加附件记录
     * @param $original_name 文件名
     * @param $file_size   文件大小
     * @param $mime_type   文件类型
     * @param $url      存储路径
     * @param string $upload_type 上传类型
     * @param int $create_time 创建时间
     * @return SysUploadfile|\think\Model
     */
    public static function fileAdd($original_name, $file_size, $mime_type, $url, $upload_type = 'local', $create_time = 0)
    {
        $file_ext = '';
        //根据文件名取文件后缀
        $index = strrpos($original_name, ".");
        if ($index) $file_ext = substr($original_name, $index + 1);
        $data['original_name'] = $original_name;
        $data['url'] = $url;
        $data['file_size'] = $file_size;
        $data['mime_type'] = $mime_type;
        $data['upload_type'] = $upload_type;
        $data['create_time'] = $create_time ? $create_time : time();
        $data['file_ext'] = $file_ext;
        return self::create($data);
    }

}