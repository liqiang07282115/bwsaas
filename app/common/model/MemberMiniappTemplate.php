<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace app\common\model;

use buwang\base\TimeModel;

/**
 * 租户应用升级管理表
 * Class MemberMiniapp
 * @package app\common\model
 */
class MemberMiniappTemplate extends TimeModel
{
    protected $deleteTime = false;
    /**
     * 关联应用
     * @return \think\model\relation\HasOne
     */
    public function miniapp()
    {
        return $this->hasOne('Miniapp', 'id', 'miniapp_id');
    }

    /**
     * 应用后台所属管理员
     * @return \think\model\relation\HasOne
     */
    public function member()
    {
        return $this->hasOne('Member', 'id', 'member_id');
    }
    //获取下拉列表
    public function getMiniappList()
    {
        return Miniapp::column('title', 'id');
    }

    /**
     * @param int $id
     * @return array
     */
    public function getMemberMiniappList(int $id = 0)
    {
        if(!$id) return MemberMiniapp::column('appname,version', 'id');
        return MemberMiniapp::where('miniapp_id',$id)->column('appname,version', 'id');
    }
    public function getThirdTemplate(int $template_id = 0){
        if(!$template_id) return MiniappThirdTemplate::column('template_id,user_version,user_desc', 'id');;
        return MiniappThirdTemplate::where('template_id',$template_id)->field('template_id,user_version,user_desc')->find();
    }

}
