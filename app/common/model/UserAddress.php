<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace app\common\model;

use buwang\base\BaseModel;
use buwang\traits\ModelTrait;

class UserAddress extends BaseModel
{

    use ModelTrait;

    protected $pk = 'id';

    /**
     * 获取用户收货地址并分页
     * @param $uid
     * @param int $page
     * @param int $limit
     * @param string $field
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function getUserValidAddressList($uid, $page = 1, $limit = 8, $field = '*')
    {
        if ($page) return self::userValidAddressWhere()->where('user_id', $uid)->order('create_time DESC')->field($field)->page((int)$page, (int)$limit)->select()->toArray() ?: [];
        else return self::userValidAddressWhere()->where('user_id', $uid)->order('create_time DESC')->field($field)->select()->toArray() ?: [];
    }

    /**
     * 设置用户地址查询初始条件
     * @param null $model
     * @param string $prefix
     * @return ModelTrait|\think\Model
     */
    public static function userValidAddressWhere($model = null, $prefix = '')
    {
        if ($prefix) $prefix .= '.';
        $model = self::getSelfModel($model);
        return $model->where("{$prefix}is_del", 0);
    }

}