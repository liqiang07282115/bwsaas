<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace app\common\model;

use buwang\base\BaseModel;

/**
 * 租户应用提交记录
 * Class MemberMiniapp
 * @package app\manage\model
 */
class MemberMiniappAudit extends BaseModel
{
    protected $pk = 'id';
    // 模型初始化
    protected static function init()
    {
        //TODO:初始化内容
    }

    /**
     * 添加编辑
     * @param array $where
     * @param array $data
     * @return MemberMiniappAudit|int|string
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function edit(array $where, array $data)
    {
        $rel = self::where($where)->find();
        if ($rel) {
            return self::where($where)->update($data);
        } else {
            return self::insert($data);
        }
    }
}