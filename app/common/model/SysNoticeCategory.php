<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace app\common\model;

use buwang\base\TimeModel;

class SysNoticeCategory extends TimeModel
{

    protected $name = "sys_notice_category";

    protected $deleteTime = false;



    public function getStatusList()
    {
        return ['0'=>'隐藏','1'=>'正常',];
    }


    /**得到基础条件
     * @param $status
     * @param null $model
     * @param string $alisa
     */
    public static function getBaseWhere($whereData = [], $model = null, $alisa = '')
    {
        if (!$model) {
            $model = new self;
            if ($alisa) $model = $model->alias($alisa);
        }
        if ($alisa) $alisa = $alisa . '.';
        $model = $model->where($alisa . 'status', 1);
        return $model;
    }


    /**
     * 基础列表
     */
    public static function getBaseList($whereData = [], $page = 0, $limit = 0, $sort = '',$field ="a.*",$where=[])
    {
        if(!$sort)$sort = 'a.weight desc,a.id desc';
        $self = self::getBaseWhere($whereData, null, 'a')->where($where)->field($field);
        if($page&&$limit)$self = $self->page($page, $limit);
        $list = $self->orderRaw($sort)->select();
        $count = self::getBaseWhere($whereData, null, 'a')
            ->where($where)
            ->count();
        return compact('list', 'count','page','limit');
    }


}