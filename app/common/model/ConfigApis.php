<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace app\common\model;

use buwang\base\BaseModel;

class ConfigApis extends BaseModel
{

    protected $pk = 'id';

    /**
     * 读取接口信息
     */
    public static function Config($name)
    {
        $info = self::where(['name' => $name])->find();
        if (empty($info)) {
            return;
        } else {
            $data = empty($info->apikey) ? [] : $info->apikey;
            return json_decode($data, true);
        }
    }

    /**
     * 修改配置或新增
     */
    public static function edit($name, array $apikey = [])
    {
        $apikey = json_encode($apikey);
        $info = self::where(['name' => $name])->find();
        if (empty($info)) {
            $data['name'] = trim($name);
            $data['apikey'] = $apikey;
            return self::insert($data);
        } else {
            $info->apikey = $apikey;
            return $info->save();
        }
    }
}