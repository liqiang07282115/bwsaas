<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace app\common\model;

use buwang\base\TimeModel;

/**
 * Class WechatMessageLog
 * @package app\common\model
 */
class WechatMessageLog extends TimeModel
{

    protected $name = "wechat_message_log";

    protected $deleteTime = false;


    public function getTypeList()
    {
        return ['0' => '小程序订阅消息', '1' => '公众号模板消息',];
    }

    public function getStatusList()
    {
        return ['0' => '失败', '1' => '成功',];
    }
    /**管理员
     * @return mixed
     */
    public function template()
    {
        return $this->belongsTo('\app\common\model\WechatMessageTemplate', 'key', 'key');
    }


}