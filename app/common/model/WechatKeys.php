<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace app\common\model;

use buwang\base\BaseModel;

class WechatKeys extends BaseModel
{
    protected $append = ['is_mp'];

    public function memberMiniapp()
    {
        return $this->belongsTo('\app\common\model\MemberMiniapp', 'member_miniapp_id', 'id');
    }

    public function getIsMpAttr($value,$data)
    {
        $is_mp = 1;
        $memberMiniapp = \app\manage\model\MemberMiniapp::find($data['member_miniapp_id']);
        $memberMiniapp && ($memberMiniapp['miniapp_appid'] == $data['appid']) && $is_mp = 0;
        return $is_mp;
    }
}