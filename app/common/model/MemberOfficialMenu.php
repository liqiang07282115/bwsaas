<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace app\common\model;

use buwang\base\BaseModel;

class MemberOfficialMenu extends BaseModel
{
    protected $pk = 'id';

    /**
     * 菜单修正
     * @param $appid
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function official_menu($appid)
    {
        $official_menu = self::where(['member_miniapp_id' => $appid])->order('sort asc,id desc')->select();
        $menu = [];
        $i = 0;
        //类型判断
        foreach ($official_menu as $value) {
            $i++;
            $menu[$i]['id'] = $value['id'];
            $menu[$i]['parent_id'] = $value['parent_id'];
            $menu[$i]['type'] = $value['types'];
            $menu[$i]['name'] = $value['name'];
            switch ($value['types']) {
                case 'click':
                case 'scancode_push':
                case 'scancode_waitmsg':
                case 'pic_sysphoto':
                case 'pic_photo_or_album':
                case 'pic_weixin':
                case 'location_select':
                    $menu[$i]['key'] = $value['key'];
                    break;
                case 'media_id':
                case 'view_limited':
                    $menu[$i]['media_id'] = $value['key'];
                    break;
                case 'miniprogram':
                    $menu[$i]['url'] = $value['url'];
                    $menu[$i]['appid'] = $value['appid'];
                    $menu[$i]['pagepath'] = $value['pagepath'];
                    break;
                default:
                    $menu[$i]['url'] = $value['url'];
                    break;
            }
        }
        //重新排序
        $mpmenu = [];
        $i = 0;
        foreach ($menu as $value) {
            if ($value['parent_id'] == 0) {
                $val = $value;
                unset($val['id']);
                unset($val['parent_id']);
                $mpmenu[$i] = $val;
                foreach ($menu as $k => $v) {
                    $values = $v;
                    unset($values['id']);
                    unset($values['parent_id']);
                    if ($v['parent_id'] == $value['id']) {
                        $mpmenu[$i]['sub_button'][] = $values;
                    }
                }
                $i++;
            }
        }

        return $mpmenu;
    }
}