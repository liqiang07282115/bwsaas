<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace app\common\model;

use buwang\base\BaseModel;

/**
 * 租户应用表
 * Class MemberMiniapp
 * @package app\manage\model
 */
class MemberMiniapp extends BaseModel
{
    // 模型初始化
    protected static function init()
    {
        //TODO:初始化内容
    }

    /**
     * 关联应用
     *
     * @return \think\model\relation\HasOne
     */
    public function miniapp()
    {
        return $this->hasOne('Miniapp', 'id', 'miniapp_id');
    }

    /**
     * 应用后台所属管理员
     * @return \think\model\relation\HasOne
     */
    public function member()
    {
        return $this->hasOne('Member', 'id', 'member_id');
    }

    /**
     * 应用绑定的用户端口创始人
     * @return \think\model\relation\HasOne
     */
    public function user()
    {
        return $this->hasOne('User', 'id', 'member_id');
    }

    /**
     * 用户购买的应用
     * @return \think\model\relation\HasOne
     */
    public function order()
    {
        return $this->hasOne('MemberMiniappOrder', 'id', 'miniapp_order_id');
    }

    /**通过租户id和应用标识查询租户应用id
     * @param $dir
     * @param $member_id
     */
    public static function getMemberMiniappIdByDir($dir,$member_id){
        //查询应用
        $miniapp = Miniapp::where('dir', $dir)->find();
        if (!$miniapp) throw new \Exception('未找到应用');
        //得到购买了应用的租户
        return self::where('miniapp_id', $miniapp['id'])->where('member_id',$member_id)->value('id');
    }


    /**
     * 租户已购买的租户应用列表
     */
    public static function getMemberBuyList($member_id,$field = '*'){
        return self::where('member_id',$member_id)->column($field,'id');
    }

    /**
     * 租户已购买的租户应用名
     */
    public static function getMemberBuyNames($member_id){
        $list  =  self::where('member_id',$member_id)->column("*",'id');
        foreach ($list as &$memberMiniapp)
        {
            $memberMiniapp = "{$memberMiniapp['appname']} / {$memberMiniapp['id']}";
        }
        return $list;
    }

}
