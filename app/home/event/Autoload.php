<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace app\home\event;

class Autoload
{
//public function __construct()
//    {
//        require __DIR__ . '/../vendor/autoload.php';
//        file_put_contents('232323.txt','33333');
//    }
    public function handle()
    {
        require __DIR__ . '/../vendor/autoload.php';
        //file_put_contents('232323.txt', '33333');
        // 事件监听处理
    }
}