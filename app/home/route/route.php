<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

use think\facade\Route;

/**
 * 无需登录与鉴权的接口
 */
Route::rule('/', 'Index/index');
Route::rule('/index/hello', 'Index/hello');
Route::rule('/index/store', 'Index/store');
Route::rule('/index/about', 'Index/about');
Route::rule('/index/join', 'Index/join');
Route::rule('/index/help', 'Index/help');
Route::rule('/index/news', 'Index/news');
Route::rule('/index/newsDetail', 'Index/newsDetail');