<?php

namespace app\manage\controller\member;

use buwang\base\AdminBaseController;
use buwang\base\MemberBaseController;

/**
 * 微信消息记录
 */
class WechatMessageLog extends MemberBaseController
{
    use \buwang\traits\Crud;


    public function initialize()
    {
        parent::initialize();
        $this->layout && $this->app->view->engine()->layout($this->layout);
        $this->model = new \app\common\model\WechatMessageLog();

        $this->assign('getTypeList', $this->model->getTypeList());

        $this->assign('getStatusList', $this->model->getStatusList());

    }
    /**
     * @NodeAnotation(title="列表")
     */
    public function index()
    {
        $member_miniapp_id = $this->member_miniapp_id;
        if ($this->request->isAjax()) {
            if (input('selectFieds')) {
                return $this->selectList();
            }
            list($page, $limit, $where) = $this->buildTableParames();
            $count = $this->model
                ->where($where)
                ->where('member_miniapp_id', $this->member_miniapp_id)
                ->count();
            $list = $this->model
                ->where($where)
                ->with('template')
                ->where('member_miniapp_id', $this->member_miniapp_id)
                ->page($page, $limit)
                ->order($this->sort)
                ->select();
            $data = [
                'total' => $count,
                'list' => $list,
                'member_miniapp_id' => $this->member_miniapp_id,
                'member_miniapp_id_e' => $member_miniapp_id
            ];
            return $this->success('success', $data);
        }
        return view();
    }
    /**
     * @NodeAnotation(title="删除")
     */
    public function del($id)
    {
        $row = $this->model->whereIn('id', $id)->select();
        if ($row->isEmpty()) return $this->error('数据不存在');
        foreach ($row as $v){
            $this->isValidMember($v['member_miniapp_id']);
        }
        try {
            $save = $row->delete();
        } catch (\Exception $e) {
            return $this->error('删除失败');
        }
        if ($save) return $this->success('删除成功');
        else return $this->error('删除失败');
    }


}