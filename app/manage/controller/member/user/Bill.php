<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace app\manage\controller\member\user;

use buwang\base\MemberBaseController;

//use EasyAdmin\annotation\ControllerAnnotation;
//use EasyAdmin\annotation\NodeAnotation;
//use think\App;
use app\common\model\UserBill;
use app\common\model\MemberMiniapp;

/**
 * @ControllerAnnotation(title="用户账单表")
 */
class Bill extends MemberBaseController
{
    use \buwang\traits\Crud;

    protected $relationSearch = true;

    public function initialize()
    {
        parent::initialize();
        $this->layout && $this->app->view->engine()->layout($this->layout);
        $this->model = new UserBill();
        $this->assign('getMemberMiniappList', $this->model->getMemberMiniappList($this->user->top_id));//租户应用

        $this->assign('getUserList', $this->model->getUserList($this->user->top_id));

        $this->assign('getPmList', $this->model->getPmList());

        $this->assign('getCategoryList', $this->model->getCategoryList());

        $this->assign('getTypeList', $this->model->getTypeList($this->user->top_id, 'money,integral'));

        $this->assign('getStatusList', $this->model->getStatusList());

    }


    /**
     * @NodeAnotation(title="列表")
     */
    public function index()
    {
        $uid = $this->request->param('uid', 0);
        if($uid) $this->assign('uid', $uid);
        if ($this->request->isAjax()) {
            if (input('selectFieds')) {
                return $this->selectList();
            }
            //查询该租户的所有应用
            $ids = MemberMiniapp::where('member_id', $this->user->top_id)->column('id');
            if (!$ids) return $this->success('success', ['total' => 0, 'list' => []]);
            list($page, $limit, $where) = $this->buildTableParames();
            if($uid) $where[] = ['user_bill.uid', '=',$uid];
            $count = $this->model
                ->withJoin('user', 'LEFT')
                ->join('bw_member_miniapp memberMiniapp', 'memberMiniapp.id=user.member_miniapp_id', 'LEFT')
                ->where($where)
                ->where('category', 'in', ['money', 'integral'])
                ->where('user.member_miniapp_id', 'in', $ids)
                ->count();
            $list = $this->model
                ->withJoin('user', 'LEFT')
                ->join('bw_member_miniapp memberMiniapp', 'memberMiniapp.id=user.member_miniapp_id', 'LEFT')
                ->where($where)
                ->where('category', 'in', ['money', 'integral'])
                ->where('user.member_miniapp_id', 'in', $ids)
                ->page($page, $limit)
                ->order($this->sort)
                ->field('memberMiniapp.appname,memberMiniapp.head_img,memberMiniapp.miniapp_head_img,memberMiniapp.mp_head_img')  //新加字段
                ->select();
            $data = [
                'total' => $count,
                'list' => $list,
            ];
            return $this->success('success', $data);
        }
        return $this->fetch();
    }
}