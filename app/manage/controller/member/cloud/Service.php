<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace app\manage\controller\member\cloud;

use buwang\base\MemberBaseController;
use think\facade\Db;

class Service extends MemberBaseController
{
    protected $model = null;//模型实例

    protected function initialize()
    {
        parent::initialize();
        $this->layout && $this->app->view->engine()->layout($this->layout);
        $this->model = new \app\common\model\cloud\Service();
    }

    /**
     * 查看
     * @menu true
     */
    public function index()
    {
        if (request()->isAjax()) {
            /*$page = request()->get('page/d', 1);
            $limit = request()->get('limit/d', 10);

            //TODO 获取搜索条件
            //TODO 如果有不属于表内的字段,查询会报错
            $where = request()->get();
            unset($where['page']);
            unset($where['limit']);
            foreach ($where as $k => $v) {
                if(!$v && $v !== '0' && $v !== 0 && $v !== false) {
                    unset($where[$k]);
                }
            }*/

            try {
//                $total = $this->model->where($where)->count();
//                $list = $this->model->where($where)->order($this->model->getPk(), 'DESC')->page($page, $limit)->select();
                $sql = "SELECT s.id, name, s.file_name, 
                            IFNULL((SELECT amount FROM bw_cloud_wallet where member_id = {$this->uid} AND service_id = s.id),0) as amount,
                            IFNULL((SELECT total FROM bw_cloud_wallet where member_id = {$this->uid} AND service_id = s.id),0) as total 
                            FROM bw_cloud_service s";
                $list = Db::query($sql);
            } catch (\Exception $e) {
                return $this->error('查询失败', ['errorMsg' => $e->getMessage()]);
            }

            $data = compact('list');
            return $this->success('successful', $data);
        }

        return view();
    }
    /**
     * 详情
     * @menu true
     */
    public function detail()
    {
        if (request()->isPost()) {
            $id = $this->request->get('service_id', 0);
            if (!$id) return $this->error('服务参数不正确');
            //$service = \app\common\model\cloud\Service::find($id);
            $sql = "SELECT s.id, name, s.file_name, 
                            IFNULL((SELECT amount FROM bw_cloud_wallet where member_id = {$this->uid} AND service_id = s.id),0) as amount,
                            IFNULL((SELECT total FROM bw_cloud_wallet where member_id = {$this->uid} AND service_id = s.id),0) as total 
                            FROM bw_cloud_service s WHERE s.id = {$id}";
            $service = Db::query($sql);
            if (!$service) return $this->error('服务不存在');
            $service = $service[0];
            $packet = \app\common\model\cloud\Packet::where('service_id', $id)->select();

            return $this->success('successful', compact('service','packet'));
        }
        $id = $this->request->get('service_id', 0);
        $service_name = $this->request->get('service_name', '');
        $this->assign('service_id', $id);
        $this->assign('service_name', $service_name);
        return view();
    }

}
