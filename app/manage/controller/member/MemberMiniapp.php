<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace app\manage\controller\member;

use buwang\base\MemberBaseController;
use think\facade\View;
use app\common\model\MiniappTerm;


/**
 * @ControllerAnnotation(title="租户应用")
 */
class MemberMiniapp extends MemberBaseController
{
    use \buwang\traits\Crud;

    protected $relationSearch = true;

    public function initialize()
    {
        parent::initialize();
        $this->layout && $this->app->view->engine()->layout($this->layout);
        $this->model = new \app\common\model\MemberMiniapp();
    }

    /**
     * @NodeAnotation(title="列表")
     */
    public function index()
    {
        if (input('selectFieds')) {
            return $this->selectList();
        }
        list($page, $limit, $where) = $this->buildTableParames();
        $count = $this->model
            ->withJoin('miniapp')
            ->where("{$this->getTableName()}.member_id", $this->uid)
            ->where($where)
            ->count();
        $list = $this->model
            ->withJoin('miniapp')
            ->where("{$this->getTableName()}.member_id", $this->uid)
            ->where($where)
            ->order($this->sort)
            ->select();

        foreach ($list as &$item) {
            $item['create_time'] = date('Y-m-d H:i:s', $item['create_time']);
            //是否过期
            $is_expire = false;
            if($item['expire_time'] > 0 && $item['expire_time']< time()) $is_expire = true;
            //得到剩余天数
            $item['ex_days']  = MiniappTerm::getDaysRemaining($this->uid,null,$item['expire_time']);
            $item['is_expire'] = $is_expire;
            if($item['expire_time'] == 0){
                $item['expire_time'] = '永久';
            }else{
                $item['expire_time'] = date('Y-m-d H:i:s', $item['expire_time']);
            }
        }
        $data = [
            'default_miniapp_dir' => $this->user->default_miniapp_dir,
            'total' => $count,
            'list' => $list,
        ];
        if ($this->request->isAjax()) {
            return $this->success('success', $data);
        }
        View::assign(compact('data'));
        return view();
    }

    public function setDefaultMiniapp($miniapp_dir = '')
    {
        //TODO 应用是否存在
        //应用是否已购买
        //应用是否过期
        \app\common\model\Member::update(['default_miniapp_dir' => $miniapp_dir], ['id' => $this->user->id]);
        return $this->success('设置成功');
    }



    /**
     * 应用续费(废弃)
     */
    public function renew(){
        $dir = $this->request->get('dir/s');
        //查找续费套餐
        if ($this->request->isPost()) {
            $post = $this->request->post();
            $post = reform_keys($post);
            $post['member_id'] =$this->uid; //得到租户id
            try {
                (new MiniappTerm)->renew($post);
            } catch (\Exception $e) {
                return $this->error('续费失败:' . $e->getMessage());
            }
            return $this->success('续费成功');
        }
        $list = MiniappTerm::where('dir',$dir)->where('status',1)->order('weight desc,price asc')->select()->toArray();
//        if(!$list) return $this->error_jump('该应用未设置续费套餐，请联系系统管理员设置！');
        $price =  \app\common\model\member\Wallet::getMoney($this->uid);
        $days = MiniappTerm::getDaysRemaining($this->uid,$dir);
        View::assign(compact('list','price','days'));
        return view();
    }

}