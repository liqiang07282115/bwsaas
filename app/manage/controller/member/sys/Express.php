<?php

namespace app\manage\controller\member\sys;

use buwang\base\MemberBaseController;
//use EasyAdmin\annotation\ControllerAnnotation;
//use EasyAdmin\annotation\NodeAnotation;
//use think\App;
use buwang\util\CommonTool;
use jianyan\excel\Excel;
use think\facade\Db;
use think\facade\View;
/**
 * @ControllerAnnotation(title="快递公司表")
 */
class Express extends MemberBaseController
{
    use \buwang\traits\Crud;

    /**
     * 允许修改的字段
     * @var array
     */
    protected $allowModifyFileds = [
        'status',
        'sort',
        'remark',
        'is_delete',
        'is_auth',
        'is_show',
        'title',
    ];

    protected $sort = 'sort desc,id desc';

    public function initialize()
    {
        parent::initialize();
        $this->layout && $this->app->view->engine()->layout($this->layout);
        $this->model = new \app\common\model\system\SysExpress();
        
        $this->assign('getIsShowList', $this->model->getIsShowList());

    }

    /**
     * @NodeAnotation(title="列表")
     */
    public function index()
    {
        //得到该租户的所有日志
        $member_id = $this->user->top_id;
        if ($this->request->isAjax()) {
            if (input('selectFieds')) {
                return $this->selectList();
            }
            list($page, $limit, $where) = $this->buildTableParames();
            $count = $this->model
                ->where($where)
                ->where('member_id',$member_id)
                ->count();
            $list = $this->model
                ->where($where)
                ->where('member_id',$member_id)
                ->page($page, $limit)
                ->order($this->sort)
                ->select();
            $data = [
                'total' => $count,
                'list' => $list,
            ];
            return $this->success('success', $data);
        }
        return view();
    }

    /**
     * @NodeAnotation(title="添加")
     */
    public function add()
    {
        $member_id = $this->user->top_id;
        if ($this->request->isAjax()) {
            $post = $this->request->post();
            $post = reform_keys($post);
            $rule = [];
            $this->validate($post, $rule);
            try {
                $post['member_id']= $member_id;
                $save = $this->model->save($post);
            } catch (\Exception $e) {
                return $this->error('保存失败:' . $e->getMessage());
            }
            if ($save) return $this->success('保存成功');
            else return $this->error('保存失败');
        }
        return view();
    }

    
}