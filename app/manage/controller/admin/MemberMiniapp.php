<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace app\manage\controller\admin;

use buwang\base\AdminBaseController;
use buwang\traits\CrudControllerTrait;
use think\facade\View;

class MemberMiniapp extends AdminBaseController
{
    use CrudControllerTrait;

    /*protected $middleware = [
        'login' 	=> ['except' 	=> [] ]
    ];*/

    protected $model = null;//模型实例

    protected function initialize()
    {
        parent::initialize();

        $this->model = new \app\common\model\MemberMiniapp();
    }

    /**
     * 查看
     */
    public function index()
    {
        if (request()->isAjax()) {
            $page = request()->get('page/d', 1);
            $limit = request()->get('limit/d', 10);

            //TODO 获取搜索条件
            //TODO 如果有不属于表内的字段,查询会报错
            $where = request()->get();
            unset($where['page']);
            unset($where['limit']);
            foreach ($where as $k => $v) {
                if (!$v && $v !== '0' && $v !== 0 && $v !== false) {
                    unset($where[$k]);
                }
            }

            try {
                $total = $this->model->with(['Member', 'Miniapp'])->where($where)->count();
                $list = $this->model->with(['Member', 'Miniapp'])->where($where)->order($this->model->getPk(), 'DESC')->page($page, $limit)->select();
            } catch (\Exception $e) {
                return $this->error('查询失败', ['errorMsg' => $e->getMessage()]);
            }

            $data = compact("total", 'list');
            return $this->success('successful', $data);
        }
        //搜索条件
        $where = [];
        $miniapp_id = request()->param('miniapp_id/d', 0);
        if ($miniapp_id !== 0) $where['miniapp_id'] = $miniapp_id;
        View::assign(compact('where'));
        return view();
    }

    /**
     * 编辑
     * @menu true
     * @param int $id
     */
    public function edit($id = 0)
    {
        $row = $this->model->find($id);
        if (request()->isPost()) {
            if (!$row) return $this->error('记录不存在');

            $param = request()->post();
            //TODO 参数验证

            $this->model->startTrans();
            try {
                //TODO 更新记录
                $row->save($param);
                $this->model->commit();
            } catch (\Exception $e) {
                $this->model->rollback();
                return $this->error('编辑失败', ['errorMsg' => $e->getMessage()]);
            }
            return $this->success('编辑成功');
        }

        if (!$row) return $this->error_jump('记录不存在');

        View::assign('row', $row);
        return view();
    }

}
