<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace app\manage\controller\admin\plugin;

use buwang\base\AdminBaseController;
use think\facade\View;
use think\facade\Cache;

/**
 * @ControllerAnnotation(title="平台插件中心")
 */
class Core extends AdminBaseController
{
    use \buwang\traits\Crud;

    public function initialize()
    {
        parent::initialize();
        $this->layout && $this->app->view->engine()->layout($this->layout);
        $this->model = new \app\common\model\Plugin();
    }


    /**
     * 插件中心页面
     */
    public function index()
    {
        //已启用插件列表
        if (request()->isPost()) {

//            //取得模块目录名称
//            $dirs = array_map('basename', glob(ADDON_PATH . '*', GLOB_ONLYDIR));
//            if ($dirs === false || !file_exists(ADDON_PATH)) return $this->error('插件目录不可读或者不存在');
//            // 读取数据库插件表
//            $list = $this->model->with('group')->order('id asc')->column('*', 'name');
//            //TODO 清除插件列表缓存【后期加上清除插件缓存按钮】
//            Cache::has('addonslist') && Cache::delete('addonslist');
//            //插件列表
//            $dir_addons_list = get_addons_list();
//            //遍历所有插件目录
//            foreach ($dirs as $name) {
//                //如果该插件没有安装过则取插件配置（plugin.php）的信息
//                if (!isset($list[$name])&&isset($dir_addons_list[$name])) {
//                    $list[$name] = $dir_addons_list[$name];
//                    if ($list[$name]) {
//                        $list[$name]['status'] = -1;
//                        $list[$name]['create_time'] = 0;
//                        $list[$name]['update_time'] = 0;
//                    }
//                    //未安装
//                    $list[$name]['install'] = false;
//                }else{
//                    //已安装
//                    $list[$name]['install'] = true;
//                }
//            }


            //得到插件列表
            $list = $this->model->with('group')->where(['status' => 1])->where('type','in',['all_system','admin_system'])->order('id asc')->select();
            $count = $this->model->with('group')->where(['status' => 1])->where('type','in',['all_system','admin_system'])->order('id asc')->count();
            //过滤掉无菜单的插件
            $invalid = 0;
            $list = array_map(
                function ($item)use(&$invalid){
//                    if($item['group']){
                        $item['icon'] = isset($item['logo_image'])&&$item['logo_image']? $item['logo_image'] :'/static/bwmall/logo.png';
                    //得到插件配置数据
                        $item['info'] = get_addons_info($item['name']);
                        $url =  (string)$item['info']['url'];
                     //取出插件url
                        $item['info']['url'] = $url;
//                        $found_index = array_search('admin', array_column($item['group'], 'scopes'));
//                        if($found_index !==false){
                            return $item;
//                        }else{
//                            $invalid ++;
//                            return null;
//                        }
//                    }else{
//                        $invalid ++;
//                        return null;
//                    }
                },
                $list->toArray()
            );
            //过滤掉无菜单插件
            //过滤掉无菜单插件
            $list = array_filter($list,function($val){
                return $val?true:false;
            });
            //
            $data = [
                'total' => $count - $invalid,
                'list' => $list,
            ];
            return $this->success('success', $data);
        }
        return view();
    }


    /**
     * 菜单渲染
     */
    public function menu()
    {
        $addons = $this->request->get('addons/s', '');
        $jump = $this->request->get('jump/s', 0);
        $menu_id = $this->request->get('menu_id/d', 0);
        $menu = $this->model->getAdminMenu($this->uid, $addons, $menu_id);
        if (!$menu['list']) return $this->error('该插件没有管理菜单');
        //跳转渲染
        if ($jump) {
            View::assign('menu_id', $menu_id);
            View::assign('list', $menu['list']);
            View::assign('menu', $menu['menu']);
            View::assign('url_info', $menu['url_info']);
            View::assign('addons', $addons);
            return view();
        }
        //得到菜单
        if (request()->isPost()) {
            return $this->success('得到菜单', ['list' => $menu]);
        }
        //查看插件是否存在菜单
        if (request()->isGet()) {
            //查询该插件是否存在总后台根节点
            return $this->success('存在菜单');
        }

    }

}