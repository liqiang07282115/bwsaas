<?php

namespace app\manage\controller\admin;

use buwang\base\AdminBaseController;
use buwang\util\Caches;
use think\facade\View;

/**
 * 小程序第三方模板库代码管理
 * Class MiniappTemplate
 * @package app\manage\controller\admin
 */
class MiniappTemplate extends AdminBaseController
{
    use \buwang\traits\Crud;

    public function initialize()
    {
        parent::initialize();
        $this->layout && $this->app->view->engine()->layout($this->layout);
        $this->model = new \app\common\model\MemberMiniappTemplate();
        $this->assign('getMiniappList', $this->model->getMiniappList());
        $this->assign('getThirdTemplate', $this->model->getThirdTemplate());
        $this->assign('getMemberMiniappList', $this->model->getMemberMiniappList());
    }
    public function index()
    {
        if ($this->request->isAjax()) {
            if (input('selectFieds')) {
                return $this->selectList();
            }
            list($page, $limit, $where) = $this->buildTableParames();
            //筛选条件
            $miniapp_id = Caches::get('miniapp_id',0);
            $get = $this->request->get();
            $filters = isset($get['filter']) && !empty($get['filter']) ? $get['filter'] : '{}';
            $filters = json_decode($filters, true);
            if($miniapp_id && ($miniapp_id >0) && !isset($filters['miniapp_id'])) $where[] = ['miniapp_id', '=', $miniapp_id];
            //筛选条件END
            $count = $this->model
                ->where($where)
                ->count();
            $list = $this->model
                ->where($where)
                ->page($page, $limit)
                ->order($this->sort)
                ->select();
            $data = [
                'total' => $count,
                'list' => $list,
            ];
            return $this->success('success', $data);
        }
        $miniapp_id = request()->param('miniapp_id/d',0);
        Caches::set('miniapp_id', $miniapp_id, 300);
        return view();
    }
    /**
     * @NodeAnotation(title="添加")
     */
    public function add()
    {
        if ($this->request->isAjax()) {
            $post = $this->request->post();
            $post = reform_keys($post);
            $rule = [];
            $this->validate($post, $rule);
            if((int)$post['template_id'] <= 0) return $this->error('模板ID不正确');
            $existData = $this->model->where(['template_id'=>$post['template_id'],'member_miniapp_id'=>$post['member_miniapp_id']])->find();
            if($existData) return $this->error('同一代码模板只能添加一次！');
            try {
                $save = $this->model->save($post);
            } catch (\Exception $e) {
                return $this->error('保存失败:' . $e->getMessage());
            }
            if ($save) return $this->success('保存成功');
            else return $this->error('保存失败');
        }
        return view();
    }
    public function getMemberMiniappList(int $id =0)
    {
        return $this->success('获取成功',$this->model->getMemberMiniappList($id));
    }
    public function getThirdTemplate(int $template_id = 0){
        return $this->success('获取成功',$this->model->getThirdTemplate($template_id));
    }
}