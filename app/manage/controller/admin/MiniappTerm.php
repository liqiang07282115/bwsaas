<?php

namespace app\manage\controller\admin;

use buwang\base\AdminBaseController;
use app\common\model\MiniappModule;
use app\common\model\Miniapp;
use think\facade\View;

/**
 * 应用套餐表
 */
class MiniappTerm extends AdminBaseController
{
    use \buwang\traits\Crud;
    protected $relationSearch = true;

    public function initialize()
    {
        parent::initialize();
        $this->layout && $this->app->view->engine()->layout($this->layout);
        $this->model = new \app\common\model\MiniappTerm();

        $this->assign('getMiniappList', $this->model->getMiniappList());

    }


    /**
     * 列表
     */
    public function index()
    {
      $miniapp_id =  $this->request->get('miniapp_id/d',0);
        if ($this->request->isAjax()) {
            if (input('selectFieds')) {
                return $this->selectList();
            }
            list($page, $limit, $where) = $this->buildTableParames();
            if($miniapp_id)$where[] = ['miniapp_id','=',$miniapp_id];
            $count = $this->model
                ->withJoin('miniapp', 'LEFT')

                ->where($where)
                ->count();
            $list = $this->model
                ->withJoin('miniapp', 'LEFT')

                ->where($where)
                ->page($page, $limit)
                ->order($this->sort)
                ->select();

            $data = [
                'total' => $count,
                'list' => $list,
            ];
            return $this->success('success', $data);
        }
        $this->assign('miniapp_id', $miniapp_id);
        return view();
    }


    /**
     * @NodeAnotation(title="添加")
     */
    public function add()
    {
        $miniapp_id =  $this->request->get('miniapp_id/d',0);
        if ($this->request->isAjax()) {
            $post = $this->request->post();
            $post = reform_keys($post);
            $rule = [
                'days'  => 'require|number|>=:0',
                'price'   => 'require|float|>=:0.01',
            ];
            $this->validate($post, $rule);
            //查询基础套餐id
            $miniappModule = MiniappModule::where('miniapp_id',$post['miniapp_id'])->where('type',1)->find();
            if(!$miniappModule) return $this->error("该应用缺少基础功能，无法添加套餐");
            $miniapp = Miniapp::where('id',$miniapp_id)->find();
            if(!$miniapp) return $this->error("找不到应用");
            $post['miniapp_module_id'] = $miniappModule['id'];
            $post['dir'] = $miniapp['dir'];
            try {
                $save = $this->model->save($post);
            } catch (\Exception $e) {
                return $this->error('保存失败:' . $e->getMessage());
            }
            if ($save) return $this->success('保存成功');
            else return $this->error('保存失败');
        }
        $this->assign('miniapp_id', $miniapp_id);
        return view();
    }

    /**
     * @NodeAnotation(title="编辑")
     */
    public function edit($id)
    {
        $miniapp_id =  $this->request->get('miniapp_id/d',0);
        $row = $this->model->find($id);
        if ($row->isEmpty()) return $this->error('数据不存在');
        if ($this->request->isAjax()) {
            $post = $this->request->post();
            $post = reform_keys($post);
            $rule = [
                'days'  => 'require|number|>=:0',
                'price'   => 'require|float|>=:0.01',
            ];
            $this->validate($post, $rule);
            //查询基础套餐id
            $miniappModule = MiniappModule::where('miniapp_id',$post['miniapp_id'])->where('type',1)->find();
            if(!$miniappModule) return $this->error("该应用缺少基础功能，无法添加套餐");
            $miniapp = Miniapp::where('id',$miniapp_id)->find();
            if(!$miniapp) return $this->error("找不到应用");
            $post['miniapp_module_id'] = $miniappModule['id'];
            $post['dir'] = $miniapp['dir'];
            try {
                $save = $row->save($post);
            } catch (\Exception $e) {
                return $this->error('保存失败');
            }
            if ($save) return $this->success('保存成功');
            else return $this->error('保存失败');
        }
        $this->assign('miniapp_id', $miniapp_id);
        View::assign('row', $row);
        return view();
    }
}