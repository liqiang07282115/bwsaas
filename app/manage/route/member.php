<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

use think\facade\Route;

/**
 * 无需登录与鉴权的接口
 */
Route::group(function () {
    Route::get('member/login', 'member.Index/login');
    Route::post('member/login', 'member.Index/login');
    Route::get('member/forget', 'member.Index/forget');
    Route::post('member/forget', 'member.Index/forget');
    Route::rule('member/add', 'member.Index/add');
    Route::get('common/map', 'Common/map');
    Route::get('common/baiduMap', 'Common/baiduMap');
    Route::get('common/getMapData', 'Common/getMapData');
    Route::get('common/cityInfo', 'Common/cityInfo');

    //充值回调
    Route::rule('recharge/notify', 'member.Notify/index');

    //公众号管理图文预览
    Route::rule('member.WechatKeyword/previewView', 'member.WechatKeyword/previewView');
})->middleware(buwang\middleware\Login::class, false);

/**
 * 需要登录不鉴权
 */
Route::group(function () {


})->middleware(buwang\middleware\Login::class, true);
/**
 * 需要登录与鉴权的接口
 */
Route::group('member', function () {
    Route::post('logout', 'member.Index/logout');
    Route::get('', 'member.Index/index');
    Route::get('index', 'member.Index/index');
    Route::get('menu', 'member.Index/menu');
    Route::get('dashboard', 'member.Index/dashboard');
    Route::get('openAuth', 'member.WechatAuth/openAuth');
    Route::get('openAuthCallback', 'member.WechatAuth/openAuthCallback');
    Route::get('loginInfo', 'member.Index/getMemberLoginInfo');
    Route::get('noticeList', 'member.Index/getNoticeList');
    Route::get('echartsInfo', 'member.Index/getEchartsInfo');
    //开放平台设置小程序域名
    Route::post('setDomain', 'member.Miniapp/setDomain');
    //第三方平台代提交代码接口
    Route::post('upCode', 'member.Miniapp/upCode');
    //提交审核页面
    Route::rule('submitAudit', 'member.Miniapp/submitAudit');
    //第三方平台拉取小程序体验二维码
    Route::get('getQrCode', 'member.Miniapp/getQrCode');
    //第三方平台获取小程序代码审核状态
    Route::get('getAuditStatus', 'member.Miniapp/getAuditStatus');
    //第三方平台强制撤销审核
    Route::post('resetAudit', 'member.Miniapp/resetAudit');
    //第三方平台发布小程序
    Route::post('publishCode', 'member.Miniapp/publishCode');

    Route::post('publicCommon/upload', 'member.publicCommon/upload');//上传文件
    Route::post('publicCommon/uploadimg', 'member.publicCommon/uploadimg');//上传文件

    //租户资金变更类
    Route::group('walletBill', function () {
        Route::rule('', 'member.WalletBill/index');
        Route::rule('index', 'member.WalletBill/index');
        Route::rule('add', 'member.WalletBill/add');
        Route::rule('edit', 'member.WalletBill/edit');
    });

    //充值
    Route::group('recharge', function () {
        Route::get('', 'member.Recharge/index');
        Route::post('pay', 'member.Recharge/pay');
    });

    //站内信
    Route::group('message', function () {
        Route::get('list', 'member.Index/messageList');
        Route::get('read', 'member.Index/messageRead');
        Route::rule('send', 'member.User/sendMessage');//发送站内信
    });

    //插件市场
    Route::group('plugin', function () {
        Route::get('', 'member.plugin.Plugin/index');
        Route::get('/detail', 'member.plugin.Plugin/detail');
        Route::post('/buy', 'member.plugin.Plugin/buy');
    });

    //微信
    Route::group('wechat', function () {
        //消息
        Route::group('message', function () {
            //消息模板
            Route::group('template', function () {
                Route::get('index', 'member.WechatMessageTemplate/index');
                Route::get('export', 'member.WechatMessageTemplate/export');
                Route::rule('add', 'member.WechatMessageTemplate/add');
                Route::rule('edit', 'member.WechatMessageTemplate/edit');
                Route::post('modify', 'member.WechatMessageTemplate/modify');
                Route::post('del', 'member.WechatMessageTemplate/del');
            });
            //消息记录
            Route::group('messageLog', function () {
                Route::get('index', 'member.WechatMessageLog/index');
                Route::get('export', 'member.WechatMessageLog/export');
                Route::rule('add', 'member.WechatMessageLog/add');
                Route::rule('edit', 'member.WechatMessageLog/edit');
                Route::post('modify', 'member.WechatMessageLog/modify');
                Route::post('del', 'member.WechatMessageLog/del');
            });
        });
    });

    //系统插件管理
    Route::group('plugin', function () {
        //插件中心
        Route::group('core', function () {
            Route::rule('index', 'member.plugin.Core/index'); //插件列表
            Route::rule('menu', 'member.plugin.Core/menu');   //渲染菜单
        });
    });

})->middleware(buwang\middleware\Login::class, true)->middleware(buwang\middleware\Auth::class);

//云市场
Route::group('member/cloud', function () {
    //云市场服务类
    Route::group('service', function () {
        Route::rule('', 'member.cloud.Service/index');
        Route::rule('index', 'member.cloud.Service/index');
        Route::rule('detail', 'member.cloud.Service/detail');
    });

    //云市场套餐类
    Route::group('packet', function () {
        Route::rule('', 'member.cloud.Packet/index');
        Route::rule('index', 'member.cloud.Packet/index');
        Route::post('buy', 'member.cloud.Packet/buy');
    });

    //云市场次数记录类
    Route::group('walletBill', function () {
        Route::rule('', 'member.cloud.WalletBill/index');
        Route::rule('index', 'member.cloud.WalletBill/index');
    });

    //云市场订单类
    Route::group('order', function () {
        Route::rule('', 'member.cloud.Order/index');
        Route::rule('index', 'member.cloud.Order/index');
    });
})->middleware(buwang\middleware\Login::class, true)->middleware(buwang\middleware\Auth::class);

Route::group(function () {
    //关于应用
    Route::get('member.setting/index', 'member.setting/index');//菜单
    Route::post('member.setting/wechat', 'member.setting/wechat');
    Route::post('member.setting/alipay', 'member.setting/alipay');
    Route::post('member.setting/tt_wechat', 'member.setting/tt_wechat');//头条微信支付配置
    Route::post('member.setting/edit', 'member.setting/edit');
})->middleware(buwang\middleware\Login::class, true)->middleware(buwang\middleware\Auth::class)->append(['bwsaas' => true]);
Route::group(function () {
    //公众号菜单管理
    Route::rule('member.official/index', 'member.official/index');//菜单
    Route::rule('member.official/add', 'member.official/add');
    Route::rule('member.official/edit', 'member.official/edit');
    Route::rule('member.official/del', 'member.official/del');
    Route::rule('member.official/sync', 'member.official/sync');
    Route::rule('member.official/sort', 'member.official/sort');
    Route::rule('member.official/materialList', 'member.official/materialList');//选择素材接口

    //图文素材管理
    Route::rule('member.wechatKeyword/materialList', 'member.wechatKeyword/materialList');//菜单
    Route::rule('member.wechatKeyword/addMaterial', 'member.wechatKeyword/addMaterial');
    Route::post('member.wechatKeyword/removeMaterial', 'member.wechatKeyword/removeMaterial');

    //关键字回复规则管理
    Route::rule('member.wechatKeyword/index', 'member.wechatKeyword/index');//菜单
    Route::get('member.wechatKeyword/editMaterial', 'member.wechatKeyword/editMaterial');
    Route::rule('member.wechatKeyword/edit', 'member.wechatKeyword/edit');
    Route::rule('member.wechatKeyword/del', 'member.wechatKeyword/del');
    Route::rule('member.wechatKeyword/addKeys', 'member.wechatKeyword/addKeys');
    Route::get('member.wechatKeyword/previewText', 'member.wechatKeyword/previewText');
    Route::get('member.wechatKeyword/previewNews', 'member.wechatKeyword/previewNews');
    Route::get('member.wechatKeyword/select', 'member.wechatKeyword/select');
    Route::get('member.wechatKeyword/previewImage', 'member.wechatKeyword/previewImage');
    Route::get('member.wechatKeyword/previewMusic', 'member.wechatKeyword/previewMusic');
    Route::get('member.wechatKeyword/previewVideo', 'member.wechatKeyword/previewVideo');
    Route::get('member.wechatKeyword/previewVoice', 'member.wechatKeyword/previewVoice');
    Route::post('member.wechatKeyword/modify', 'member.wechatKeyword/modify');
    Route::rule('member.wechatKeyword/subscribe', 'member.wechatKeyword/subscribe');

    //应用商店
    Route::rule('member/miniapp/index', 'member.miniapp/index');
    Route::rule('member/miniapp/detail', 'member.miniapp/detail');
    Route::post('member/miniapp/buy', 'member.miniapp/buy');
    //租户管理员管理
    Route::get('member.login/user', 'member.login/user');

    //租户角色
    Route::get('member.Role/index', 'member.Role/index');
    Route::rule('member.Role/add', 'member.Role/add');
    Route::rule('member.Role/edit', 'member.Role/edit');
    Route::rule('member.Role/getRoleTree', 'member.Role/getRoleTree');
    Route::rule('member.Role/getNodeTree', 'member.Role/getNodeTree');
    Route::rule('member.Role/delete', 'member.Role/delete');
    Route::rule('member.Role/setStatus', 'member.Role/setStatus');

    //子租户管理
    Route::rule('member.Login/add', 'member.Login/add');
    Route::rule('member.Login/edit', 'member.Login/edit');
    Route::rule('member.Login/delete', 'member.Login/delete');
    Route::rule('member.Login/setStatus', 'member.Login/setStatus');
    Route::rule('member/login/userSetting', 'member.Login/userSetting');
    Route::rule('member/login/userPassword', 'member.Login/userPassword');
    //组合数据
    Route::get('member/groupData/<type>/<app?>/<plugin?>', 'member.GroupData/index');
    Route::rule('member/groupData/edit', 'member.GroupData/edit');
    Route::rule('member.GroupData/del', 'member.GroupData/softdleting');
    Route::rule('member/groupData/add', 'member.GroupData/add');
    Route::post('member/groupData/softdleting', 'member.GroupData/softdleting');
    Route::rule('member/groupData/getConfigList', 'member.GroupData/getConfigList');
    Route::rule('member/groupData/setConfigShow', 'member.GroupData/setConfigShow');
    //配置
    Route::rule('member/config/showMemberConfig', 'member.Config/showMemberConfig');
    Route::rule('member/config/showMemberConfig/<type>/<tab_name?>/<config_ids?>/<app?>/<plugin?>', 'member.Config/showMemberConfig');//动态config访问规则
    Route::rule('member/config/setMemberValues', 'member.Config/setMemberValues');

    //用户管理
    Route::get('member/user/index', 'member.User/index');
    Route::get('member/user/export', 'member.User/export');
    Route::rule('member/user/add', 'member.User/add');
    Route::rule('member/user/edit', 'member.User/edit');
    Route::rule('member/user/detail', 'member.User/detail');
    Route::rule('member/user/editPw', 'member.User/editPw');
    Route::rule('member/user/changeMoney', 'member.User/changeMoney');
    Route::post('member/user/modify', 'member.User/modify');
    //用户充值管理
    Route::rule('member/user/recharge/index', 'member.user.Recharge/index');
    Route::get('member/user/recharge/export', 'member.user.Recharge/export');
    Route::rule('member/user/recharge/add', 'member.user.Recharge/add');
    Route::rule('member/user/recharge/edit', 'member.user.Recharge/edit');
    Route::post('member/user/recharge/modify', 'member.user.Recharge/modify');
    //用户资金记录
    Route::rule('member/user/bill/index', 'member.user.Bill/index');
    Route::get('member/user/bill/export', 'member.user.Bill/export');
    Route::rule('member/user/bill/add', 'member.user.Bill/add');
    Route::rule('member/user/bill/edit', 'member.user.Bill/edit');
    Route::post('member/user/bill/modify', 'member.user.Bill/modify');
    //提现管理页面
    Route::rule('member/user/extract/index', 'member.user.Extract/index');
    Route::get('member/user/extract/export', 'member.user.Extract/export');
    Route::rule('member/user/extract/add', 'member.user.Extract/add');
    Route::rule('member/user/extract/edit', 'member.user.Extract/edit');
    Route::post('member/user/extract/modify', 'member.user.Extract/modify');
    Route::post('member/user/extract/del', 'member.user.Extract/del');
    //提现记录
    Route::rule('member/user/extractLog/index', 'member.user.ExtractLog/index');
    Route::get('member/user/extractLog/export', 'member.user.ExtractLog/export');
    Route::rule('member/user/extractLog/add', 'member.user.ExtractLog/add');
    Route::rule('member/user/extractLog/edit', 'member.user.ExtractLog/edit');
    Route::post('member/user/extractLog/modify', 'member.user.ExtractLog/modify');
    Route::post('member/user/extractLog/del', 'member.user.ExtractLog/del');
    //系统操作日志
    Route::rule('member/sys/log/index', 'member.sys.Log/index');
    Route::get('member/sys/log/export', 'member.sys.Log/export');
    Route::rule('member/sys/log/add', 'member.sys.Log/add');
    Route::rule('member/sys/log/edit', 'member.sys.Log/edit');
    Route::post('member/sys/log/modify', 'member.sys.Log/modify');

    //快递管理
    Route::rule('member/sys/express/index', 'member.sys.Express/index');
    Route::get('member/sys/express/export', 'member.sys.Express/export');
    Route::rule('member/sys/express/add', 'member.sys.Express/add');
    Route::rule('member/sys/express/edit', 'member.sys.Express/edit');
    Route::post('member/sys/express/modify', 'member.sys.Express/modify');
    Route::post('member/sys/express/del', 'member.sys.Express/del');

    //租户应用
    Route::group('member/miniapp/my', function () {
        Route::rule('index', 'member.MemberMiniapp/index');
        Route::rule('renew', 'member.MemberMiniapp/renew');//应用续费
        Route::post('default', 'member.MemberMiniapp/setDefaultMiniapp');//设置默认应用
    });
})->middleware(buwang\middleware\Login::class, true)->middleware(buwang\middleware\Auth::class);
