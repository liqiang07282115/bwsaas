<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

use think\facade\Route;
use think\Response;

/**
 * 无需登录与鉴权的接口
 */
Route::get('bwCaptcha', 'Common/captcha');

/**
 * 需登录接口
 */
Route::group(function () {
    Route::post('publicCommon/upload', 'publicCommon/upload');//上传文件
    Route::post('publicCommon/delFile', 'publicCommon/delFile');//删除文件
    Route::post('publicCommon/uploadEditor', 'publicCommon/uploadEditor');//上传编辑器文件
    Route::post('publicCommon/uploadimg', 'publicCommon/uploadimg');//上传编辑器文件
    Route::post('publicCommon/uploadUeditor', 'publicCommon/uploadUeditor');//上传编辑器文件（百度富文本）
    Route::get('publicCommon/getUploadFiles', 'publicCommon/getUploadFiles');//获取上传的文件列表
    Route::get('publicCommon/newPic', 'publicCommon/newPic');//新文件上传
    //清除缓存
    Route::get('common/clearCache', 'Common/clearCache');
})->middleware(buwang\middleware\Login::class, true);

/**
 * miss 路由
 */
Route::miss(function () {
    if (app()->request->isOptions()) {
        $header = Config::get('cookie.header');
        $header['Access-Control-Allow-Origin'] = app()->request->header('origin');
        return Response::create('ok')->code(200)->header($header);
    }
    $result = [
        'code' => 0,
        'msg' => '找不到路由路径，请添加相应路由',
        'data' => '',
        'url' => '',
        'wait' => 0,
    ];
    return Response::create(app()->config->get('jump.dispatch_success_tmpl'), 'view')->assign($result);
    //return Response::create()->code(403)->content('找不到路由路径，请添加相应路由');

});

//参数过滤
Route::pattern([
    'miniapp_id' => '\d+',
    'appid' => '\w+',
    'id' => '\d+',
    'version' => '[0-9a-zA-Z]+',
    'module' => '[a-zA-Z]+',
    'controller' => '[a-zA-Z]+',
    'action' => '[a-zA-Z]+',
    'type' => '\w+'
]);