<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

use think\facade\Route;

/**
 * 无需登录与鉴权的接口
 */
Route::group(function () {
    Route::get('admin/login', 'admin.Index/login');
    Route::post('admin/login', 'admin.Index/login');

    Route::get('test', 'admin.Index/test');
})->middleware(buwang\middleware\Login::class, false);


/**
 * 需要登录与鉴权的接口
 */
Route::group('admin', function () {
    Route::post('logout', 'admin.Index/logout');

    Route::get('index', 'admin.Index/index');
    Route::get('menu', 'admin.Index/menu');
    Route::get('dashboard', 'admin.Index/dashboard');
    Route::rule('userSetting', 'admin.Index/userSetting');
    Route::rule('userPassword', 'admin.Index/userPassword');
    Route::get('loginInfo', 'admin.Index/getAdminLoginInfo');


    Route::get('user', 'Admin/user');

    Route::get('member', 'admin.member/index');
    Route::get('member/index', 'admin.member/index');
    Route::get('member/add', 'admin.member/add');
    Route::post('Member/add', 'admin.Member/add');
    Route::get('Member/edit', 'admin.Member/edit');
    Route::post('member/edit', 'admin.member/edit');
    Route::post('member/del', 'admin.member/del');
    Route::post('member/setStatus', 'admin.member/setStatus');
    Route::rule('member/detail', 'admin.member/detail');


    //租户资金变更类
    Route::group('miniapp/module', function () {
        Route::rule('index', 'admin.miniappModule/index');
        Route::get('export', 'admin.miniappModule/export');
        Route::rule('add', 'admin.miniappModule/add');
        Route::rule('edit', 'admin.miniappModule/edit');
        Route::post('modify', 'admin.miniappModule/modify');
        Route::post('del', 'admin.miniappModule/del');

        Route::get('group', 'admin.miniappModule/group');
    });

    //应用套餐类
    Route::group('miniapp/term', function () {
        Route::rule('index', 'admin.MiniappTerm/index');
        Route::get('export', 'admin.MiniappTerm/export');
        Route::rule('add', 'admin.MiniappTerm/add');
        Route::rule('edit', 'admin.MiniappTerm/edit');
        Route::post('modify', 'admin.MiniappTerm/modify');
        Route::post('del', 'admin.MiniappTerm/del');
    });

    //站内信
    Route::group('message', function () {
        Route::post('send', 'admin.Member/sendMessage');
    });


    //系统插件管理
    Route::group('plugin', function () {
        //插件中心
        Route::group('core', function () {
            Route::rule('index', 'admin.plugin.Core/index'); //插件列表
            Route::rule('menu', 'admin.plugin.Core/menu');   //渲染菜单
        });
    });

})->middleware(buwang\middleware\Login::class, true)->middleware(buwang\middleware\Auth::class);


//租户
Route::group('admin/member', function () {
    //租户资金类
    Route::group('wallet', function () {
        Route::rule('', 'admin.member.Wallet/index');
        Route::rule('index', 'admin.member.Wallet/index');
        Route::rule('edit', 'admin.member.Wallet/edit');
    });
    //租户资金变更类
    Route::group('walletBill', function () {
        Route::rule('', 'admin.member.WalletBill/index');
        Route::rule('index', 'admin.member.WalletBill/index');
        Route::rule('add', 'admin.member.WalletBill/add');
        Route::rule('edit', 'admin.member.WalletBill/edit');
        Route::rule('del', 'admin.member.WalletBill/del');
    });
})->middleware(buwang\middleware\Login::class, true)->middleware(buwang\middleware\Auth::class);;

//云市场
Route::group('admin/cloud', function () {
    //云市场服务类
    Route::rule('service', 'admin.cloud.Service/index');
    Route::rule('service/index', 'admin.cloud.Service/index');
    Route::rule('service/add', 'admin.cloud.Service/add');
    Route::rule('service/edit', 'admin.cloud.Service/edit');
    Route::rule('service/del', 'admin.cloud.Service/del');

    //云市场套餐类
    Route::rule('packet', 'admin.cloud.Packet/index');
    Route::rule('packet/index', 'admin.cloud.Packet/index');
    Route::rule('packet/add', 'admin.cloud.Packet/add');
    Route::rule('packet/edit', 'admin.cloud.Packet/edit');
    Route::rule('packet/del', 'admin.cloud.Packet/del');

    //云市场次数类
    Route::rule('wallet', 'admin.cloud.Wallet/index');
    Route::rule('wallet/index', 'admin.cloud.Wallet/index');
    Route::rule('wallet/add', 'admin.cloud.Wallet/add');
    Route::rule('wallet/edit', 'admin.cloud.Wallet/edit');
    Route::rule('wallet/del', 'admin.cloud.Wallet/del');

    //云市场次数消耗记录类
    Route::rule('walletBill', 'admin.cloud.WalletBill/index');
    Route::rule('walletBill/index', 'admin.cloud.WalletBill/index');
    Route::rule('walletBill/add', 'admin.cloud.WalletBill/add');
    Route::rule('walletBill/edit', 'admin.cloud.WalletBill/edit');
    Route::rule('walletBill/del', 'admin.cloud.WalletBill/del');

    //云市场订单类
    Route::rule('order', 'admin.cloud.Order/index');
    Route::rule('order/index', 'admin.cloud.Order/index');
    Route::rule('order/add', 'admin.cloudmenu.Order/add');
    Route::rule('order/edit', 'admin.cloud.Order/edit');
    Route::rule('order/del', 'admin.cloud.Order/del');

    //云市场日志类
    Route::rule('log/smsLog', 'admin.cloud.log.SmsLog/index');
    Route::rule('log/smsLog/index', 'admin.cloud.log.SmsLog/index');
    Route::rule('log/smsLog/add', 'admin.cloud.log.SmsLog/add');
    Route::rule('log/smsLog/edit', 'admin.cloud.log.SmsLog/edit');
    Route::rule('log/smsLog/del', 'admin.cloud.log.SmsLog/del');

})->middleware(buwang\middleware\Login::class, true)->middleware(buwang\middleware\Auth::class);

Route::group(function () {
//上传文件管理
    Route::get('uploadfile/index', 'admin.Uploadfile/index');
    Route::post('uploadfile/del', 'admin.Uploadfile/del');
    Route::rule('uploadfile/add', 'admin.Uploadfile/add');
    Route::get('uploadfile/export', 'admin.Uploadfile/export');

    Route::get('admin/sysCrud', 'admin.SysCrud/index');
    Route::get('admin/sysCrud/index', 'admin.SysCrud/index');
    Route::get('admin/sysCrud/add', 'admin.SysCrud/add');
    Route::post('admin/sysCrud/add', 'admin.SysCrud/add');
    Route::get('admin/sysCrud/edit', 'admin.SysCrud/edit');
    Route::post('admin/sysCrud/edit', 'admin.SysCrud/edit');
    Route::post('admin/sysCrud/del', 'admin.SysCrud/del');

    Route::get('admin/sysArticle', 'admin.SysArticle/index');
    Route::get('admin/sysArticle/index', 'admin.SysArticle/index');
    Route::rule('admin/sysArticle/add', 'admin.SysArticle/add');
    Route::rule('admin/sysArticle/edit', 'admin.SysArticle/edit');
    Route::post('admin/sysArticle/del', 'admin.SysArticle/del');

    Route::get('admin/sysArticleCategory', 'admin.SysArticleCategory/index');
    Route::get('admin/sysArticleCategory/index', 'admin.SysArticleCategory/index');
    Route::rule('admin/sysArticleCategory/add', 'admin.SysArticleCategory/add');
    Route::rule('admin/sysArticleCategory/edit', 'admin.SysArticleCategory/edit');
    Route::post('admin/sysArticleCategory/del', 'admin.SysArticleCategory/del');

    //系统日志
    Route::rule('admin/sys/log/index', 'admin.sys.Log/index');
    Route::get('admin/sys/log/export', 'admin.sys.Log/export');
    Route::rule('admin/sys/log/add', 'admin.sys.Log/add');
    Route::rule('admin/sys/log/edit', 'admin.sys.Log/edit');
    Route::post('admin/sys/log/modify', 'admin.sys.Log/modify');
    Route::post('admin/sys/log/del', 'admin.sys.Log/del');

    //系统通知
    Route::rule('admin/sys/notice/index', 'admin.sys.Notice/index');
    Route::get('admin/sys/notice/export', 'admin.sys.Notice/export');
    Route::rule('admin/sys/notice/add', 'admin.sys.Notice/add');
    Route::rule('admin/sys/notice/edit', 'admin.sys.Notice/edit');
    Route::post('admin/sys/notice/modify', 'admin.sys.Notice/modify');
    Route::post('admin/sys/notice/del', 'admin.sys.Notice/del');

    //系统资讯分类
    Route::rule('admin/sys/notice/category/index', 'admin.sys.NoticeCategory/index');
    Route::get('admin/sys/notice/category/export', 'admin.sys.NoticeCategory/export');
    Route::rule('admin/sys/notice/category/add', 'admin.sys.NoticeCategory/add');
    Route::rule('admin/sys/notice/category/edit', 'admin.sys.NoticeCategory/edit');
    Route::post('admin/sys/notice/category/modify', 'admin.sys.NoticeCategory/modify');
    Route::post('admin/sys/notice/category/del', 'admin.sys.NoticeCategory/del');

    //城市数据管理
    Route::rule('admin/sys/city/index', 'admin.sys.City/index');
    Route::get('admin/sys/city/export', 'admin.sys.City/export');
    Route::rule('admin/sys/city/add', 'admin.sys.City/add');
    Route::rule('admin/sys/city/edit', 'admin.sys.City/edit');
    Route::post('admin/sys/city/modify', 'admin.sys.City/modify');
    Route::post('admin/sys/city/del', 'admin.sys.City/del');
    Route::rule('admin/sys/city/download', 'admin.sys.City/download');

    Route::get('crudDemo', 'crudDemo/index');
    Route::get('crudDemo/index', 'crudDemo/index');
    Route::get('crudDemo/add', 'crudDemo/add');
    Route::post('crudDemo/add', 'crudDemo/add');
    Route::get('crudDemo/edit', 'crudDemo/edit');
    Route::post('crudDemo/edit', 'crudDemo/edit');
    Route::post('crudDemo/del', 'crudDemo/del');

    Route::get('admin/miniapp', 'admin.Miniapp/index');
    Route::rule('admin/miniapp/index', 'admin.Miniapp/index');
    Route::get('admin/miniapp/edit', 'admin.Miniapp/edit');
    Route::post('admin/miniapp/edit', 'admin.Miniapp/edit');
    Route::post('admin/miniapp/install', 'admin.Miniapp/install');
    Route::post('admin/miniapp/uninstall', 'admin.Miniapp/uninstall');
    Route::rule('admin/miniapp/update', 'admin.Miniapp/update'); //检查是否存在更新

    Route::get('admin/memberMiniapp', 'admin.MemberMiniapp/index');
    Route::get('admin/memberMiniapp/index', 'admin.MemberMiniapp/index');
    Route::rule('admin/memberMiniapp/edit', 'admin.MemberMiniapp/edit');

    //管理员账户
    Route::rule('admin/add', 'Admin/add');
    Route::rule('admin/setStatus', 'Admin/setStatus');
    Route::rule('admin/edit', 'Admin/edit');
    Route::post('admin/delete', 'Admin/delete');

//权限管理
    Route::rule('Node/add', 'Node/add');
    Route::rule('Node/edit', 'Node/edit');
    Route::get('PublicCommon/icon', 'PublicCommon/icon');
    Route::post('Node/setShow', 'Node/setShow');
    Route::post('Node/setStatus', 'Node/setStatus');
    Route::get('Node/index', 'Node/index');
    Route::post('Node/delete', 'Node/delete');
    Route::rule('Node/getNodeTree', 'Node/getNodeTree');
    Route::rule('node/getMemberNodeTree', 'Node/getMemberNodeTree');
    Route::rule('node/memberIndex', 'Node/memberIndex');
    Route::rule('node/addMember', 'Node/addMember');
    Route::rule('node/editMember', 'Node/editMember');


//角色
    Route::get('Role/index', 'Role/index');
    Route::rule('Role/edit', 'Role/edit');
    Route::rule('Role/add', 'Role/add');
    Route::rule('Role/getRoleTree', 'Role/getRoleTree');
    Route::rule('Role/getRoleModelTree', 'Role/getRoleModelTree');
    Route::rule('Role/getRoleMemberTree', 'Role/getRoleMemberTree');
    Route::post('Role/delete', 'Role/delete');
    Route::rule('Role/setStatus', 'Role/setStatus');
    Route::get('Role/modelRole', 'Role/modelRole');
    Route::rule('Role/editModel', 'Role/editModel');
    Route::rule('Role/addModel', 'Role/addModel');
    Route::rule('Role/memberRole', 'Role/memberRole');
    Route::rule('Role/editMember', 'Role/editMember');
    Route::rule('Role/addMember', 'Role/addMember');
    Route::rule('Role/getNodeTree', 'Role/getNodeTree');


//配置设置
    Route::get('Config/showconfig', 'Config/showconfig');
    Route::get('Config/showMemberConfig', 'Config/showMemberConfig');
    Route::post('Config/setMemberValues', 'Config/setMemberValues');
    Route::post('Config/setValues', 'Config/setValues');
//配置分类
    Route::get('Config/index', 'Config/index');
    Route::post('Config/getlist', 'Config/getlist');
    Route::rule('Config/add', 'Config/add');
    Route::rule('Config/edit', 'Config/edit');
    Route::rule('Config/softdleting', 'Config/softdleting');
    Route::rule('Config/addconfig', 'Config/addconfig');
    Route::post('Config/setshow', 'Config/setshow');
    //配置列表
    Route::get('Config/configindex', 'Config/configindex');
    Route::post('Config/getconfiglist', 'Config/getconfiglist');
    Route::rule('Config/editconfig', 'Config/editconfig');
    Route::post('Config/setconfigshow', 'Config/setconfigshow');
    Route::post('Config/configsoftdleting', 'Config/configsoftdleting');

    //数据组
    Route::get('group', 'group/index');
    Route::post('Group/getconfiglist', 'Group/getconfiglist');
    Route::rule('Group/add', 'Group/add');
    Route::rule('Group/edit', 'Group/edit');
    Route::rule('Group/addconfig', 'Group/addconfig');
    Route::rule('Group/editconfig', 'Group/editconfig');
    Route::post('Group/configsoftdleting', 'Group/configsoftdleting');

    //组合数据
    Route::get('GroupData/:id', 'GroupData/index');
    Route::rule('GroupData/edit', 'GroupData/edit');
    Route::rule('GroupData/add', 'GroupData/add');
    Route::post('GroupData/softdleting', 'GroupData/softdleting');
    Route::rule('GroupData/getconfiglist', 'GroupData/getconfiglist');
    Route::rule('groupData/setConfigShow', 'GroupData/setConfigShow');

    Route::get('admin.Plugin', 'admin.Plugin/index');
    Route::get('admin.Plugin/index', 'admin.Plugin/index');
    Route::get('admin.Plugin/add', 'admin.Plugin/add');
    Route::post('admin.Plugin/add', 'admin.Plugin/add');
    Route::get('admin.Plugin/edit', 'admin.Plugin/edit');
    Route::post('admin.Plugin/edit', 'admin.Plugin/edit');
    Route::post('admin.Plugin/del', 'admin.Plugin/del');
    Route::post('admin.Plugin/install', 'admin.Plugin/install');
    Route::post('admin.Plugin/uninstall', 'admin.Plugin/uninstall');
    Route::post('admin.Plugin/enable', 'admin.Plugin/enable');
    Route::post('admin.Plugin/disable', 'admin.Plugin/disable');
    Route::get('admin.Plugin/config', 'admin.Plugin/config');
    Route::post('admin.Plugin/config', 'admin.Plugin/config');

    //第三方平台代码模板管理，用于租户购买应用后升级管理
    Route::get('admin/template', 'admin.MiniappTemplate/index');
    Route::rule('admin/template/add', 'admin.MiniappTemplate/add');
    Route::rule('admin/template/edit', 'admin.MiniappTemplate/edit');
    Route::post('admin/template/del', 'admin.MiniappTemplate/del');
    Route::get('admin/getMemberMiniappList', 'admin.MiniappTemplate/getMemberMiniappList');
    Route::get('admin/getThirdTemplate', 'admin.MiniappTemplate/getThirdTemplate');

})->middleware(buwang\middleware\Login::class, true)->middleware(buwang\middleware\Auth::class);
