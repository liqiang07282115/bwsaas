<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace app\manage\model;

use buwang\base\TimeModel;
use buwang\util\Util;
use app\common\model\UserBill;

class User extends TimeModel
{

    protected $name = "user";

    protected $deleteTime = false;


    /**头像获取器
     * @param $value
     * @return mixed
     */
    public function getAvatarAttr($value)
    {
        return bw_img_url($value?:'/static/manage/images/default_user_head.png',request()->domain());//TODO 2021/5/20 用户无头像增加默认头像
    }


    public function memberMiniapp()
    {
        return $this->belongsTo('\app\manage\model\MemberMiniapp', 'member_miniapp_id', 'id');
    }




    public function getMemberMiniappList($member_id = 0)
    {
        $MemberMiniapp = new \app\common\model\MemberMiniapp();
        if ($member_id) $MemberMiniapp = $MemberMiniapp->where('member_id', $member_id);

        return $MemberMiniapp->column('appname', 'id');
    }

    public function getStatusList()
    {
        return ['0' => '锁定', '1' => '正常',];
    }

    /**
     * 获取用户余额
     */
    public static function getMoney($user_id)
    {
        $value = self::where('id', $user_id)->value('money');
        return $value ?: 0;
    }


    /**
     * 获取用户积分
     */
    public static function getIntegral($user_id)
    {
        $value = self::where('id', $user_id)->value('integral');
        return $value ?: 0;
    }

    /**
     * 更改余额
     * @param $user_id
     * @param int $amount
     * @param string $memoContent
     * @param string $typeContent
     * @param string $markContent
     * @param int $link_id
     * @return bool
     */
    public static function changeMoney($user_id, $amount = 0, $memoContent = '', $typeContent = 'score_pool', $markContent = '', $link_id = 0)
    {
        if ($amount == 0) return true;
        self::stringParamHandle($memoContent);
        self::stringParamHandle($markContent);
        self::stringParamHandle($typeContent);
        if ($amount > 0) {
            $memo = $memoContent[0];//增加
            $mark = $markContent[0];//增加
            $type = $typeContent[0];//增加
        } else {
            $memo = $memoContent[1];//减少
            $mark = $markContent[1];//减少
            $type = $typeContent[1];//减少
        }

        if ($amount > 0) {
            self::bcInc($user_id, 'money', $amount, 'id');
        } else {
            self::bcDec($user_id, 'money', abs($amount), 'id');
        }
        $after = self::getMoney($user_id);//变更后
        //增加log
        if ($amount > 0) {
            UserBill::income($memo, $user_id, 'money', $type, $amount, $link_id, $after, $mark);
        } else {
            UserBill::expend($memo, $user_id, 'money', $type, abs($amount), $link_id, $after, $mark);
        }
        return true;
    }


    /**
     * 更改积分
     * @param $user_id
     * @param int $amount
     * @param $memoContent
     * @param string $typeContent
     * @param string $markContent
     * @param int $link_id
     * @return bool
     */
    public static function changeIntegral($user_id, $amount = 0, $memoContent, $typeContent = 'score_pool', $markContent = '', $link_id = 0)
    {
        if ($amount == 0) return true;
        self::stringParamHandle($memoContent);
        self::stringParamHandle($markContent);
        self::stringParamHandle($typeContent);
        if ($amount > 0) {
            $memo = $memoContent[0];//增加
            $mark = $markContent[0];//增加
            $type = $typeContent[0];//增加
        } else {
            $memo = $memoContent[1];//减少
            $mark = $markContent[1];//减少
            $type = $typeContent[1];//减少
        }

        $before = self::getIntegral($user_id);//变更前
        //变更金额
        if ($amount > 0) {
            self::bcInc($user_id, 'integral', $amount, 'id');
        } else {
            self::bcDec($user_id, 'integral', abs($amount), 'id');
        }

        $after = self::getIntegral($user_id);//变更后
        //增加log
        if ($amount > 0) {
            UserBill::income($memo, $user_id, 'integral', $type, $amount, $link_id, $after, $mark);
        } else {
            UserBill::expend($memo, $user_id, 'integral', $type, abs($amount), $link_id, $after, $mark);
        }
        return true;
    }


    /**
     * 额度变更记录参数处理
     */
    public static function stringParamHandle(&$param)
    {
        if (!$param) {
            $param = [
                0 => '',
                1 => '',
            ];
            //如果是数组，且只有一个参数，取第一个参数
        } elseif (is_array($param)) {
            if (count($param) == 1) {
                $param = [
                    0 => $param[0],
                    1 => $param[0],
                ];
            }
            //如果是字符取第一个参数
        } elseif (is_string($param)) {
            $param = [
                0 => $param,
                1 => $param,
            ];
        }
    }

}