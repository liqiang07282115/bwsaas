<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace app\manage\model;

use buwang\base\BaseModel;
use buwang\traits\JwtTrait;
use buwang\util\Util;
use think\facade\Db;

/**城市选择器
 * Class BwAuthNode
 * @package app\manage\model
 */
class City extends BaseModel
{
    protected $pk = 'id';
    // 表名
    protected $table = 'bw_sys_city';
    protected $updateTime = '';

    protected $deleteTime = '';

    /**
     * 得到全区域数据
     */
    public static function getCityData()
    {
        //查询所有城市数据
        $data = self::where('is_show', 1)->where('level', 'in', [0, 1])->field('city_id,parent_id,name')->select()->toArray();
        $data = Util::tree($data, 'city_id', 'parent_id', 'child');

        return [0 => ['name' => '全国', 'data' => $data]];//= self::getCityTree($data);
    }

    /**只要城市
     * @param $names
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public static function getProvinceIdAndCityId($names)
    {
        if (is_string($names)) $names = explode(',', $names);//字符串分割数组
        foreach ($names as &$name) {
            $name = explode("-", $name);
            if (count($name) > 1) $name = $name[1]; else $name = $name[0];
        }
        //return self::setError(implode(",",$names));
        $data2 = [];
        $data = self::where('name', 'in', $names)->where('level', 'in', [1])->field('level,city_id,parent_id,name,merger_name,area_code')->select()->toArray();//城市
        $pids = self::where('name', 'in', $names)->where('level', 'in', [0])->column('city_id');
        if ($pids) $data2 = self::where('parent_id', 'in', $pids)->where('level', 'in', [1])->field('level,city_id,parent_id,name,merger_name,area_code')->select()->toArray();
        //省
        return array_merge($data, $data2);
    }

    /**
     * 得到城市名
     */
    public static function getCityName($cityIds, $is_array = false)
    {
        $names = [];
        $parent_ids = self::where('city_id', 'in', $cityIds)->where('level', '=', 1)->column('parent_id');
        $parent_id_name = self::where('city_id', 'in', $cityIds)->where('level', '=', 1)->column('parent_id', 'name');
        if ($parent_ids && $parent_id_name) {
            $parent_data = self::where('city_id', 'in', $parent_ids)->where('level', 'in', [0])->column('name', 'city_id');
            $city_ids = self::where('city_id', 'in', $cityIds)->where('level', '=', 1)->column('city_id');
            //查询出城市全部选中的省的名字
            $parent_names = self::alias('a')
                ->whereRaw('(SELECT	count(b.city_id)
		FROM	`bw_sys_city` `b`WHERE(b.parent_id = a.city_id)
		AND `b`.`city_id` NOT IN (	' . implode(",", $city_ids) . '	) ) = 0')
                ->where('a.city_id', 'in', $parent_ids)
                ->column('a.name');
            if ($parent_data) {
                foreach ($parent_id_name as $name => $parent_id) {
                    $befor_name = $parent_data[$parent_id];
                    $after_name = $name;
                    $names[] = $befor_name . '-' . $after_name;
                }
            }
            $names = array_merge($parent_names, $names);
        } else {
            $names = $parent_id_name ? array_keys($parent_id_name) : [];
        }
        if (!$is_array) $names = implode(",", $names);
        return $names;
    }

}
