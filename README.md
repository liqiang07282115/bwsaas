# bwsaas框架  
![输入图片说明](https://images.gitee.com/uploads/images/2021/0114/114038_bc2aff6b_8166929.jpeg "bwsaas框架")

#### 开源说明
* bwsaas框架源码已经全部开源，可免费商用，务必保留代码签名！
* bwsaas从开源起又经过多轮开发验证和完善已经达到稳定安全商用级别，能满足开发多租户多应用多端应用售卖管理系统，开发效率也比正常使用TP框架至少提升30%，为了广大开发者的方便，决定发布release版
* 基于好多人不理解saas应用的运营开发，特提供了开发使用说明文档，仔细阅读理解你会大有收获哦；文档地址[https://www.kancloud.cn/hnlg666/bwsaas/2306667](https://www.kancloud.cn/hnlg666/bwsaas/2306667)

#### 介绍
“bwsaas多端SAAS平台运营系统”接入微信开放平台（第三方服务商）,微信公众号管理及微信小程序一键授权发布，具备多租户管理、多应用上架购买、多终端（公众号，H5，小程序，PC，APP）可接入、强大的权限节点控制（管理员权限，租户及租户应用权限）、 强大的一键CRUD生成代码（页面JS,控制器controller，模型及关联模型model）、基于ThinkPHP6及layui快速布局扩展等等特性、详细的二次开发及系统使用说明文档！让您不管是自己学习使用还是公司运营，轻松快速完成二次的开发集成。

#### 软件架构
- 技术：Thinkphp6.X+Layui2.5+easywechat4.X(微信开发框架)
- 后台：租户管理后台(域名+/manage/member/login)和平台管理后台(域名+/manage/admin/login)
- 环境（建议使用宝塔面板一键搭建lnmp）：
- 系统：Windows,Linux(推荐)
- PHP > 7.2(推荐7.4)
- Nginx >=1.14或者apache >=2.4
- Mysql >=5.7 数据库引擎InnoDB
- Redis >=5.0
- PHP扩展 fileinfo,curl,openssl,simpleXML,redis,mbstring,mysqli,openssl,gd,zip

##### Bwsaas框架【全部开源】
      saas多租户管理系统：租户管理系统和运营平台管理系统；
      微信公众号，微信小程序，H5，APP，PC，抖音小程序，头条小程序等多种都能完美对接及管理；
      多终端统一前后端分离采用jwt+redis状态存储用户认证token机制；
## 站点地址

* 官方网站：[http://www.buwangyun.com](http://www.buwangyun.com)

* 开发使用文档地址：[https://www.kancloud.cn/hnlg666/bwsaas/2306667](https://www.kancloud.cn/hnlg666/bwsaas/2306667)

* 框架中商业版应用bwmall的uniapp模块公众号/H5/小程序/安卓APP演示[IOS无法演示]

![小程序演示](https://images.gitee.com/uploads/images/2020/1019/094446_f5e5f7ea_847233.png "240.png")
![公众号演示](https://images.gitee.com/uploads/images/2020/1226/121943_68cc010c_847233.png "67ff8a518a9f688fc4be9b9593c91e26.png")
![安卓APP](https://images.gitee.com/uploads/images/2021/0624/110726_309ea118_847233.png "bwmall_android (1).png")

* 演示平台：
  * 平台官网 [http://demo.buwangyun.com](http://demo.buwangyun.com)
  * 租户后台 [http://demo.buwangyun.com/manage/member/index](http://demo.buwangyun.com/manage/member/index)（账号：17777777777，密码：a12345678，资金密码123456。）
  * 总平台后台 [http://demo.buwangyun.com/manage/admin/index](http://demo.buwangyun.com/manage/admin/index)（账号：admin，密码：a12345678）

* bwsaas框架自带运营平台官网【免费送】：
![输入图片说明](https://images.gitee.com/uploads/images/2021/0520/215846_c896c24f_847233.jpeg "SaaS云管理系统.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0520/220203_d790267f_847233.png "应用大厅.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0520/220216_ec78b71c_847233.png "最新动态.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0520/220224_a0284651_847233.png "招商加盟.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0520/220238_62fc6b44_847233.png "关于我们.png")

* 总后台管理【演示】：
![输入图片说明](https://images.gitee.com/uploads/images/2021/0301/110157_1cad4459_847233.png "登录.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0301/112359_ca933452_847233.jpeg "后台首页.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0301/112521_834bfd2c_847233.jpeg "菜单节点.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0301/112549_6b6960e6_847233.jpeg "权限角色.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0301/112559_a0de2b5d_847233.jpeg "操作日志.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0301/112420_634dd265_847233.jpeg "租户管理.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0301/112610_a5a8e0c2_847233.jpeg "平台配置.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0301/112637_687a1563_847233.jpeg "附件管理.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0301/112648_4cf5a6bb_847233.jpeg "插件.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0301/112657_c0d54e29_847233.jpeg "阿里云市场API.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0301/112705_10e55adf_847233.jpeg "租户购买的应用.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0301/112720_280d689b_847233.jpeg "平台应用安装配置.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0301/112731_40e757db_847233.jpeg "平台应用配置.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0301/112739_179b1289_847233.jpeg "应用功能组售卖.jpg")
* 租户后台管理【演示】：
![输入图片说明](https://images.gitee.com/uploads/images/2021/0301/113637_dacee58b_847233.jpeg "登录.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0301/113647_2ab6d1c8_847233.jpeg "扫码登录.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0301/113705_392b0cbd_847233.jpeg "应用首页.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0301/113713_24c64492_847233.jpeg "租户首页.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0301/113721_1373b001_847233.jpeg "财务记录.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0301/113729_65ce6fa1_847233.jpeg "角色管理.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0301/113737_0cf0d0fd_847233.jpeg "租户管理.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0301/113745_0b81e019_847233.jpeg "操作日志.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0301/113752_8bbc7c56_847233.jpeg "应用商店.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0301/113759_f050ed2a_847233.jpeg "用户管理.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0301/113805_8c3fdc23_847233.jpeg "应用的用户管理.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0301/113812_09215fb5_847233.jpeg "插件市场.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0301/113818_03f4a2bd_847233.jpeg "云市场服务购买.jpg")

##### Bwsaas框架下的bwwechat模块应用【全部开源(此模块只有后端管理)】
       微信公众号管理：素材管理，关键词回复管理（文字、图片、图文、视频、音乐、网址、转客服），关注自动回复等；
       小程序：关键词回复管理（文字、图片、小程序卡片、网址、转客服）

#### 技术交流

* 客服QQ：[911098002]`添加好友请备注来源：如gitee、github、布网云等`。

![输入图片说明](https://images.gitee.com/uploads/images/2021/0624/103730_a6a24c0c_847233.png "6de753326c821f6dbb071a68f4188284.png")



## bwsaas应用开发说明

* bwsaas里面的应用模块已经开源1个模块bwwechat（独立微信公众号小程序管理）,开发者也可自行利用bwsaas框架开发自己的模块应用（应用版权归开发者所有）

## 代码仓库

* Gitee地址：[https://gitee.com/buwangyun/bwsaas](https://gitee.com/buwangyun/bwsaas)


## 项目特性
* 接入微信开放平台（第三方服务商）,SaaS应用(微信小程序)一键授权发布；
* 完善的微信公众号、小程序帐号授权体系；不管你的公众号是前后端分离开发，还是SSR形式开发模式，都能在继承相关Base基类的情况下轻松快速完成授权；小程序完美支持基于第三方服务商模式的一键授权上架，可轻松实现多用户多版本的SAAS平台运营需求；
* 具备租户应用的购买、授权、充值、消费账单管理；
* 每个租户可对每个购买授权的应用进行会员独立管理；
* 一个租户同时购买授权无数个产品应用，但是同一个应用只能一个租户购买一次，如果您有多个公众号或者小程序需要对接，只需要再注册个租户帐号就可以实现；
* 目前已经接入微信支付（租户的扫码充值，小程序用户充值及订单支付，公众号及H5用户充值及订单支付）；
* 阿里云API市场接口的高度封装集成，实现API接口的二次销售盈利；已经整合完整的短信、IP定位、经纬度定位等接口,每个服务具备多套餐管理，且支持用户调用接口的充值扣费（按次扣费）
* 支持bwsaas框架中应用一键安装、卸载、升级（可基于TP6的多应用模式开发自己的独立应用,降低学习成本）；
* 支持开发APP（安卓，IOS）、公众号、小程序、H5、PC等独立应用；
* 快速一键CRUD命令行支持
  * 一键生成控制器、模型、视图、JS文件（require.js模块化管理）
  * 支持关联模型查询、显示字段设置等等
* 基于`auth`的权限管理系统
  * 通过`后台统一管理`来实现`auth`权限节点添加管理及认证
  * 系统开启强制路由控制，来保证系统的安全节点访问
  * 系统通过统一的权限控制中间件Auth.php文件实现路由结合权限的详细权限控制
  * 完善的后端权限验证以及前面页面按钮显示、隐藏控制
* 完善的菜单管理
  * 分模块管理
  * 无限极菜单
  * 菜单节点的显示与隐藏一键控制
* 完善的上传组件功能
  * 本地存储
  * 阿里云OSS`建议使用`
  * 腾讯云COS
  * 七牛云OSS
* 完善的前端组件功能
  * 对layui的form表单重新封装，无需手动拼接数据请求，支持表单提交数据的前置回调处理表单数据
  * 封装好的自动监听form表单，封装完善的ajax请求，支持post get等请求方式
  * 简单好用的`图片、文件`上传组件
  * 简单好用的富文本编辑器`ckeditor`
  * 对弹出层进行再次封装，以极简的方式使用
  * 对table表格再次封装，在使用上更加舒服
  * 根据table的`cols`参数再次进行封装，提供接口实现`image`、`switch`、`list`等功能，在此基础上可以自己再次扩展
  * 根据table参数一键生成`搜索表单`，自由控制隐藏显示项，无需自己编写
* 记录完善的后台操作日志
  * 记录用户的详细操作信息（操作节点记录，IP，时间）
* 一键部署静态资源到OSS上
  * 所有在`public\static`目录下的文件都可以一键部署
  * 一个配置项切换静态资源（oss/本地）
* 上传文件记录管理
* ![输入图片说明](https://images.gitee.com/uploads/images/2020/1106/101238_75baa739_847233.png "Bwsaas201021.png")

## 特别感谢

以下项目排名不分先后

* ThinkPHP：[https://github.com/top-think/framework](https://github.com/top-think/framework)

* Easyadmin：[https://gitee.com/zhongshaofa/easyadmin](https://gitee.com/zhongshaofa/easyadmin)

* overtrue/wechat：[https://github.com/overtrue/wechat](https://github.com/overtrue/wechat)

* SAPI++：[https://gitee.com/goodline/sapixx](https://gitee.com/goodline/sapixx)

* Layui：[https://github.com/sentsin/layui](https://github.com/sentsin/layui)

* Jquery：[https://github.com/jquery/jquery](https://github.com/jquery/jquery)

* RequireJs：[https://github.com/requirejs/requirejs](https://github.com/requirejs/requirejs)

* CKEditor：[https://github.com/ckeditor/ckeditor4](https://github.com/ckeditor/ckeditor4)

* Echarts：[https://github.com/apache/incubator-echarts](https://github.com/apache/incubator-echarts)

#### 版权软著说明
* 本bwsaas框架软件著作权已经申请软著。
* 本bwsaas框架软件著作权归我们所有，禁止进行二次的软著申请，侵权必究。
* 开发者使用bwsaas框架来开发的应用版权归开发者所有。
* 请保留版权,而无任何其他的限制.也就是说,您必须在您的发行版里包含原许可协议的声明,无论您是以二进制发布的还是以源代码发布的。
* 开源版遵循GPL-3.0开源协议发布，并提供免费使用，但不允许修改后和衍生的代码做为闭源的商业软件发布和销售！

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request