<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit2a994fa183fa3f2e4ea8cc533ced5027
{
    public static $prefixLengthsPsr4 = array (
        'A' => 
        array (
            'App\\' => 4,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'App\\' => 
        array (
            0 => __DIR__ . '/..' . '/yly-openapi/yly-openapi-sdk/Lib',
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit2a994fa183fa3f2e4ea8cc533ced5027::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit2a994fa183fa3f2e4ea8cc533ced5027::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInit2a994fa183fa3f2e4ea8cc533ced5027::$classMap;

        }, null, ClassLoader::class);
    }
}
