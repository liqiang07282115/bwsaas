<?php

namespace addons\addon_demo;

use AlibabaCloud\Client\AlibabaCloud;
use buwang\base\BaseAddons;
use think\Exception;
use think\exception\HttpException;

/**
 * demo插件
 * @author byron sampson
 */
class Plugin extends BaseAddons    // 需继承think\Addons类
{
    // 该插件的基础信息
    public $info = [
    ];
    /**
     * 插件安装方法
     * @return bool
     */
    public function install()
    {
        return true;
    }

    /**
     * 插件卸载方法
     * @return bool
     */
    public function uninstall()
    {
        return true;
    }
    public function enabled()
    {
        // TODO: Implement enabled() method.
    }
    public function disabled()
    {
        // TODO: Implement disabled() method.
    }
}