<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace addons\addon_demo\controller;

use App\Oauth\YlyOauthClient;
use App\Config\YlyConfig;
/**
 * Copyright
 */

class Index extends PluginBaseController
{
//    protected $middleware = [
//        'login' => ['except' => []]
//    ];

    public function index()
    {
        //示例：在插件中使用了composer包
        //$config = new YlyConfig('你的应用id', '你的应用密钥');
        return $this->fetch();
    }


    public function _empty($param)
    {
        var_dump("不存在方法：" . $param);
    }

}