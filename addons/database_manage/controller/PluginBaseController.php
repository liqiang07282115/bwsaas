<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace addons\database_manage\controller;

use buwang\base\PluginBaseController as Base;
use think\App;

/**
 * 控制器基础类
 */
abstract class PluginBaseController extends Base
{

    //布局模板
    protected $layout = '../../../view/public/layout';

    /**
     * 构造方法
     * @access public
     * @param App $app 应用对象
     */
    public function __construct(App $app)
    {
        $this->layout && $app->view->engine()->layout($this->layout);
        // 父类的调用必须放在设置模板路径之后
        parent::__construct($app);

    }
}
