<?php
$dir = ADDONS_DIR;
return [
    [
        //角色名称/功能名称
        'name' => $dir . '数据库管理功能',
        //角色唯一标识,不可重复
        'group_name' => 'group',
        //角色备注/功能描述
        'remark' => '',
        'scopes' => 'admin',
        //角色拥有的节点
        'nodes' => [
            [
                "title" => '数据库',
                //菜单url
                "menu_path" => '/addons/' . $dir . '/Index/index',
                //实际
                "name" => '/addons/' . $dir . '/Index/index',
                //权限标识,必填 唯一
                "auth_name" => 'addons_database_manage_Index_index',
                //附加参数 ?id=1&name=demo
                "param" => '',
                //打开方式
                "target" => '_self',
                //是否菜单 1=是,0=否
                "ismenu" => '1',
                //图标
                "icon" => 'fa fa-database',
                //备注
                "remark" => '',
                //子节点
                'children' => [
                    [
                        "title" => '表详情',
                        //菜单url
                        "menu_path" => '/addons/' . $dir . '/Index/detail',
                        //实际
                        "name" => '/addons/' . $dir . '/Index/detail',
                        //权限标识,必填 唯一
                        "auth_name" => '',
                        //附加参数 ?id=1&name=demo
                        "param" => '',
                        //打开方式
                        "target" => '_self',
                        //是否菜单 1=是,0=否
                        "ismenu" => ' 0',
                        //图标
                        "icon" => '',
                        //备注
                        "remark" => '',
                        //子节点
                        'children' => []

                    ],
                    [
                        "title" => '数据备份',
                        //菜单url
                        "menu_path" => '/addons/' . $dir . '/Index/backup',
                        //实际
                        "name" => '/addons/' . $dir . '/Index/backup',
                        //权限标识,必填 唯一
                        "auth_name" => '',
                        //附加参数 ?id=1&name=demo
                        "param" => '',
                        //打开方式
                        "target" => '_self',
                        //是否菜单 1=是,0=否
                        "ismenu" => '0',
                        //图标
                        "icon" => '',
                        //备注
                        "remark" => '',
                        //子节点
                        'children' => []
                    ],
                    [
                        "title" => '表优化',
                        //菜单url
                        "menu_path" => '/addons/' . $dir . '/Index/optimize',
                        //实际
                        "name" => '/addons/' . $dir . '/Index/optimize',
                        //权限标识,必填 唯一
                        "auth_name" => '',
                        //附加参数 ?id=1&name=demo
                        "param" => '',
                        //打开方式
                        "target" => '_self',
                        //是否菜单 1=是,0=否
                        "ismenu" => '0',
                        //图标
                        "icon" => '',
                        //备注
                        "remark" => '',
                        //子节点
                        'children' => []
                    ],
                    [
                        "title" => '表修复',
                        //菜单url
                        "menu_path" => '/addons/' . $dir . '/Index/repair',
                        //实际
                        "name" => '/addons/' . $dir . '/Index/repair',
                        //权限标识,必填 唯一
                        "auth_name" => '',
                        //附加参数 ?id=1&name=demo
                        "param" => '',
                        //打开方式
                        "target" => '_self',
                        //是否菜单 1=是,0=否
                        "ismenu" => '0',
                        //图标
                        "icon" => '',
                        //备注
                        "remark" => '',
                        //子节点
                        'children' => []
                    ],
                    [
                        "title" => '备份删除',
                        //菜单url
                        "menu_path" => '/addons/' . $dir . '/Index/del',
                        //实际
                        "name" => '/addons/' . $dir . '/Index/del',
                        //权限标识,必填 唯一
                        "auth_name" => '',
                        //附加参数 ?id=1&name=demo
                        "param" => '',
                        //打开方式
                        "target" => '_self',
                        //是否菜单 1=是,0=否
                        "ismenu" => '0',
                        //图标
                        "icon" => '',
                        //备注
                        "remark" => '',
                        //子节点
                        'children' => []
                    ],
                    [
                        "title" => '备份列表',
                        //菜单url
                        "menu_path" => '/addons/' . $dir . '/Index/backupList',
                        //实际
                        "name" => '/addons/' . $dir . '/Index/backupList',
                        //权限标识,必填 唯一
                        "auth_name" => '',
                        //附加参数 ?id=1&name=demo
                        "param" => '',
                        //打开方式
                        "target" => '_self',
                        //是否菜单 1=是,0=否
                        "ismenu" => '0',
                        //图标
                        "icon" => '',
                        //备注
                        "remark" => '',
                        //子节点
                        'children' => []
                    ],
                    [
                        "title" => '备份导入（备份覆盖）',
                        //菜单url
                        "menu_path" => '/addons/' . $dir . '/Index/import',
                        //实际
                        "name" => '/addons/' . $dir . '/Index/import',
                        //权限标识,必填 唯一
                        "auth_name" => '',
                        //附加参数 ?id=1&name=demo
                        "param" => '',
                        //打开方式
                        "target" => '_self',
                        //是否菜单 1=是,0=否
                        "ismenu" => '0',
                        //图标
                        "icon" => '',
                        //备注
                        "remark" => '',
                        //子节点
                        'children' => []
                    ],
                    [
                        "title" => '备份下载',
                        //菜单url
                        "menu_path" => '/addons/' . $dir . '/Index/download',
                        //实际
                        "name" => '/addons/' . $dir . '/Index/download',
                        //权限标识,必填 唯一
                        "auth_name" => '',
                        //附加参数 ?id=1&name=demo
                        "param" => '',
                        //打开方式
                        "target" => '_self',
                        //是否菜单 1=是,0=否
                        "ismenu" => '0',
                        //图标
                        "icon" => '',
                        //备注
                        "remark" => '',
                        //子节点
                        'children' => []
                    ],]
            ],
        ]
    ]
];