<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace addons\alisms\controller;

/**
 * Copyright
 */
class MyDemo extends PluginBaseController
{
    public function getUserId()
    {
        //var_dump($this->view->getConfig('tpl_replace_string'));die;
        var_dump(config('view.tpl_replace_string.__ADDON__'));
        die;
    }

    public function _empty($param)
    {
        var_dump("不存在方法：" . $param);
    }

}