<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------
namespace upload;

use think\facade\Filesystem;

/**
 * 文件上传封装基类
 * Class Base
 * @package EasyAdmin\upload
 */
class FileBase
{
    /**
     * 上传配置
     * @var array
     */
    protected $uploadConfig;

    /**
     * 上传文件对象
     * @var object
     */
    protected $file;

    /**
     * 上传完成的文件路径
     * @var string
     */
    protected $completeFilePath;

    /**
     * 上传oss对象路径
     * @var string
     */
    protected $completeFileObjName;

    /**
     * 上传完成的文件的URL
     * @var string
     */
    protected $completeFileUrl;

    /**
     * 保存上传文件的数据表
     * @var string
     */
    protected $tableName;

    /**
     * 上传类型
     * @var string
     */
    protected $uploadType = 'local';

    /**
     * 删除文件名
     * @var string
     */
    protected $delName = '';

    /**
     * 租户id
     * @var string
     */
    protected $member_id = 0;

    /**
     * 设置租户id
     * @param $value
     * @return $this
     */
    public function setMemberId($value){
        $this->member_id = $value;
        return $this;
    }

    /**
     * 设置上传方式
     * @param $value
     * @return $this
     */
    public function setUploadType($value)
    {
        $this->uploadType = $value;
        return $this;
    }

    /**
     * 设置上传配置
     * @param $value
     * @return $this
     */
    public function setUploadConfig($value)
    {
        $this->uploadConfig = $value;
        return $this;
    }

    /**
     * 设置上传配置
     * @param $value
     * @return $this
     */
    public function setFile($value)
    {
        $this->file = $value;
        return $this;
    }

    /**
     * 设置保存文件数据表
     * @param $value
     * @return $this
     */
    public function setTableName($value)
    {
        $this->tableName = $value;
        return $this;
    }

    /**
     * 保存文件
     */
    public function save()
    {
        $this->completeFileObjName = Filesystem::disk('public')->putFile('upload', $this->file);
        $this->completeFilePath = Filesystem::getDiskConfig('public', 'root') . '/' .str_replace("\\",'/',Filesystem::disk('public')->putFile('upload', $this->file));
        $this->completeFileUrl = request()->domain() . '/' .Filesystem::getDiskConfig('public', 'url') . '/' . str_replace(DIRECTORY_SEPARATOR, '/', $this->completeFilePath);
    }

    /**
     * 删除保存在本地的文件
     * @return bool|string
     */
    public function rmLocalSave()
    {
        try {
            $rm = unlink($this->completeFilePath);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
        return $rm;
    }

    /**
     * 设置上传方式
     * @param $value
     * @return $this
     */
    public function setDelName($value)
    {
        $this->delName = $value;
        return $this;
    }

    /**
     * 删除文件
     */
    public function del()
    {
        $this->completeFilePath = Filesystem::disk('public')->path($this->delName);
        $res = $this->rmLocalSave();
        return $res === true;
    }
}