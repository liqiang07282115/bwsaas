<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace upload\driver\alioss;

use upload\interfaces\OssDriver;
use OSS\Core\OssException;
use OSS\OssClient;

class Oss implements OssDriver
{

    protected static $instance;

    protected $accessKeyId;

    protected $accessKeySecret;

    protected $endpoint;

    protected $bucket;

    protected $domain;

    protected $ossClient;

    protected function __construct($config)
    {
        $this->accessKeyId = $config['alioss_access_key_id'];
        $this->accessKeySecret = $config['alioss_access_key_secret'];
        $this->endpoint = $config['alioss_endpoint'];
        $this->bucket = $config['alioss_bucket'];
        $this->domain = $config['alioss_domain'];
        $this->ossClient = new OssClient($this->accessKeyId, $this->accessKeySecret, $this->endpoint, true);
        return $this;
    }

    public static function instance($config)
    {
        if (is_null(self::$instance)) {
            self::$instance = new static($config);
        }
        return self::$instance;
    }

    public function save($objectName,$filePath)
    {
        try {
            $upload = $this->ossClient->uploadFile($this->bucket, $objectName, $filePath);
        } catch (OssException $e) {
            return [
                'save' => false,
                'msg'  => $e->getMessage(),
            ];
        }
        if (!isset($upload['info']['url'])) {
            return [
                'save' => false,
                'msg'  => '保存失败',
            ];
        }
        return [
            'save' => true,
            'msg'  => '上传成功',
            'url'  => $upload['info']['url'],
        ];
    }
    //删除单个文件
    public function del($objectName)
    {
        try {
            $this->ossClient->deleteObject($this->bucket, $objectName);
        } catch (OssException $e) {
            return false;
        }
        return true;
    }
}