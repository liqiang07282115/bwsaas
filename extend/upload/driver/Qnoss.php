<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------
namespace upload\driver;

use upload\FileBase;
use upload\driver\qnoss\Oss;
use upload\trigger\SaveDb;

/**
 * 七牛云上传
 * Class Qnoss
 * @package EasyAdmin\upload\driver
 */
class Qnoss extends FileBase
{
    /**
     * 重写上传方法
     * @return array|void
     */
    public function save()
    {
        parent::save();
        $upload = Oss::instance($this->uploadConfig)
            ->save($this->completeFileObjName, $this->completeFilePath);
        $file_info =[];
        if ($upload['save'] == true) {
            $file_info = [
                'upload_type'   => $this->uploadType,
                'original_name' => $this->file->getOriginalName(),
                'member_id'     => $this->member_id,
                'mime_type'     => $this->file->getOriginalMime(),
                'file_ext'      => strtolower($this->file->getOriginalExtension()),
                'url'           => $upload['url'],
                'dir_path'      => $this->completeFileObjName ?:'',
                'create_time'   => time(),
            ];
            SaveDb::trigger($this->tableName,$file_info);
        }
        $upload['file_info'] =$file_info;
        $this->rmLocalSave();
        return $upload;
    }
    /**
     * 重写删除方法
     * @return bool
     */
    public function del()
    {
        parent::del();
        return Oss::instance($this->uploadConfig)
            ->del($this->delName);
    }

}