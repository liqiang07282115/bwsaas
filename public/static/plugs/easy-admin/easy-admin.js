define(["jquery", "tableSelect", "ckeditor"], function ($, tableSelect, undefined) {

    var form = layui.form,
        layer = layui.layer,
        table = layui.table,
        laydate = layui.laydate,
        upload = layui.upload,
        element = layui.element,
        laytpl = layui.laytpl,
        tableSelect = layui.tableSelect;

    layer.config({
        skin: 'layui-layer-easy'
    });
CKEDITOR.timestamp ='v1.0.1';
    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        upload_url: '/manage/publicCommon/upload',//layui.upload上传网址
        upload_exts: 'doc|gif|ico|icon|jpg|mp3|mp4|p12|pem|png|rar',
        get_upload_files: '/manage/publicCommon/getUploadFiles',
        upload_editor: '/manage/publicCommon/uploadEditor',//编辑器里面的上传URL
        tab_filter: [],//tab切换触发表格重载
    };
    //回调函数
    var callback_init = {
        tableReloadFunction: null, //数据表格重载前回调函数
        tableDataCallback: null,   //数据表格加载后回调函数
        searchBeforeFunction: null,   //数据表格搜索回调函数
        requestButtonFunction: {},   //数据表格按钮回调函数
        rowData: {},   //数据表格回调数据
    };
    var admin = {
        config: {
            shade: [0.02, '#000'],
        },
        url: function (url) {
            return '/' + CONFIG.ADMIN + '/' + url;
        },
        checkAuth: function (node, elem) {
            //jyk 2020/12/26 undefined拥有更高优先级可屏蔽不想展示的表格功能---start
            if ($(elem).attr('data-auth-' + node) === undefined) {
                return false;
            }
            //jyk 2020/12/26 null拥有更高优先级可屏蔽不想展示的表格功能---end
            if (CONFIG.IS_SUPER_ADMIN) {
                return true;
            }
            if ($(elem).attr('data-auth-' + node) === '1') {
                return true;
            } else {
                return false;
            }
        },
        parame: function (param, defaultParam) {
            return param !== undefined ? param : defaultParam;
        },
        request: {
            post: function (option, ok, no, ex) {
                return admin.request.ajax('post', option, ok, no, ex);
            },
            get: function (option, ok, no, ex) {
                return admin.request.ajax('get', option, ok, no, ex);
            },
            ajax: function (type, option, ok, no, ex) {
                type = type || 'get';
                option.url = option.url || '';
                option.data = option.data || {};
                option.headers = option.headers || {};
                option.prefix = option.prefix || false;
                option.statusName = option.statusName || 'code';
                option.statusCode = option.statusCode || 200;
                option.errCode = option.errCode || 0;
                ok = ok || function (res) {
                };
                no = no || function (res) {
                    var msg = res.msg == undefined ? '返回数据格式有误' : res.msg;
                    admin.msg.error(msg);
                    return false;
                };
                ex = ex || function (res) {
                };
                if (option.url == '') {
                    admin.msg.error('请求地址不能为空');
                    return false;
                }
                if (option.prefix == true) {
                    option.url = admin.url(option.url);
                }
                var index = admin.msg.loading('加载中');
                var headers = Object.assign({
                    'X-Requested-With': 'XMLHttpRequest',
                    "token": admin.common.getCookie('token'),//此处放置请求到的用户token
                },option.headers);
                $.ajax({
                    url: option.url,
                    type: type,
                    xhrFields:{
                        withCredentials: true
                    },
                    headers: headers,
                    contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                    dataType: "json",
                    data: option.data,
                    timeout: 60000,
                    success: function (res) {
                        admin.msg.close(index);
                        if (eval('res.' + option.statusName) == option.statusCode && eval('res.data.errcode') == option.errCode) {
                            return ok(res);
                        } else {
                            return no(res);
                        }
                    },
                    error: function (xhr, textstatus, thrown) {
                        switch (xhr.status) {
                            case(500):
                                layer.msg("服务器系统内部错误");
                                break;
                            case(401):
                                layer.msg("未登录");
                                break;
                            case(403):
                                layer.msg("无权限执行此操作");
                                break;
                            case(408):
                                layer.msg("请求超时");
                                break;
                            default:
                                admin.msg.error('Status:' + xhr.status + '，' + xhr.statusText + '，请稍后再试！', function () {
                                    ex(this);
                                });
                        }
                        return false;
                    }
                });
            }
        },
        common: {
            parseNodeStr: function (node) {
                var array = node.split('/');
                $.each(array, function (key, val) {
                    if (key === 0) {
                        val = val.split('.');
                        $.each(val, function (i, v) {
                            val[i] = admin.common.humpToLine(v.replace(v[0], v[0].toLowerCase()));
                        });
                        val = val.join(".");
                        array[key] = val;
                    }
                });
                node = array.join("/");
                return node;
            },
            lineToHump: function (name) {
                return name.replace(/\_(\w)/g, function (all, letter) {
                    return letter.toUpperCase();
                });
            },
            humpToLine: function (name) {
                return name.replace(/([A-Z])/g, "_$1").toLowerCase();
            },
            get_sec: function (str) {
                var str1 = str.substring(1, str.length) * 1;
                var str2 = str.substring(0, 1);
                if (str2 == "s") {
                    return str1 * 1000;
                } else if (str2 == "h") {
                    return str1 * 60 * 60 * 1000;
                } else if (str2 == "d") {
                    return str1 * 24 * 60 * 60 * 1000;
                }
            },
            setCookie: function (key, value, times) {
                var strsec = admin.common.get_sec(times);
                var exp = new Date();
                exp.setTime(exp.getTime() + strsec * 1);
                document.cookie = key + "=" + escape(value) + ";expires=" + exp.toGMTString() + ';path=/';
            },
            getCookie: function (key) {
                var arr, reg = new RegExp("(^| )" + key + "=([^;]*)(;|$)");
                if (arr = document.cookie.match(reg)) return unescape(arr[2]);
                else return null
            },
            delCookie: function (key) {
                var exp = new Date();
                exp.setTime(exp.getTime() - 1);
                var cval = this.getCookie(key);
                if (cval != null) document.cookie = key + "=" + cval + ";expires=" + exp.toGMTString() + ';path=/';
            },
        },
        msg: {
            // 成功消息
            success: function (msg, callback, time) {
                if(!time) time = 2000;//自定义时间

                if (callback === undefined) {
                    callback = function () {
                    }
                }
                var index = layer.msg(msg, {icon: 1, shade: admin.config.shade, scrollbar: false, time: time, shadeClose: true}, callback);
                return index;
            },
            // 失败消息
            error: function (msg, callback) {
                if (callback === undefined) {
                    callback = function () {
                    }
                }
                var index = layer.msg(msg, {icon: 2, shade: admin.config.shade, scrollbar: false, time: 3000, shadeClose: true}, callback);
                return index;
            },
            // 警告消息框
            alert: function (msg, callback) {
                var index = layer.alert(msg, {end: callback, scrollbar: false});
                return index;
            },
            // 对话框
            confirm: function (msg, ok, no) {
                var index = layer.confirm(msg, {title: '操作确认', btn: ['确认', '取消']}, function () {
                    typeof ok === 'function' && ok.call(this);
                }, function () {
                    typeof no === 'function' && no.call(this);
                    self.close(index);
                });
                return index;
            },
            // 消息提示
            tips: function (msg, time, callback) {
                var index = layer.msg(msg, {time: (time || 3) * 1000, shade: this.shade, end: callback, shadeClose: true});
                return index;
            },
            // 加载中提示
            loading: function (msg, callback) {
                var index = msg ? layer.msg(msg, {icon: 16, scrollbar: false, shade: this.shade, time: 0, end: callback}) : layer.load(2, {time: 0, scrollbar: false, shade: this.shade, end: callback});
                return index;
            },
            // 关闭消息框
            close: function (index) {
                return layer.close(index);
            }
        },
        table: {
            render: function (options) {
                options.init = options.init || init;
                options.modifyReload = admin.parame(options.modifyReload, true);
                options.elem = options.elem || options.init.table_elem;
                options.id = options.id || options.init.table_render_id;
                options.layFilter = options.id + '_LayFilter';
                options.url = options.url || admin.url(options.init.index_url);
                options.page = admin.parame(options.page, true);
                options.search = admin.parame(options.search, true);
                options.skin = options.skin || 'line';
                options.limit = options.limit || 15;
                options.limits = options.limits || [10, 15, 20, 25, 50, 100];
                options.cols = options.cols || [];
                //增加数据渲染完成回调
                var table_done =  options.done ||  function(res, curr, count){};
                options.done = function(res, curr, count){
                        table_done(res, curr, count);
                    //表格渲染后监听表格同步高度
                    admin.table.listenSynchroHeight(options.init);

               }
                if (options.init.tab_filter) init.tab_filter = options.init.tab_filter;
                if (options.init.table_render_id) init.table_render_id = options.init.table_render_id;
                try {
                    if (options.searchBeforeFunction) {
                        if (typeof options.searchBeforeFunction === "function") { //是函数    其中 FunName 为函数名称
                            callback_init.searchBeforeFunction = options.searchBeforeFunction;
                        }
                    }
                    if (options.tableReloadFunction) {
                        if (typeof options.tableReloadFunction === "function") { //是函数    其中 FunName 为函数名称
                            callback_init.tableReloadFunction = options.tableReloadFunction;
                        }
                    }
                } catch (e) {
                }

                options.parseData = function (res) { //res 即为原始返回的数据
                    //表格接口返回数据回调
                    try {
                        if (options.tableDataCallback) {
                            if (typeof options.tableDataCallback === "function") { //是函数    其中 FunName 为函数名称
                                options.tableDataCallback(res);
                                callback_init.tableDataCallback = options.tableDataCallback;
                            }
                        }
                    } catch (e) {
                    }

                    return {
                        "code": res.code, //解析接口状态
                        "msg": res.msg, //解析提示文本
                        "count": res.data.data.total, //解析数据长度
                        "data": res.data.data.list //解析数据列表
                    };
                };
                options.response = {
                    statusCode: 200 //规定成功的状态码，默认：0
                };
                options.defaultToolbar = (options.defaultToolbar === undefined && !options.search) ? ['filter', 'print', 'exports'] : ['filter', 'print', 'exports', {
                    title: '搜索',
                    layEvent: 'TABLE_SEARCH',
                    icon: 'layui-icon-search',
                    extend: 'data-table-id="' + options.id + '"'
                }];

                // 判断是否为移动端
                if (admin.checkMobile()) {
                    options.defaultToolbar = !options.search ? ['filter'] : ['filter', {
                        title: '搜索',
                        layEvent: 'TABLE_SEARCH',
                        icon: 'layui-icon-search',
                        extend: 'data-table-id="' + options.id + '"'
                    }];
                }

                // 判断元素对象是否有嵌套的
                options.cols = admin.table.formatCols(options.cols, options.init);

                // 初始化表格lay-filter
                $(options.elem).attr('lay-filter', options.layFilter);

                // 初始化表格搜索
                if (options.search === true) {
                    admin.table.renderSearch(options.cols, options.elem, options.id);
                }

                // 初始化表格左上方工具栏
                options.toolbar = options.toolbar || ['refresh', 'add', 'delete', 'export'];
                options.toolbar = admin.table.renderToolbar(options.toolbar, options.elem, options.id, options.init);

                // 判断是否有操作列表权限
                options.cols = admin.table.renderOperat(options.cols, options.elem);

                // 初始化表格
                var newTable = table.render(options); //执行layui数据表格渲染

                if (callback_init.searchBeforeFunction) admin.table_reset(options.id);
                // 监听表格搜索开关显示
                admin.table.listenToolbar(options.layFilter, options.id);

                // 监听表格开关切换
                admin.table.renderSwitch(options.cols, options.init, options.id, options.modifyReload);

                // 监听表格开关切换
                admin.table.listenEdit(options.init, options.layFilter, options.id, options.modifyReload);

                return newTable;
            },
            renderToolbar: function (data, elem, tableId, init) {
                data = data || [];
                var toolbarHtml = '';
                $.each(data, function (i, v) {
                    if (v === 'refresh') {
                        toolbarHtml += ' <button class="layui-btn layui-btn-sm layuimini-btn-primary" data-table-refresh="' + tableId + '"><i class="fa fa-refresh"></i> </button>\n';
                    } else if (v === 'add') {
                        if (admin.checkAuth('add', elem)) {
                            toolbarHtml += '<button class="layui-btn layui-btn-normal layui-btn-sm" data-open="' + init.add_url + '" data-title="添加"><i class="fa fa-plus"></i> 添加</button>\n';
                        }
                    } else if (v === 'delete') {
                        if (admin.checkAuth('delete', elem)) {
                            toolbarHtml += '<button class="layui-btn layui-btn-sm layui-btn-danger" data-url="' + init.delete_url + '" data-table-delete="' + tableId + '"><i class="fa fa-trash-o"></i> 删除</button>\n';
                        }
                    } else if (v === 'export') {
                        if (admin.checkAuth('export', elem)) {
                            toolbarHtml += '<button class="layui-btn layui-btn-sm layui-btn-success easyadmin-export-btn" data-url="' + init.export_url + "?page=1&limit=15&filter=%7B%22fruit%22%3A%22apple%22%7D&op=%7B%22fruit%22%3A%22%3D%22%7D" + '" data-table-export="' + tableId + '"><i class="fa fa-file-excel-o"></i> 导出</button>\n';
                        }
                    } else if (typeof v === "object") {
                        $.each(v, function (ii, vv) {
                            vv.class = vv.class || '';
                            vv.icon = vv.icon || '';
                            vv.auth = vv.auth || '';
                            vv.url = vv.url || '';
                            vv.method = vv.method || 'open';
                            vv.title = vv.title || vv.text;
                            vv.text = vv.text || vv.title;
                            vv.extend = vv.extend || '';
                            vv.checkbox = vv.checkbox || false;
                            if (admin.checkAuth(vv.auth, elem)) {
                                toolbarHtml += admin.table.buildToolbarHtml(vv, tableId);
                            }
                        });
                    }
                });
                return '<div>' + toolbarHtml + '</div>';
            },
            renderSearch: function (cols, elem, tableId) {
                // TODO 只初始化第一个table搜索字段，如果存在多个(绝少数需求)，得自己去扩展
                cols = cols[0] || {};
                var newCols = [];
                var formHtml = '';
                $.each(cols, function (i, d) {//处理每个字段的渲染
                    d.field = d.field || false;
                    d.fieldAlias = admin.parame(d.fieldAlias, d.field);//字段别名
                    d.title = d.title || d.field || '';
                    d.selectList = d.selectList || {};
                    d.search = admin.parame(d.search, true);//默认开启搜索
                    d.searchTip = d.searchTip || d.title || '';//'请输入' +
                    d.searchValue = d.searchValue || '';
                    d.searchOp = d.searchOp || '%*%';//默认为模糊搜索
                    d.timeType = d.timeType || 'datetime';//默认时间渲染为日期时间
                    if (d.field !== false && d.search !== false) {
                        switch (d.search) {
                            case true:
                                formHtml += '\t<div class="layui-form-item layui-inline">\n' +
                                    '<label class="layui-form-label">' + d.title + '</label>\n' +
                                    '<div class="layui-input-inline">\n' +
                                    '<input type="text" id="c-' + d.fieldAlias + '" name="' + d.fieldAlias + '" data-search-op="' + d.searchOp + '" value="' + d.searchValue + '" placeholder="' + d.searchTip + '" class="layui-input">\n' +
                                    '</div>\n' +
                                    '</div>';
                                break;
                            case  'select':
                                d.searchOp = '=';//下拉选择筛选为 精确搜索 =
                                var selectHtml = '';
                                $.each(d.selectList, function (sI, sV) {
                                    var selected = '';
                                    if (sI === d.searchValue) {
                                        selected = 'selected=""';
                                    }
                                    selectHtml += '<option value="' + sI + '" ' + selected + '>' + sV + '</option>/n';
                                });
                                formHtml += '\t<div class="layui-form-item layui-inline">\n' +
                                    '<label class="layui-form-label">' + d.title + '</label>\n' +
                                    '<div class="layui-input-inline">\n' +
                                    '<select class="layui-select" id="c-' + d.fieldAlias + '" name="' + d.fieldAlias + '"  data-search-op="' + d.searchOp + '" >\n' +
                                    '<option value="">- 全部 -</option> \n' +
                                    selectHtml +
                                    '</select>\n' +
                                    '</div>\n' +
                                    '</div>';
                                break;
                            case 'range':
                                d.searchOp = 'range';
                                formHtml += '\t<div class="layui-form-item layui-inline">\n' +
                                    '<label class="layui-form-label">' + d.title + '</label>\n' +
                                    '<div class="layui-input-inline">\n' +
                                    '<input id="c-' + d.fieldAlias + '" name="' + d.fieldAlias + '"  data-search-op="' + d.searchOp + '"  value="' + d.searchValue + '" placeholder="' + d.searchTip + '" class="layui-input">\n' +
                                    '</div>\n' +
                                    '</div>';
                                break;
                            case 'time':
                                d.searchOp = '=';
                                formHtml += '\t<div class="layui-form-item layui-inline">\n' +
                                    '<label class="layui-form-label">' + d.title + '</label>\n' +
                                    '<div class="layui-input-inline">\n' +
                                    '<input id="c-' + d.fieldAlias + '" name="' + d.fieldAlias + '"  data-search-op="' + d.searchOp + '"  value="' + d.searchValue + '" placeholder="' + d.searchTip + '" class="layui-input">\n' +
                                    '</div>\n' +
                                    '</div>';
                                break;
                            case 'between':
                                d.searchOp = 'between';
                                formHtml += '\t<div class="layui-form-item layui-inline" style="height: 32px;">\n' +
                                    '<label class="layui-form-label">' + d.title + '</label>\n' +
                                    '<div class="layui-input-inline" style="width: 80px;">\n' +
                                    '<input type="text" id="c-' + d.fieldAlias + '_min" name="' + d.fieldAlias + '_min"  data-search-op="' + d.searchOp + '"  value="' + d.searchValue + '" placeholder="' + d.searchTip + '" class="layui-input">\n' +
                                    '</div>\n' +
                                    '<div class="layui-form-mid">-</div>' +
                                    '<div class="layui-input-inline" style="width: 80px;">\n' +
                                    '<input type="text" id="c-' + d.fieldAlias + '_max" name="' + d.fieldAlias + '_max"  data-search-op="' + d.searchOp + '"  value="' + d.searchValue + '" placeholder="' + d.searchTip + '" class="layui-input">\n' +
                                    '</div>\n' +
                                    '</div>';
                                break;
                        }
                        newCols.push(d);
                    }
                });
                if (formHtml !== '') {

                    $(elem).before('<fieldset id="searchFieldset_' + tableId + '" class="table-search-fieldset layui-hide">\n' +
                        '<legend>条件搜索</legend>\n' +
                        '<form class="layui-form layui-form-pane form-search" lay-filter="select_tab_' + tableId + '">\n' +
                        formHtml +
                        '<div class="layui-form-item layui-inline" style="margin-left: 115px">\n' +
                        '<button type="submit" class="layui-btn layui-btn-normal" data-type="tableSearch" data-table="' + tableId + '" lay-submit lay-filter="' + tableId + '_filter"> 搜 索</button>\n' +
                        '<button type="reset" class="layui-btn layui-btn-primary" data-table-reset="' + tableId + '"> 重 置 </button>\n' +
                        ' </div>' +
                        '</form>' +
                        '</fieldset>');

                    admin.table.listenTableSearch(tableId);//组装请求参数数据

                    // 初始化form表单
                    form.render();
                    $.each(newCols, function (ncI, ncV) {
                        if (ncV.search === 'range') {
                            laydate.render({range: true, type: ncV.timeType, elem: '[name="' + ncV.field + '"]'});
                        }
                        if (ncV.search === 'time') {
                            laydate.render({type: ncV.timeType, elem: '[name="' + ncV.field + '"]'});
                        }
                    });
                }
            },
            renderSwitch: function (cols, tableInit, tableId, modifyReload) {
                tableInit.modify_url = tableInit.modify_url || false;
                cols = cols[0] || {};
                tableId = tableId || init.table_render_id;
                if (cols.length > 0) {
                    $.each(cols, function (i, v) {
                        v.filter = v.filter || false;
                        if (v.filter !== false && tableInit.modify_url !== false) {
                            admin.table.listenSwitch({filter: v.filter, url: tableInit.modify_url, tableId: tableId, modifyReload: modifyReload});
                        }
                    });
                }
            },
            renderOperat(data, elem) {
                for (dk in data) {
                    var col = data[dk];
                    var operat = col[col.length - 1].operat;
                    if (operat !== undefined) {
                        var check = false;
                        for (key in operat) {
                            var item = operat[key];
                            if (typeof item === 'string') {
                                if (admin.checkAuth(item, elem)) {
                                    check = true;
                                    break;
                                }
                            } else {
                                for (k in item) {
                                    var v = item[k];
                                    if (v.auth !== undefined && admin.checkAuth(v.auth, elem)) {
                                        check = true;
                                        break;
                                    }
                                }
                            }
                        }
                        if (!check) {
                            data[dk].pop()
                        }
                    }
                }
                return data;
            },
            buildToolbarHtml: function (toolbar, tableId) {
                var html = '';
                toolbar.class = toolbar.class || '';
                toolbar.icon = toolbar.icon || '';
                toolbar.auth = toolbar.auth || '';
                toolbar.url = toolbar.url || '';
                toolbar.extend = toolbar.extend || '';
                toolbar.method = toolbar.method || 'open';
                toolbar.field = toolbar.field || 'id';
                toolbar.title = toolbar.title || toolbar.text;
                toolbar.text = toolbar.text || toolbar.title;
                toolbar.checkbox = toolbar.checkbox || false;
                var formatToolbar = toolbar;
                formatToolbar.icon = formatToolbar.icon !== '' ? '<i class="' + formatToolbar.icon + '"></i> ' : '';
                formatToolbar.class = formatToolbar.class !== '' ? 'class="' + formatToolbar.class + '" ' : '';
                if (toolbar.method === 'open') {
                    formatToolbar.method = formatToolbar.method !== '' ? 'data-open="' + formatToolbar.url + '" data-title="' + formatToolbar.title + '" ' : '';
                } else {
                    formatToolbar.method = formatToolbar.method !== '' ? 'data-request="' + formatToolbar.url + '" data-title="' + formatToolbar.title + '" ' : '';
                }
                formatToolbar.checkbox = toolbar.checkbox ? ' data-checkbox="true" ' : '';
                formatToolbar.tableId = tableId !== undefined ? ' data-table="' + tableId + '" ' : '';
                html = '<button ' + formatToolbar.class + formatToolbar.method + formatToolbar.extend + formatToolbar.checkbox + formatToolbar.tableId + '>' + formatToolbar.icon + formatToolbar.text + '</button>';

                return html;
            },
            buildOperatHtml: function (operat,row_data) {
                var html = '';
                operat.class = operat.class || '';
                operat.icon = operat.icon || '';
                operat.auth = operat.auth || '';
                operat.url = operat.url || '';
                operat.extend = operat.extend || '';
                operat.method = operat.method || 'open';
                operat.field = operat.field || 'id';
                operat.title = operat.title || operat.text;
                operat.text = operat.text || operat.title;
                operat.callback = operat.callback || '';
                operat.callback_name = '';
                operat.data_id = '';
                if(row_data&&row_data.id){
                    callback_init.rowData[row_data.id] = row_data;
                    operat.data_id = 'data-id="' + row_data.id + '"';
                }
                // callback_init.rowData[row_data.id]
                if (operat.callback) {
                    if (typeof operat.callback === "function") { //是函数    其中 FunName 为函数名称
                        var function_name = init.table_render_id + operat.auth;
                        callback_init.requestButtonFunction[function_name] = operat.callback;
                    }
                    operat.callback_name = 'data-callback="' + function_name + '"';
                }

                var formatOperat = operat;
                formatOperat.icon = formatOperat.icon !== '' ? '<i class="' + formatOperat.icon + '"></i> ' : '';
                formatOperat.class = formatOperat.class !== '' ? 'class="' + formatOperat.class + '" ' : '';
                if (operat.method === 'open') {
                    formatOperat.method = formatOperat.method !== '' ? 'data-open="' + formatOperat.url + '" data-title="' + formatOperat.title + '" ' : '';
                } else if (operat.method === 'url') {
                    formatOperat.method = 'href="' + formatOperat.url + '"  data-title="' + formatOperat.title + '" ';
                } else if (operat.method === 'none') {
                    formatOperat.method = 'href="#"  data-title="' + formatOperat.title + '" ' + 'data-click="' + formatOperat.url + '" ';
                } else {
                    formatOperat.method = formatOperat.method !== '' ? 'data-request="' + formatOperat.url + '" data-title="' + formatOperat.title + '" ' : '';
                }
                //处理operat列的隐藏问题
                // console.log(operat.visible)
                if (operat.visible != 'undefined' && operat.visible == false) {
                    html = '';
                    return html;
                }
                html = '<a ' + formatOperat.class + formatOperat.method + formatOperat.extend + formatOperat.callback_name + formatOperat.data_id + '>' + formatOperat.icon + formatOperat.text + '</a>';
                return html;
            },
            toolSpliceUrl(url, field, data) {
                url = url.indexOf("?") !== -1 ? url + '&' + field + '=' + data[field] : url + '?' + field + '=' + data[field];
                return url;
            },
            formatCols: function (cols, init) {
                for (i in cols) {
                    var col = cols[i];
                    for (index in col) {
                        var val = col[index];

                        // 判断是否包含初始化数据
                        if (val.init === undefined) {
                            cols[i][index]['init'] = init;
                        }

                        // 格式化列操作栏
                        if (val.templet === admin.table.tool && val.operat === undefined) {
                            cols[i][index]['operat'] = ['edit', 'delete'];
                        }

                        // 判断是否包含开关组件
                        if (val.templet === admin.table.switch && val.filter === undefined) {
                            cols[i][index]['filter'] = val.field;
                        }

                        // 判断是否含有搜索下拉列表
                        if (val.selectList !== undefined && val.search === undefined) {
                            cols[i][index]['search'] = 'select';
                        }

                        // 判断是否初始化对齐方式
                        if (val.align === undefined) {
                            cols[i][index]['align'] = 'center';
                        }

                        // 部分字段开启排序
                        var sortDefaultFields = ['id', 'sort'];
                        if (val.sort === undefined && sortDefaultFields.indexOf(val.field) >= 0) {
                            cols[i][index]['sort'] = true;
                        }

                        // 初始化图片高度
                        if (val.templet === admin.table.image && val.imageHeight === undefined) {
                            cols[i][index]['imageHeight'] = 40;
                        }

                        // 判断是否多层对象
                        if (val.field !== undefined && val.field.split(".").length > 1) {
                            if (val.templet === undefined) {
                                cols[i][index]['templet'] = admin.table.value;
                            }
                        }

                        // 判断是否列表数据转换
                        if (val.selectList !== undefined && val.templet === undefined) {
                            cols[i][index]['templet'] = admin.table.list;
                        }

                    }
                }
                return cols;
            },
            tool: function (data, option) {
                option.operat = option.operat || ['edit', 'delete'];
                var elem = option.init.table_elem || init.table_elem;
                var html = '';
                $.each(option.operat, function (i, item) {
                    if (typeof item === 'string') {
                        switch (item) {
                            case 'edit':
                                var operat = {
                                    class: 'layui-btn layui-btn-success layui-btn-xs',
                                    method: 'open',
                                    field: 'id',
                                    icon: '',
                                    text: '编辑',
                                    title: '编辑信息',
                                    auth: 'edit',
                                    url: option.init.edit_url,
                                    extend: ""
                                };
                                operat.url = admin.table.toolSpliceUrl(operat.url, operat.field, data);
                                if (admin.checkAuth(operat.auth, elem)) {
                                    html += admin.table.buildOperatHtml(operat);
                                }
                                break;
                            case 'delete':
                                var operat = {
                                    class: 'layui-btn layui-btn-danger layui-btn-xs',
                                    method: 'get',
                                    field: 'id',
                                    icon: '',
                                    text: '删除',
                                    title: '确定删除？',
                                    auth: 'delete',
                                    url: option.init.delete_url,
                                    extend: ""
                                };
                                operat.url = admin.table.toolSpliceUrl(operat.url, operat.field, data);
                                if (admin.checkAuth(operat.auth, elem)) {
                                    html += admin.table.buildOperatHtml(operat);
                                }
                                break;
                        }

                    } else if (typeof item === 'object') {
                        $.each(item, function (i, operat) {
                            operat.class = operat.class || '';
                            operat.icon = operat.icon || '';
                            operat.auth = operat.auth || '';
                            operat.url = operat.url || '';
                            operat.method = operat.method || 'open';
                            operat.field = operat.field || 'id';
                            operat.title = operat.title || operat.text;
                            operat.text = operat.text || operat.title;
                            operat.extend = operat.extend || '';
                            operat.visible = operat.visible || true;
                            //20201009 jyk增加弹窗高度和宽度
                            operat.width = operat.width || null;
                            operat.height = operat.height || null;
                            //处理operat列的隐藏问题
                            var visibleCallback = operat.visible;
                            if (typeof visibleCallback === 'function') {
                                operat.visible = visibleCallback(data);
                            }

                            // operat.url = admin.table.toolSpliceUrl(operat.url, operat.field, data);
                            //cc 201107 扩展了url属性，使支持function和string类型
                            operat.url = typeof operat.url === 'function' ? (operat.url)(data) : admin.table.toolSpliceUrl(operat.url, operat.field, data);
                            //jiao 20210109 扩展了text属性，使支持function和string类型
                            if(typeof operat.text === 'function'){
                                operat.text = operat.text(data);
                            }
                            if(typeof operat.title === 'function'){
                                operat.title = operat.title(data);
                            }
                            //jiao 20210109 扩展了class属性，使支持function和string类型
                            if(typeof operat.class === 'function'){
                                operat.class = operat.class(data);
                            }
                            if (admin.checkAuth(operat.auth, elem)) {
                                html += admin.table.buildOperatHtml(operat,data);
                            }
                        });
                    }
                });
                return html;
            },
            list: function (data, option) {
                option.selectList = option.selectList || {};
                var field = option.field;
                try {
                    var value = eval("data." + field);
                } catch (e) {
                    var value = undefined;
                }
                if (option.selectList[value] === undefined || option.selectList[value] === '' || option.selectList[value] === null) {
                    return value;
                } else {
                    return option.selectList[value];
                }
            },
            image: function (data, option) {
                option.imageWidth = option.imageWidth || 200;
                option.imageHeight = option.imageHeight || 40;
                option.imageSplit = option.imageSplit || ',';
                option.title = option.title || option.field;
                var field = option.field,
                    title = data[option.title];
                try {
                    var value = eval("data." + field);
                } catch (e) {
                    var value = undefined;
                }
                if (value === undefined || value == '' || value == null || !value) {
                    // return '<img style="max-width: ' + option.imageWidth + 'px; max-height: ' + option.imageHeight + 'px;" src="' + value + '" data-image="' + title + '">';
                    return '<span class="layui-badge layui-bg-gray">暂无图片</span>';
                } else {
                    var values = value.split(option.imageSplit),
                        valuesHtml = [];
                    values.forEach((value, index) => {
                        valuesHtml.push('<img style="max-width: ' + option.imageWidth + 'px; max-height: ' + option.imageHeight + 'px;" src="' + value + '" data-image="' + title + '">');
                    });
                    return valuesHtml.join('');
                }
            },
            url: function (data, option) {
                var field = option.field;
                try {
                    var value = eval("data." + field);
                } catch (e) {
                    var value = undefined;
                }
                return '<a class="layuimini-table-url" href="' + value + '" target="_blank" class="label bg-green">' + value + '</a>';
            },
            date: function (data, option) {
                var field = option.field;
                try {
                    var datetime = eval("data." + field);
                } catch (e) {
                    var datetime = undefined;
                }
                if (datetime === 0 || datetime === '' || datetime === null || typeof datetime === "undefined") return '无';

                var date = new Date(datetime * 1000);//时间戳为10位需*1000，时间戳为13位的话不需乘1000
                var year = date.getFullYear(),
                    month = ("0" + (date.getMonth() + 1)).slice(-2),
                    sdate = ("0" + date.getDate()).slice(-2),
                    hour = ("0" + date.getHours()).slice(-2),
                    minute = ("0" + date.getMinutes()).slice(-2),
                    second = ("0" + date.getSeconds()).slice(-2);
                // 拼接
                var result = year + "-" + month + "-" + sdate + " " + hour + ":" + minute + ":" + second;
                return result;
            },
            switch: function (data, option) {
                var field = option.field;
                option.filter = option.filter || option.field || null;
                option.checked = option.checked || 1;
                option.tips = option.tips || '开|关';
                try {
                    var value = eval("data." + field);
                } catch (e) {
                    var value = undefined;
                }
                var checked = value == option.checked ? 'checked' : '';
                return laytpl('<input type="checkbox" name="' + option.field + '" value="' + data.id + '" lay-skin="switch" lay-text="' + option.tips + '" lay-filter="' + option.filter + '" ' + checked + ' >').render(data);
            },
            price: function (data, option) {
                var field = option.field;
                try {
                    var value = eval("data." + field);
                } catch (e) {
                    var value = undefined;
                }
                return '<span>￥' + value + '</span>';
            },
            percent: function (data, option) {
                var field = option.field;
                try {
                    var value = eval("data." + field);
                } catch (e) {
                    var value = undefined;
                }
                return '<span>' + value + '%</span>';
            },
            icon: function (data, option) {
                var field = option.field;
                try {
                    var value = eval("data." + field);
                } catch (e) {
                    var value = undefined;
                }
                return '<i class="' + value + '"></i>';
            },
            text: function (data, option) {
                var field = option.field;
                try {
                    var value = eval("data." + field);
                } catch (e) {
                    var value = undefined;
                }
                var values = value.split(','),
                    valuesHtml = [];
                values.forEach((value, index) => {
                    if (value == "") valuesHtml.push('');
                    else valuesHtml.push('<span class="layui-badge" style="margin: 0 1px;">' + value + '</span>');


                });
                return valuesHtml.join('');
            },
            value: function (data, option) {
                var field = option.field;
                try {
                    var value = eval("data." + field);
                } catch (e) {
                    var value = undefined;
                }
                return '<span>' + value + '</span>';
            },
            listenTableSearch: function (tableId) {
                admin.tab_select(tableId);//tab切换监听
                form.on('submit(' + tableId + '_filter)', function (data) {
                    admin.table_reload(tableId)
                    return false;
                });
            },
            listenSwitch: function (option, ok) {
                option.filter = option.filter || '';
                option.url = option.url || '';
                option.field = option.field || option.filter || '';
                option.tableId = option.tableId || init.table_render_id;
                option.modifyReload = option.modifyReload || false;
                form.on('switch(' + option.filter + ')', function (obj) {
                    var checked = obj.elem.checked ? 1 : 0;
                    if (typeof ok === 'function') {
                        return ok({
                            id: obj.value,
                            checked: checked,
                        });
                    } else {
                        var data = {
                            id: obj.value,
                            field: option.field,
                            value: checked,
                        };
                        admin.request.post({
                            url: option.url,
                            prefix: true,
                            data: data,
                        }, function (res) {
                            if (option.modifyReload) {
                                admin.table_reload(option.tableId);
                            }
                        }, function (res) {
                            admin.msg.error(res.msg, function () {
                                admin.table_reload(option.tableId);
                            });
                        }, function () {
                            admin.table_reload(option.tableId);
                        });
                    }
                });
            },
            listenToolbar: function (layFilter, tableId) {
                table.on('toolbar(' + layFilter + ')', function (obj) {

                    // 搜索表单的显示
                    switch (obj.event) {
                        case 'TABLE_SEARCH':
                            var searchFieldsetId = 'searchFieldset_' + tableId;
                            var _that = $("#" + searchFieldsetId);
                            if (_that.hasClass("layui-hide")) {
                                _that.removeClass('layui-hide');
                                $(".table-search-total").removeClass('layui-hide');
                            } else {
                                _that.addClass('layui-hide');
                                $(".table-search-total").addClass('layui-hide');
                            }

                            break;
                    }
                });
            },
            listenEdit: function (tableInit, layFilter, tableId, modifyReload) {
                tableInit.modify_url = tableInit.modify_url || false;
                tableId = tableId || init.table_render_id;
                if (tableInit.modify_url !== false) {
                    table.on('edit(' + layFilter + ')', function (obj) {
                        var value = obj.value,
                            data = obj.data,
                            id = data.id,
                            field = obj.field;
                        var _data = {
                            id: id,
                            field: field,
                            value: value,
                        };
                        admin.request.post({
                            url: tableInit.modify_url,
                            prefix: true,
                            data: _data,
                        }, function (res) {
                            if (modifyReload) {
                                //表格接口返回数据回调
                                admin.table_reload(tableId);
                            }
                        }, function (res) {
                            admin.msg.error(res.msg, function () {
                                admin.table_reload(tableId);
                            });
                        }, function () {
                            //表格接口返回数据回调
                            admin.table_reload(tableId);
                        });
                    });
                }
            },
            listenSynchroHeight: function (options) {
                setTimeout(function () {

                    var out_rows = $(".layui-table-main > table > tbody > tr"); //获取最外侧table所有行的jquery对象
                    var in_rows = $(".layui-table-fixed > .layui-table-body > table > tbody > tr"); //获取最内侧table所有行的jquery对象
                    var rowslen = out_rows.length; //最外侧table的总行数
                    var inrowslen = in_rows.length; //最外侧table的总行数
                    //获取最外侧的table高度
                    var out_table = $(".layui-table-main > table"); //获取最外侧table
                    var in_table = $(".layui-table-fixed > .layui-table-body"); //获取最内侧table
                    var out_table_height = out_table.height(); //外侧的高度
                    //设置内侧table高等于外侧table高
                    in_table.height(out_table_height);
                    //循环开始调整每一行的高度
                    for (var i = 0; i < inrowslen; i++) {
                        var out_row = $(out_rows.get(i%rowslen)); //获取外侧每一行的jquery对象
                        var in_row = $(in_rows.get(i)); //获取内侧侧每一行的jquery对象
                        var out_height = out_row.height(); //外侧的高
                        in_row.height(out_height);
                    }

                }, 200 );


            }
        },
        checkMobile: function () {
            var userAgentInfo = navigator.userAgent;
            var mobileAgents = ["Android", "iPhone", "SymbianOS", "Windows Phone", "iPad", "iPod"];
            var mobile_flag = false;
            //根据userAgent判断是否是手机
            for (var v = 0; v < mobileAgents.length; v++) {
                if (userAgentInfo.indexOf(mobileAgents[v]) > 0) {
                    mobile_flag = true;
                    break;
                }
            }
            var screen_width = window.screen.width;
            var screen_height = window.screen.height;
            //根据屏幕分辨率判断是否是手机
            if (screen_width < 600 && screen_height < 800) {
                mobile_flag = true;
            }
            return mobile_flag;
        },
        open: function (title, url, width, height, isResize, successCallback, cancelCallback) {
            isResize = isResize === undefined ? true : isResize;
            var index = layer.open({
                title: title,
                type: 2,
                area: [width, height],
                content: url,
                maxmin: true,
                moveOut: true,
                success: function (layero, index) {
                    if ('function' == typeof successCallback) successCallback(layero, index);

                    var body = layer.getChildFrame('body', index);
                    if (body.length > 0) {
                        $.each(body, function (i, v) {

                            // todo 优化弹出层背景色修改
                            $(v).before('<style>\n' +
                                'html, body {\n' +
                                '    background: #ffffff;\n' +
                                '}\n' +
                                '</style>');
                        });
                    }
                },
                cancel: function () {
                    // 你点击右上角 X 取消后要做什么
                    if ('function' == typeof cancelCallback) cancelCallback();
                }
            });
            if (admin.checkMobile() || width === undefined || height === undefined) {
                layer.full(index);
            }
            if (isResize) {
                $(window).on("resize", function () {
                    layer.full(index);
                })
            }
            return index;
        },
        listen: function (preposeCallback, ok, no, ex, config) {

            // 监听表单是否为必填项 lay-verify ="required" lay-reqtext="提示语" 默认取placeholder值
            admin.api.formRequired();

            // 监听表单提交事件
            admin.api.formSubmit(preposeCallback, ok, no, ex);

            // 初始化图片显示以及监听上传事件
            admin.api.upload(config);

            // 监听富文本初始化
            admin.api.editor();

            // 监听下拉选择生成
            admin.api.select();

            // 监听多选checkbox格式化为,拼接
            admin.api.checkbox();

            // 监听表单switch 赋值0和1
            admin.api.formSwitch();

            // 监听时间控件生成
            admin.api.date();

            // 初始化layui表单渲染
            form.render();

            // 表格修改提示监听
            $("body").on("mouseenter", ".table-edit-tips", function () {
                var openTips = layer.tips('点击行内容可以进行修改', $(this), {tips: [2, '#e74c3c'], time: 4000});
            });

            // 监听弹出层的打开data-open
            $('body').on('click', '[data-open]', function () {

                var clienWidth = $(this).attr('data-width'),
                    clientHeight = $(this).attr('data-height'),
                    dataFull = $(this).attr('data-full'),
                    checkbox = $(this).attr('data-checkbox'),
                    url = $(this).attr('data-open'),
                    tableId = $(this).attr('data-table');
                if (checkbox === 'true') {
                    tableId = tableId || init.table_render_id;
                    var checkStatus = table.checkStatus(tableId),
                        data = checkStatus.data;
                    if (data.length <= 0) {
                        admin.msg.error('请勾选需要操作的数据!');
                        return false;
                    }
                    var ids = [];
                    $.each(data, function (i, v) {
                        ids.push(v.id);
                    });
                    if (url.indexOf("?") === -1) {
                        url += '?id=' + ids.join(',');
                    } else {
                        url += '&id=' + ids.join(',');
                    }
                }

                if (clienWidth === undefined || clientHeight === undefined) {
                    var width = document.body.clientWidth,
                        height = document.body.clientHeight;
                    if (width >= 800 && height >= 600) {
                        clienWidth = '800px';
                        clientHeight = '600px';
                    } else {
                        clienWidth = '100%';
                        clientHeight = '100%';
                    }
                }
                if (dataFull === 'true') {
                    clienWidth = '100%';
                    clientHeight = '100%';
                }

                admin.open(
                    $(this).attr('data-title'),
                    admin.url(url),
                    clienWidth,
                    clientHeight,
                );
            });

            // 放大图片
            $('body').on('click', '[data-image]', function () {
                var title = $(this).attr('data-image'),
                    src = $(this).attr('src'),
                    alt = $(this).attr('alt');
                var photos = {
                    "title": title,
                    "id": Math.random(),
                    "data": [
                        {
                            "alt": alt,
                            "pid": Math.random(),
                            "src": src,
                            "thumb": src
                        }
                    ]
                };
                layer.photos({
                    photos: photos,
                    anim: 5
                });
                return false;
            });


            // 监听动态表格刷新
            $('body').on('click', '[data-table-refresh]', function () {
                var tableId = $(this).attr('data-table-refresh');
                if (tableId === undefined || tableId === '' || tableId == null) {
                    tableId = init.table_render_id;
                }
                //表格接口返回数据回调
                admin.table_reload(tableId);
                // table.reload(tableId);
            });

            // 监听搜索表格重置
            $('body').on('click', '[data-table-reset]', function () {
                var tableId = $(this).attr('data-table-reset');
                if (tableId === undefined || tableId === '' || tableId == null) {
                    tableId = init.table_render_id;
                }
                try {
                    if (callback_init.searchBeforeFunction) {
                        if (typeof callback_init.searchBeforeFunction === "function") { //是函数    其中 FunName 为函数名称
                            callback_init.searchBeforeFunction();
                        }
                    }
                } catch (e) {
                }
                table.reload(tableId, {
                    page: {
                        curr: 1
                    }
                    , where: {
                        filter: '{}',
                        op: '{}'
                    }
                }, 'data');
                // admin.table_reset(tableId);
            });

            // 监听请求
            $('body').on('click', '[data-request]', function () {
                var title = $(this).attr('data-title'),
                    url = $(this).attr('data-request'),
                    tableId = $(this).attr('data-table'),
                    addons = $(this).attr('data-addons'),
                    checkbox = $(this).attr('data-checkbox');
                var callback = $(this).attr('data-callback');
                var id = $(this).attr('data-id');
                var postData = {};
                if (checkbox === 'true') {
                    tableId = tableId || init.table_render_id;
                    var checkStatus = table.checkStatus(tableId),
                        data = checkStatus.data;
                    if (data.length <= 0) {
                        admin.msg.error('请勾选需要操作的数据!');
                        return false;
                    }
                    var ids = [];
                    $.each(data, function (i, v) {
                        ids.push(v.id);
                    });
                    postData.id = ids;
                }

                if (addons !== true && addons !== 'true') {
                    url = admin.url(url);
                }
                title = title || '确定进行该操作？';
                tableId = tableId || init.table_render_id;
                var that = $(this);
                admin.msg.confirm(title, function () {
                    try {
                        if (callback) {
                            // console.log(callback_init.requestButtonFunction);
                            if (typeof callback_init.requestButtonFunction[callback] === "function") { //是函数    其中 FunName 为函数名称
                                var requestButtonFunction = callback_init.requestButtonFunction[callback];
                                var row_data = null;
                                if(id)row_data = callback_init.rowData[id] ;
                                requestButtonFunction(url, postData,row_data);
                            }
                        }
                    } catch (e) {
                    }

                    admin.request.post({
                        url: url,
                        data: postData,
                    }, function (res) {
                        admin.msg.success(res.msg, function () {

                            admin.table_reload(tableId);
                        });
                    })
                });
                return false;
            });


            // 监听请求
            $('body').on('click', '[data-click]', function () {
                var title = $(this).attr('data-title'),
                    url = $(this).attr('data-request'),
                    tableId = $(this).attr('data-table'),
                    addons = $(this).attr('data-addons'),
                    checkbox = $(this).attr('data-checkbox');
                var callback = $(this).attr('data-callback');
                var id = $(this).attr('data-id');
                var postData = {};
                if (checkbox === 'true') {
                    tableId = tableId || init.table_render_id;
                    var checkStatus = table.checkStatus(tableId),
                        data = checkStatus.data;
                    if (data.length <= 0) {
                        admin.msg.error('请勾选需要操作的数据!');
                        return false;
                    }
                    var ids = [];
                    $.each(data, function (i, v) {
                        ids.push(v.id);
                    });
                    postData.id = ids;
                }
                if (addons !== true && addons !== 'true') {
                    url = admin.url(url);
                }
                try {
                        if (callback) {
                            // console.log(callback_init.requestButtonFunction);
                            if (typeof callback_init.requestButtonFunction[callback] === "function") { //是函数    其中 FunName 为函数名称
                                var requestButtonFunction = callback_init.requestButtonFunction[callback];
                                var row_data = null;
                                if(id)row_data = callback_init.rowData[id] ;
                                requestButtonFunction(url, postData,row_data);
                            }
                        }
                 } catch (e) {
                 }
                return false;
            });


            // excel导出
            $('body').on('click', '[data-table-export]', function () {
                var tableId = $(this).attr('data-table-export'),
                    url = $(this).attr('data-url');
                var index = admin.msg.confirm('根据查询进行导出，确定导出？', function () {
                    window.location = admin.url(url);
                    layer.close(index);
                });
            });

            // 数据表格多删除
            $('body').on('click', '[data-table-delete]', function () {
                var tableId = $(this).attr('data-table-delete'),
                    url = $(this).attr('data-url');
                tableId = tableId || init.table_render_id;
                url = url !== undefined ? admin.url(url) : window.location.href;
                var checkStatus = table.checkStatus(tableId),
                    data = checkStatus.data;
                if (data.length <= 0) {
                    admin.msg.error('请勾选需要删除的数据!');
                    return false;
                }
                var ids = [];
                $.each(data, function (i, v) {
                    ids.push(v.id);
                });
                admin.msg.confirm('确定删除？', function () {
                    admin.request.post({
                        url: url,
                        data: {
                            id: ids
                        },
                    }, function (res) {
                        admin.msg.success(res.msg, function () {
                            admin.table_reload(tableId);
                        });
                    });
                });
                return false;
            });

        },//TODO listen end
        api: {
            form: function (url, data, ok, no, ex, refreshTable) {
                if (refreshTable === undefined) {
                    refreshTable = true;
                }
                ok = ok || function (res) {
                    res.msg = res.msg || '';
                    admin.msg.success(res.msg, function () {
                        admin.api.closeCurrentOpen({
                            refreshTable: refreshTable
                        });
                    });
                    return false;
                };
                admin.request.post({
                    url: url,
                    data: data,
                }, ok, no, ex);
                return false;
            },
            closeCurrentOpen: function (option) {
                option = option || {};
                option.refreshTable = option.refreshTable || false;
                option.refreshFrame = option.refreshFrame || false;
                if (option.refreshTable === true) {
                    option.refreshTable = init.table_render_id;
                }
                var index = parent.layer.getFrameIndex(window.name);
                parent.layer.close(index);
                if (option.refreshTable !== false) {
                    parent.layui.table.reload(option.refreshTable);
                }
                if (option.refreshFrame) {
                    parent.location.reload();
                }
                return false;
            },
            refreshFrame: function () {
                parent.location.reload();
                return false;
            },
            refreshTable: function (tableName) {
                tableName = tableName || 'currentTable';
                table.reload(tableName);
            },
            formRequired: function () {
                var verifyList = document.querySelectorAll("[lay-verify]");
                if (verifyList.length > 0) {
                    $.each(verifyList, function (i, v) {
                        var verify = $(this).attr('lay-verify');

                        // todo 必填项处理
                        if (verify === 'required') {
                            var label = $(this).parent().prev();
                            if (label.is('label') && !label.hasClass('required')) {
                                label.addClass('required');
                            }
                            if ($(this).attr('lay-reqtext') === undefined && $(this).attr('placeholder') !== undefined) {
                                $(this).attr('lay-reqtext', $(this).attr('placeholder'));
                            }
                            if ($(this).attr('placeholder') === undefined && $(this).attr('lay-reqtext') !== undefined) {
                                $(this).attr('placeholder', $(this).attr('lay-reqtext'));
                            }
                        }

                    });
                }
            },
            formSubmit: function (preposeCallback, ok, no, ex) {
                var formList = document.querySelectorAll("[lay-submit]");

                // 表单提交自动处理
                if (formList.length > 0) {
                    $.each(formList, function (i, v) {
                        var filter = $(this).attr('lay-filter'),
                            type = $(this).attr('data-type'),
                            refresh = $(this).attr('data-refresh'),
                            url = $(this).attr('lay-submit');
                        // 表格搜索不做自动提交
                        if (type === 'tableSearch') {
                            return false;
                        }
                        // 判断是否需要刷新表格
                        if (refresh === 'false') {
                            refresh = false;
                        } else {
                            refresh = true;
                        }
                        // 自动添加layui事件过滤器
                        if (filter === undefined || filter === '') {
                            filter = 'save_form_' + (i + 1);
                            $(this).attr('lay-filter', filter)
                        }
                        if (url === undefined || url === '' || url === null) {
                            url = window.location.href;
                        } else {
                            url = admin.url(url);
                        }
                        form.on('submit(' + filter + ')', function (data) {
                            var dataField = data.field;////当前容器的全部表单字段，名值对形式：{name: value}
                            // 富文本数据处理
                            var editorList = document.querySelectorAll(".editor");
                            if (editorList.length > 0) {
                                $.each(editorList, function (i, v) {
                                    var name = $(this).attr("name");
                                    dataField[name] = CKEDITOR.instances[name].getData();
                                });
                            }
                            if (typeof preposeCallback === 'function') {
                                dataField = preposeCallback(dataField);
                            }
                            admin.api.form(url, dataField, ok, no, ex, refresh);

                            return false;
                        });
                    });
                }

            },
            upload: function (config) {
                var uploadList = document.querySelectorAll("[data-upload]");
                var uploadSelectList = document.querySelectorAll("[data-upload-select]");

                if (uploadList.length > 0) {
                    $.each(uploadList, function (i, v) {
                        var exts = $(this).attr('data-upload-exts'),
                            uploadName = $(this).attr('data-upload'),//上传input组件名字name保持一致
                            uploadNumber = $(this).attr('data-upload-number'),
                            uploadSign = $(this).attr('data-upload-sign');//可以自定义分隔符 默认为|
                        exts = exts || init.upload_exts;
                        uploadNumber = uploadNumber || 'one';
                        uploadSign = uploadSign || ',';
                        var elem = "input[name='" + uploadName + "']",
                            uploadElem = this;
                        // 监听上传事件
                        upload.render({
                            elem: this,
                            url: init.upload_url,
                            accept: 'file',
                            exts: exts,
                            done: function (res) {
                                if (res.code === 200 && res.data.errcode === 0) {
                                    var url = res.data.data.src;
                                    if (uploadNumber !== 'one') {
                                        var oldUrl = $(elem).val();
                                        if (oldUrl !== '') {
                                            url = oldUrl + uploadSign + url;
                                        }
                                    }
                                    admin.msg.success(res.msg, function () {
                                        $(elem).val(url);
                                        $(elem).trigger("input");
                                        if(config && config.picCallback)config.picCallback(elem,url);
                                    });
                                } else {
                                    admin.msg.error(res.msg);
                                }
                                return false;
                            }
                        });

                        // 监听上传input值变化
                        $(elem).bind("input propertychange", function (event) {
                            var urlString = $(this).val(),
                                urlArray = urlString.split(uploadSign),
                                uploadMime = $('#select_' + $(uploadElem).attr('data-upload')).attr('data-upload-mimetype'),//默认为image/*
                                uploadIcon = $(uploadElem).attr('data-upload-icon'),//默认为file
                                uploadIcon = uploadIcon || "file";
                            $('#bing-' + uploadName).remove();
                            //保证是图片才会渲染出来DIV
                            if (urlString.length > 0 && uploadMime && (uploadMime.indexOf("image") != -1)) {
                                var parant = $(this).parent('div');
                                var liHtml = '';
                                $.each(urlArray, function (i, v) {
                                    liHtml += '<li><a><img src="' + v + '" data-image  onerror="this.src=\'' + BASE_URL + 'admin/images/upload-icons/' + uploadIcon + '.png\';this.onerror=null"></a><small style="cursor: pointer;" class="uploads-delete-tip bg-red badge" data-upload-delete="' + uploadName + '" data-upload-url="' + v + '" data-upload-sign="' + uploadSign + '">×</small></li>\n';
                                });
                                parant.after('<ul id="bing-' + uploadName + '" class="layui-input-block layuimini-upload-show">\n' + liHtml + '</ul>');
                            }

                        });

                        // 非空初始化图片显示
                        if ($(elem).val() !== '') {
                            $(elem).trigger("input");
                            //触发一下该input的input事件
                            //$(elem)[0].dispatchEvent(new Event('input'));
                        }
                    });

                    // 监听上传文件的删除角标的单击事件
                    $('body').on('click', '[data-upload-delete]', function () {
                        var uploadName = $(this).attr('data-upload-delete'),
                            deleteUrl = $(this).attr('data-upload-url'),
                            sign = $(this).attr('data-upload-sign');
                        var confirm = admin.msg.confirm('确定删除？', function () {
                            var elem = "input[name='" + uploadName + "']";
                            var currentUrl = $(elem).val();
                            var url = '';
                            if (currentUrl !== deleteUrl) {
                                url = currentUrl.replace(sign + deleteUrl, '');
                                $(elem).val(url);
                                $(elem).trigger("input");
                                if(config && config.picCallback)config.picCallback(elem,url);
                            } else {
                                $(elem).val(url);
                                $('#bing-' + uploadName).remove();
                                if(config && config.picCallback)config.picCallback(elem,url);
                            }
                            admin.msg.close(confirm);
                        });
                        return false;
                    });
                }

                if (uploadSelectList.length > 0) {
                    $.each(uploadSelectList, function (i, v) {
                        var exts = $(this).attr('data-upload-exts'),
                            uploadName = $(this).attr('data-upload-select'),
                            uploadNumber = $(this).attr('data-upload-number'),
                            uploadSign = $(this).attr('data-upload-sign');
                        exts = exts || init.upload_exts;
                        uploadNumber = uploadNumber || 'one';
                        uploadSign = uploadSign || ',';
                        var selectCheck = uploadNumber === 'one' ? 'radio' : 'checkbox';
                        var elem = "input[name='" + uploadName + "']",
                            uploadElem = $(this).attr('id');

                        tableSelect.render({
                            elem: "#" + uploadElem,
                            checkedKey: 'id',
                            searchType: 'more',
                            searchList: [
                                {searchKey: 'title', searchPlaceholder: '请输入文件名'},
                            ],
                            table: {
                                url: init.get_upload_files,
                                cols: [[
                                    {type: selectCheck},
                                    {field: 'id', title: 'ID'},
                                    {field: 'url', minWidth: 80, search: false, title: '图片信息', imageHeight: 40, align: "center", templet: admin.table.image},
                                    {field: 'original_name', width: 150, title: '文件原名', align: "center"},
                                    {field: 'mime_type', width: 120, title: 'mime类型', align: "center"},
                                    {field: 'create_time', width: 200, title: '创建时间', align: "center", search: 'range'},
                                ]]
                            },
                            done: function (e, data) {
                                var urlArray = [];
                                $.each(data.data, function (index, val) {
                                    urlArray.push(val.url)
                                });
                                var url = urlArray.join(uploadSign);
                                admin.msg.success('选择成功', function () {
                                    $(elem).val(url);
                                    $(elem).trigger("input");
                                    //触发一下该input的input事件
                                    $(elem)[0].dispatchEvent(new Event('input'));
                                    if(config && config.picCallback)config.picCallback(elem,url);
                                });
                            }
                        })

                    });

                }
            },
            editor: function () {
                var editorList = document.querySelectorAll(".editor");
                if (editorList.length > 0) {
                    $.each(editorList, function (i, v) {
                        CKEDITOR.replace(
                            $(this).attr("name"),
                            {
                                height: $(this).height(),
                                filebrowserImageUploadUrl: init.upload_editor,
                            });
                    });
                }
            },
            select: function () {
                var selectList = document.querySelectorAll("[data-select]");
                $.each(selectList, function (i, v) {
                    var url = $(this).attr('data-select'),//懒加载获取select数据的Url
                        selectFields = $(this).attr('data-fields'),//url传入的过滤参数
                        value = $(this).attr('data-value'),//传入默认选中的值
                        that = this,
                        html = '<option value=""></option>';
                    var fields = selectFields.replace(/\s/g, "").split(',');
                    if (fields.length !== 2) {
                        return admin.msg.error('错误：下拉选择字段至少2个');
                    }
                    admin.request.get(
                        {
                            url: url,
                            data: {
                                selectFieds: selectFields
                            },
                        }, function (res) {
                            // var list = res.data;
                            var list = res.data.data;
                            list.forEach(val => {
                                var key = val[fields[0]];
                                if (value !== undefined && key.toString() === value) {
                                    html += '<option value="' + key + '" selected="">' + val[fields[1]] + '</option>';
                                } else {
                                    html += '<option value="' + key + '">' + val[fields[1]] + '</option>';
                                }
                            });
                            $(that).html(html);
                            form.render();
                        }
                    );
                });
            },
            checkbox: function () {
                var checkboxList = document.querySelectorAll(".formatCheckbox");
                // 表单提交自动处理
                if (checkboxList.length > 0) {
                    $.each(checkboxList, function (i, v) {
                        var filter = $(this).attr('name');
                        //赋值
                        var param = $('input[name="' + filter + '"]'), checkBoxs = $('input:checkbox:checked'), paramstr = [];
                        $.each(checkBoxs, function (k, m) {
                            if (filter == $(this).attr('lay-filter')) {
                                paramstr.push($(this).val());
                            }
                        })
                        param.val(paramstr.join(","));
                        //赋值
                        form.on('checkbox(' + filter + ')', function (data) {
                            var obj = $('input[name="' + filter + '"]'), value = data.value, array = obj.val().split(",");
                            if (data.elem.checked) {
                                obj.val(obj.val() != "" ? obj.val() + "," + value : value);
                            } else {
                                var newstr = [];
                                for (var i = 0; i < array.length; i++) {
                                    var str = array[i];
                                    if (str != value && str != "" && str != null) {
                                        newstr.push(str);
                                    }
                                    //newstr += (str != value && str != "" && str != null) ? str + "," : "";
                                }
                                obj.val(newstr.join(","));
                            }
                            return false;
                        });
                    });
                }
            },
            formSwitch: function () {
                var switchList = document.querySelectorAll("[lay-skin]");
                // 表单提交自动处理
                if (switchList.length > 0) {
                    $.each(switchList, function (i, v) {

                        if ($(this).attr("lay-skin") == 'switch') {
                            var filter = $(this).attr('lay-filter');
                            form.on('switch(' + filter + ')', function (data) {
                                //console.log(data.elem.checked)
                                var checked = data.elem.checked ? 1 : 0;
                                var obj = $("#" + filter);
                                obj.val(checked);
                                return false;
                            });
                        }
                    });
                }
            },
            date: function () {
                var dateList = document.querySelectorAll("[data-date]");
                if (dateList.length > 0) {
                    $.each(dateList, function (i, v) {
                        var format = $(this).attr('data-date'),
                            type = $(this).attr('data-date-type'),
                            range = $(this).attr('data-date-range');
                        if (type === undefined || type === '' || type === null) {
                            type = 'datetime';
                        }
                        var options = {
                            elem: this,
                            type: type,
                        };
                        if (format !== undefined && format !== '' && format !== null) {
                            options['format'] = format;
                        }
                        if (range !== undefined) {
                            if (range === null || range === '') {
                                range = '-';
                            }
                            options['range'] = range;
                        }
                        //附加参数 解决闪退问题
                        options['trigger'] = 'click';
                        laydate.render(options);
                    });
                }
            },
        },
        tab_select: function (tableId) {
            if (init.tab_filter) {
                for (j = 0, len = init.tab_filter.length; j < len; j++) {
                    //一些事件监听
                    element.on("tab(" + init.tab_filter[j] + ")", function (data) {
                        admin.table_reload(tableId, data)
                    });
                }
            }
        },
        table_reload: function (tableId, data) {
            if (!tableId) tableId = init.table_render_id
            //获取表单区域所有值
            var dataField = form.val('select_tab_' + tableId);
            var formatFilter = {},
                formatOp = {};
            $.each(dataField, function (key, val) {
                if (val !== '') {
                    //处理范围搜索 start
                    if (key.search("_min") != -1) {
                        var wz = key.indexOf('_min');
                        //20210629 JYK 范围搜索bug修复
                        if(key.length - wz ==4){
                            var key_pre = key.substring(0, wz);
                            var keyMax = key_pre + '_max';
                            formatFilter[key_pre] = val + ' - ' + dataField[keyMax];
                            delete dataField[key];
                            delete dataField[keyMax];
                            //20210630 修复 带点符号的id取不到的错误
                            var op =  $("[id='c-"+key+"']").attr('data-search-op');
                            op = op || '%*%';
                            formatOp[key_pre] = op;
                            return true;
                        }
                    }
                    //处理范围搜索 end
                    formatFilter[key] = val;
                    //20210630 修复 带点符号的id取不到的错误
                    var op =  $("[id='c-"+key+"']").attr('data-search-op');
                    op = op || '%*%';
                    formatOp[key] = op;
                }
            });
            //表格接口返回数据回调
            try {
                if (callback_init.searchBeforeFunction) {
                    if (typeof callback_init.searchBeforeFunction === "function") { //是函数    其中 FunName 为函数名称
                        callback_init.searchBeforeFunction(formatFilter, formatOp, data);
                    }
                }
            } catch (e) {
            }

            table.reload(tableId, {
                page: {
                    curr: 1
                }
                , where: {
                    filter: JSON.stringify(formatFilter),
                    op: JSON.stringify(formatOp)
                }
            }, 'data');


        },
        table_reset: function (tableId, data) {
            if (!tableId) tableId = init.table_render_id
            var formatFilter, formatOp = {};
            //表格接口返回数据回调
            try {
                if (callback_init.searchBeforeFunction) {
                    if (typeof callback_init.searchBeforeFunction === "function") { //是函数    其中 FunName 为函数名称
                        callback_init.searchBeforeFunction(formatFilter, formatOp, data);
                    }
                }
            } catch (e) {
            }
            table.reload(tableId, {
                where: {
                    filter: JSON.stringify(formatFilter),
                    op: JSON.stringify(formatOp)
                }
            }, 'data');
        }

    };

    return admin;
});
