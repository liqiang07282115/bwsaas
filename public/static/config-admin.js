var BASE_URL = document.scripts[document.scripts.length - 1].src.substring(0, document.scripts[document.scripts.length - 1].src.lastIndexOf("/") + 1);
window.BASE_URL = BASE_URL; //取加载页面所在的dom的第一个js的路径的基础地址
require.config({
    urlArgs: "v=" + CONFIG.VERSION,//给每个生成的依赖路径加参数
    baseUrl: BASE_URL,   //依赖基地址
    shim:{
        'QRCode':{
            exports:'QRCode',    //hello 与b中暴露的全局变量一致
            deps:[]
        },
        'angular': {exports: 'angular'},
    },
    paths: {    //配置一些依赖路径别名，用于简写依赖url
        "jquery": ["plugs/jquery-3.4.1/jquery-3.4.1.min"],
        "jquery-particleground": ["plugs/jq-module/jquery.particleground.min"],
        "echarts": ["plugs/echarts/echarts.min"],
        "echarts-theme": ["plugs/echarts/echarts-theme"],
        "easy-admin": ["plugs/easy-admin/easy-admin"],
        "layuiall": ["plugs/layui-v2.5.6/layui.all"],
        "layui": ["plugs/layui-v2.5.6/layui"],
        "miniAdmin": ["plugs/lay-module/layuimini/miniAdmin"],
        "miniMenu": ["plugs/lay-module/layuimini/miniMenu"],
        "miniTab": ["plugs/lay-module/layuimini/miniTab"],
        "miniTheme": ["plugs/lay-module/layuimini/miniTheme"],
        "miniTongji": ["plugs/lay-module/layuimini/miniTongji"],
        "treetable": ["plugs/lay-module/treetable-lay/treetable"],
        "tableSelect": ["plugs/lay-module/tableSelect/tableSelect"],
        "iconPickerFa": ["plugs/lay-module/iconPicker/iconPickerFa"],
        "autocomplete": ["plugs/lay-module/autocomplete/autocomplete"],
        "vue": ["plugs/vue-2.6.10/vue.min"],
        "ckeditor": ["plugs/ckeditor4/ckeditor"],
        'angular': ['plugs/angular/angular.min'],
        "region": ["plugs/lay-module/region-check-box/region"],
        "QRCode": ["plugs/qrcode/qrcode.min"],
        'jquery.masonry': ['plugs/jquery/masonry.min'],
        'tag': ['plugs/lay-module/lih-tag/modules/tag'],
        'layarea': ['plugs/lay-module/layarea/layarea'],
        "mapCheck": ["plugs/lay-module/baidu-map-check/map"],//百度地图定位
        "step": ["plugs/lay-module/step-lay/step"],//分布表单
        "amapCheck": ["plugs/lay-module/gaode-map-check/map"],//高德地图定位
    }
});

// 路径配置信息
var PATH_CONFIG = {
    iconLess: BASE_URL + "plugs/font-awesome-4.7.0/less/variables.less",
};
window.PATH_CONFIG = PATH_CONFIG;

// 初始化控制器对应的JS自动加载（加载生成crud对应的jsmo模块）
if ("undefined" != typeof CONFIG.AUTOLOAD_JS && CONFIG.AUTOLOAD_JS) {
    require([BASE_URL + CONFIG.CONTROLLER_JS_PATH], function (Controller) {
        //得到当前路由要访问的控制器方法，如果该方法在js中存在对应方法，就执行他
        if (eval('Controller.' + CONFIG.ACTION)) {
            eval('Controller.' + CONFIG.ACTION + '()');
        }
    });
}

