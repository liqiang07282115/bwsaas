define(["jquery", "easy-admin"], function ($, ea) {

    var init = {
        detail_url: '/manage/member/plugin/detail',
        buy_url: 'member/plugin/buy',
    };
    var element=layui.element;
    var Controller = {
        index: function () {
            //监听块被点击，跳转详情
            $(".go-detail").click(function(){
                ea.open('详情', init.detail_url + "?id=" + $(this).attr("data-plugin-id"));
            })

            ea.listen();
        },
        detail: function () {
            element.init();
            //监听购买按钮被点击
            $(".plugin-buy").click(function(){
                var plugin_id = $(this).attr("data-plugin-id");
                var content = "<input id='paypwd' type=\"password\" placeholder=\"请输入6位数字支付密码\" class=\"layui-input\">";

                layer.open({
                    title: '确认购买',
                    type: 1,
                    area: ['300px', '180px'],
                    closeBtn: 0,
                    content: content
                    , shadeClose: true
                    , btn: ['支付']
                    , btnAlign: 'c' //按钮居中
                    , yes: function (index, layero) {
                        var paypwd = $('#paypwd').val();
                        if (!paypwd) {
                            layer.msg('请填写支付密码', {icon: 2, time: 1000});
                            return;
                        }

                        var data = {
                            id: plugin_id,
                            paypwd: paypwd,
                        };
                        ea.request.post({
                            url: init.buy_url,
                            prefix: true,
                            data: data,
                        }, function (res) {
                            ea.msg.success(res.msg, function () {
                                // location.reload();//功能购买成功后刷新应用详情页面
                                // parent.location.reload();//功能购买成功后刷新父页面
                                window.location.href = '/manage/member/plugin/core/index';
                            });
                        }, function (res) {
                            ea.msg.error(res.msg, function () {
                                layer.close(index);
                            });
                        });
                    }
                });
            })

            ea.listen();
        },
    };
    return Controller;
});