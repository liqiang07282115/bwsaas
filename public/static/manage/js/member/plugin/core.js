define(["jquery", "easy-admin"], function ($, ea) {

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'member/plugin/core/index',
        menu_url: 'member/plugin/core/menu',
    };
    var laytpl = layui.laytpl;
    var dropdown = layui.dropdown
        , util = layui.util;
    var element=layui.element;
    var Controller = {
        index: function () {
            plugin.init();
            element.init();
            ea.listen();
        },
        menu: function () {
            element.init();
            ea.listen();
        }
    };

    //操作对象
    var plugin = {
        data: {
            plugin_id: 0,
            buy:true,
            list:[],
            url:'',
        },
        init: function () {
            //加载列表
            this.setPluginList();
            this.detail_listen();
        },
        setPluginList: function () {
            var _this = this;
            ea.request.post({
                url: '/' + CONFIG.ADMIN + '/' + init.index_url + '?app=' + app,
            }, function (res) {
                _this.data.list = res.data.data.list;
                _this.loadList(res.data.data, laytpl);//插件列表渲染
                _this.menuListen();//菜单事件
                _this.websiteListen();
            }, function (res) {
                ea.msg.error(res.msg); //失败
            }, function (that) {
                ea.msg.error('查询失败');//异常
            });
        },
        loadList: function (data, laytpl) {
            //渲染插件
            var getTpl = plugin_template.innerHTML
                , view = document.getElementById('plugin_list');
            laytpl(getTpl).render(data.list, function (html) {
                view.innerHTML = html;
            });
        },
        menuListen: function () {
            var _this = this;
            $(".manage").click(function (event) {
                _this.data.plugin_id = $(this).attr("data-plugin-id");
                _this.data.buy = $(this).attr("data-buy");
                console.log(_this.data.buy);
                _this.checkPlugin(); //检测菜单渲染页面
                event.stopPropagation();
                return false;
            });
        },
        websiteListen: function () {
            var _this = this;
            //插件首页跳转
            $(".sudpku-top").click(function (event) {
                _this.data.plugin_id = $(this).attr("data-plugin-id");
                _this.data.url = $(this).attr("data-url");
                if(_this.data.url.indexOf("member.plugin.Core") != -1||_this.data.url.indexOf("member.plugin.core") != -1){
                    ea.msg.error('插件暂无前台页面');//异常
                }else{
                    window.open(_this.data.url);
                }
                event.stopPropagation();
                return false;
            });
        },
        checkPlugin: function () {
            var _this = this;
            //如果未购买跳转购买页
             if(_this.data.buy=='false'){
                 this.jump_detail();
                 return ;
             }
            //检测插件是否存在菜单
            ea.request.get({
                url: '/' + CONFIG.ADMIN + '/' + init.menu_url + '?addons=' + this.data.plugin_id + '&app=' + app,
            }, function (res) {
                _this.jumpMenu();
            }, function (res) {
                ea.msg.error(res.msg); //失败
            }, function (that) {
                ea.msg.error('查询失败');//异常
            });
        }
        ,
        jumpMenu: function () {
            //页面iframe跳转
            var url = '/' + CONFIG.ADMIN + '/' + init.menu_url + '?addons=' + this.data.plugin_id + '&jump=1&app=' + app;
            console.log(url);
            //菜单跳转
            window.location.href = url;
        },
        detail_listen:function () { //插件详情
             var _this = this;
            $('body').on("click",".sudokus-bottom",function(event){
                _this.data.plugin_id = $(this).attr("data-plugin-id");
                _this.open_desc();
                return false;
            });
        }
        ,jump_detail:function () {
            window.location.href = '/manage/member/plugin/detail?id='+this.data.plugin_id;
        }
        ,open_desc:function () {
            var plugin = null;
            for(var i=0;i<this.data.list.length;i++){
                if(this.data.list[i].id == this.data.plugin_id) plugin = this.data.list[i];
            }
            //渲染插件
            var getTpl = plugin_desc.innerHTML;
            laytpl(getTpl).render(plugin, function (html) {
                layer.msg(html, {
                    time: 0 //不自动关闭
                    ,area: ['671px', '420px'] //宽高
                    ,btn: ['关闭']
                    ,yes: function(index){
                        layer.close(index);

                    }
                });
            });

        }
    };


    return Controller;
});