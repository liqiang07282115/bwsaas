define(["easy-admin", "QRCode", "miniAdmin", "echarts"], function (ea, QRCode, miniAdmin, echarts) {
    easy_admin = ea;

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'member/message/list',
        message_read_url: 'member/message/read',
    };

    var Controller = {
        index: function () {
            //获取访问应用目录名
            var miniapp_dir = ea.common.getCookie('miniapp_dir') ? ea.common.getCookie('miniapp_dir') : '';
            ea.common.delCookie('miniapp_dir');
            var options = {
                iniUrl: ea.url('member/menu?miniapp_dir=') + miniapp_dir,    // 初始化接口
                clearUrl: ea.url("index/clear"), // 缓存清理接口
                urlHashLocation: true,      // 是否打开hash定位
                bgColorDefault: 1,      // 主题默认配置
                multiModule: true,          // 是否开启多模块
                menuChildOpen: false,       // 是否默认展开菜单
                loadingTime: 0,             // 初始化加载时间
                pageAnim: true,             // iframe窗口动画
                maxTabNum: 20,              // 最大的tab打开数量
            };
            miniAdmin.render(options);

            $('.login-out').on("click", function () {
                ea.request.post({
                    url: ea.url('member/logout'),
                    prefix: true,
                }, function (res) {
                    if(res.data.errcode == 0){
                        ea.msg.success(res.msg, function () {
                            window.location = ea.url('member/login');
                        })
                    }else{
                        ea.msg.error(res.data.msg);
                    }
                });
            });
            //打开站内信弹窗
            $('.message').on("click", function () {
                layui.use(['layer'], function () {
                    layui.layer.open({
                        title: "站内信",
                        type: 2,
                        shade: 0.2,
                        maxmin: true,
                        area: ['400px', '550px'],
                        content: '/manage/member/message/list',
                    });
                })
            });
            //获取我的应用
            ea.request.get({
                url: 'member/miniapp/my/index',
                prefix: true,
            }, function (res) {
                if(res.data.errcode == 0){
                    //当前应用默认为控制台
                    var activeMiniappDir = 'manage',//当前应用的目录
                        activeMiniappName = '控制台',//当前应用的名称
                        html_miniapp_list = '';//我的应用列表
                    $.map(res.data.data.list, function (item) {
                        //该应用是默认应用
                        var isDefaultMiniapp = default_miniapp_dir === item.miniapp.dir;
                        //默认应用标识
                        var badge_default = isDefaultMiniapp ? '<span style="margin:0 6px;padding:0 3px;color: #fff;border-radius: 2px;background-color: #6e56ff;font-size: 12px;text-align: center;">默认应用</span>' : '';
                        //该应用是当前应用
                        (miniapp_dir && miniapp_dir === item.miniapp.dir || (!miniapp_dir && isDefaultMiniapp)) && (activeMiniappDir = item.miniapp.dir) && (activeMiniappName = item.appname);

                        var setDefaultMiniapp_dir = isDefaultMiniapp ? 'manage' : item.miniapp.dir,
                            set_default_miniapp_color = isDefaultMiniapp ? '#FF5722' : '#6e56ff',
                            set_default_miniapp_text = isDefaultMiniapp ? '取消默认' : '设为默认';

                        var html = "";
                        html += "";
                        html += "                        <dd class=\"menu-miniapp_switch\" data-dir=\"" + item.miniapp.dir + "\" data-name=\"" + item.appname + "\" style=\"width:350px;\">";
                        html += "                            <a href=\"javascript:;\" data-icon=\"fa fa-gears\" style=\"display:flex;width: 300px;margin: 10px auto;background-color: #fff;padding: 10px;border-radius: 10px;align-items: center\"> ";
                        html += "                                <img src=\"" + item.miniapp.logo_image + "\" style=\"height: 60px;width:60px;margin-right: 12px;\">";
                        html += "                                <div style=\"display:flex;flex-direction: column;justify-content: space-around;width:170px;height: 60px;\">";
                        html += "                                    <div style=\"font-size:12px;height: 1em;line-height: 1em;\">" + item.appname + badge_default + "<\/div>";
                        html += "                                    <div style=\"font-size: 12px;max-height: 2.4em;line-height: 1.2em;overflow: hidden;width: 170px;text-overflow: ellipsis;white-space: normal;display: -webkit-box;-webkit-box-orient: vertical;-webkit-line-clamp: 2;\">" + item.miniapp.describe + "<\/div>";
                        html += "                                <\/div>";
                        html += "                                <div onclick=\"setDefaultMiniapp('" + setDefaultMiniapp_dir + "')\">";
                        html += "                                    <div style=\"margin:1px;padding:0 3px;color: #fff;border-radius: 2px;background-color: " + set_default_miniapp_color + ";font-size: 12px;text-align: center;border-radius: 10px;\">" + set_default_miniapp_text + "<\/div>";
                        html += "                                <\/div>";
                        html += "                            <\/a>";
                        html += "                        <\/dd>";

                        //把默认应用放在列表的最上面
                        isDefaultMiniapp ? html_miniapp_list = html + html_miniapp_list : html_miniapp_list += html;
                        return html;
                    });
                    $('.my_miniapp_list').append(html_miniapp_list);
                    //监听应用切换被点击 必须在请求完url后再加载 否则后增加的无法被监听
                    $('.menu-miniapp_switch').on("click", function () {
                        var miniapp_dir = $(this).attr('data-dir');
                        ea.common.setCookie('miniapp_dir', miniapp_dir, 's60');
                        parent.window.location = '/manage/member/index';
                    });
                    //设置当前应用名
                    $(".active-miniapp-name").text(activeMiniappName);
                    //渲染当前应用
                    $(".menu-miniapp_switch[data-dir='" + activeMiniappDir + "'] a").css('border', "2px solid #6e56ff");
                }else{
                    ea.msg.error(res.data.msg);
                }
            });

        },
        // dashboard: function () {
        //     ea.request.get({
        //         url: 'loginInfo',
        //     }, function (res) {
        //         if(res.data.errcode == 0) {
        //             var data_list = res.data.data;
        //             var html = '';
        //             for (var i = 0; i < data_list.length; i++) {
        //
        //                 html += '<tr>\n' +
        //                     '                                    <td>' + data_list[i].name + '</td>\n' +
        //                     '                                    <td>' + data_list[i].add_time_date + '</td>\n' +
        //                     '                                    <td >' + data_list[i].ip + '</td>\n' +
        //                     '                                    <td>' + data_list[i].city_name + '</td>\n' +
        //                     '                                </tr>';
        //
        //             }
        //             if (data_list.length < 1) {
        //                 html = ' <td colspan="4" >没有记录...</td>';
        //             }
        //             $('#city_info').html(html);
        //         }
        //     }, function (res) {
        //         ea.msg.error(res.msg); //失败
        //     }, function (that) {
        //         ea.msg.error('查询失败');//异常
        //     });
        //     ea.request.get({
        //         url: 'echartsInfo',
        //     }, function (res) {
        //         if(res.data.errcode == 0) {
        //
        //             var source  = res.data.data.data;
        //             var count  = res.data.data.count;
        //             var year  = res.data.data.year;
        //             for (var i=0;i<source[0].length;i++)
        //             {
        //                 source[0][i] = source[0][i].toString();
        //             }
        //
        //             //拼装series
        //             var series  =new Array();
        //             for (var i=0;i<count;i++)
        //             {
        //                 var car =  {type: 'line', smooth: true, seriesLayoutBy: 'row'};
        //                 series.push(car);
        //             }
        //
        //             var last =  {
        //                 type: 'pie',
        //                 id: 'pie',
        //                 radius: '25%',
        //                 center: ['50%', '30%'],
        //                 label: {
        //                     formatter: '{b}: {@'+year[0]+'} ({d}%)'
        //                 },
        //                 encode: {
        //                     itemName: 'product',
        //                     value: year[0],
        //                     tooltip: year[0]
        //                 }
        //             };
        //
        //             series.push(last);
        //             console.log(series);
        //             var dom = document.getElementById("container");
        //             var myChart = echarts.init(dom);
        //             var option = null;
        //
        //             setTimeout(function () {
        //
        //                 option = {
        //                     legend: {},
        //                     tooltip: {
        //                         trigger: 'axis',
        //                         showContent: false
        //                     },
        //                     dataset: {
        //                         source: source
        //                     },
        //                     xAxis: {type: 'category'},
        //                     yAxis: {gridIndex: 0},
        //                     grid: {top: '55%'},
        //                     series: series
        //                 };
        //
        //                 myChart.on('updateAxisPointer', function (event) {
        //                     var xAxisInfo = event.axesInfo[0];
        //                     if (xAxisInfo) {
        //                         var dimension = xAxisInfo.value + 1;
        //                         myChart.setOption({
        //                             series: {
        //                                 id: 'pie',
        //                                 label: {
        //                                     formatter: '{b}: {@[' + dimension + ']} ({d}%)'
        //                                 },
        //                                 encode: {
        //                                     value: dimension,
        //                                     tooltip: dimension
        //                                 }
        //                             }
        //                         });
        //                     }
        //                 });
        //
        //                 myChart.setOption(option);
        //
        //             });
        //
        //             if (option && typeof option === "object") {
        //                 myChart.setOption(option, true);
        //             }
        //         }
        //     }, function (res) {
        //         ea.msg.error(res.msg); //失败
        //     }, function (that) {
        //         ea.msg.error('查询失败');//异常
        //     });
        //     var notice = {
        //         data:{
        //             cate_id:{$cate_id},
        //             page:1,
        //             limit:7,
        //             notice_list:[],
        //             count:0,
        //         },
        //         init:function (bwajax) {
        //             this.listen('.layuimini-notice'); //资讯点击监听
        //             this.tab_init('notice_tab'); //资讯分类加载
        //             this.init_notice(bwajax); //资讯分页加载
        //         },
        //         listen:function (el) {
        //             $('body').on('click', el, function () {
        //                 var jsonObj = {};
        //                 jsonObj.title = $(this).children('.layuimini-notice-title').text(),
        //                     jsonObj.noticeTime = $(this).children('.layuimini-notice-extra').text(),
        //                     jsonObj.content = $(this).children('.layuimini-notice-content').html();
        //                 var getTpl = notice_detail_template.innerHTML;
        //                 laytpl(getTpl).render(jsonObj, function(html){
        //                     parent.layer.open({
        //                         type: 1,
        //                         title: '系统资讯'+'<span style="float: right;right: 1px;font-size: 12px;color: #b1b3b9;margin-top: 1px">'+jsonObj.noticeTime+'</span>',
        //                         area: ['500px', '400px'],
        //                         shade: 0.2,
        //                         id: 'layuimini-notice',
        //                         btn: ['查看', '取消'],
        //                         btnAlign: 'c',
        //                         moveType: 1,
        //                         content:html,
        //                         success: function (layero) {
        //                             var btn = layero.find('.layui-layer-btn');
        //                             btn.find('.layui-layer-btn0').attr({
        //                                 href: 'https://gitee.com/buwangyun/bwsaas',
        //                                 target: '_blank'
        //                             });
        //                         }
        //                     });
        //                 });
        //
        //             });
        //         },
        //         init_notice:function (bwajax) {
        //             var _this = this;
        //             this.setNoticeList(bwajax,function () {
        //                 //obj.curr,obj.limit,
        //                 laypage.render({
        //                     elem: 'test1'
        //                     ,limit:_this.data.limit
        //                     ,count:_this.data.count
        //                     ,jump: function(obj, first){
        //                         _this.data.page = obj.curr;
        //                         _this.data.limit = obj.limit;
        //                         //非首次
        //                         if(!first){
        //                             //obj包含了当前分页的所有参数，比如：
        //                             _this.setNoticeList(bwajax);
        //                         }
        //                     }
        //                 });
        //             })
        //         },
        //         setNoticeList:function (bwajax,call_back){
        //             laytpl = layui.laytpl;
        //             var _this = this;
        //             bwajax.get('manage/member/noticeList?page='+_this.data.page+'&limit='+_this.data.limit+'&cate_id='+_this.data.cate_id).then(function (response) {
        //                 if(response.data.data.errcode == 0){
        //                     _this.data.count = response.data.data.data.total;
        //                     //console.log(response.data.data);
        //                     _this.data.notice_list = response.data.data.data.list;
        //
        //                     _this.build_notice(laytpl);
        //                     if(call_back){
        //                         call_back();
        //                     }
        //                 }else{
        //                     layer.msg(response.data.msg);
        //                 }
        //             }) .catch(function (error) {
        //                 console.log(error);
        //                 var html =  '  <div >\n' +
        //                     '                                    <div class="layuimini-notice-title" style="color: red">数据出错</div>\n' +
        //                     '                                </div>';
        //                 $('#notice_list').html(html);
        //             });
        //         },
        //         build_notice:function (laytpl){
        //             var getTpl = notice_template.innerHTML
        //                 , view = document.getElementById('notice_list');
        //             laytpl(getTpl).render(this.data.notice_list, function(html){
        //                 view.innerHTML = html;
        //             });
        //         },
        //         tab_init:function (lay_filter) {
        //             var _this = this;
        //             element.on("tab("+lay_filter+")", function(elem){
        //                 _this.data.cate_id = $(this).attr('data-cate');
        //                 _this.data.page = 1;
        //                 _this.init_notice(bwajax); //资讯分页加载
        //             });
        //         }
        //
        //     };
        //     notice.init();//加载公告
        //     ea.listen();
        // },
        login: function () {

            if (top.location !== self.location) {
                top.location = self.location;
            }

            $('.bind-password').on('click', function () {
                if ($(this).hasClass('icon-5')) {
                    $(this).removeClass('icon-5');
                    $("input[name='password']").attr('type', 'password');
                } else {
                    $(this).addClass('icon-5');
                    $("input[name='password']").attr('type', 'text');
                }
            });
            $('.reg-member').on('click', function () {
                ea.open('注册租户','/manage/member/add','50%','90%');
            });
            $('.fog-member').on('click', function () {
                ea.open('找回密码','/manage/member/forget','50%','90%');
            });

            $('.icon-nocheck').on('click', function () {
                if ($(this).hasClass('icon-check')) {
                    $(this).removeClass('icon-check');
                } else {
                    $(this).addClass('icon-check');
                }
            });
            //扫码登录JS  Start
            var isQrcode;
            $(".login_type").click(function(){
                $("#account").toggleClass('account-icon');
                $("#scan").toggle(function(){
                    $("#show_qrcode").empty();
                    if ($(this).css("display") == 'block'){
                        $.getJSON("/api/wechat_auth/createScanCode",function(rel){
                            if(rel.data.errcode == 0){
                                new QRCode(parent.document.getElementById("show_qrcode"),rel.data.data.url);
                            }else {
                                parent.document.getElementById("show_qrcode").innerText = rel.msg;
                            }

                        })
                        isQrcode = setInterval(function(){
                            $.getJSON("/api/wechat_auth/getLoginState",function(data){
                                if(data.code == 200 && data.data.errcode == 400200){
                                    clearInterval(isQrcode);
                                    ea.common.setCookie('token', data.data.data.token, 's'+data.data.data.expires_in);
                                    ea.msg.success("登录成功", function () {
                                        window.location.href = data.data.data.url;
                                    })
                                }
                            })
                        },1000)
                    }else{
                        clearInterval(isQrcode);
                    }
                });
            });
        //扫码登录JS  End
            ea.listen(function (data) {
                data['keep_login'] = $('.icon-nocheck').hasClass('icon-check') ? 1 : 0;
                return data;
            }, function (res) {
                ea.common.setCookie('token', res.data.data.token, 's'+res.data.data.expires_in);
                ea.msg.success("登录成功", function () {
                    window.location = ea.url('member/index');
                })
            }, function (res) {
                ea.msg.error(res.msg, function () {
                    $('#refreshCaptcha').trigger("click");
                });
            });

        },
        messageList: function () {

            var laypage = layui.laypage;

            //执行一个laypage实例
            laypage.render({
                elem: 'test1' //注意，这里的 test1 是 ID，不用加 # 号
                ,count: total //数据总数，从服务端得到
                ,limit: 5
                ,jump: function(obj, first) {
                    getMessageList(obj.curr, obj.limit);
                    //首次不执行
                    if (!first) {
                        //do something
                    }
                }
            });

            ea.listen();
        }
    };


    return Controller;
});