define(["jquery", "easy-admin"], function ($, ea) {

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'member/wechat/message/MessageLog/index?miniapp_id=' + CONFIG.MINIAPP_ID,
        add_url: 'member/wechat/message/MessageLog/add?miniapp_id=' + CONFIG.MINIAPP_ID,
        edit_url: 'member/wechat/message/MessageLog/edit?miniapp_id=' + CONFIG.MINIAPP_ID,
        delete_url: 'member/wechat/message/MessageLog/del?miniapp_id=' + CONFIG.MINIAPP_ID,
        export_url: 'member/wechat/message/MessageLog/export?miniapp_id=' + CONFIG.MINIAPP_ID,
        modify_url: 'member/wechat/message/MessageLog/modify?miniapp_id=' + CONFIG.MINIAPP_ID,
    };

    var Controller = {

        index: function () {
            ea.table.render({
                init: init,
                cols: [[
                    {type: 'checkbox'},
                    {field: 'id', title: 'ID'},
                    {field: 'type', search: 'select', selectList: ["小程序订阅消息","公众号模板消息"], title: '类型'},
                    {field: 'member_miniapp_id', title: '租户应用ID'},
                    {field: 'user_id', title: '用户ID'},
                    {field: 'key', title: '模板标识'},
                    {field: 'template.name', title: '模板名称'},
                    {field: 'param', title: '参数'},
                    {field: 'status', search: 'select', selectList: ["失败","成功"], tips: '成功|失败', title: '状态'},
                    {field: 'fail_msg', title: '失败原因'},
                    {field: 'create_time', title: '创建时间'},
                    {width: 250, title: '操作', templet: ea.table.tool},
                ]],
            });

            ea.listen();
        },
        add: function () {
            ea.listen();
        },
        edit: function () {
            ea.listen();
        },
    };
    return Controller;
});