define(["easy-admin", "miniTab", "QRCode"], function (ea, miniTab, QRCode) {
    var Controller = {
        userSetting: function () {
            $('#recharge').on('click',function(event){
                // 打开新的窗口
                miniTab.openNewTabByIframe({
                    href:"/manage/member/recharge",
                    title:"账户充值",
                });
                return false;
            });
            $('#wallet_bill').on('click',function(event){
                layer.open({
                    type: 2,
                    content: '/manage/member/walletBill',
                    area: ['90%', '90%']
                });

                return false;
            });
            if(CONFIG.JS.url){
                var options ={
                    text: CONFIG.JS.url,
                    width: 256,
                    height: 256,
                };
                new QRCode(document.getElementById("qrcode"), options);  // 设置要生成二维码的链接
            }
            ea.listen();
        },
    };
    return Controller;
});