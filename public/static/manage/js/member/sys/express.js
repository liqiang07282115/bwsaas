define(["jquery", "easy-admin"], function ($, ea) {

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'member/sys/express/index',
        add_url: 'member/sys/express/add',
        edit_url: 'member/sys/express/edit',
        delete_url: 'member/sys/express/del',
        export_url: 'member/sys/express/export',
        modify_url: 'member/sys/express/modify',
    };

    var Controller = {

        index: function () {
            ea.table.render({
                init: init,
                cols: [[
                    {type: 'checkbox'},
                    {field: 'id', title: '快递公司id'},
                    {field: 'code', title: '快递公司编码'},
                    {field: 'name', title: '快递公司全称'},
                    {field: 'sort', title: '排序', edit: 'text'},
                    {field: 'is_show', search: 'select', selectList: ["取消","展示"], tips: '展示|取消', title: '是否显示', templet: ea.table.switch},
                    {width: 250, title: '操作', templet: ea.table.tool},
                ]],
            });

            ea.listen();
        },
        add: function () {
            ea.listen();
        },
        edit: function () {
            ea.listen();
        },
    };
    return Controller;
});