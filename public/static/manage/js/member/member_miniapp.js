define(["jquery", "easy-admin"], function ($, ea) {

    var init = {
        index_url: '/manage/member/miniapp/my/index',
    };
    var laytpl = layui.laytpl;
    var Controller = {
        index: function () {
            ea.request.get(
                {
                    url: init.index_url,
                    data: {},
                }, function (res) {
                    var list = res.data.data.list;
                    //渲染插件
                    var getTpl = app_template.innerHTML
                        , view = document.getElementById('my-miniapp');
                    laytpl(getTpl).render(list, function (html) {
                        view.innerHTML = html;
                    });
                }
            );

            //监听列表内管理应用按钮被点击
            $('body').off('click', '[data-event]').on('click', '[data-event]', function (event) {
                var miniapp_dir = $(this).attr('data-dir');
                var event = $(this).attr('data-event');
                switch(event) {
                    case 'open':
                        //将目标应用的目录写入cookie,在租户首页进行消费
                        ea.common.setCookie('miniapp_dir', miniapp_dir, 's60');
                        parent.window.location = '/manage/member/index';
                        break;
                    case 'renew':

                        break;

                }

            })

            ea.listen();
        },
        add: function () {
            ea.listen();
        },
        edit: function () {
            ea.listen();
        },
        renew: function () {
            //应用续费
            renew.init();
            ea.listen(null,function () {
                parent.location.reload();
            });
        }
    };

    var renew = {
        param:{
            recharge_index:null
        },
        init:function(){
           this.hover();//套餐选择特效
           this.listen();//监听
        },
        hover: function () {
            $(function () {
                $(".body-style").mouseover(function () {
                    $(this).addClass("background");
                }).mouseout(function () {
                    $(this).removeClass("background");
                });

                $(".plugin-item").click(function () {
                    $(".state img").attr("src","/static/manage/images/member/member_miniapp/renew/icon_no.png");
                    $(this).find('img').attr("src","/static/manage/images/member/member_miniapp/renew/icon_yes.png");
                    $(".plugin-item").removeClass("image-shade");
                    $(this).addClass("image-shade");
                    var id = $(this).attr('data-id');
                    $("#term_id").val(id);
                });
            });
        },
        listen:function () {
            var that = this;
            //监听充值
            $(".go-recharge").click(function () {
                that.param.recharge_index = ea.open('租户金额充值', '/manage/member/recharge', '100%', '100%');
            });
        }
    }


    return Controller;
});