define(["jquery", "easy-admin"], function ($, ea) {

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'member/user/recharge/index',
        add_url: 'member/user/recharge/add',
        edit_url: 'member/user/recharge/edit',
        delete_url: 'member/user/recharge/del',
        export_url: 'member/user/recharge/export',
        modify_url: 'member/user/recharge/modify',
    };

    var Controller = {

        index: function () {
            ea.table.render({
                init: init,
                cols: [[
                    {type: 'checkbox',fixed: 'left'},
                    {field: 'id', title: 'id',searchOp:'='},
                    {field: 'user.member_miniapp_id', search: 'select', selectList: getMemberMiniappSelectList, title: '所属应用',minWidth:150 ,templet:function (row) {
                            return getMemberMiniappSelectList[row.user.member_miniapp_id];
                        },searchOp:'='},
                    {field: 'order_id', title: '订单号',minWidth:170},
                    {field: 'uid', title: '充值用户ID',minWidth:150},
                    {field: 'price', title: '充值金额',minWidth:150},
                    {field: 'give_price', title: '购买赠送金额',minWidth:150},
                    {field: 'paid', search: 'select', selectList: ["未充值","已充值"],  title: '是否充值',minWidth:150},
                    {field: 'user.mobile', title: '手机号',minWidth:150},
                    {field: 'user.nickname', title: '昵称',minWidth:150},
                    {field: 'user.avatar', title: '头像', templet: ea.table.image,minWidth:150,search:false},
                    {field: 'recharge_type', search: 'select', selectList: {"mini_program":"小程序充值","official":"公众号充值"}, title: '充值类型',minWidth:150},

                    {field: 'pay_time', title: '充值支付时间',minWidth:150,templet: ea.table.date, search: 'range'},
                    {field: 'add_time', title: '充值时间',minWidth:150,templet: ea.table.date, search: 'range'},
                    // {field: 'refund_price', title: '退款金额'},

                    {width: 250, title: '操作',fixed: 'right', templet: ea.table.tool},
                ]],
            });

            ea.listen();
        },
        add: function () {
            ea.listen();
        },
        edit: function () {
            ea.listen();
        },
    };
    return Controller;
});