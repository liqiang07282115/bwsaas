define(["jquery", "easy-admin"], function ($, ea) {

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'admin/sys/city/index',
        add_url: 'admin/sys/city/add',
        edit_url: 'admin/sys/city/edit',
        delete_url: 'admin/sys/city/del',
        export_url: 'admin/sys/city/export',
        modify_url: 'admin/sys/city/modify',
    };

    var Controller = {

        index: function () {
            ea.table.render({
                init: init,
                cols: [[
                    {type: 'checkbox'},
                    {field: 'id', title: 'id'},
                    {field: 'city_id', title: '城市id'},
                    {field: 'level', title: '省市级别'},
                    {field: 'parent_id', title: '父级id'},
                    {field: 'area_code', title: '行政区划代码'},
                    {field: 'name', title: '名称'},
                    {field: 'merger_name', title: '合并名称'},
                    {field: 'lng', title: '经度'},
                    {field: 'lat', title: '纬度'},
                    {field: 'is_show', search: 'select', selectList: {"1":"是","0":"否"}, title: '是否展示'},
                    {width: 250, title: '操作', templet: ea.table.tool},
                ]],
            });

            ea.listen();
        },
        add: function () {
            ea.listen();
        },
        edit: function () {
            ea.listen();
        },
    };
    return Controller;
});