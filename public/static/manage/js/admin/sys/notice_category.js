define(["jquery", "easy-admin"], function ($, ea) {

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'admin/sys/notice/category/index',
        add_url: 'admin/sys/notice/category/add',
        edit_url: 'admin/sys/notice/category/edit',
        delete_url: 'admin/sys/notice/category/del',
        export_url: 'admin/sys/notice/category/export',
        modify_url: 'admin/sys/notice/category/modify',
    };

    var Controller = {

        index: function () {
            ea.table.render({
                init: init,
                cols: [[
                    {type: 'checkbox'},
                    {field: 'id', title: 'id'},
                    {field: 'name', title: '分类名'},
                    {field: 'image', title: '缩略图', templet: ea.table.image},
                    {field: 'weight', title: '权重'},
                    {field: 'status', search: 'select', selectList: ["隐藏","正常"], tips: '正常|隐藏', title: '状态', templet: ea.table.switch},
                    {field: 'create_time', title: '创建时间'},
                    {width: 250, title: '操作', templet: ea.table.tool},
                ]],
            });

            ea.listen();
        },
        add: function () {
            ea.listen();
        },
        edit: function () {
            ea.listen();
        },
    };
    return Controller;
});