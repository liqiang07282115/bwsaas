define(["jquery", "easy-admin"], function ($, ea) {

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'admin/plugin/core/index',
        menu_url: 'admin/plugin/core/menu',
    };
    var laytpl = layui.laytpl;
    var dropdown = layui.dropdown
        , util = layui.util;
    var element=layui.element;
    var Controller = {
        index: function () {
            plugin.init();
            element.init();
            ea.listen();
        },
        menu: function () {
            element.init();
            ea.listen();
        }
    };

    //操作对象
    var plugin = {
        data: {
            plugin_id: 0,
        },
        init: function () {
            //加载列表
            this.setPluginList();
        },
        setPluginList: function () {
            var _this = this;
            ea.request.post({
                url: '/' + CONFIG.ADMIN + '/' + init.index_url,
            }, function (res) {
                _this.loadList(res.data.data, laytpl);//插件列表渲染
                _this.hover();//鼠标样式
                _this.menuListen();//菜单事件
            }, function (res) {
                ea.msg.error(res.msg); //失败
            }, function (that) {
                ea.msg.error('查询失败');//异常
            });
        },
        loadList: function (data, laytpl) {
            //渲染插件
            var getTpl = plugin_template.innerHTML
                , view = document.getElementById('plugin_list');
            laytpl(getTpl).render(data.list, function (html) {
                view.innerHTML = html;
            });
        },
        hover: function () {
            $(function () {
                $(".body-style").mouseover(function () {
                    $(this).addClass("hover-sty");
                }).mouseout(function () {
                    $(this).removeClass("hover-sty");
                });
            });
        },
        menuListen: function () {
            var _this = this;
            $(".plugin-item").click(function () {
                _this.data.plugin_id = $(this).attr("data-plugin-id");
                _this.checkPlugin(); //检测菜单渲染页面
            });
        },
        checkPlugin: function () {
            //检测插件是否存在菜单
            var _this = this;
            ea.request.get({
                url: '/' + CONFIG.ADMIN + '/' + init.menu_url + '?addons=' + this.data.plugin_id,
            }, function (res) {
                _this.jumpMenu();
            }, function (res) {
                ea.msg.error(res.msg); //失败
            }, function (that) {
                ea.msg.error('查询失败');//异常
            });
        }
        ,
        jumpMenu: function () {
            //页面iframe跳转
            var url = '/' + CONFIG.ADMIN + '/' + init.menu_url + '?addons=' + this.data.plugin_id + '&jump=1';
            console.log(url);
            //菜单跳转
            window.location.href = url;
        },
    };


    return Controller;
});