define(["jquery", "easy-admin"], function ($, ea) {

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'admin.Plugin/index',
    };

    var Controller = {

        config: function () {
            ea.listen();
        },
        index: function () {
            ea.table.render({
                init: init,
                cols: [[
                    {field: 'id', title: 'ID', align: 'center', width: 80, fixed: 'left'},
                    {field: 'name', title: '标识', align: 'center', minWidth: 150, templet: '#name'},
                    {field: 'title', title: '名称', align: 'center', minWidth: 150, templet: '#title'},
                    {field: 'author', title: '作者', align: 'center', width: 100, templet: '#author'},
                    {field: 'price', title: '价钱', align: 'center', width: 80},
                    {field: 'version', title: '版本号', align: 'center', width: 80, templet: '#version'},
                    {field: 'status', title: '状态', align: 'center', width: 100, templet: '#status'},
                    {field: 'create_time', title: '安装时间', align: 'center', width: 160, templet: ea.table.date},
                    {field: 'update_time', title: '更新时间', align: 'center', width: 160, templet: ea.table.date},

                    {title: '操作', toolbar: '#listBar', align: 'center', fixed: "right", minWidth: 180}
                ]],
            });
            //监听工具条
            layui.table.on('tool(currentTableRenderId_LayFilter)', function (obj) { //注：tool 是工具条事件名，test 是 table 原始容器的属性 lay-filter="对应的值"
                var data = obj.data; //获得当前行数据
                var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
                switch (layEvent) {
                    case 'edit':
                        bw_edit(obj.data.id);
                        break;
                    case 'del':
                        del(obj.data.id);
                        break;
                    case 'install'://安装
                        install(obj.data.name);
                        break;
                    case 'uninstall'://卸载
                        uninstall(obj.data.name);
                        break;
                    case 'enable'://启用
                        enable(obj.data.name);
                        break;
                    case 'disable'://禁用
                        disable(obj.data.name);
                        break;
                    case 'config'://修改插件配置
                        config(obj.data.name);
                        break;
                }
            });
            ea.listen();
        }
    };

    //安装
    function install(name) {
        layer.confirm('确认安装?', function (index) {
            //ajax调用后台接口
            ea.request.post({
                url: "admin.Plugin/install?name=" + name,
            },function (res){
                if(res.data.errcode == 0) {
                    ea.msg.success('安装成功')
                    ea.table_reload('currentTableRenderId',{});
                }
            }, function (res) {
                ea.msg.error(res.msg); //失败
            }, function (that) {
                ea.msg.error('查询失败');//异常
            });
            layer.close(index)
        });
    }

    //卸载
    function uninstall(name) {
        layer.confirm('确认卸载?', function (index) {
            //ajax调用后台接口
            ea.request.post({
                url: "admin.Plugin/uninstall?name=" + name,
            },function (res){
                if(res.data.errcode == 0) {
                    ea.msg.success('卸载成功')
                    ea.table_reload('currentTableRenderId',{});
                }
            }, function (res) {
                ea.msg.error(res.msg); //失败
            }, function (that) {
                ea.msg.error('查询失败');//异常
            });
            layer.close(index);
        });
    }

    //启用
    function enable(name) {
        layer.confirm('确认启用?', function (index) {
            //ajax调用后台接口
            ea.request.post({
                url: "admin.Plugin/enable?name=" + name,
            },function (res){
                if(res.data.errcode == 0) {
                    ea.msg.success('启用成功')
                    ea.table_reload('currentTableRenderId',{});
                }
            }, function (res) {
                ea.msg.error(res.msg); //失败
            }, function (that) {
                ea.msg.error('启用异常');//异常
            });
            layer.close(index);
        });
    }

    //禁用
    function disable(name) {
        layer.confirm('确认禁用?', function (index) {
            //ajax调用后台接口
            ea.request.post({
                url: "admin.Plugin/disable?name=" + name,
            },function (res){
                if(res.data.errcode == 0) {
                    ea.msg.success('禁用成功')
                    ea.table_reload('currentTableRenderId',{});
                }
            }, function (res) {
                ea.msg.error(res.msg); //失败
            }, function (that) {
                ea.msg.error('禁用异常');//异常
            });
            layer.close(index);
        });
    }

    return Controller;
});