define(["jquery", "easy-admin"], function ($, ea) {

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'admin/template',
        add_url: 'admin/template/add',
        edit_url: 'admin/template/edit',
        delete_url: 'admin/template/del',
        sync_url: 'admin/miniapp/index?action=syncTemplate',
    };

    var Controller = {

        index: function () {
            ea.table.render({
                init: init,
                toolbar: ['add',
                    'delete',
                    [{
                        text: '同步第三方代码模板',
                        url: init.sync_url,
                        method: 'request',
                        auth: 'add',
                        class: 'layui-btn layui-btn-normal layui-btn-sm',
                        icon: 'fa fa-cloud-download ',
                    }],'refresh'],
                cols: [[
                    {type: 'checkbox'},
                    {field: 'id', title: 'ID'},
                    {field: 'miniapp_id', title: '应用id'},
                    {field: 'member_id', title: '租户id',hide:true},
                    {field: 'member_miniapp_id', title: '租户应用id'},
                    {field: 'version', title: '版本号'},
                    {field: 'template_id', title: '模板id'},
                    {field: 'images', title: '模板图', templet: ea.table.image},
                    {field: 'create_time', title: '创建时间', search: 'range', searchTip: ' - ', templet: ea.table.date},
                    {field: 'desc', title: '描述'},
                    {width: 250, title: '操作', templet: ea.table.tool},
                ]],
            });

            ea.listen();
        },
        add: function () {
            // 联动查询用户购买的应用
            layui.form.on('select(miniappIdFilter)', function(data){
               var option = {
                   url:'/admin/getMemberMiniappList',
                   prefix:true,
                   data:{
                       id:data.value
                   }
               }
               ea.request.get(option, function (res){
                   $("#memberMiniappId").empty();//清空下拉框的值
                   $.each(res.data.data, function (index, item) {
                       $('#memberMiniappId').append("<option value=''></option>");
                       $('#memberMiniappId').append(new Option(item.appname+ '-V' +item.version, item.id));// 下拉菜单里添加元素
                   });
                   layui.form.render("select");//重新渲染 固定写法
               });
            });
            layui.form.on('select(templateFilter)', function(data){
                var option = {
                    url:'/admin/getThirdTemplate',
                    prefix:true,
                    data:{
                        template_id:data.value
                    }
                }
                ea.request.get(option, function (res){
                    console.log(res)
                    $("#version").val(res.data.data.user_version);
                    layui.form.render("select");//重新渲染 固定写法
                });

            });
            ea.listen();
        },
        edit: function () {
            ea.listen();
        },
    };
    return Controller;
});