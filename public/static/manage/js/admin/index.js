define(["easy-admin", "QRCode", "miniAdmin", "miniTab"], function (ea, QRCode, miniAdmin, miniTab) {
    var Controller = {
        index: function () {
            var options = {
                iniUrl: ea.url('admin/menu'),    // 初始化接口
                clearUrl: ea.url("common/clearCache"), // 缓存清理接口
                urlHashLocation: true,      // 是否打开hash定位
                bgColorDefault: 1,      // 主题默认配置
                multiModule: true,          // 是否开启多模块
                menuChildOpen: false,       // 是否默认展开菜单
                loadingTime: 0,             // 初始化加载时间
                pageAnim: true,             // iframe窗口动画
                maxTabNum: 20,              // 最大的tab打开数量
            };
            miniAdmin.render(options);

            $('.login-out').on("click", function () {
                ea.request.post({
                    url: ea.url('admin/logout'),
                    prefix: true,
                }, function (res) {
                    if(res.data.errcode == 0){
                        ea.msg.success(res.msg, function () {
                            window.location = ea.url('admin/login');
                        })
                    }else{
                        ea.msg.error(res.data.msg);
                    }
                });
            });
        },
        login: function () {
            if (top.location !== self.location) {
                top.location = self.location;
            }
            ea.listen(function (data) {
                console.warn(JSON.stringify(data))
                // data['keep_login'] = $('.icon-nocheck').hasClass('icon-check') ? 1 : 0;
                return data;
            }, function (res) {
                ea.common.setCookie('token', res.data.data.token, 's' + res.data.data.expires_in);
                ea.msg.success("登录成功", function () {
                    window.location = ea.url('admin/index');
                })
            }, function (res) {
                ea.msg.error(res.msg, function () {
                    $('#captchaImg').trigger("click");
                });
            });
        },
        dashboard: function () {
            ea.request.get({
                url: 'loginInfo',
            }, function (res) {
                if(res.data.errcode == 0) {
                    var data_list = res.data.data;
                    var html = '';
                    for (var i = 0; i < data_list.length; i++) {

                        html += '<tr>\n' +
                            '                                    <td>' + data_list[i].name + '</td>\n' +
                            '                                    <td>' + data_list[i].add_time_date + '</td>\n' +
                            '                                    <td >' + data_list[i].ip + '</td>\n' +
                            '                                    <td>' + data_list[i].city_name + '</td>\n' +
                            '                                </tr>';

                    }
                    if (data_list.length < 1) {
                        html = ' <td colspan="4" >没有记录...</td>';
                    }
                    $('#city_info').html(html);
                }
            }, function (res) {
                ea.msg.error(res.msg); //失败
            }, function (that) {
                ea.msg.error('查询失败');//异常
            });
            ea.listen();
        },
        userSetting: function (){
            ea.listen();
        },
        userPassword: function (){
            ea.listen();
        }
    };
    return Controller;
});