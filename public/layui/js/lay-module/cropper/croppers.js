/*!
 * Cropper v3.0.0
 */

layui.config({
    base: '/layui/js/lay-module/' //layui自定义layui组件目录
}).define(['jquery','layer','cropper'],function (exports) {
    var $ = layui.jquery
        ,layer = layui.layer;

    var obj = {
        render: function(e){
            var self = this,
                elem = e.elem,
                saveW = e.saveW,
                saveH = e.saveH,
                mark = e.mark,
                area = e.area,
                url = e.url,
                done = e.done,
                class_index = e.class_index||'';
            var html = "<link rel=\"stylesheet\" href=\"/layui/js/lay-module/cropper/cropper.css\">\n" +
                "<div class=\"layui-fluid showImgEdit"+class_index+"\" style=\"display: none\">\n" +
                "    <div class=\"layui-form-item\">\n" +
                "        <div class=\"layui-input-inline layui-btn-container\" style=\"width: auto;\">\n" +
                "            <label for=\"cropper_avatarImgUpload"+class_index+"\" class=\"layui-btn layui-btn-primary\">\n" +
                "                <i class=\"layui-icon\">&#xe67c;</i>选择图片\n" +
                "            </label>\n" +
                "            <input class=\"layui-upload-file\" id=\"cropper_avatarImgUpload"+class_index+"\" type=\"file\" value=\"选择图片\" name=\"file"+class_index+"\">\n" +
                "        </div>\n" +
                "        <div class=\"layui-form-mid layui-word-aux\">头像的尺寸限定150x150px,大小在50kb以内</div>\n" +
                "    </div>\n" +
                "    <div class=\"layui-row layui-col-space15\">\n" +
                "        <div class=\"layui-col-xs9\">\n" +
                "            <div class=\"readyimg"+class_index+"\" style=\"height:450px;background-color: rgb(247, 247, 247);\">\n" +
                "                <img src=\"\" >\n" +
                "            </div>\n" +
                "        </div>\n" +
                "        <div class=\"layui-col-xs3\">\n" +
                "            <div class=\"img-preview"+class_index+"\" style=\"width:200px;height:200px;overflow:hidden\">\n" +
                "            </div>\n" +
                "        </div>\n" +
                "    </div>\n" +
                "    <div class=\"layui-row layui-col-space15\">\n" +
                "        <div class=\"layui-col-xs9\">\n" +
                "            <div class=\"layui-row\">\n" +
                "                <div class=\"layui-col-xs6\">\n" +
                "                    <button type=\"button\" class=\"layui-btn layui-icon layui-icon-left\" cropper-event=\"rotate\" data-option=\"-15\" title=\"Rotate -90 degrees\"> 向左旋转</button>\n" +
                "                    <button type=\"button\" class=\"layui-btn layui-icon layui-icon-right\" cropper-event=\"rotate\" data-option=\"15\" title=\"Rotate 90 degrees\"> 向右旋转</button>\n" +
                "                </div>\n" +
                "                <div class=\"layui-col-xs5\" style=\"text-align: right;\">\n" +
                // "                    <button type=\"button\" class=\"layui-btn\" title=\"移动\"></button>\n" +
                // "                    <button type=\"button\" class=\"layui-btn\" title=\"放大图片\"></button>\n" +
                // "                    <button type=\"button\" class=\"layui-btn\" title=\"缩小图片\"></button>\n" +
                "                    <button type=\"button\" class=\"layui-btn layui-icon layui-icon-refresh\" cropper-event=\"reset\" title=\"重置图片\"></button>\n" +
                "                </div>\n" +
                "            </div>\n" +
                "        </div>\n" +
                "        <div class=\"layui-col-xs3\">\n" +
                "            <button class=\"layui-btn layui-btn-fluid\" id ='"+class_index+"' cropper-event=\"confirmSave\" type=\"button\"> 保存修改</button>\n" +
                "        </div>\n" +
                "    </div>\n" +
                "\n" +
                "</div>";

            $('body').append(html);
            var content = $('.showImgEdit'+class_index)
                ,image = $(".showImgEdit"+class_index+"  .readyimg"+class_index+" img")
                ,preview = '.showImgEdit'+class_index+'  .img-preview'+class_index
                ,file = $(".showImgEdit"+class_index+"  input[name='file"+class_index+"']")
                , options = {aspectRatio: mark,preview: preview,viewMode:1};

            $(elem).on('click',function () {
                layer.open({
                    type: 1
                    , content: content
                    , area: area
                    , success: function () {
                        console.log(content)
                        image.cropper(options);
                    }
                    , cancel: function (index) {
                        layer.close(index);
                        image.cropper('destroy');
                    }
                });
            });
            $(".layui-btn").on('click',function () {
                var event = $(this).attr("cropper-event");
                //监听确认保存图像
                if(event === 'confirmSave'){
                    image.cropper("getCroppedCanvas",{
                        width: saveW,
                        height: saveH
                    }).toBlob(function(blob){
                        var formData=new FormData();
                        formData.append('file',blob,'head.jpg');
                        //console.log(blob);

                        var objs = document.getElementById("cropper_avatarImgUpload"+class_index);
                        var file_name = objs.files[0].name;
                        //console.log(temp);

                        //文件类型
                        var file_type = blob.type;
                        //文件大小
                        var file_size = blob.size;

                        $.ajax({
                            method:"post",
                            url: url, //用于文件上传的服务器端请求地址
                            data: formData,
                            processData: false,
                            contentType: false,
                            success:function(result){
                                if(result.data.errcode == 0){
                                    layer.msg(result.msg,{icon: 1});
                                    layer.closeAll('page');
                                    return done(result.data.data.src,{
                                        name: file_name,//裁剪后文件名
                                        src: result.data.data.src,//目标文件url
                                        size: file_size,//大小
                                        type: file_type,//文件类型
                                    });
                                }else if(result.data.errcode != 0){
                                    layer.alert(result.msg,{icon: 2});
                                }

                            }
                        });
                    });
                    //监听旋转
                }else if(event === 'rotate'){
                    var option = $(this).attr('data-option');
                    image.cropper('rotate', option);
                    //重设图片
                }else if(event === 'reset'){
                    image.cropper('reset');
                }
                //文件选择
                file.change(function () {
                    var r= new FileReader();
                    var f=this.files[0];
                    r.readAsDataURL(f);
                    r.onload=function (e) {
                        image.cropper('destroy').attr('src', this.result).cropper(options);
                    };
                });
            });
        }

    };
    exports('croppers', obj);
});