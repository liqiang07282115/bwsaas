/**
 * date:2019/08/16
 * author:Mr.Chung
 * description:此处放layui自定义扩展
 * version:2.0.4
 */
window.rootPath = (function (src) {
    src = document.scripts[document.scripts.length - 1].src;
    return src.substring(0, src.lastIndexOf("/") + 1);
})();
layui.config({
    base: "/layui/js/lay-module/",
    version: true
}).extend({
    miniAdmin: "layuimini/miniAdmin", // layuimini后台扩展
    miniMenu: "layuimini/miniMenu", // layuimini菜单扩展
    miniTab: "layuimini/miniTab", // layuimini tab扩展
    miniTheme: "layuimini/miniTheme", // layuimini 主题扩展
    miniTongji: "layuimini/miniTongji", // layuimini 统计扩展
    step: 'step-lay/step', // 分步表单扩展
    treetable: 'treetable-lay/treetable', //table树形扩展
    tableSelect: 'tableSelect/tableSelect', // table选择扩展
    iconPickerFa: 'iconPicker/iconPickerFa', // fa图标选择扩展
    echarts: 'echarts/echarts', // echarts图表扩展
    echartsTheme: 'echarts/echartsTheme', // echarts图表主题扩展
    wangEditor: 'wangEditor/wangEditor', // wangEditor富文本扩展
    layarea: 'layarea/layarea', //  省市县区三级联动下拉选择器
    bwajax:'bwutil/bwajax',// 封装全局配置axios
    manage: 'api/manage',//请求封装例子
    bwutil:'bwutil/bwutil',// 封装layer方法
    cropper:'cropper/cropper',// 图片裁剪
    croppers:'cropper/croppers',// 图片裁剪上传
    xmSelect:'xmSelect/xm-select',// 下拉多选，单选多功能组件
    tag: 'lih-tag/modules/tag',//标签组件
    eleTree: 'eleTree/eleTree',//扩展树组件，可与xmSelect组件结合
    regionCheckBox: 'region-check-box/regionCheckBox',//城市多选组件
    vue: 'vue-2.6.10/vue.min',
    bwimage: 'bwutil/bwimage',
}).use(['jquery','layer', 'bwutil', 'bwajax'], function () {
    window.$ = layui.jquery;
    window.layer = layui.layer;
    window.bwutil = layui.bwutil;
    window.bwajax = layui.bwajax.instance();
    });

