<?php

return [
    // 加密key
    'local_disk_url' => 'public/uploads',
    //加密方式
    'alg' => 'HS256',
    //签发者 可选
    'iss' => 'www.buwangyun.com',
    //接收该JWT的一方，可选
    'aud' => 'buwang',
    //access_token有效时间
    'accessTokenExp' => 3600 * 24 * 7,
    //refresh_token有效时间
    'refreshTokenExp' => 3600 * 24 * 7,
    //token验证类型
    'tokenType' => 'bearer',
    //CacheService缓存设定时间 单位 s
    'cache_time' => 3600*24*7,
];
