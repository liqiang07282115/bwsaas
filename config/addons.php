<?php

return [
  'autoload' => true,
  'hooks' => [
    'alismsSendHook' => [
      0 => 'alisms',
    ],
  ],
  'route' => [
      0 =>[
          'domain'=>'www.buwangyun.com',
          'rule'=>[
              '/'=>'website/home.index/index',
              'index'=>'website/home.index/index',
              'addons/website/home.Index/index'=>'website/home.index/index',
              'addons/website/home.index/saas'=>'website/home.index/saas',
              'addons/website/home.index/mall'=>'website/home.index/mall',
              'addons/website/home.index/o2osolution'=>'website/home.index/o2osolution',
              'addons/website/home.index/onlineMall'=>'website/home.index/onlineMall',
              'addons/website/home.index/newsList'=>'website/home.index/newsList',
              'addons/website/home.index/purchase'=>'website/home.index/purchase',
              'addons/website/home.index/apply'=>'website/home.index/apply',
              'addons/website/home.index/search'=>'website/home.index/search',
              'addons/website/home.Plugin/index'=>'website/home.Plugin/index',
              'addons/website/home.Plugin/pluginList'=>'website/home.Plugin/pluginList',
              'addons/website/home.Plugin/detail'=>'website/home.Plugin/detail',
              'addons/website/home.Plugin/buy'=>'website/home.Plugin/buy',
              'addons/website/home.Index/login'=>'website/home.Index/login',
              'addons/website/home.Index/my'=>'website/home.Index/my',
              'addons/website/home.Index/userinfo'=>'website/home.Index/userinfo',
              'addons/website/home.Index/editInfo'=>'website/home.Index/editInfo',
              'addons/website/home.Index/order'=>'website/home.Index/order',
              'addons/website/home.Index/authorization'=>'website/home.Index/authorization',
              'addons/website/home.Index/purchased'=>'website/home.Index/purchased',
              'addons/website/home.Index/editpwd'=>'website/home.Index/editpwd',
              'addons/website/home.index/article'=>'website/home.index/article',
              'article'=>'website/home.index/article',
              'addons/website/api.ProductAuthorize/search'=>'website/api.ProductAuthorize/search',
              'addons/website/home.Plugin/cataList'=>'website/home.Plugin/cataList',
              'addons/website/home.Index/charge'=>'website/home.Index/charge',
          ]
      ],
  ],
  'service' => [

  ],
];