<?php
//框架应用配置
return [
    // 云接口域名
    'domain' => 'https://mall.buwangkeji.com',
    // 应用更新下载默认路由
//    'download' => '',
    // 获取最新应用信息路由
    'info' => '/addons/cloud_service/MiniappCloud/appVersionInfo',
];
