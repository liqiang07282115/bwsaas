<?php
declare (strict_types = 1);

namespace buwang\command;

use buwang\util\console\CliEcho;
use buwang\util\crud\BuildCrud;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use think\Exception;
use buwang\service\MiniappService;
use buwang\service\PluginService;

class App extends Command
{
    protected function configure()
    {
        // 指令配置
        $this->setName('build')
            ->addOption('app', 'a', Option::VALUE_REQUIRED, '应用目录标识', null)
            ->addOption('plugin', 'p', Option::VALUE_REQUIRED, '插件目录标识', null)
            ->setDescription('应用或插件安装文件生成指令');
    }

    protected function execute(Input $input, Output $output)
    {
        $dir = $input->getOption('app');
        $addons = $input->getOption('plugin');
        if(!$dir&&!$addons){
            CliEcho::error('缺少生成安装文件的[ 应用目录标识参数（ 请使用命令：-a [目录标识] 或 --app [目录标识]）] 或 [ 插件标识参数（ 请使用命令：-p [目录标识] 或 --plugin [目录标识]）]');
            return false;
        }
        if($dir){
            //判断是否存在该目录
            $group_php_path = base_path($dir);
            if (!file_exists($group_php_path)){
                CliEcho::error($group_php_path.'应用目录不存在');
                return false;
            }
            $array = MiniappService::exportPath($dir);
            $output->info("生成应用打包文件：");
            $output->info(">>>>>>>>>>>>>>>");
            foreach ($array as $key => $val) {
                $output->info($val);
            }
            $output->info(">>>>>>>>>>>>>>>");
            $output->info("确定生成上方所有文件? 如果以上文件存在会直接覆盖。 请输入 'y' 按回车键继续操作: ");
            $line = fgets(defined('STDIN') ? STDIN : fopen('php://stdin', 'r'));
            if (trim($line) != 'y') {
                CliEcho::error('取消生成应用安装文件');
                return false;
            }
            try{
                MiniappService::exportPackage($dir);
            }catch (\Exception $s){
                CliEcho::error($s->getMessage().",file:".$s->getFile().',line:'.$s->getLine());
                return false;
            }
            CliEcho::success("{$dir}应用安装文件生成完毕！");
        }
        if($addons){
            $group_php_path = root_path() . "addons/" .$addons;
            if (!file_exists($group_php_path)){
                CliEcho::error($group_php_path.'插件目录不存在');
                return false;
            }
            $array = PluginService::exportPath($addons);
            $output->info("生成插件安装文件：");
            $output->info(">>>>>>>>>>>>>>>");
            foreach ($array as $key => $val) {
                $output->info($val);
            }
            $output->info(">>>>>>>>>>>>>>>");
            $output->info("确定生成上方所有文件? 如果以上文件存在会直接覆盖。 请输入 'y' 按回车键继续操作: ");
            $line = fgets(defined('STDIN') ? STDIN : fopen('php://stdin', 'r'));
            if (trim($line) != 'y') {
                CliEcho::error('取消生成插件安装文件');
                return false;
            }
            try{
                PluginService::exportPackage($addons);
            }catch (\Exception $s){
                CliEcho::error($s->getMessage().",file:".$s->getFile().',line:'.$s->getLine());
                return false;
            }
            CliEcho::success("{$addons}插件安装文件生成完毕！");
        }



    }
}
