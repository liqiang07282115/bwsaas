<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace buwang\subscribe;

use app\common\model\MemberMiniapp;

/**
 * 用户事件
 * Class ProductSubscribe
 * @package buwang\subscribes
 */
class UserSubscribe
{
    public function handle()
    {
    }

    /**
     * 用户注册成功后
     * @param array $event
     */
    public function onUserRegisterSuccess($event)
    {
        list($user, $type) = $event;//注册用户信息，注册类型
        //得到租户应用信息
        $MemberMiniapp = MemberMiniapp::find($user['member_miniapp_id']);
        if ($MemberMiniapp) {
            //得到应用信息
            if ($MemberMiniapp->miniapp) {
                //查询调用应用事件
                set_miniapp_database_prefix($MemberMiniapp->miniapp->dir);//跨应用调用model前调用此方法设置应用表前缀
                \buwang\service\MiniappService::handle($MemberMiniapp->miniapp->dir, $event, 'userRegisterSuccess');
            }
        }

    }

    /**
     * 用户更新基础信息
     * @param array $event
     */
    public function onUserUpdateSuccess($event)
    {
        list($user, $type) = $event;//注册用户信息，注册类型
        //得到租户应用信息
        $MemberMiniapp = MemberMiniapp::find($user['member_miniapp_id']);
        if ($MemberMiniapp) {
            //得到应用信息
            if ($MemberMiniapp->miniapp) {
                //        var_dump(\buwang\service\MiniappService::getAppConfig('bwmall'));die;
                //查询调用应用事件
                set_miniapp_database_prefix($MemberMiniapp->miniapp->dir);//跨应用调用model前调用此方法设置应用表前缀
                \buwang\service\MiniappService::handle($MemberMiniapp->miniapp->dir, $event, 'userUpdateSuccess');
            }
        }
    }

    /**
     * 用户登录成功
     * @param array $event
     */
    public function onUserLoginSuccess($event)
    {
        list($user, $scopes) = $event;//注册用户信息，注册类型
        //得到租户应用信息
        $MemberMiniapp = MemberMiniapp::find($user['member_miniapp_id']);
        if ($MemberMiniapp) {
            //得到应用信息
            if ($MemberMiniapp->miniapp) {
                //查询调用应用事件
                set_miniapp_database_prefix($MemberMiniapp->miniapp->dir);//跨应用调用model前调用此方法设置应用表前缀
                \buwang\service\MiniappService::handle($MemberMiniapp->miniapp->dir, $event, 'userLoginSuccess');
            }
        }
    }

    /**
     * 用户提现成功
     * @param array $event
     */
    public function onUserExtractSuccess($event)
    {
        list($user, $extract) = $event;//注册用户信息，注册类型
        //得到租户应用信息
        $MemberMiniapp = MemberMiniapp::find($user['member_miniapp_id']);
        if ($MemberMiniapp) {
            //得到应用信息
            if ($MemberMiniapp->miniapp) {
                //查询调用应用事件
                set_miniapp_database_prefix($MemberMiniapp->miniapp->dir);//跨应用调用model前调用此方法设置应用表前缀
                \buwang\service\MiniappService::handle($MemberMiniapp->miniapp->dir, $event, 'userExtractSuccess');
            }
        }
    }

    /**
     * 用户提现失败
     * @param array $event
     */
    public function onUserExtractFail($event)
    {
        list($user, $extract, $error_msg) = $event;//注册用户信息，注册类型
        //得到租户应用信息
        $MemberMiniapp = MemberMiniapp::find($user['member_miniapp_id']);
        if ($MemberMiniapp) {
            //得到应用信息
            if ($MemberMiniapp->miniapp) {
                //查询调用应用事件
                set_miniapp_database_prefix($MemberMiniapp->miniapp->dir);//跨应用调用model前调用此方法设置应用表前缀
                \buwang\service\MiniappService::handle($MemberMiniapp->miniapp->dir, $event, 'userExtractFail');
            }
        }
    }


    /**
     * 用户充值成功
     * @param array $event
     */
    public function onUserRechargeSuccess($event)
    {
        list($user, $recharge) = $event;//注册用户信息，注册类型
        //得到租户应用信息
        $MemberMiniapp = MemberMiniapp::find($user['member_miniapp_id']);
        if ($MemberMiniapp) {
            //得到应用信息
            if ($MemberMiniapp->miniapp) {
                //查询调用应用事件
                set_miniapp_database_prefix($MemberMiniapp->miniapp->dir);//跨应用调用model前调用此方法设置应用表前缀
                \buwang\service\MiniappService::handle($MemberMiniapp->miniapp->dir, $event, 'userRechargeSuccess');
            }
        }
    }

    /**
     * 用户充值失败
     * @param array $event
     */
    public function onUserRechargeFail($event)
    {
        list($user, $recharge, $error_msg) = $event;//注册用户信息，注册类型
        //得到租户应用信息
        $MemberMiniapp = MemberMiniapp::find($user['member_miniapp_id']);
        if ($MemberMiniapp) {
            //得到应用信息
            if ($MemberMiniapp->miniapp) {
                //查询调用应用事件
                set_miniapp_database_prefix($MemberMiniapp->miniapp->dir);//跨应用调用model前调用此方法设置应用表前缀
                \buwang\service\MiniappService::handle($MemberMiniapp->miniapp->dir, $event, 'userRechargeFail');
            }
        }
    }
}