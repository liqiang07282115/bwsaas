<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace buwang\subscribe;

use app\manage\model\AuthNode;
use app\common\model\SysLog;
use buwang\jobs\SysLogAutoCencel;
use buwang\base\Queue;

/**
 * 微信平台事件
 * Class ProductSubscribe
 * @package buwang\subscribes
 */
class WechatSubscribe
{
    public function handle()
    {
    }



    /**
     * 小程序认证回调事件
     * @param array $event
     */
    public function onMiniappAuthNotify($event)
    {
        list($message) = $event; //app对象
        \buwang\service\MiniappService::handle(CALL_ALL, $event, 'miniappAuthNotify');
    }
}