<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace buwang\subscribe;

use app\manage\model\AuthNode;
use app\common\model\SysLog;
use buwang\jobs\SysLogAutoCancel;
use buwang\base\Queue;

/**
 * 系统事件
 * Class ProductSubscribe
 * @package buwang\subscribes
 */
class SystemSubscribe
{
    public function handle()
    {
    }

    /**
     * 请求到达事件
     * @param array $event
     */
    public function onRequestStart($event)
    {
        list($request, $scopes, $node_name, $user, $code, $err_msg, $err_code, $force) = $event;
        //如果是租户或者总管理员则记录下请求信息
        $data = [];
        $is_add = false;
        if ($scopes == 'member') {
            $is_add = true;
            //记录用户信息
            if ($user) {
                $data['member_id'] = $user['id'];
                $data['name'] = $user['nickname'];
                $data['username'] = $user['username'];
            }
        }
        if ($scopes == 'admin') {
            $is_add = true;
            //记录用户信息
            if ($user) {
                $data['admin_id'] = $user['id'];
                $data['name'] = $user['nickname'];
                $data['username'] = $user['username'];
            }
        }
        $param = $request->param();
        //不记录分页的请求
        if ($is_add && !isset($param['page'])) {
            $ip = get_real_ip();
            $url = $request->url();//路由url
            $method = AuthNode::where('menu_path', $url)->value('title');
            $data['param'] = json_encode($param);
            //如果是登录则记录下地址
            if ($node_name == '/manage/admin.Index/login' || $node_name == '/manage/member.Index/login') {
                $city = SysLog::getCityByIp($ip);
                if ($city) $data['city_name'] = $city['country'] . '  ' . $city['regionName'] . '  ' . $city['city'] . '  ' . $city['zip'];
                //TODO 2021/7/5 jyk增加判断：如果是框架登录则不记录参数
                $data['param'] = '';
            }
            if (!$method) $method = '';
//            if($force===true){
//                if($err_code)$data['err_msg'] = $err_msg;
//                $data['code'] = $code;
//                $data['err_code'] = $err_code;
//            }
            $data['method'] = $method;
            $data['menu_path'] = $url;
            $data['path'] = $node_name;
            $data['ip'] = $ip;
            $data['scopes'] = $scopes;
            $data['add_time'] = time();
            $sysLog = SysLog::create($data);
            //得到执行清除时间
            $sys_log_time = bw_config('sys_log_time', 604800);
            if ($sys_log_time <= 0) return true;
            //放入清除队列
//            Queue::instance()->job(SysLogAutoCancel::class)->secs($sys_log_time)->data($sysLog)->push();
        }
    }

    /**
     * 响应结束事件
     * @param array $event
     */
    public function onResponseEnd($event)
    {
        list($response) = $event;
    }

    /**
     * 应用安装成功事件
     * @param array $event
     */
    public function onMiniappInstall($event)
    {
        list($miniapp) = $event;
        //跨应用调用model前调用此方法设置应用表前缀
        set_miniapp_database_prefix($miniapp->dir);
        //检测安装配置
//        \buwang\service\MiniappService::installConfigAndGroupData($miniapp->dir); //对当前平台下每个租户都添加 当前应用 对应的 应用配置 和 应用组合数据
        \buwang\service\MiniappService::installConfigAndGroupDataPlus($miniapp->dir); //（表数据分离）对当前平台下每个租户都添加 当前应用 对应的 应用配置 和 应用组合数据
        \buwang\service\MiniappService::handle($miniapp->dir, $event, 'miniappInstallSuccess'); //调用应用配置的钩子函数（如果有）
    }

    /**
     * 购买应用成功事件
     * @param array $event
     */
    public function onMiniappBuySuccess($event)
    {
        list($member_id, $miniapp, $module) = $event;
        //跨应用调用model前调用此方法设置应用表前缀
        set_miniapp_database_prefix($miniapp->dir);
        if ($module['type'] == 1) {
            //购买基础功能时租户配置和组合数据初始化
            \buwang\service\MiniappService::initMemberConfigAndData($member_id, $miniapp->dir);
        }
        \buwang\service\MiniappService::handle($miniapp->dir, $event, 'miniappBuySuccess'); //调用应用配置的钩子函数（如果有）
    }


    /**应用版本更新事件
     * @param $event
     */
    public function onMiniappUpdate($event)
    {
        list($dir, $remote_version_info) = $event;
        //跨应用调用model前调用此方法设置应用表前缀
        set_miniapp_database_prefix($dir);
        \buwang\service\MiniappService::handle($dir, $event, 'miniappUpdateSuccess'); //调用应用更新的钩子函数（如果有）
    }
}