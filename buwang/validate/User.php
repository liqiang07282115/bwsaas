<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace buwang\validate;

use think\Validate;

/**
 * 用户验证
 */
class User extends Validate
{

    protected $rule = [
        'token' => 'require|max: 25|token',
        'safe_password' => 'require|length:6',
        'password_confirm' => 'require|confirm:safe_password',
        'mobile' => 'require|mobile',
        'code' => 'require|number|min:4',
        'captcha' => 'require|number|captcha|length:4',
        'username' => 'require|mobile',
        'password' => 'require|min: 6',
        'password_two' => 'require|confirm:password',
    ];

    protected $message = [
        'token' => '不合法的数据来源',
        'safe_password.require' => '密码必须输入',
        'safe_password.length' => '密码只能输入6位数字',
        'password_confirm' => '密码输入不一致',
        'mobile' => '手机号错误',
        'code' => '验证码错误',
        'username' => '用户名错误',
        'captcha' => '验证码错误',
        'password.require' => '密码必须输入',
        'password.length' => '密码密码最低6位',
        'password_two' => '密码输入不一致',
    ];

    protected $scene = [
        'editPassword' => ['id', 'safe_password', 'password_confirm'], //管理修改登录密码
        'safePassword' => ['safe_password'],
        'setSafePassword' => ['safe_password', 'password_confirm', 'code'],
        'setPassword' => ['password', 'password_two', 'code'],
        'getMobile' => ['mobile'],
        'bindMobile' => ['mobile', 'code'],
        'userLogin' => ['mobile', 'password'],
        'userLoginPc' => ['mobile', 'password', 'captcha'],
        'userReg' => ['mobile', 'password', 'code'],
        'userRegPc' => ['mobile', 'password', 'code']
    ];
}