<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace buwang\base;

use buwang\traits\ErrorTrait;
use think\Model;
use buwang\traits\TransTrait;
use buwang\traits\ModelTrait;
use think\model\concern\SoftDelete;

class BaseModel extends Model
{
    //use SoftDelete; jyk 2020 4 20 加入该块后model查询方法报错，暂时屏蔽
    use TransTrait;
    use ErrorTrait;

    //use ModelTrait;
    protected $pk = 'id';
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';
    protected $deleteTime = 'delete_time';
    protected $defaultSoftDelete = 0;
    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = true;

    
}