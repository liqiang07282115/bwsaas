<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace buwang\base;

use think\Addons;
use think\facade\Config;

abstract class BaseAddons extends Addons
{
    /**
     * 检查基础配置信息是否完整.
     *
     * @return bool
     */
    final public function checkInfo()
    {
        $info = $this->getInfo();
        //ADDON_INFO_TYPE 插件配置文件info.ini必须项
        foreach (ADDON_INFO_TYPE as $value) {
            if (!array_key_exists($value, $info)) {
                return false;
            }
        }

        return true;
    }

    /**
     * 修改基础信息
     * @param string $name
     * @param array $value
     * @return array
     */
    final public function setInfo($name = '', $value = [])
    {
        if (empty($name)) {
            $name = $this->name;
        }
        $info = $this->getInfo();
        $info = array_merge($info, $value);
        Config::set($info, $this->addon_info);

        return $info;
    }

    /**
     * 修改配置
     * @param string $name
     * @param array $value
     * @return array
     */
    final public function setConfig($name = '', $value = [])
    {
        if (empty($name)) {
            $name = $this->name;
        }
        $config = $this->getConfig();
        $config = array_merge($config, $value);
        Config::set($config, $this->addon_config);

        return $config;
    }
}