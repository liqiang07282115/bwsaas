<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace buwang\traits;

/**
 * 同用户新增请求限制
 * @author: JiaoYuKun
 * @day: 2019/12/9
 */
trait RequestTrait
{
    public static $countdown = 0; //剩余提交时间

    /** 根据时间间隔限制新增数据数量
     * @param int $interval 插入数据时间间隔
     * @param array $where 查询条件
     * @param null $time_field 新增时间字段
     * @return bool
     * @throws \Exception
     */
    public function existInterval($interval = 180, $where = [], $time_field = null)
    {
        self::$countdown = $interval;
        //获取当前新增时间戳字段名
        if ($time_field) {
            $addTimeField = $time_field;
        } else {
            //如果存在模型时间戳字段则使用
            if (property_exists($this, 'createTime')) {
                if ($interval) {
                    $addTimeField = $this->createTime;
                }
            } else {
                throw new \Exception("未设置新增字段");//主动抛异常
            }

        }
        //得到该数据集最后的添加时间
        $addtime_start = self::where($where)->max($addTimeField);
        //通过间隔时间计算得到时间区间
        $addtime_end = $addtime_start + $interval;
        $time = time();
        //判断当前时间是否属于该时间区间集
        //属于则说明在间隔时间内，返回false，不允许继续
        if ($addtime_start < $time && $time < $addtime_end) {
            //得到剩余倒计时秒数
            self::$countdown = $addtime_end - $time;
            return false;
        } else {
            self::$countdown = 0;
            //不属于则说明在间隔时间外，返回true，允许继续
            return true;
        }

    }


}