<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace buwang\util;

use think\facade\Request;
use think\Response;
use think\exception\HttpResponseException;

class apiReturn
{
    private $code = 200;

    public function code(int $code): self
    {
        $this->code = $code;
        return $this;
    }

    /**
     * 操作成功返回的数据
     * @param string $msg 提示信息
     * @param mixed $data 要返回的数据
     * @param int $code 错误码，默认为1
     * @param string $type 输出类型
     * @param array $header 发送的 Header 信息
     */
    public function success($msg = 'ok', $data = [], $errcode = 0, $type = 'json', array $header = []): Respone
    {
        if (is_array($msg)) {
            $data = $msg;
            $msg = 'ok';
        }
        $datas = compact('errcode', 'data');

        $this->result($msg, $datas, $this->code, $type, $header);
    }

    /**
     * 操作失败返回的数据
     * @param string $msg 提示信息
     * @param mixed $data 要返回的数据
     * @param int $code 错误码，默认为0
     * @param string $type 输出类型
     * @param array $header 发送的 Header 信息
     */
    public function error($msg = 'error', $data = [], $errcode = 400000, $type = 'json', array $header = []): Respone
    {
        if (is_array($msg)) {
            $data = $msg;
            $msg = 'ok';
        }
        $datas = compact('errcode', 'data');
        $this->result($msg, $datas, $this->code, $type, $header);
    }

    /**
     * 返回封装后的 API 数据到客户端
     * @access protected
     * @param mixed $msg 提示信息
     * @param mixed $data 要返回的数据
     * @param int $errcode
     * @param string $type 输出类型，支持json/xml/jsonp
     * @param array $header 发送的 Header 信息
     * @return Respone
     */
    public function result($msg, $data = [], $errcode = 0, $type = 'json', array $header = []): Respone
    {

        $result = [
            'code' => $this->code == 200 ? $this->code : $errcode,
            'msg' => $msg,
            'time' => Request::instance()->server('REQUEST_TIME'),
            'data' => $data,
        ];

        // 如果未设置类型则自动判断
        $type = $type ? $type : ($this->request->param(config('var_jsonp_handler')) ? 'jsonp' : $this->responseType);

        if (isset($header['statuscode'])) {
            $code = $header['statuscode'];
            unset($header['statuscode']);
        } else {
            //未设置状态码,根据code值判断$code = $code >= 1000 || $code < 200 ? 200 : $code;
            $code = $errcode >= 1000 || $this->code < 200 ? 200 : $this->code;
        }
        $response = Response::create($result, $type, $code)->header($header);
        throw new HttpResponseException($response);
    }
}