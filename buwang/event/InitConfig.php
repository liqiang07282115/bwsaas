<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-10-24 11:55:00
// +----------------------------------------------------------------------

namespace buwang\event;

use think\facade\Config;
use think\facade\Env;

/**
 * 初始化配置信息
 *
 */
class InitConfig
{
    public function handle()
    {
        // 初始化常量
        $this->initConst();
        //初始化配置信息
        $this->initConfig();
    }

    /**
     * 初始化常量
     */
    private function initConst()
    {
        //定义租户应用支持端的类型,后续扩展字节跳动小程序，抖音小程序等
        defined('BW_CLIENT_TYPE') or define('BW_CLIENT_TYPE', ['mini_program', 'app', 'h5', 'official' ,'pc', 'tt']);
        //定义bwsaas框架支持端的类型
        defined('BW_SCOPES') or define('BW_SCOPES', array_merge(BW_CLIENT_TYPE,['member' ,'admin']));
        //加载版本信息
        defined('BW_VERSION') or define('BW_VERSION', Config::get('bwsaas.version'));                        //版本号如1.2.0
        defined('BW_VERSION_NO') or define('BW_VERSION_NO', Config::get('bwsaas.version_no'));                        //版本号编码
        defined('BW_VERSION_NAME') or define('BW_VERSION_NAME', Config::get('bwsaas.name'));                     //版本名称

        //加载基础化配置信息
        define('__ROOT__', str_replace(['/index.php', '/install.php'], '', request()->root(true)));
        define('__PUBLIC__', __ROOT__ . '/public');
        define('__UPLOAD__', 'upload');

        //简化路径分割符
        !defined('DS') && define('DS', DIRECTORY_SEPARATOR);
        // 插件目录
        define('ADDON_PATH', app()->getRootPath() . 'addons' . DS);
        //插件配置info.ini必须项
        define('ADDON_INFO_TYPE', ['name', 'title', 'description', 'type', 'author', 'version', 'status']);
        //模板目录 public/templates/
        define('TEMPLATE_PATH', app()->getRootPath() . 'public' . DS . 'templates' . DS);
        //页面不跳转参数
        define('NOT_JUMP', md5(uniqid(microtime(true), true)));
        //伪静态模式是否开启
        define('REWRITE_MODULE', true);

        // public目录绝对路径
        define('PUBLIC_PATH', dirname(dirname(dirname(__FILE__))) . '/public/');
        // 项目绝对路径
        define('ROOT_PATH', dirname(dirname(dirname(__FILE__))));

        //兼容模式访问
        if (!REWRITE_MODULE) {
            define('ROOT_URL', request()->root(true) . '/?s=');
        } else {
            define('ROOT_URL', request()->root(true));
        }
        //系统事件调用标识
        define('CALL_ALL', md5(uniqid(microtime(true), true)));
    }

    /**
     * 初始化配置信息
     */
    private function initConfig()
    {
        $view_array = [
            // 模板引擎类型使用Think
            'type'          => 'Think',
            // 默认模板渲染规则 1 解析为小写+下划线 2 全部转换小写 3 保持操作方法
            'auto_rule'     => 1,
            // 模板目录名
            'view_dir_name' => 'view',
            // 模板后缀
            'view_suffix'   => 'html',
            // 模板文件名分隔符
            'view_depr'     => DIRECTORY_SEPARATOR,
            // 模板引擎普通标签开始标记
            'tpl_begin'     => '{',
            // 模板引擎普通标签结束标记
            'tpl_end'       => '}',
            // 标签库标签开始标记
            'taglib_begin'  => '{',
            // 标签库标签结束标记
            'taglib_end'    => '}',
            // 预先加载的标签库
            'taglib_pre_load'     =>    '',
            // 模板渲染缓存
            'display_cache' => false,
            //是否开启模板编译缓存,设为false则每次都会重新编译
            'tpl_cache'     => false, //部署模式后改为true
            // 字符替换
            'tpl_replace_string' => [
                '__STATIC__' => Env::get('buwang.static_path', '/static'),
                '__MODULE__'     => '/layui',
            ]
        ];

        Config::set($view_array, 'view');
    }


}
