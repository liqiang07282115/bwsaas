<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace buwang\service;

/**
 * 系统参数管理服务
 * Class SystemService
 * @package think\admin\service
 */
class SystemService
{

    /**
     * 配置数据缓存
     * @var array
     */
    protected $data = [];


    /**
     * 生成最短URL地址
     * @param string $url 路由地址
     * @param array $vars 变量
     * @param boolean|string $suffix 后缀
     * @param boolean|string $domain 域名
     * @return string
     */
    public function sysuri($url = '', array $vars = [], $suffix = true, $domain = false)
    {
        $d1 = app()->config->get('app.default_app');
        $d2 = app()->config->get('route.default_controller');
        $d3 = app()->config->get('route.default_action');
        $location = app()->route->buildUrl($url, $vars)->suffix($suffix)->domain($domain)->build();
        return preg_replace(["|^/{$d1}/{$d2}/{$d3}(\.html)?$|i", "|/{$d2}/{$d3}(\.html)?$|i", "|/{$d3}(\.html)?$|i", '|/\.html$|'], ['$1', '$1', '$1', ''], $location);
    }

}