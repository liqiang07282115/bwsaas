<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace buwang\service\cloud;

/**
 * https://market.aliyun.com/products/56928004/cmapi027240.html?spm=5176.730005.productlist.d_cmapi027240.3e353524nleIiY&innerSource=search_%E7%9F%AD%E4%BF%A1#sku=yuncode2124000000
 * Class Sms
 * @package buwang\service\cloud
 */
class Sms extends Base
{
    protected $base_uri = 'http://feginesms.market.alicloudapi.com';

    public function run(array $param)
    {
        //请求参数
        $uri = "/codeNotice?param={$param['code']}&phone={$param['mobile']}&sign=1&skin=1";

        //Guzzle Http遇到非200返回码会抛出异常,需要进行捕获
        try {
            $res = json_decode($this->client->request($this->method, $uri, $this->options)->getBody()->getContents(), true);
            if ($res['Code'] != 'OK') return self::setError($res['Message']);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $err = json_decode($e->getResponse()->getBody()->getContents(), true);

            if (isset($err['Code']) && isset($err['Message'])) return self::setError($err['Message']);
            else return self::setError('短信服务异常,请检查云市场配置');
        }

        return true;
    }
}