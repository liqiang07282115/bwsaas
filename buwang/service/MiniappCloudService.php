<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace buwang\service;

use app\common\model\member\Wallet;
use app\common\model\MemberMiniapp;
use app\common\model\MemberMiniappOrder;
use app\common\model\Miniapp;
use app\common\model\MiniappModule;
use app\manage\model\AuthGroup;
use app\manage\model\AuthGroupAccess;
use app\manage\model\AuthGroupNode;
use app\manage\model\AuthNode;
use buwang\util\File;
use buwang\util\Sql;
use think\Exception;
use think\facade\Db;
use app\manage\model\Member;
use app\manage\model\Config;
use app\manage\model\ConfigTab;
use app\manage\model\ConfigGroup;
use app\manage\model\ConfigGroupData;
use buwang\traits\ErrorTrait;
/**
 * 应用云端服务类
 * Class PluginService
 * @package buwang\service
 */
class MiniappCloudService
{
    use ErrorTrait;



    /**获取应用最新更新信息
     * @param $dir 应用标识
     */
    public function getNewUnpdate($dir){
        $info = "1.2.0 最新社区版";//版本描述
        $version = '1.2.0';//最新版本号
        $update_time = 1618219740;//更新时间戳
        $update_time_date = date("Y-m-d H:i:s ",$update_time);//更新时间
        $have_app_file = true;//是否有文件
        $download_url = "https://mall.buwangkeji.com/cloud_depository/cescioql/scdwdcxa.zip";//云路径
        //模拟数据
        //新增列表
        $add = [
            ['msg'=>'新增菜单规则自定义URL功能'],
            ['msg'=>'新增菜单规则弹窗&Ajax&外部链接功能'],
            ['msg'=>'新增清除浏览器缓存(JS、CSS、图片等资源)'],
        ];
        //优化列表
        $optimize = [
            ['msg'=>'优化支持PHP7.4+版本'],
            ['msg'=>'优化过期Token删除'],
            ['msg'=>'优化权限规则菜单展示'],
        ];
        //修复列表
        $repair = [
            ['msg'=>'修复分片上传兼容性BUG'],
            ['msg'=>'修复API文档模板参数格式校验'],
            ['msg'=>'修复插件配置规则图片和文件类型时不生效'],
            ['msg'=>'修复Date::unixtime方法计算周偏移时的错误'],
        ];
        $return_data =  compact('info','version','add','optimize','repair','update_time','have_app_file','download_url','update_time_date');
        return $return_data;
    }
}