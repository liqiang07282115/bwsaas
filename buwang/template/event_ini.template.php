<?php
/**
 * 填写需要调用的事件函数
 * 当前可用事件 userRegisterSuccess:用户注册事件,userLoginSuccess 用户登录事件,miniappInstallSuccess:应用安装事件,miniappBuySuccess:购买应用成功事件,
 * 方法传参 array $event
 * 格式：['class'=>'调用的方法所在的类(类构造方法不能带参),没有传null','method'=>'方法名','scope'=>'方法类型 （字符串：static:类方法 object 对象方法 common公共方法）']
 */
return [
//    'userRegisterSuccess'=>[   //注册用户事件 注册用户时调用的应用指定方法，比如需要添加用户附表
//        'class'=> app\demo\Demo::class,
//        'method'=>'createMallUser',
//        'scope'=>'static',//static:类方法 object 对象方法 common公共方法
//    ],
//    'userUpdateSuccess'=>[   //用户基本数据更新事件 用户登录取微信信息更新时调用的应用指定方法，比如需要更新用户上级
//        'class'=> app\demo\Demo::class,
//        'method'=>'createMallUser',
//        'scope'=>'static',//static:类方法 object 对象方法 common公共方法
//    ],
//    'miniappBuySuccess'=>[   //应用购买事件
//        'class'=> app\demo\Demo::class,
//        'method'=>'buySuccess',
//        'scope'=>'static',//static:类方法 object 对象方法 common公共方法
//    ],
//    'userExtractSuccess'=>[   //用户提现成功
//        'class'=> app\demo\Demo::class,
//        'method'=>'extractSuccess',
//        'scope'=>'object',//static:类方法 object 对象方法 common公共方法
//    ],
//    'userExtractFail'=>[   //用户提现失败
//        'class'=> app\demo\Demo::class,
//        'method'=>'extractFail',
//        'scope'=>'object',//static:类方法 object 对象方法 common公共方法
//    ],
//    'userRechargeSuccess'=>[   //用户充值成功
//        'class'=> app\demo\Demo::class,
//        'method'=>'rechargeSuccess',
//        'scope'=>'object',//static:类方法 object 对象方法 common公共方法
//    ],
//    'userRechargeFail'=>[   //用户充值失败
//        'class'=> app\demo\Demo::class,
//        'method'=>'rechargeFail',
//        'scope'=>'object',//static:类方法 object 对象方法 common公共方法
//    ],
];