{extend name="../../../view/public/base" /}

{block name="css"}
<style>
    body {
        background-color: #F2F2F2;
    }
</style>
{/block}

{block name="body"}
<div style="padding: 20px; background-color: #F2F2F2;">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">
            <div class="layui-card" style="padding: 10px">
                <form class="layui-form" lay-filter="addForm" autocomplete="off">
<?php echo $edit_html_html?>
                    <div class="layui-form-item">
                        <div class="layui-input-block">
                            <button class="layui-btn layui-btn-normal" lay-submit lay-filter="submitBtn">立即提交</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

{/block}

{block name="js"}
<script>
    var layer, form, bwajax, upload, $, laydate;

    layui.use(['layer', 'form', 'bwajax', 'upload', 'laydate'], function () {
        layer = layui.layer;
        form = layui.form;
        bwajax = layui.bwajax.instance();
        upload = layui.upload;
        $ = layui.jquery;
        laydate = layui.laydate;

<?php echo $edit_html_js?>

        //监听提交按钮
        form.on('submit(submitBtn)', function (obj) {
            data = obj.field;

<?php echo $edit_html_submit_js?>

            //ajax调用后台接口
            bwajax.post("{:Url('edit')}?id={$row.id}", data)
                .then(function (response) {
                    if (response.data.data.errcode === 0) {
                        layer.msg('提交成功', {icon: 1});
                        submitSuccess();
                    } else {
                        layer.msg(response.data.msg, {icon: 2});
                    }
                })

            return false;
        })
    });
</script>
{/block}